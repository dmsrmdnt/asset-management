USE [master]
GO
/****** Object:  Database [db.asset.management]    Script Date: 02/12/2013 16:46:04 ******/
CREATE DATABASE [db.asset.management] ON  PRIMARY 
( NAME = N'db.asset.management', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\db.asset.management.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'db.asset.management_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\db.asset.management_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [db.asset.management] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db.asset.management].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [db.asset.management] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [db.asset.management] SET ANSI_NULLS OFF
GO
ALTER DATABASE [db.asset.management] SET ANSI_PADDING OFF
GO
ALTER DATABASE [db.asset.management] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [db.asset.management] SET ARITHABORT OFF
GO
ALTER DATABASE [db.asset.management] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [db.asset.management] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [db.asset.management] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [db.asset.management] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [db.asset.management] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [db.asset.management] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [db.asset.management] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [db.asset.management] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [db.asset.management] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [db.asset.management] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [db.asset.management] SET  DISABLE_BROKER
GO
ALTER DATABASE [db.asset.management] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [db.asset.management] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [db.asset.management] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [db.asset.management] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [db.asset.management] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [db.asset.management] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [db.asset.management] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [db.asset.management] SET  READ_WRITE
GO
ALTER DATABASE [db.asset.management] SET RECOVERY SIMPLE
GO
ALTER DATABASE [db.asset.management] SET  MULTI_USER
GO
ALTER DATABASE [db.asset.management] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [db.asset.management] SET DB_CHAINING OFF
GO
USE [db.asset.management]
GO
/****** Object:  Table [dbo].[t_mutation]    Script Date: 02/12/2013 16:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_mutation](
	[id_employee1] [int] NULL,
	[id_employee2] [int] NULL,
	[id_asset] [int] NULL,
	[date_req] [date] NULL,
	[date_app] [date] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_login]    Script Date: 02/12/2013 16:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_login](
	[username] [nvarchar](50) NOT NULL,
	[password] [nvarchar](max) NULL,
	[id_employee] [int] NULL,
	[auth] [nchar](10) NULL,
 CONSTRAINT [PK_t_login] PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_condition]    Script Date: 02/12/2013 16:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_condition](
	[id_employee] [int] NULL,
	[id_asset] [int] NULL,
	[id_status] [int] NULL,
	[note] [ntext] NULL,
	[date] [date] NULL,
	[acc] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[t_asset]    Script Date: 02/12/2013 16:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_asset](
	[id_employee] [int] NOT NULL,
	[id_asset] [int] NOT NULL,
 CONSTRAINT [PK_t_asset] PRIMARY KEY CLUSTERED 
(
	[id_employee] ASC,
	[id_asset] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_status]    Script Date: 02/12/2013 16:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_status](
	[id_status] [int] IDENTITY(1,1) NOT NULL,
	[status] [nvarchar](50) NULL,
 CONSTRAINT [PK_m_status] PRIMARY KEY CLUSTERED 
(
	[id_status] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_location]    Script Date: 02/12/2013 16:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_location](
	[id_location] [int] IDENTITY(1,1) NOT NULL,
	[location] [nvarchar](50) NULL,
 CONSTRAINT [PK_m_location] PRIMARY KEY CLUSTERED 
(
	[id_location] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_employee]    Script Date: 02/12/2013 16:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_employee](
	[id_employee] [int] IDENTITY(1,1) NOT NULL,
	[id_dept] [int] NULL,
	[nama] [nvarchar](50) NULL,
	[nik] [nvarchar](50) NULL,
	[password] [nvarchar](max) NULL,
	[id_auth] [int] NULL,
 CONSTRAINT [PK_m_employee] PRIMARY KEY CLUSTERED 
(
	[id_employee] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_dept]    Script Date: 02/12/2013 16:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_dept](
	[id_dept] [int] IDENTITY(1,1) NOT NULL,
	[dept] [nvarchar](50) NULL,
 CONSTRAINT [PK_m_dept] PRIMARY KEY CLUSTERED 
(
	[id_dept] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_category]    Script Date: 02/12/2013 16:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_category](
	[id_category] [int] IDENTITY(1,1) NOT NULL,
	[category] [nvarchar](50) NULL,
 CONSTRAINT [PK_m_category] PRIMARY KEY CLUSTERED 
(
	[id_category] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_brand]    Script Date: 02/12/2013 16:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_brand](
	[id_brand] [int] IDENTITY(1,1) NOT NULL,
	[brand] [nvarchar](50) NULL,
 CONSTRAINT [PK_m_brand] PRIMARY KEY CLUSTERED 
(
	[id_brand] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_auth]    Script Date: 02/12/2013 16:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_auth](
	[id_auth] [int] IDENTITY(1,1) NOT NULL,
	[auth] [nvarchar](50) NULL,
 CONSTRAINT [PK_m_auth] PRIMARY KEY CLUSTERED 
(
	[id_auth] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[m_asset]    Script Date: 02/12/2013 16:46:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[m_asset](
	[id_asset] [int] IDENTITY(1,1) NOT NULL,
	[id_category] [int] NULL,
	[id_brand] [int] NULL,
	[model] [nvarchar](50) NULL,
	[serial] [nvarchar](50) NULL,
	[pur_date] [date] NULL,
	[description] [ntext] NULL,
	[price] [int] NULL,
	[id_location] [int] NULL,
	[id_status] [int] NULL,
 CONSTRAINT [PK_m_asset] PRIMARY KEY CLUSTERED 
(
	[id_asset] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
