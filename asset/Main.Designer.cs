﻿namespace asset
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.listOfAssetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.requestAssetStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.operationStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mutationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.breakLoseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.userManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assetManagementToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kelolaSoftwareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.kelolaLisensiSoftwareToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.klaimKehilanganKerusakanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.updateDepartemenToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.daftarPermintaanAsetBaruToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uploadDataMasterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backupDatabaseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.summaryReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanMutasiKerusakanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.laporanAsetKeseluruhanToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cetakBuktiKepemilikanAsetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sejarahAsetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.summaryAssetPerKategoriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.summaryUserAktifToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.daftarMutasiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.profilStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nMutation = new System.Windows.Forms.NotifyIcon(this.components);
            this.nClaim = new System.Windows.Forms.NotifyIcon(this.components);
            this.nSoftware = new System.Windows.Forms.NotifyIcon(this.components);
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.searchAsset = new System.Windows.Forms.ToolStripButton();
            this.restoreDatabaseServerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.listOfAssetToolStripMenuItem,
            this.requestAssetStripMenuItem,
            this.operationStripMenuItem1,
            this.administrationToolStripMenuItem,
            this.reportToolStripMenuItem,
            this.profilStripMenuItem,
            this.logoutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // listOfAssetToolStripMenuItem
            // 
            this.listOfAssetToolStripMenuItem.Name = "listOfAssetToolStripMenuItem";
            this.listOfAssetToolStripMenuItem.Size = new System.Drawing.Size(77, 20);
            this.listOfAssetToolStripMenuItem.Text = "Daftar Aset";
            this.listOfAssetToolStripMenuItem.Click += new System.EventHandler(this.listOfAssetToolStripMenuItem_Click);
            // 
            // requestAssetStripMenuItem
            // 
            this.requestAssetStripMenuItem.Name = "requestAssetStripMenuItem";
            this.requestAssetStripMenuItem.Size = new System.Drawing.Size(141, 20);
            this.requestAssetStripMenuItem.Text = "Permohonan Aset Baru";
            this.requestAssetStripMenuItem.Click += new System.EventHandler(this.requestAssetStripMenuItem_Click);
            // 
            // operationStripMenuItem1
            // 
            this.operationStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mutationToolStripMenuItem,
            this.breakLoseToolStripMenuItem});
            this.operationStripMenuItem1.Name = "operationStripMenuItem1";
            this.operationStripMenuItem1.Size = new System.Drawing.Size(114, 20);
            this.operationStripMenuItem1.Text = "Mutasi/Kerusakan";
            // 
            // mutationToolStripMenuItem
            // 
            this.mutationToolStripMenuItem.Name = "mutationToolStripMenuItem";
            this.mutationToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.mutationToolStripMenuItem.Text = "Mutasi";
            this.mutationToolStripMenuItem.Click += new System.EventHandler(this.mutationToolStripMenuItem_Click);
            // 
            // breakLoseToolStripMenuItem
            // 
            this.breakLoseToolStripMenuItem.Name = "breakLoseToolStripMenuItem";
            this.breakLoseToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.breakLoseToolStripMenuItem.Text = "Kerusakan/Kehilangan";
            this.breakLoseToolStripMenuItem.Click += new System.EventHandler(this.breakLoseToolStripMenuItem_Click);
            // 
            // administrationToolStripMenuItem
            // 
            this.administrationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.userManagementToolStripMenuItem,
            this.assetManagementToolStripMenuItem,
            this.kelolaSoftwareToolStripMenuItem,
            this.kelolaLisensiSoftwareToolStripMenuItem,
            this.klaimKehilanganKerusakanToolStripMenuItem,
            this.updateDepartemenToolStripMenuItem,
            this.daftarPermintaanAsetBaruToolStripMenuItem,
            this.uploadDataMasterToolStripMenuItem,
            this.backupDatabaseToolStripMenuItem,
            this.restoreDatabaseServerToolStripMenuItem});
            this.administrationToolStripMenuItem.Name = "administrationToolStripMenuItem";
            this.administrationToolStripMenuItem.Size = new System.Drawing.Size(85, 20);
            this.administrationToolStripMenuItem.Text = "Administrasi";
            // 
            // userManagementToolStripMenuItem
            // 
            this.userManagementToolStripMenuItem.Name = "userManagementToolStripMenuItem";
            this.userManagementToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.userManagementToolStripMenuItem.Text = "Kelola Pengguna";
            this.userManagementToolStripMenuItem.Click += new System.EventHandler(this.userManagementToolStripMenuItem_Click);
            // 
            // assetManagementToolStripMenuItem
            // 
            this.assetManagementToolStripMenuItem.Name = "assetManagementToolStripMenuItem";
            this.assetManagementToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.assetManagementToolStripMenuItem.Text = "Kelola Aset";
            this.assetManagementToolStripMenuItem.Click += new System.EventHandler(this.assetManagementToolStripMenuItem_Click);
            // 
            // kelolaSoftwareToolStripMenuItem
            // 
            this.kelolaSoftwareToolStripMenuItem.Name = "kelolaSoftwareToolStripMenuItem";
            this.kelolaSoftwareToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.kelolaSoftwareToolStripMenuItem.Text = "Kelola Software";
            this.kelolaSoftwareToolStripMenuItem.Click += new System.EventHandler(this.kelolaSoftwareToolStripMenuItem_Click);
            // 
            // kelolaLisensiSoftwareToolStripMenuItem
            // 
            this.kelolaLisensiSoftwareToolStripMenuItem.Name = "kelolaLisensiSoftwareToolStripMenuItem";
            this.kelolaLisensiSoftwareToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.kelolaLisensiSoftwareToolStripMenuItem.Text = "Kelola Lisensi Software";
            this.kelolaLisensiSoftwareToolStripMenuItem.Click += new System.EventHandler(this.kelolaLisensiSoftwareToolStripMenuItem_Click);
            // 
            // klaimKehilanganKerusakanToolStripMenuItem
            // 
            this.klaimKehilanganKerusakanToolStripMenuItem.Name = "klaimKehilanganKerusakanToolStripMenuItem";
            this.klaimKehilanganKerusakanToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.klaimKehilanganKerusakanToolStripMenuItem.Text = "Laporan Kehilangan/Kerusakan";
            this.klaimKehilanganKerusakanToolStripMenuItem.Click += new System.EventHandler(this.klaimKehilanganKerusakanToolStripMenuItem_Click);
            // 
            // updateDepartemenToolStripMenuItem
            // 
            this.updateDepartemenToolStripMenuItem.Name = "updateDepartemenToolStripMenuItem";
            this.updateDepartemenToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.updateDepartemenToolStripMenuItem.Text = "Update Departemen";
            this.updateDepartemenToolStripMenuItem.Click += new System.EventHandler(this.updateDepartemenToolStripMenuItem_Click);
            // 
            // daftarPermintaanAsetBaruToolStripMenuItem
            // 
            this.daftarPermintaanAsetBaruToolStripMenuItem.Name = "daftarPermintaanAsetBaruToolStripMenuItem";
            this.daftarPermintaanAsetBaruToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.daftarPermintaanAsetBaruToolStripMenuItem.Text = "Daftar Permintaan Aset Baru";
            this.daftarPermintaanAsetBaruToolStripMenuItem.Click += new System.EventHandler(this.daftarPermintaanAsetBaruToolStripMenuItem_Click);
            // 
            // uploadDataMasterToolStripMenuItem
            // 
            this.uploadDataMasterToolStripMenuItem.Name = "uploadDataMasterToolStripMenuItem";
            this.uploadDataMasterToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.uploadDataMasterToolStripMenuItem.Text = "Upload Data Master";
            this.uploadDataMasterToolStripMenuItem.Click += new System.EventHandler(this.uploadDataMasterToolStripMenuItem_Click);
            // 
            // backupDatabaseToolStripMenuItem
            // 
            this.backupDatabaseToolStripMenuItem.Name = "backupDatabaseToolStripMenuItem";
            this.backupDatabaseToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.backupDatabaseToolStripMenuItem.Text = "Backup Database(Server)";
            this.backupDatabaseToolStripMenuItem.Click += new System.EventHandler(this.backupDatabaseToolStripMenuItem_Click);
            // 
            // reportToolStripMenuItem
            // 
            this.reportToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.summaryReportToolStripMenuItem,
            this.laporanMutasiKerusakanToolStripMenuItem,
            this.laporanAsetKeseluruhanToolStripMenuItem,
            this.cetakBuktiKepemilikanAsetToolStripMenuItem,
            this.sejarahAsetToolStripMenuItem,
            this.summaryAssetPerKategoriToolStripMenuItem,
            this.summaryUserAktifToolStripMenuItem,
            this.daftarMutasiToolStripMenuItem,
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
            this.reportToolStripMenuItem.Name = "reportToolStripMenuItem";
            this.reportToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.reportToolStripMenuItem.Text = "Report";
            // 
            // summaryReportToolStripMenuItem
            // 
            this.summaryReportToolStripMenuItem.Name = "summaryReportToolStripMenuItem";
            this.summaryReportToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.summaryReportToolStripMenuItem.Text = "Laporan Tahun Pembelian Aset";
            this.summaryReportToolStripMenuItem.Click += new System.EventHandler(this.summaryReportToolStripMenuItem_Click);
            // 
            // laporanMutasiKerusakanToolStripMenuItem
            // 
            this.laporanMutasiKerusakanToolStripMenuItem.Name = "laporanMutasiKerusakanToolStripMenuItem";
            this.laporanMutasiKerusakanToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.laporanMutasiKerusakanToolStripMenuItem.Text = "Laporan Mutasi/Kerusakan";
            this.laporanMutasiKerusakanToolStripMenuItem.Click += new System.EventHandler(this.laporanMutasiKerusakanToolStripMenuItem_Click);
            // 
            // laporanAsetKeseluruhanToolStripMenuItem
            // 
            this.laporanAsetKeseluruhanToolStripMenuItem.Name = "laporanAsetKeseluruhanToolStripMenuItem";
            this.laporanAsetKeseluruhanToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.laporanAsetKeseluruhanToolStripMenuItem.Text = "Laporan Aset Keseluruhan";
            this.laporanAsetKeseluruhanToolStripMenuItem.Click += new System.EventHandler(this.laporanAsetKeseluruhanToolStripMenuItem_Click);
            // 
            // cetakBuktiKepemilikanAsetToolStripMenuItem
            // 
            this.cetakBuktiKepemilikanAsetToolStripMenuItem.Name = "cetakBuktiKepemilikanAsetToolStripMenuItem";
            this.cetakBuktiKepemilikanAsetToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.cetakBuktiKepemilikanAsetToolStripMenuItem.Text = "Cetak Bukti Kepemilikan Aset";
            this.cetakBuktiKepemilikanAsetToolStripMenuItem.Click += new System.EventHandler(this.cetakBuktiKepemilikanAsetToolStripMenuItem_Click);
            // 
            // sejarahAsetToolStripMenuItem
            // 
            this.sejarahAsetToolStripMenuItem.Name = "sejarahAsetToolStripMenuItem";
            this.sejarahAsetToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.sejarahAsetToolStripMenuItem.Text = "Sejarah Aset";
            this.sejarahAsetToolStripMenuItem.Click += new System.EventHandler(this.sejarahAsetToolStripMenuItem_Click);
            // 
            // summaryAssetPerKategoriToolStripMenuItem
            // 
            this.summaryAssetPerKategoriToolStripMenuItem.Name = "summaryAssetPerKategoriToolStripMenuItem";
            this.summaryAssetPerKategoriToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.summaryAssetPerKategoriToolStripMenuItem.Text = "Summary Asset Per Kategori";
            this.summaryAssetPerKategoriToolStripMenuItem.Click += new System.EventHandler(this.summaryAssetPerKategoriToolStripMenuItem_Click);
            // 
            // summaryUserAktifToolStripMenuItem
            // 
            this.summaryUserAktifToolStripMenuItem.Name = "summaryUserAktifToolStripMenuItem";
            this.summaryUserAktifToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.summaryUserAktifToolStripMenuItem.Text = "Summary User Aktif";
            this.summaryUserAktifToolStripMenuItem.Click += new System.EventHandler(this.summaryUserAktifToolStripMenuItem_Click);
            // 
            // daftarMutasiToolStripMenuItem
            // 
            this.daftarMutasiToolStripMenuItem.Name = "daftarMutasiToolStripMenuItem";
            this.daftarMutasiToolStripMenuItem.Size = new System.Drawing.Size(239, 22);
            this.daftarMutasiToolStripMenuItem.Text = "Daftar Mutasi";
            this.daftarMutasiToolStripMenuItem.Click += new System.EventHandler(this.daftarMutasiToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(239, 22);
            this.toolStripMenuItem1.Text = "Daftar Kerusakan";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(239, 22);
            this.toolStripMenuItem2.Text = "Daftar Kehilangan";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // profilStripMenuItem
            // 
            this.profilStripMenuItem.Name = "profilStripMenuItem";
            this.profilStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.profilStripMenuItem.Text = "Profil";
            this.profilStripMenuItem.Click += new System.EventHandler(this.profilStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem
            // 
            this.logoutToolStripMenuItem.Name = "logoutToolStripMenuItem";
            this.logoutToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.logoutToolStripMenuItem.Text = "Logout";
            this.logoutToolStripMenuItem.Click += new System.EventHandler(this.logoutToolStripMenuItem_Click);
            // 
            // nMutation
            // 
            this.nMutation.Icon = ((System.Drawing.Icon)(resources.GetObject("nMutation.Icon")));
            this.nMutation.Text = "Notifikasi Mutasi";
            this.nMutation.Visible = true;
            this.nMutation.BalloonTipClicked += new System.EventHandler(this.nMutation_BalloonTipClicked);
            this.nMutation.Click += new System.EventHandler(this.nMutation_Click);
            // 
            // nClaim
            // 
            this.nClaim.Icon = ((System.Drawing.Icon)(resources.GetObject("nClaim.Icon")));
            this.nClaim.Text = "Notifikasi Klaim";
            this.nClaim.Visible = true;
            this.nClaim.BalloonTipClicked += new System.EventHandler(this.nClaim_BalloonTipClicked);
            this.nClaim.Click += new System.EventHandler(this.nClaim_Click);
            // 
            // nSoftware
            // 
            this.nSoftware.Icon = ((System.Drawing.Icon)(resources.GetObject("nSoftware.Icon")));
            this.nSoftware.Text = "nSoftware";
            this.nSoftware.Visible = true;
            this.nSoftware.BalloonTipClicked += new System.EventHandler(this.nSoftware_BalloonTipClicked);
            this.nSoftware.Click += new System.EventHandler(this.nSoftware_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.toolStripSeparator1,
            this.searchAsset});
            this.toolStrip1.Location = new System.Drawing.Point(0, 537);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(784, 25);
            this.toolStrip1.TabIndex = 3;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(154, 22);
            this.toolStripLabel1.Text = "Asset Management GIS V1.0";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // searchAsset
            // 
            this.searchAsset.Image = global::asset.Properties.Resources.search;
            this.searchAsset.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.searchAsset.Name = "searchAsset";
            this.searchAsset.Size = new System.Drawing.Size(132, 22);
            this.searchAsset.Text = "Cari Informasi Asset";
            this.searchAsset.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.searchAsset.Click += new System.EventHandler(this.searchAsset_Click);
            // 
            // restoreDatabaseServerToolStripMenuItem
            // 
            this.restoreDatabaseServerToolStripMenuItem.Name = "restoreDatabaseServerToolStripMenuItem";
            this.restoreDatabaseServerToolStripMenuItem.Size = new System.Drawing.Size(238, 22);
            this.restoreDatabaseServerToolStripMenuItem.Text = "Restore Database(Server)";
            this.restoreDatabaseServerToolStripMenuItem.Click += new System.EventHandler(this.restoreDatabaseServerToolStripMenuItem_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::asset.Properties.Resources.asset;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.Text = "Asset Management Information System";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Main_FormClosed);
            this.Load += new System.EventHandler(this.Main_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Main_Paint);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem listOfAssetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem administrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem userManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem assetManagementToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem operationStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mutationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem breakLoseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem klaimKehilanganKerusakanToolStripMenuItem;
        private System.Windows.Forms.NotifyIcon nMutation;
        private System.Windows.Forms.NotifyIcon nClaim;
        private System.Windows.Forms.ToolStripMenuItem summaryReportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanMutasiKerusakanToolStripMenuItem;
        private System.Windows.Forms.NotifyIcon nSoftware;
        private System.Windows.Forms.ToolStripMenuItem profilStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem updateDepartemenToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cetakBuktiKepemilikanAsetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem laporanAsetKeseluruhanToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sejarahAsetToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton searchAsset;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripMenuItem summaryAssetPerKategoriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem summaryUserAktifToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem daftarMutasiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kelolaSoftwareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem kelolaLisensiSoftwareToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem requestAssetStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem daftarPermintaanAsetBaruToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uploadDataMasterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backupDatabaseToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem restoreDatabaseServerToolStripMenuItem;
    }
}