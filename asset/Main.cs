﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Threading;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class Main : Form
    {
        private String id;
        private Boolean logout;
        lib lib = new lib();
        Thread t;
        public Main(String id_employee)
        {
            InitializeComponent();
            foreach (Control control in this.Controls)
            {
                MdiClient client = control as MdiClient;
                if (!(client == null))
                {
                    client.BackColor = Color.FromArgb(37, 66, 82);
                    break;
                }
            }
            this.id = id_employee;
            t = new Thread(new ThreadStart(notifyIcon));

        }
        public String getID
        {
            get
            {
                return id;
            }
        }
        
        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                if (this.logout != true)
                {
                    t.Abort();
                    nClaim.Dispose();
                    nSoftware.Dispose();
                    nMutation.Dispose();
                    Application.Exit();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void userManagementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    asset.UserManagement fUser = asset.UserManagement.GetChildInstance();
                    fUser.id = this.id;
                    fUser.MdiParent = this;
                    fUser.Show();
                    fUser.BringToFront();
                    fUser.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void assetManagementToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    AssetManagement fAsset = AssetManagement.GetChildInstance(this.id);
                    fAsset.MdiParent = this;
                    fAsset.Show();
                    fAsset.BringToFront();
                    fAsset.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult dr = MessageBox.Show("Apakah anda yakin untuk keluar aplikasi ? ",
                "Konfirmasi Keluar Aplikasi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    login l = new login();
                    l.Show();
                    t.Abort();
                    nClaim.Dispose();
                    nSoftware.Dispose();
                    nMutation.Dispose();
                    this.logout = true;
                    
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void listOfAssetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                AssetList fList = AssetList.GetChildInstance(id);
                fList.MdiParent = this;
                fList.Show();
                fList.BringToFront();
                fList.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void mutationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                MutationsManagement m = MutationsManagement.GetChildInstance(id);
                m.MdiParent = this;
                m.Show();
                m.BringToFront();
                m.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void breakLoseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                BreakManagement b = BreakManagement.GetChildInstance(id);
                b.MdiParent = this;
                b.Show();
                b.BringToFront();
                b.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void klaimKehilanganKerusakanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    ClaimManagement k = ClaimManagement.GetChildInstance(id);
                    k.MdiParent = this;
                    k.Show();
                    k.BringToFront();
                    k.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Main_Load(object sender, EventArgs e)
        {
            try
            {
                t.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void notifyIcon() {
            try
            {
                Boolean Auth = cekAuthority(this.id);
                while (true)
                {
                    if (Auth == true)
                    {
                        showC();
                        showS();
                    }
                    else {
                        nClaim.Visible = false;
                        nSoftware.Visible = false;
                    }
                    showM();
                    Thread.Sleep(120000);
                }
            }
            catch (Exception )
            {
                //MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showM() 
        {
            try
            {
                int countMReq = getCountMReq(this.id);
                if (countMReq != 0)
                {
                    nMutation.Visible = true;
                    notifyMutation(countMReq);
                }
                else
                {
                    nMutation.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showC()
        {
            try
            {
                int countCReq = getCountCReq(this.id);
                if (countCReq != 0)
                {
                    nClaim.Visible = true;
                    notifyClaim(countCReq);
                }
                else
                {
                    nClaim.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showS()
        {
            try
            {
                int countCSoft = getCountCSoft();
                if (countCSoft != 0)
                {
                    nSoftware.Visible = true;
                    notifySoftware(countCSoft);
                }
                else
                {
                    nSoftware.Visible = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public int getCountCSoft()
        {
            String date = DateTime.Now.ToString();
            int count = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select count(*) from t_license_software a where (select DATEDIFF(DAY,'" + date + "',a.exp_date))<30 ", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    count = Convert.ToInt32(read[0]);
                }
                conn.Close();
            }catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
                return count;
        }
        public int getCountMReq(String id_employee) 
        {
            int count = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select count(*) from t_mutation where id_employee2='" + id_employee + "' and stat='1'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    count = Convert.ToInt32(read[0]);
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return count;
        }
        public int getCountCReq(String id_employee)
        {
            int count = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select count(*) from t_lose where acc='2'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    count += Convert.ToInt32(read[0]);
                }
                read.Close();
                myCommand = new SqlCommand("select count(*) from t_condition where acc='2'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    count += Convert.ToInt32(read[0]);
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return count;
        }
        public void notifySoftware(int count)
        {
            try
            {
                nSoftware.ShowBalloonTip(1000, "Update Lisensi Software", "Ada " + count + " lisensi software yang akan habis dalam jangka waktu 30 hari", ToolTipIcon.Info);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void notifyMutation(int count) 
        {
            try
            {
                nMutation.ShowBalloonTip(1000, "Permintaan Mutasi Aset", "Anda memperoleh " + count + " permintaan mutasi sekarang", ToolTipIcon.Info);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void notifyClaim(int count)
        {
            try
            {
                nClaim.ShowBalloonTip(1000, "Permintaan Klaim Kehilangan/Kerusakan", "Anda memperoleh " + count + " permintaan klaim kehilangan/kerusakan sekarang", ToolTipIcon.Info);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        public Boolean cekAuthority(String id_employee) 
        {
            Boolean auth = false;
            try
            {
                int cek = 0;
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select id_auth from m_employee where id_employee='" + id_employee + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    cek = Convert.ToInt32(read["id_auth"].ToString());
                }
                if (cek == 1 || cek == 0)
                {
                    auth = true;
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return auth;
        }

        private void nMutation_BalloonTipClicked(object sender, EventArgs e)
        {
            try
            {
                MutationsManagement m = MutationsManagement.GetChildInstance(id);
                m.MdiParent = this;
                m.Show();
                m.BringToFront();
                m.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void nMutation_Click(object sender, EventArgs e)
        {
            try
            {
                MutationsManagement m = MutationsManagement.GetChildInstance(id);
                m.MdiParent = this;
                m.Show();
                m.BringToFront();
                m.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void nSoftware_Click(object sender, EventArgs e)
        {
            try
            {
                Administrasi.SoftwareLicenseManagement k = Administrasi.SoftwareLicenseManagement.GetChildInstance();
                k.id = this.id;
                k.MdiParent = this;
                k.Show();
                k.BringToFront();
                k.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void nClaim_BalloonTipClicked(object sender, EventArgs e)
        {
            try
            {
                ClaimManagement k = ClaimManagement.GetChildInstance(id);
                k.MdiParent = this;
                k.Show();
                k.BringToFront();
                k.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void nSoftware_BalloonTipClicked(object sender, EventArgs e)
        {
            try
            {
                Administrasi.SoftwareLicenseManagement k = Administrasi.SoftwareLicenseManagement.GetChildInstance();
                k.id = this.id;
                k.MdiParent = this;
                k.Show();
                k.BringToFront();
                k.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void nClaim_Click(object sender, EventArgs e)
        {
            try
            {
                ClaimManagement k = ClaimManagement.GetChildInstance(id);
                k.MdiParent = this;
                k.Show();
                k.BringToFront();
                k.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void summaryReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    PDateReport k = PDateReport.GetChildInstance();
                    k.MdiParent = this;
                    k.Show();
                    k.BringToFront();
                    k.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void laporanMutasiKerusakanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    MutationReport k = MutationReport.GetChildInstance(this.id);
                    k.MdiParent = this;
                    k.Show();
                    k.BringToFront();
                    k.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void updateLisensiSoftwareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    LicenseManagement k = LicenseManagement.GetChildInstance(this.id);
                    k.MdiParent = this;
                    k.Show();
                    k.BringToFront();
                    k.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void profilStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                ProfilManagement k = ProfilManagement.GetChildInstance(this.id);
                k.MdiParent = this;
                k.Show();
                k.BringToFront();
                k.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void updateDepartemenToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    DepartmentManagement k = DepartmentManagement.GetChildInstance(this.id);
                    k.MdiParent = this;
                    k.Show();
                    k.BringToFront();
                    k.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cetakBuktiKepemilikanAsetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    PrintReport k = PrintReport.GetChildInstance(this.id);
                    k.MdiParent = this;
                    k.Show();
                    k.BringToFront();
                    k.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }            
        }

        private void Main_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                LinearGradientBrush br = new LinearGradientBrush(this.ClientRectangle, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, this.ClientRectangle);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void laporanAsetKeseluruhanToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    AssetReport k = AssetReport.GetChildInstance(this.id);
                    k.MdiParent = this;
                    k.Show();
                    k.BringToFront();
                    k.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void mutasiAsetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    MutationManagementAll m = MutationManagementAll.GetChildInstance(id);
                    m.MdiParent = this;
                    m.Show();
                    m.BringToFront();
                    m.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void sejarahAsetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    HistoryAsset m = HistoryAsset.GetChildInstance(id);
                    m.MdiParent = this;
                    m.Show();
                    m.BringToFront();
                    m.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void searchAsset_Click(object sender, EventArgs e)
        {
            try
            {
                Administrasi.SearchAsset k = Administrasi.SearchAsset.GetChildInstance();
                k.MdiParent = this;
                k.Show();
                k.BringToFront();
                k.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void summaryUserAktifToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    Report.ActiveUser m = Report.ActiveUser.GetChildInstance();
                    m.MdiParent = this;
                    m.Show();
                    m.BringToFront();
                    m.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void daftarMutasiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    Report.MutationHistory m = Report.MutationHistory.GetChildInstance();
                    m.MdiParent = this;
                    m.Show();
                    m.BringToFront();
                    m.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void summaryAssetPerKategoriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    Report.SummaryCategory m = Report.SummaryCategory.GetChildInstance();
                    m.MdiParent = this;
                    m.Show();
                    m.BringToFront();
                    m.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void kelolaSoftwareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    Administrasi.SoftwareManagement m = Administrasi.SoftwareManagement.GetChildInstance();
                    m.id = this.id;
                    m.MdiParent = this;
                    m.Show();
                    m.BringToFront();
                    m.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void kelolaLisensiSoftwareToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    Administrasi.SoftwareLicenseManagement m = Administrasi.SoftwareLicenseManagement.GetChildInstance();
                    m.id = this.id;
                    m.MdiParent = this;
                    m.Show();
                    m.BringToFront();
                    m.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    Report.BreakHistory m = Report.BreakHistory.GetChildInstance();
                    m.MdiParent = this;
                    m.Show();
                    m.BringToFront();
                    m.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    Report.LostHistory m = Report.LostHistory.GetChildInstance();
                    m.MdiParent = this;
                    m.Show();
                    m.BringToFront();
                    m.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void requestAssetStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                User.requestAsset k = User.requestAsset.GetChildInstance(this.id);
                k.MdiParent = this;
                k.Show();
                k.BringToFront();
                k.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void daftarPermintaanAsetBaruToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    Administrasi.RequestManagement k = Administrasi.RequestManagement.GetChildInstance(this.id);
                    k.MdiParent = this;
                    k.Show();
                    k.BringToFront();
                    k.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void uploadDataMasterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    Administrasi.UploadMaster k = Administrasi.UploadMaster.GetChildInstance();
                    k.MdiParent = this;
                    k.Show();
                    k.BringToFront();
                    k.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void backupDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    Administrasi.BackupDatabase k = Administrasi.BackupDatabase.GetChildInstance();
                    k.MdiParent = this;
                    k.Show();
                    k.BringToFront();
                    k.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void restoreDatabaseServerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekAuth = cekAuthority(this.id);
                if (cekAuth == true)
                {
                    Administrasi.RestoreDatabase k = Administrasi.RestoreDatabase.GetChildInstance();
                    k.MdiParent = this;
                    k.Show();
                    k.BringToFront();
                    k.WindowState = System.Windows.Forms.FormWindowState.Maximized;
                }
                else
                {
                    MessageBox.Show("Anda tidak mempunyai hak akses untuk menu ini ! ",
                        "Akses ditolak", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
