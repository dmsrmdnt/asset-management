﻿namespace asset
{
    partial class MutationReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bPrint = new System.Windows.Forms.Button();
            this.bExport2 = new System.Windows.Forms.Button();
            this.bExport = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dAkhir = new System.Windows.Forms.DateTimePicker();
            this.bCari = new System.Windows.Forms.Button();
            this.dAwal = new System.Windows.Forms.DateTimePicker();
            this.lAwal = new System.Windows.Forms.Label();
            this.dataGU = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGU)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(994, 81);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pencarian";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bPrint);
            this.groupBox2.Controls.Add(this.bExport2);
            this.groupBox2.Controls.Add(this.bExport);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(6, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(263, 52);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Cetak";
            // 
            // bPrint
            // 
            this.bPrint.ForeColor = System.Drawing.Color.Black;
            this.bPrint.Location = new System.Drawing.Point(178, 19);
            this.bPrint.Name = "bPrint";
            this.bPrint.Size = new System.Drawing.Size(80, 30);
            this.bPrint.TabIndex = 2;
            this.bPrint.Text = "Print";
            this.bPrint.UseVisualStyleBackColor = true;
            this.bPrint.Click += new System.EventHandler(this.bPrint_Click);
            // 
            // bExport2
            // 
            this.bExport2.ForeColor = System.Drawing.Color.Black;
            this.bExport2.Location = new System.Drawing.Point(92, 19);
            this.bExport2.Name = "bExport2";
            this.bExport2.Size = new System.Drawing.Size(80, 30);
            this.bExport2.TabIndex = 1;
            this.bExport2.Text = "PDF";
            this.bExport2.UseVisualStyleBackColor = true;
            this.bExport2.Click += new System.EventHandler(this.bExport2_Click);
            // 
            // bExport
            // 
            this.bExport.ForeColor = System.Drawing.Color.Black;
            this.bExport.Location = new System.Drawing.Point(6, 19);
            this.bExport.Name = "bExport";
            this.bExport.Size = new System.Drawing.Size(80, 30);
            this.bExport.TabIndex = 0;
            this.bExport.Text = "Excel";
            this.bExport.UseVisualStyleBackColor = true;
            this.bExport.Click += new System.EventHandler(this.bExport_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dAkhir);
            this.groupBox5.Controls.Add(this.bCari);
            this.groupBox5.Controls.Add(this.dAwal);
            this.groupBox5.Controls.Add(this.lAwal);
            this.groupBox5.ForeColor = System.Drawing.Color.White;
            this.groupBox5.Location = new System.Drawing.Point(275, 19);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(713, 52);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Filter";
            // 
            // dAkhir
            // 
            this.dAkhir.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dAkhir.Location = new System.Drawing.Point(174, 19);
            this.dAkhir.Name = "dAkhir";
            this.dAkhir.Size = new System.Drawing.Size(102, 20);
            this.dAkhir.TabIndex = 23;
            // 
            // bCari
            // 
            this.bCari.ForeColor = System.Drawing.Color.Black;
            this.bCari.Location = new System.Drawing.Point(282, 18);
            this.bCari.Name = "bCari";
            this.bCari.Size = new System.Drawing.Size(75, 23);
            this.bCari.TabIndex = 22;
            this.bCari.Text = "Cari";
            this.bCari.UseVisualStyleBackColor = true;
            this.bCari.Click += new System.EventHandler(this.bCari_Click);
            // 
            // dAwal
            // 
            this.dAwal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dAwal.Location = new System.Drawing.Point(66, 19);
            this.dAwal.Name = "dAwal";
            this.dAwal.Size = new System.Drawing.Size(102, 20);
            this.dAwal.TabIndex = 1;
            // 
            // lAwal
            // 
            this.lAwal.AutoSize = true;
            this.lAwal.Location = new System.Drawing.Point(8, 21);
            this.lAwal.Name = "lAwal";
            this.lAwal.Size = new System.Drawing.Size(49, 13);
            this.lAwal.TabIndex = 0;
            this.lAwal.Text = "Periode :";
            // 
            // dataGU
            // 
            this.dataGU.AllowUserToAddRows = false;
            this.dataGU.AllowUserToDeleteRows = false;
            this.dataGU.AllowUserToResizeColumns = false;
            this.dataGU.AllowUserToResizeRows = false;
            this.dataGU.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGU.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGU.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGU.Location = new System.Drawing.Point(12, 99);
            this.dataGU.MultiSelect = false;
            this.dataGU.Name = "dataGU";
            this.dataGU.ReadOnly = true;
            this.dataGU.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGU.Size = new System.Drawing.Size(994, 370);
            this.dataGU.TabIndex = 20;
            this.dataGU.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGU_CellClick);
            // 
            // MutationReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 482);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGU);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MutationReport";
            this.Text = "Laporan Mutasi/Kerusakan/Kehilangan";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MutationReport_FormClosed);
            this.Load += new System.EventHandler(this.MutationReport_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MutationReport_Paint);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGU)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button bCari;
        private System.Windows.Forms.DateTimePicker dAwal;
        private System.Windows.Forms.Label lAwal;
        private System.Windows.Forms.DataGridView dataGU;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button bExport;
        private System.Windows.Forms.Button bExport2;
        private System.Windows.Forms.Button bPrint;
        private System.Windows.Forms.DateTimePicker dAkhir;

    }
}