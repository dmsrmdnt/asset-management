﻿namespace asset
{
    partial class AssetReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGUser = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tSearch = new System.Windows.Forms.TextBox();
            this.bSearch = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.bExport2 = new System.Windows.Forms.Button();
            this.bExport = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGUser)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGUser
            // 
            this.dataGUser.AllowUserToAddRows = false;
            this.dataGUser.AllowUserToDeleteRows = false;
            this.dataGUser.AllowUserToResizeColumns = false;
            this.dataGUser.AllowUserToResizeRows = false;
            this.dataGUser.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGUser.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGUser.Location = new System.Drawing.Point(12, 88);
            this.dataGUser.MultiSelect = false;
            this.dataGUser.Name = "dataGUser";
            this.dataGUser.ReadOnly = true;
            this.dataGUser.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGUser.Size = new System.Drawing.Size(994, 397);
            this.dataGUser.TabIndex = 9;
            this.dataGUser.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGUser_CellContentClick);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.tSearch);
            this.groupBox3.Controls.Add(this.bSearch);
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(361, 16);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(641, 52);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Pemilik";
            // 
            // tSearch
            // 
            this.tSearch.Location = new System.Drawing.Point(6, 19);
            this.tSearch.Name = "tSearch";
            this.tSearch.Size = new System.Drawing.Size(531, 20);
            this.tSearch.TabIndex = 1;
            this.tSearch.TextChanged += new System.EventHandler(this.bSearch_Click);
            // 
            // bSearch
            // 
            this.bSearch.ForeColor = System.Drawing.Color.Black;
            this.bSearch.Location = new System.Drawing.Point(543, 17);
            this.bSearch.Name = "bSearch";
            this.bSearch.Size = new System.Drawing.Size(75, 23);
            this.bSearch.TabIndex = 0;
            this.bSearch.Text = "Cari";
            this.bSearch.UseVisualStyleBackColor = true;
            this.bSearch.Click += new System.EventHandler(this.bSearch_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.bExport2);
            this.groupBox2.Controls.Add(this.bExport);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(11, 16);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(344, 52);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Export to Excel";
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(227, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 30);
            this.button1.TabIndex = 2;
            this.button1.Text = "Software Report";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // bExport2
            // 
            this.bExport2.ForeColor = System.Drawing.Color.Black;
            this.bExport2.Location = new System.Drawing.Point(117, 19);
            this.bExport2.Name = "bExport2";
            this.bExport2.Size = new System.Drawing.Size(104, 30);
            this.bExport2.TabIndex = 1;
            this.bExport2.Text = "Detail Report";
            this.bExport2.UseVisualStyleBackColor = true;
            this.bExport2.Click += new System.EventHandler(this.bExport2_Click);
            // 
            // bExport
            // 
            this.bExport.ForeColor = System.Drawing.Color.Black;
            this.bExport.Location = new System.Drawing.Point(6, 19);
            this.bExport.Name = "bExport";
            this.bExport.Size = new System.Drawing.Size(105, 30);
            this.bExport.TabIndex = 0;
            this.bExport.Text = "All Report";
            this.bExport.UseVisualStyleBackColor = true;
            this.bExport.Click += new System.EventHandler(this.bExport_Click);
            // 
            // AssetReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 572);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.dataGUser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AssetReport";
            this.Text = "Laporan Aset";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AssetReport_FormClosed);
            this.Load += new System.EventHandler(this.AssetReport_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AssetReport_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.dataGUser)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGUser;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tSearch;
        private System.Windows.Forms.Button bSearch;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button bExport;
        private System.Windows.Forms.Button bExport2;
        private System.Windows.Forms.Button button1;
    }
}