﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using System.Diagnostics;

namespace asset.Report
{
    public partial class PreviewPrintRequest : Form
    {
        lib lib = new lib();
        public string id_request;
        public PreviewPrintRequest()
        {
            InitializeComponent();
        }

        private void PreviewPrintRequest_Load(object sender, EventArgs e)
        {
            showReport(this.id_request);
        }
        void showReport(String id_request)
        {
            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(lib.param());
                cnn.Open();
                SqlCommand myCommand = cnn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "print_request";
                myCommand.Parameters.AddWithValue("@id_request", id_request);
                SqlDataAdapter dscmd = new SqlDataAdapter();
                dscmd.SelectCommand = myCommand;
                Report.DSreportAsset ds = new Report.DSreportAsset();
                dscmd.Fill(ds, "Request");
                cnn.Close();
                Report.request_form objRpt = new Report.request_form();
                objRpt.SetDataSource(ds.Tables["Request"]);

                crystalReportViewer1.ReportSource = objRpt;
                crystalReportViewer1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
