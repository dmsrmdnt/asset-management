﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace asset.Report
{
    public partial class BreakHistory : Form
    {
        lib lib = new lib();
        public BreakHistory()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }
        private static BreakHistory form;
        public static BreakHistory GetChildInstance()
        {
            if (form == null)
                form = new BreakHistory();
            return form;
        }
        void listStatus() 
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(string));
            dt.Columns.Add("status", typeof(string));
            dt.Rows.Add(new object[] { "", "All" });
            dt.Rows.Add(new object[] { "1", "Process" });
            dt.Rows.Add(new object[] { "0", "Finish" });
            dt.Rows.Add(new object[] { "2", "Pending" });
            cStatus.ValueMember = "id";
            cStatus.DisplayMember = "status";
            cStatus.DataSource = dt;
        }
        void process() 
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "break_history";
                myCommand.Parameters.AddWithValue("@awalReq", dAwal.Text);
                myCommand.Parameters.AddWithValue("@akhirReq", dAkhir.Text);
                myCommand.Parameters.AddWithValue("@acc", cStatus.SelectedValue.ToString());
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                DataTable t = new DataTable();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(t);
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGAssetL.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGAssetL.Columns["IDEmployee"].Visible = false;
                this.dataGAssetL.Columns["IDAsset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void MutationHistory_Load(object sender, EventArgs e)
        {
            DataGridViewLinkColumn links = new DataGridViewLinkColumn();
            links.UseColumnTextForLinkValue = true;
            links.HeaderText = "Download Berita Acara";
            links.DataPropertyName = "lnkColumn";
            links.ActiveLinkColor = Color.White;
            links.LinkBehavior = LinkBehavior.SystemDefault;
            links.LinkColor = Color.Blue;
            links.Text = "Click here to download";
            links.TrackVisitedState = true;
            links.VisitedLinkColor = Color.YellowGreen;
            dataGAssetL.Columns.Add(links);

            DataGridViewLinkColumn links2 = new DataGridViewLinkColumn();
            links2.UseColumnTextForLinkValue = true;
            links2.HeaderText = "Download Quotation";
            links2.DataPropertyName = "lnkColumn";
            links2.ActiveLinkColor = Color.White;
            links2.LinkBehavior = LinkBehavior.SystemDefault;
            links2.LinkColor = Color.Blue;
            links2.Text = "Click here to download";
            links2.TrackVisitedState = true;
            links2.VisitedLinkColor = Color.YellowGreen;
            dataGAssetL.Columns.Add(links2);

            listStatus();
            process();
        }

        private void MutationHistory_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

        private void bSearch_Click(object sender, EventArgs e)
        {
            process();
        }

        private void MutationHistory_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void dAwal_ValueChanged(object sender, EventArgs e)
        {
            process();
        }

        private void dAkhir_ValueChanged(object sender, EventArgs e)
        {
            process();
        }

        private void cStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            process();
        }

        private void dataGAssetL_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGAssetL.Columns[0].Index)
                {
                    //Download
                    try
                    {
                        String id_employee = dataGAssetL[2, e.RowIndex].Value.ToString();
                        String id_asset = dataGAssetL[4, e.RowIndex].Value.ToString();
                        String date_req = dataGAssetL[13, e.RowIndex].Value.ToString();
                        FileStream FS = null;
                        byte[] dbbyte;

                        //Get selected file ID field
                        SqlConnection conn = new SqlConnection(lib.param());
                        conn.Open();
                        SqlCommand sqlcmd = new SqlCommand();
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataTable dt = new DataTable();
                        sqlcmd = new SqlCommand("select * from t_condition where id_asset='" + id_asset + "' and id_employee='" + id_employee + "' and date_req='" + Convert.ToDateTime(date_req) + "'", conn);
                        da = new SqlDataAdapter(sqlcmd);
                        dt = new DataTable();
                        da.Fill(dt);
                        conn.Close();


                        if (dt.Rows.Count > 0)
                        {
                            //Get a stored PDF bytes
                            String dname = dt.Rows[0]["dname"].ToString();
                            if (dname != "")
                            {

                                dbbyte = (byte[])dt.Rows[0]["dcontent"];
                                String[] filetype = dname.Split('.');

                                //store file Temporarily 
                                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                                string filepath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\BA\\" + dname + "." + filetype[1];

                                //Assign File path create file
                                FS = new FileStream(filepath, System.IO.FileMode.Create);

                                //Write bytes to create file
                                FS.Write(dbbyte, 0, dbbyte.Length);

                                //Close FileStream instance
                                FS.Close();

                                // Open fila after write 

                                //Create instance for process class
                                Process Proc = new Process();

                                //assign file path for process
                                Proc.StartInfo.FileName = filepath;
                                Proc.Start();
                            }
                            else
                            {
                                MessageBox.Show("Dokumen kerusakan tidak tersedia", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetL.Columns[1].Index)
                {
                    //Download
                    try
                    {
                        String id_employee = dataGAssetL[2, e.RowIndex].Value.ToString();
                        String id_asset = dataGAssetL[4, e.RowIndex].Value.ToString();
                        String date_req = dataGAssetL[13, e.RowIndex].Value.ToString();
                        FileStream FS = null;
                        byte[] dbbyte;

                        //Get selected file ID field
                        SqlConnection conn = new SqlConnection(lib.param());
                        conn.Open();
                        SqlCommand sqlcmd = new SqlCommand();
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataTable dt = new DataTable();
                        sqlcmd = new SqlCommand("select * from t_condition where id_asset='" + id_asset + "' and id_employee='" + id_employee + "' and date_req='" + Convert.ToDateTime(date_req) + "'", conn);
                        da = new SqlDataAdapter(sqlcmd);
                        dt = new DataTable();
                        da.Fill(dt);
                        conn.Close();


                        if (dt.Rows.Count > 0)
                        {
                            //Get a stored PDF bytes
                            String dname = dt.Rows[0]["qname"].ToString();
                            if (dname != "")
                            {

                                dbbyte = (byte[])dt.Rows[0]["qcontent"];
                                String[] filetype = dname.Split('.');

                                //store file Temporarily 
                                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                                string filepath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\BA\\" + dname + "." + filetype[1];

                                //Assign File path create file
                                FS = new FileStream(filepath, System.IO.FileMode.Create);

                                //Write bytes to create file
                                FS.Write(dbbyte, 0, dbbyte.Length);

                                //Close FileStream instance
                                FS.Close();

                                // Open fila after write 

                                //Create instance for process class
                                Process Proc = new Process();

                                //assign file path for process
                                Proc.StartInfo.FileName = filepath;
                                Proc.Start();
                            }
                            else
                            {
                                MessageBox.Show("Dokumen kerusakan tidak tersedia", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }     
        }
    }
}
