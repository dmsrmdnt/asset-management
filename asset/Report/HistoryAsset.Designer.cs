﻿namespace asset
{
    partial class HistoryAsset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGAsset = new System.Windows.Forms.DataGridView();
            this.Filter = new System.Windows.Forms.GroupBox();
            this.bCari = new System.Windows.Forms.Button();
            this.tSearch = new System.Windows.Forms.TextBox();
            this.cKategori = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGAsset)).BeginInit();
            this.Filter.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGAsset
            // 
            this.dataGAsset.AllowUserToAddRows = false;
            this.dataGAsset.AllowUserToDeleteRows = false;
            this.dataGAsset.AllowUserToResizeColumns = false;
            this.dataGAsset.AllowUserToResizeRows = false;
            this.dataGAsset.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGAsset.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGAsset.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGAsset.Location = new System.Drawing.Point(10, 96);
            this.dataGAsset.MultiSelect = false;
            this.dataGAsset.Name = "dataGAsset";
            this.dataGAsset.ReadOnly = true;
            this.dataGAsset.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGAsset.Size = new System.Drawing.Size(986, 440);
            this.dataGAsset.TabIndex = 7;
            this.dataGAsset.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGAsset_CellContentClick);
            // 
            // Filter
            // 
            this.Filter.BackColor = System.Drawing.Color.Transparent;
            this.Filter.Controls.Add(this.bCari);
            this.Filter.Controls.Add(this.tSearch);
            this.Filter.Controls.Add(this.cKategori);
            this.Filter.Controls.Add(this.label2);
            this.Filter.Controls.Add(this.label1);
            this.Filter.ForeColor = System.Drawing.Color.White;
            this.Filter.Location = new System.Drawing.Point(12, 12);
            this.Filter.Name = "Filter";
            this.Filter.Size = new System.Drawing.Size(984, 78);
            this.Filter.TabIndex = 8;
            this.Filter.TabStop = false;
            this.Filter.Text = "Filter";
            // 
            // bCari
            // 
            this.bCari.ForeColor = System.Drawing.Color.Black;
            this.bCari.Location = new System.Drawing.Point(576, 48);
            this.bCari.Name = "bCari";
            this.bCari.Size = new System.Drawing.Size(75, 23);
            this.bCari.TabIndex = 4;
            this.bCari.Text = "Cari";
            this.bCari.UseVisualStyleBackColor = true;
            this.bCari.Click += new System.EventHandler(this.bCari_Click);
            // 
            // tSearch
            // 
            this.tSearch.Location = new System.Drawing.Point(80, 50);
            this.tSearch.Name = "tSearch";
            this.tSearch.Size = new System.Drawing.Size(490, 20);
            this.tSearch.TabIndex = 3;
            this.tSearch.TextChanged += new System.EventHandler(this.bCari_Click);
            // 
            // cKategori
            // 
            this.cKategori.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cKategori.FormattingEnabled = true;
            this.cKategori.Location = new System.Drawing.Point(80, 22);
            this.cKategori.Name = "cKategori";
            this.cKategori.Size = new System.Drawing.Size(121, 21);
            this.cKategori.TabIndex = 2;
            this.cKategori.SelectedIndexChanged += new System.EventHandler(this.cKategori_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Pemilik";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Kategori";
            // 
            // HistoryAsset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 558);
            this.Controls.Add(this.Filter);
            this.Controls.Add(this.dataGAsset);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "HistoryAsset";
            this.Text = "Sejarah Aset";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.HistoryAsset_FormClosed);
            this.Load += new System.EventHandler(this.HistoryAsset_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.HistoryAsset_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.dataGAsset)).EndInit();
            this.Filter.ResumeLayout(false);
            this.Filter.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGAsset;
        private System.Windows.Forms.GroupBox Filter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cKategori;
        private System.Windows.Forms.TextBox tSearch;
        private System.Windows.Forms.Button bCari;
    }
}