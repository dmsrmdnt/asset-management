﻿namespace asset
{
    partial class PDateReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tSearchU = new System.Windows.Forms.TextBox();
            this.bSearchU = new System.Windows.Forms.Button();
            this.dataGU = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.gSearch = new System.Windows.Forms.GroupBox();
            this.tSearchC = new System.Windows.Forms.TextBox();
            this.bSearch = new System.Windows.Forms.Button();
            this.dataGC = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGB = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tSearchB = new System.Windows.Forms.TextBox();
            this.bSearchB = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dataGL = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tSearchL = new System.Windows.Forms.TextBox();
            this.bSearchL = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGU)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.gSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGC)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGB)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGL)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(994, 495);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.dataGU);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(986, 469);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "by User";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tSearchU);
            this.groupBox1.Controls.Add(this.bSearchU);
            this.groupBox1.Location = new System.Drawing.Point(6, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 44);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cari Nama Pengguna";
            // 
            // tSearchU
            // 
            this.tSearchU.Location = new System.Drawing.Point(6, 17);
            this.tSearchU.Name = "tSearchU";
            this.tSearchU.Size = new System.Drawing.Size(881, 20);
            this.tSearchU.TabIndex = 1;
            this.tSearchU.TextChanged += new System.EventHandler(this.bSearchU_Click);
            // 
            // bSearchU
            // 
            this.bSearchU.Location = new System.Drawing.Point(893, 15);
            this.bSearchU.Name = "bSearchU";
            this.bSearchU.Size = new System.Drawing.Size(75, 23);
            this.bSearchU.TabIndex = 2;
            this.bSearchU.Text = "Cari";
            this.bSearchU.UseVisualStyleBackColor = true;
            this.bSearchU.Click += new System.EventHandler(this.bSearchU_Click);
            // 
            // dataGU
            // 
            this.dataGU.AllowUserToAddRows = false;
            this.dataGU.AllowUserToDeleteRows = false;
            this.dataGU.AllowUserToResizeColumns = false;
            this.dataGU.AllowUserToResizeRows = false;
            this.dataGU.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGU.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGU.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGU.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGU.Location = new System.Drawing.Point(6, 62);
            this.dataGU.MultiSelect = false;
            this.dataGU.Name = "dataGU";
            this.dataGU.ReadOnly = true;
            this.dataGU.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGU.Size = new System.Drawing.Size(974, 401);
            this.dataGU.TabIndex = 18;
            this.dataGU.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGU_CellClick);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.gSearch);
            this.tabPage2.Controls.Add(this.dataGC);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(986, 469);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "by Category";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // gSearch
            // 
            this.gSearch.Controls.Add(this.tSearchC);
            this.gSearch.Controls.Add(this.bSearch);
            this.gSearch.Location = new System.Drawing.Point(6, 12);
            this.gSearch.Name = "gSearch";
            this.gSearch.Size = new System.Drawing.Size(974, 44);
            this.gSearch.TabIndex = 19;
            this.gSearch.TabStop = false;
            this.gSearch.Text = "Cari Kategori";
            // 
            // tSearchC
            // 
            this.tSearchC.Location = new System.Drawing.Point(6, 17);
            this.tSearchC.Name = "tSearchC";
            this.tSearchC.Size = new System.Drawing.Size(881, 20);
            this.tSearchC.TabIndex = 1;
            this.tSearchC.TextChanged += new System.EventHandler(this.bSearch_Click);
            // 
            // bSearch
            // 
            this.bSearch.Location = new System.Drawing.Point(893, 15);
            this.bSearch.Name = "bSearch";
            this.bSearch.Size = new System.Drawing.Size(75, 23);
            this.bSearch.TabIndex = 2;
            this.bSearch.Text = "Cari";
            this.bSearch.UseVisualStyleBackColor = true;
            this.bSearch.Click += new System.EventHandler(this.bSearch_Click);
            // 
            // dataGC
            // 
            this.dataGC.AllowUserToAddRows = false;
            this.dataGC.AllowUserToDeleteRows = false;
            this.dataGC.AllowUserToResizeColumns = false;
            this.dataGC.AllowUserToResizeRows = false;
            this.dataGC.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGC.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGC.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGC.Location = new System.Drawing.Point(6, 62);
            this.dataGC.MultiSelect = false;
            this.dataGC.Name = "dataGC";
            this.dataGC.ReadOnly = true;
            this.dataGC.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGC.Size = new System.Drawing.Size(974, 401);
            this.dataGC.TabIndex = 19;
            this.dataGC.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGC_CellClick);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGB);
            this.tabPage3.Controls.Add(this.groupBox2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(986, 469);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "by Brand";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGB
            // 
            this.dataGB.AllowUserToAddRows = false;
            this.dataGB.AllowUserToDeleteRows = false;
            this.dataGB.AllowUserToResizeColumns = false;
            this.dataGB.AllowUserToResizeRows = false;
            this.dataGB.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGB.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGB.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGB.Location = new System.Drawing.Point(6, 62);
            this.dataGB.MultiSelect = false;
            this.dataGB.Name = "dataGB";
            this.dataGB.ReadOnly = true;
            this.dataGB.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGB.Size = new System.Drawing.Size(974, 401);
            this.dataGB.TabIndex = 22;
            this.dataGB.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGB_CellClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tSearchB);
            this.groupBox2.Controls.Add(this.bSearchB);
            this.groupBox2.Location = new System.Drawing.Point(6, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(974, 44);
            this.groupBox2.TabIndex = 21;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Cari Brand";
            // 
            // tSearchB
            // 
            this.tSearchB.Location = new System.Drawing.Point(6, 17);
            this.tSearchB.Name = "tSearchB";
            this.tSearchB.Size = new System.Drawing.Size(881, 20);
            this.tSearchB.TabIndex = 1;
            this.tSearchB.TextChanged += new System.EventHandler(this.bSearchB_Click);
            // 
            // bSearchB
            // 
            this.bSearchB.Location = new System.Drawing.Point(893, 15);
            this.bSearchB.Name = "bSearchB";
            this.bSearchB.Size = new System.Drawing.Size(75, 23);
            this.bSearchB.TabIndex = 2;
            this.bSearchB.Text = "Cari";
            this.bSearchB.UseVisualStyleBackColor = true;
            this.bSearchB.Click += new System.EventHandler(this.bSearchB_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dataGL);
            this.tabPage4.Controls.Add(this.groupBox3);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(986, 469);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "by Location";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dataGL
            // 
            this.dataGL.AllowUserToAddRows = false;
            this.dataGL.AllowUserToDeleteRows = false;
            this.dataGL.AllowUserToResizeColumns = false;
            this.dataGL.AllowUserToResizeRows = false;
            this.dataGL.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGL.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGL.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGL.Location = new System.Drawing.Point(6, 62);
            this.dataGL.MultiSelect = false;
            this.dataGL.Name = "dataGL";
            this.dataGL.ReadOnly = true;
            this.dataGL.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGL.Size = new System.Drawing.Size(974, 401);
            this.dataGL.TabIndex = 23;
            this.dataGL.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGL_CellClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tSearchL);
            this.groupBox3.Controls.Add(this.bSearchL);
            this.groupBox3.Location = new System.Drawing.Point(6, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(974, 44);
            this.groupBox3.TabIndex = 22;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Cari Lokasi";
            // 
            // tSearchL
            // 
            this.tSearchL.Location = new System.Drawing.Point(6, 17);
            this.tSearchL.Name = "tSearchL";
            this.tSearchL.Size = new System.Drawing.Size(881, 20);
            this.tSearchL.TabIndex = 1;
            this.tSearchL.TextChanged += new System.EventHandler(this.bSearchL_Click);
            // 
            // bSearchL
            // 
            this.bSearchL.Location = new System.Drawing.Point(893, 15);
            this.bSearchL.Name = "bSearchL";
            this.bSearchL.Size = new System.Drawing.Size(75, 23);
            this.bSearchL.TabIndex = 2;
            this.bSearchL.Text = "Cari";
            this.bSearchL.UseVisualStyleBackColor = true;
            this.bSearchL.Click += new System.EventHandler(this.bSearchL_Click);
            // 
            // PDateReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 519);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "PDateReport";
            this.Text = "Laporan Tahun Pembelian";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SummaryReport_FormClosed);
            this.Load += new System.EventHandler(this.SummaryReport_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.PDateReport_Paint);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGU)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.gSearch.ResumeLayout(false);
            this.gSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGC)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGB)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGL)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dataGU;
        private System.Windows.Forms.DataGridView dataGC;
        private System.Windows.Forms.GroupBox gSearch;
        private System.Windows.Forms.TextBox tSearchC;
        private System.Windows.Forms.Button bSearch;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tSearchU;
        private System.Windows.Forms.Button bSearchU;
        private System.Windows.Forms.DataGridView dataGB;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tSearchB;
        private System.Windows.Forms.Button bSearchB;
        private System.Windows.Forms.DataGridView dataGL;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tSearchL;
        private System.Windows.Forms.Button bSearchL;
    }
}