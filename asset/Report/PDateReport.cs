﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class PDateReport : Form
    {
        lib lib = new lib();
        public PDateReport()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }
        private static PDateReport form;
        public static PDateReport GetChildInstance()
        {
            if (form == null)
                form = new PDateReport();
            return form;
        }
        private void SummaryReport_Load(object sender, EventArgs e)
        {
            try
            {
                showDataC();
                showDataU();
                showDataB();
                showDataL();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                showDataC();
                showDataU();
                showDataB();
                showDataL();
                clearText();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void clearText() 
        {
            tSearchC.Text = "";
            tSearchU.Text = "";
            tSearchB.Text = "";
            tSearchL.Text = "";
        }
        ///////////////////////////////////////////////////// Category /////////////////////////////////////////
        public void showDataC() 
        {
            try
            {
                String[] c = getPivotColumnsData();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter(
                    "SELECT A.E \"ID\",A.C Kategori, " + getSPivot(c, "A") +
                    ", ISNULL(A.[X] ,0) 'Total' FROM( " +
                    "SELECT (select count(M.id_asset) from t_asset M, m_asset O where O.id_category=B.id_category and O.id_asset=M.id_asset) X, B.id_category E, B.category C, ISNULL(COUNT(A.model),0) J, LEFT(A.pur_date,4) y " +
                    "FROM m_asset A, m_category B " +
                    "WHERE A.id_category=B.id_category " +
                    "GROUP BY B.id_category,B.category,LEFT(A.pur_date,4) " +
                    ") AS j " +
                    "PIVOT " +
                    "(" +
                    "MAX(J) FOR y IN (" + getCPivot(c) + ") " +
                    ") AS A ", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGC.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGC.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public String [] getPivotColumnsData() {
            List<String> s=new List<String>();
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("SELECT LEFT(p.pur_date,4) dt FROM m_asset AS p " +
                    "INNER JOIN m_category AS o " +
                    "ON p.id_category = o.id_category " +
                    "GROUP BY LEFT(p.pur_date,4) " +
                    "ORDER BY LEFT(p.pur_date,4) DESC", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    if (read[0].ToString() != null && read[0].ToString() != "")
                    {
                        s.Add(read[0].ToString());
                    }
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return s.ToArray();
        }
        public String getSPivot(String [] s, String prefix) {
            String hasil = "";
            try
            {
                for (int i = 0; i < s.Length; i++)
                {
                    if (i == (s.Length - 1))
                    {
                        hasil += " ISNULL(" + prefix + ".[" + s[i] + "] ,0) '" + s[i] + "' ";
                    }
                    else
                    {
                        hasil += " ISNULL(" + prefix + ".[" + s[i] + "] ,0)  '" + s[i] + "' ,";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
        public String getCPivot(String[] s)
        {
            String hasil = "";
            try
            {
                for (int i = 0; i < s.Length; i++)
                {
                    if (i == (s.Length - 1))
                    {
                        hasil += " [" + s[i] + "] ";
                    }
                    else
                    {
                        hasil += " [" + s[i] + "] ,";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }

        private void bSearch_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearchC.Text;
                searchDataC(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void searchDataC(String str)
        {
            try
            {
                String[] c = getPivotColumnsData();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter(
                    "SELECT A.E \"ID\",A.C Kategori, " + getSPivot(c, "A") +
                    ", ISNULL(A.[X] ,0) 'Total' FROM( " +
                    "SELECT (select count(M.id_asset) from t_asset M, m_asset O where O.id_category=B.id_category and O.id_asset=M.id_asset) X, B.id_category E, B.category C, ISNULL(COUNT(A.model),0) J, LEFT(A.pur_date,4) y " +
                    "FROM m_asset A, m_category B " +
                    "WHERE A.id_category=B.id_category " +
                    "GROUP BY B.id_category,B.category,LEFT(A.pur_date,4) " +
                    ") AS j " +
                    "PIVOT " +
                    "(" +
                    "MAX(J) FOR y IN (" + getCPivot(c) + ") " +
                    ") AS A WHERE A.C like '%" + str + "%' ", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGC.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGC.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGC_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex > 1)
            {
                if (dataGC.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                {
                    int Num;
                    bool isNum = int.TryParse(dataGC.Columns[e.ColumnIndex].Name, out Num);
                    if (isNum)
                    {
                        String id = dataGC.Rows[e.RowIndex].Cells[0].Value.ToString();
                        String year = dataGC.Columns[e.ColumnIndex].Name;
                        AssetListD asset = new AssetListD(id, year, 1);
                        asset.ShowDialog();
                        asset.BringToFront();
                    }
                }
            }
        }
        ///////////////////////////////////////////////////// User /////////////////////////////////////////
        private void bSearchU_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearchU.Text;
                searchDataU(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showDataU()
        {
            try
            {
                String[] c = getPivotColumnsData();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter(
                    "SELECT A.E \"ID\",A.C \"Pengguna\", " + getSPivot(c, "A") +
                    ", ISNULL(A.[X] ,0) 'Total' FROM( " +
                    "SELECT (select count(M.id_asset) from t_asset M, m_asset O where M.id_employee=B.id_employee and O.id_asset=M.id_asset) X, B.id_employee E, B.nama C, ISNULL(COUNT(A.model),0) J, LEFT(A.pur_date,4) y " +
                    "FROM m_asset A, m_employee B, t_asset C " +
                    "WHERE A.id_asset=C.id_asset and B.id_employee=C.id_employee  " +
                    "GROUP BY B.id_employee,B.nama,LEFT(A.pur_date,4) " +
                    ") AS j " +
                    "PIVOT " +
                    "(" +
                    "MAX(J) FOR y IN (" + getCPivot(c) + ") " +
                    ") AS A ", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGU.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGU.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void searchDataU(String str)
        {
            try
            {
                String[] c = getPivotColumnsData();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter(
                    "SELECT A.E \"ID\",A.C \"Pengguna\", " + getSPivot(c, "A") +
                    ", ISNULL(A.[X] ,0) 'Total' FROM( " +
                    "SELECT (select count(M.id_asset) from t_asset M, m_asset O where M.id_employee=B.id_employee and O.id_asset=M.id_asset) X, B.id_employee E, B.nama C, ISNULL(COUNT(A.model),0) J, LEFT(A.pur_date,4) y " +
                    "FROM m_asset A, m_employee B, t_asset C " +
                    "WHERE A.id_asset=C.id_asset and B.id_employee=C.id_employee  " +
                    "GROUP BY B.id_employee,B.nama,LEFT(A.pur_date,4) " +
                    ") AS j " +
                    "PIVOT " +
                    "(" +
                    "MAX(J) FOR y IN (" + getCPivot(c) + ") " +
                    ") AS A WHERE A.C like '%" + str + "%' ", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGU.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGU.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void dataGU_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //MessageBox.Show(e.RowIndex.ToString()+" "+e.ColumnIndex.ToString());
            if (e.RowIndex > -1 && e.ColumnIndex > 1)
            {
                if (dataGU.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                {
                    int Num;
                    bool isNum = int.TryParse(dataGU.Columns[e.ColumnIndex].Name, out Num);
                    if (isNum)
                    {
                        String id = dataGU.Rows[e.RowIndex].Cells[0].Value.ToString();
                        String year = dataGU.Columns[e.ColumnIndex].Name;
                        AssetListD asset = new AssetListD(id, year, 2);
                        asset.ShowDialog();
                        asset.BringToFront();
                    }
                }
            }
        }
        ///////////////////////////////////////////////////// Brand /////////////////////////////////////////
        private void bSearchB_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearchB.Text;
                searchDataB(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showDataB()
        {
            try
            {
                String[] c = getPivotColumnsData();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter(
                    "SELECT A.E \"ID\", A.C Brand, " + getSPivot(c, "A") +
                    ", ISNULL(A.[X] ,0) 'Total' FROM( " +
                    "SELECT (select count(M.id_asset) from t_asset M, m_asset O where O.id_brand=B.id_brand and O.id_asset=M.id_asset) X, B.id_brand E, B.brand C, ISNULL(COUNT(A.model),0) J, LEFT(A.pur_date,4) y " +
                    "FROM m_asset A, m_brand B " +
                    "WHERE A.id_brand=B.id_brand " +
                    "GROUP BY B.id_brand, B.brand,LEFT(A.pur_date,4) " +
                    ") AS j " +
                    "PIVOT " +
                    "(" +
                    "MAX(J) FOR y IN (" + getCPivot(c) + ") " +
                    ") AS A ", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGB.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGB.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void searchDataB(String str)
        {
            try
            {
                String[] c = getPivotColumnsData();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter(
                    "SELECT A.E \"ID\", A.C Brand, " + getSPivot(c, "A") +
                    ", ISNULL(A.[X] ,0) 'Total' FROM( " +
                    "SELECT (select count(M.id_asset) from t_asset M, m_asset O where O.id_brand=B.id_brand and O.id_asset=M.id_asset) X, B.id_brand E, B.brand C, ISNULL(COUNT(A.model),0) J, LEFT(A.pur_date,4) y " +
                    "FROM m_asset A, m_brand B " +
                    "WHERE A.id_brand=B.id_brand " +
                    "GROUP BY B.id_brand, B.brand,LEFT(A.pur_date,4) " +
                    ") AS j " +
                    "PIVOT " +
                    "(" +
                    "MAX(J) FOR y IN (" + getCPivot(c) + ") " +
                    ") AS A WHERE A.C like '%" + str + "%' ", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGB.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGB.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void dataGB_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex > 1)
            {
                if (dataGB.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                {
                    int Num;
                    bool isNum = int.TryParse(dataGB.Columns[e.ColumnIndex].Name, out Num);
                    if (isNum)
                    {
                        String id = dataGB.Rows[e.RowIndex].Cells[0].Value.ToString();
                        String year = dataGB.Columns[e.ColumnIndex].Name;
                        AssetListD asset = new AssetListD(id, year, 3);
                        asset.ShowDialog();
                        asset.BringToFront();
                    }
                }
            }
        }
        ///////////////////////////////////////////////////// Location /////////////////////////////////////////
        private void bSearchL_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearchL.Text;
                searchDataL(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showDataL()
        {
            try
            {
                String[] c = getPivotColumnsData();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter(
                    "SELECT A.E \"ID\", A.C Lokasi, " + getSPivot(c, "A") +
                    ", ISNULL(A.[X] ,0) 'Total' FROM( " +
                    "SELECT (select count(M.id_asset) from t_asset M, m_asset O where O.id_location=B.id_location and O.id_asset=M.id_asset) X, B.id_location E, B.location C, ISNULL(COUNT(A.model),0) J, LEFT(A.pur_date,4) y " +
                    "FROM m_asset A, m_location B " +
                    "WHERE A.id_location=B.id_location " +
                    "GROUP BY B.id_location, B.location,LEFT(A.pur_date,4) " +
                    ") AS j " +
                    "PIVOT " +
                    "(" +
                    "MAX(J) FOR y IN (" + getCPivot(c) + ") " +
                    ") AS A ", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGL.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGL.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void searchDataL(String str)
        {
            try
            {
                String[] c = getPivotColumnsData();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter(
                    "SELECT A.E \"ID\", A.C Lokasi, " + getSPivot(c, "A") +
                    ", ISNULL(A.[X] ,0) 'Total' FROM( " +
                    "SELECT (select count(M.id_asset) from t_asset M, m_asset O where O.id_location=B.id_location and O.id_asset=M.id_asset) X, B.id_location E, B.location C, ISNULL(COUNT(A.model),0) J, LEFT(A.pur_date,4) y " +
                    "FROM m_asset A, m_location B " +
                    "WHERE A.id_location=B.id_location " +
                    "GROUP BY B.id_location, B.location,LEFT(A.pur_date,4) " +
                    ") AS j " +
                    "PIVOT " +
                    "(" +
                    "MAX(J) FOR y IN (" + getCPivot(c) + ") " +
                    ") AS A WHERE A.C like '%" + str + "%' ", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGL.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGL.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SummaryReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void dataGL_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex > 1)
            {
                if (dataGL.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                {
                    int Num;
                    bool isNum = int.TryParse(dataGL.Columns[e.ColumnIndex].Name, out Num);
                    if (isNum)
                    {
                        String id = dataGL.Rows[e.RowIndex].Cells[0].Value.ToString();
                        String year = dataGL.Columns[e.ColumnIndex].Name;
                        AssetListD asset = new AssetListD(id, year, 4);
                        asset.ShowDialog();
                        asset.BringToFront();
                    }
                }
            }
        }

        private void PDateReport_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
    }
}
