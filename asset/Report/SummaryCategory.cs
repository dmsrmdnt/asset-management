﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset.Report
{
    public partial class SummaryCategory : Form
    {
        lib lib = new lib();
        public SummaryCategory()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }
        private static SummaryCategory form;
        public static SummaryCategory GetChildInstance()
        {
            if (form == null)
                form = new SummaryCategory();
            return form;
        }

        private void SummaryCategory_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void SummaryCategory_Load(object sender, EventArgs e)
        {
            showSummary();
            showTotal();
        }
        void showSummary() 
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "summary_category_asset";
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                DataTable t = new DataTable();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(t);
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGSearch.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGSearch.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void showTotal() 
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select count(*) from m_asset", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                   label2.Text = read[0].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void SummaryCategory_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
    }
}
