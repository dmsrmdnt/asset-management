﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using System.Diagnostics;

namespace asset
{
    public partial class PreviewPrintMutation : Form
    {
        private SqlCommand sql;
        private String date;
        lib lib = new lib();
        public PreviewPrintMutation(SqlCommand sql, String date)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.sql = sql;
            this.date = date;
        }

        private void PreviewPrintReport_Load(object sender, EventArgs e)
        {
            try
            {
                showReport(this.sql, this.date);
                //convertPDF(this.id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void showReport(SqlCommand sql, String date)
        {
            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(lib.param());
                cnn.Open();

                SqlDataAdapter dscmd = new SqlDataAdapter();
                dscmd.SelectCommand = sql;
                Report.DSreportAsset ds = new Report.DSreportAsset();
                dscmd.Fill(ds, "Mutation");
                cnn.Close();
                Report.reportMutation objRpt = new Report.reportMutation();
                objRpt.SetDataSource(ds.Tables["Mutation"]);

                ParameterFieldDefinitions crParameterFieldDefinitions;
                ParameterFieldDefinition crParameterFieldDefinition;
                ParameterValues crParameterValues = new ParameterValues();
                ParameterDiscreteValue crParameterDiscreteValue = new ParameterDiscreteValue();

                crParameterDiscreteValue.Value = this.date;
                crParameterFieldDefinitions = objRpt.DataDefinition.ParameterFields;
                crParameterFieldDefinition = crParameterFieldDefinitions["date"];
                crParameterValues = crParameterFieldDefinition.CurrentValues;

                crParameterValues.Clear();
                crParameterValues.Add(crParameterDiscreteValue);
                crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);

                crystalReportViewer1.ReportSource = objRpt;
                crystalReportViewer1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
    }
}
