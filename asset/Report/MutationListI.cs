﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;
using System.IO;
using System.Diagnostics;

namespace asset
{
    public partial class MutationListI : Form
    {
        private String id;
        private String tanggal;
        lib lib = new lib();
        public MutationListI(String id, String tanggal)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
            this.tanggal = tanggal;
        }
        String searchData(String id, String tanggal)
        {
            String sql="select (select nama from m_employee where id_employee=h.id_employee1) as \"Pemilik Lama\", h.date_app as \"Tanggal Mutasi\", a.id_asset as \"ID Aset\", b.category as Kategori, c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                "from m_asset a, m_category b, m_brand c, m_location d, m_status e, t_asset f, m_employee g, t_mutation h " +
                "where h.id_employee2='" + id + "' and a.id_asset=h.id_asset and a.id_asset=f.id_asset and g.id_employee=f.id_employee and a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status and h.stat='0' and h.date_app between "+
                "(SELECT DATEADD(dd,-(DAY(DATEADD(mm,-1,'" + tanggal + "'))-1),DATEADD(mm,-1,'" + tanggal + "'))) and (SELECT DATEADD(dd,-(DAY('" + tanggal + "')),'" + tanggal + "'))";
                   
            return sql;
        }
        private void MutationListI_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewLinkColumn links = new DataGridViewLinkColumn();
                links.UseColumnTextForLinkValue = true;
                links.HeaderText = "Download Dokumen Mutasi";
                links.DataPropertyName = "lnkColumn";
                links.ActiveLinkColor = Color.White;
                links.LinkBehavior = LinkBehavior.SystemDefault;
                links.LinkColor = Color.Blue;
                links.Text = "Click here to download";
                links.TrackVisitedState = true;
                links.VisitedLinkColor = Color.YellowGreen;
                dataGM.Columns.Add(links);
                String sql = searchData(this.id, this.tanggal);
                showDataGM(this.id, sql);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void showDataGM(String id, String sql)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter(sql, conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGM.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGM.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bSearch_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearch.Text;
                String sql = searchData(this.id, this.tanggal);
                searchDataGM(this.id, sql, str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void searchDataGM(String id,String sql,String str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter(sql + " and (select nama from m_employee where id_employee=h.id_employee1) like '%" + str + "%'", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGM.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGM.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void MutationListI_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

        private void dataGM_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGM.Columns[0].Index)
                {
                    //Download
                    try
                    {
                        String id = dataGM[3, e.RowIndex].Value.ToString();
                        FileStream FS = null;
                        byte[] dbbyte;

                        //Get selected file ID field
                        SqlConnection conn = new SqlConnection(lib.param());
                        conn.Open();
                        SqlCommand sqlcmd = new SqlCommand();
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataTable dt = new DataTable();
                        sqlcmd = new SqlCommand("select * from t_mutation where id_asset='" + id + "' and stat='0' and id_employee2='" + this.id + "'", conn);
                        da = new SqlDataAdapter(sqlcmd);
                        dt = new DataTable();
                        da.Fill(dt);
                        conn.Close();


                        if (dt.Rows.Count > 0)
                        {
                            //Get a stored PDF bytes
                            String dname = dt.Rows[0]["dname"].ToString();
                            if (dname != "")
                            {
                                dbbyte = (byte[])dt.Rows[0]["dcontent"];
                                String[] filetype = dname.Split('.');

                                //store file Temporarily 
                                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                                string filepath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\BA\\" + dname + "." + filetype[1];

                                //Assign File path create file
                                FS = new FileStream(filepath, System.IO.FileMode.Create);

                                //Write bytes to create file
                                FS.Write(dbbyte, 0, dbbyte.Length);

                                //Close FileStream instance
                                FS.Close();

                                // Open fila after write 

                                //Create instance for process class
                                Process Proc = new Process();

                                //assign file path for process
                                Proc.StartInfo.FileName = filepath;
                                Proc.Start();
                            }
                            else
                            {
                                MessageBox.Show("Dokumen mutasi tidak tersedia", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
