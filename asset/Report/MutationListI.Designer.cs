﻿namespace asset
{
    partial class MutationListI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tSearch = new System.Windows.Forms.TextBox();
            this.gSearch = new System.Windows.Forms.GroupBox();
            this.bSearch = new System.Windows.Forms.Button();
            this.dataGM = new System.Windows.Forms.DataGridView();
            this.gSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGM)).BeginInit();
            this.SuspendLayout();
            // 
            // tSearch
            // 
            this.tSearch.Location = new System.Drawing.Point(6, 17);
            this.tSearch.Name = "tSearch";
            this.tSearch.Size = new System.Drawing.Size(465, 20);
            this.tSearch.TabIndex = 1;
            this.tSearch.TextChanged += new System.EventHandler(this.bSearch_Click);
            // 
            // gSearch
            // 
            this.gSearch.BackColor = System.Drawing.Color.Transparent;
            this.gSearch.Controls.Add(this.tSearch);
            this.gSearch.Controls.Add(this.bSearch);
            this.gSearch.ForeColor = System.Drawing.Color.White;
            this.gSearch.Location = new System.Drawing.Point(12, 8);
            this.gSearch.Name = "gSearch";
            this.gSearch.Size = new System.Drawing.Size(558, 44);
            this.gSearch.TabIndex = 12;
            this.gSearch.TabStop = false;
            this.gSearch.Text = "Nama";
            // 
            // bSearch
            // 
            this.bSearch.ForeColor = System.Drawing.Color.Black;
            this.bSearch.Location = new System.Drawing.Point(477, 15);
            this.bSearch.Name = "bSearch";
            this.bSearch.Size = new System.Drawing.Size(75, 23);
            this.bSearch.TabIndex = 2;
            this.bSearch.Text = "Cari";
            this.bSearch.UseVisualStyleBackColor = true;
            this.bSearch.Click += new System.EventHandler(this.bSearch_Click);
            // 
            // dataGM
            // 
            this.dataGM.AllowUserToAddRows = false;
            this.dataGM.AllowUserToDeleteRows = false;
            this.dataGM.AllowUserToResizeColumns = false;
            this.dataGM.AllowUserToResizeRows = false;
            this.dataGM.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGM.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGM.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGM.Location = new System.Drawing.Point(12, 58);
            this.dataGM.MultiSelect = false;
            this.dataGM.Name = "dataGM";
            this.dataGM.ReadOnly = true;
            this.dataGM.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGM.Size = new System.Drawing.Size(558, 176);
            this.dataGM.TabIndex = 11;
            this.dataGM.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGM_CellContentClick);
            // 
            // MutationListI
            // 
            this.AcceptButton = this.bSearch;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(587, 252);
            this.Controls.Add(this.gSearch);
            this.Controls.Add(this.dataGM);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MutationListI";
            this.Text = "Daftar Mutasi Masuk";
            this.Load += new System.EventHandler(this.MutationListI_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MutationListI_Paint);
            this.gSearch.ResumeLayout(false);
            this.gSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGM)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tSearch;
        private System.Windows.Forms.GroupBox gSearch;
        private System.Windows.Forms.Button bSearch;
        private System.Windows.Forms.DataGridView dataGM;
    }
}