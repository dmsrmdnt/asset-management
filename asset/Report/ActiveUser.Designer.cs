﻿namespace asset.Report
{
    partial class ActiveUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label10 = new System.Windows.Forms.Label();
            this.dAkhir = new System.Windows.Forms.DateTimePicker();
            this.dAwal = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.dataGSearch = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGSearch)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(275, 22);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 22;
            this.label10.Text = "s/d";
            // 
            // dAkhir
            // 
            this.dAkhir.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dAkhir.Location = new System.Drawing.Point(315, 19);
            this.dAkhir.Name = "dAkhir";
            this.dAkhir.Size = new System.Drawing.Size(188, 20);
            this.dAkhir.TabIndex = 21;
            this.dAkhir.ValueChanged += new System.EventHandler(this.dAkhir_ValueChanged);
            // 
            // dAwal
            // 
            this.dAwal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dAwal.Location = new System.Drawing.Point(64, 19);
            this.dAwal.Name = "dAwal";
            this.dAwal.Size = new System.Drawing.Size(188, 20);
            this.dAwal.TabIndex = 20;
            this.dAwal.Value = new System.DateTime(2000, 12, 25, 0, 0, 0, 0);
            this.dAwal.ValueChanged += new System.EventHandler(this.dAwal_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(9, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Tanggal ";
            // 
            // dataGSearch
            // 
            this.dataGSearch.AllowUserToAddRows = false;
            this.dataGSearch.AllowUserToDeleteRows = false;
            this.dataGSearch.AllowUserToResizeColumns = false;
            this.dataGSearch.AllowUserToResizeRows = false;
            this.dataGSearch.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGSearch.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGSearch.Location = new System.Drawing.Point(17, 70);
            this.dataGSearch.MultiSelect = false;
            this.dataGSearch.Name = "dataGSearch";
            this.dataGSearch.ReadOnly = true;
            this.dataGSearch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGSearch.Size = new System.Drawing.Size(978, 416);
            this.dataGSearch.TabIndex = 23;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.dAwal);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.dAkhir);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(17, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(978, 52);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tanggal";
            // 
            // ActiveUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1007, 507);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGSearch);
            this.Name = "ActiveUser";
            this.Text = "User Aktif";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ActiveUser_FormClosed);
            this.Load += new System.EventHandler(this.ActiveUser_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ActiveUser_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.dataGSearch)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dAkhir;
        private System.Windows.Forms.DateTimePicker dAwal;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridView dataGSearch;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}