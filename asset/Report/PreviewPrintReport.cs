﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.IO;
using System.Diagnostics;

namespace asset
{
    public partial class PreviewPrintReport : Form
    {
        private String id;
        lib lib = new lib();
        Class.Email.email mail = new Class.Email.email();
        public PreviewPrintReport(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
        }

        private void PreviewPrintReport_Load(object sender, EventArgs e)
        {
            try
            {
                //convertPDF(this.id);
                showReport(this.id);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void showReport(String id_employee)
        {
            try
            {
                SqlConnection cnn;
                cnn = new SqlConnection(lib.param());
                cnn.Open();
                SqlCommand myCommand = cnn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "print_report";
                myCommand.Parameters.AddWithValue("@id_employee", id_employee);
                SqlDataAdapter dscmd = new SqlDataAdapter();
                dscmd.SelectCommand = myCommand;
                Report.DSreportAsset ds = new Report.DSreportAsset();
                dscmd.Fill(ds, "Report");
                cnn.Close();
                Report.reportAsset objRpt = new Report.reportAsset();
                objRpt.SetDataSource(ds.Tables[0]);

                crystalReportViewer1.ReportSource = objRpt;
                crystalReportViewer1.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void convertPDF(String id_employee)
        {
            try
            {
                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                var file = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\Report\\" +
                            "reportP" + id_employee + date + ".pdf";
                if (File.Exists(file)) File.Delete(file);
                SqlConnection cnn;
                cnn = new SqlConnection(lib.param());
                cnn.Open();
                SqlCommand myCommand = cnn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "print_report";
                myCommand.Parameters.AddWithValue("@id_employee", id_employee);
                SqlDataAdapter dscmd = new SqlDataAdapter();
                dscmd.SelectCommand = myCommand;
                Report.DSreportAsset ds = new Report.DSreportAsset();
                dscmd.Fill(ds, "Report");
                cnn.Close();
                Report.reportAsset objRpt = new Report.reportAsset();
                objRpt.SetDataSource(ds.Tables[0]);

                ExportOptions CrExportOptions;
                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                CrDiskFileDestinationOptions.DiskFileName = file;
                CrExportOptions = objRpt.ExportOptions;
                {
                    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                    CrExportOptions.FormatOptions = CrFormatTypeOptions;
                }
                objRpt.Export();
                /*View.SendEmail s = new View.SendEmail();
                s.id_employee = id_employee;
                s.stat = 2;
                s.file = file.ToString();
                s.form = 4;
                s.ShowDialog();

                if (asset.Class.Email.email.cek == true)
                {
                    MessageBox.Show("Laporan daftar aset berhasil dikirim", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Laporan daftar aset gagal dikirim", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Close();
                }*/
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
