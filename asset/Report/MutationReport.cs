﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace asset
{
    public partial class MutationReport : Form
    {
        lib lib = new lib();
        private String date = "";
        private SqlCommand sql;
        DataTable t = new DataTable();
        private String id;
        public MutationReport(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
        }
        private static MutationReport form;
        public static MutationReport GetChildInstance(String id)
        {
            if (form == null)
                form = new MutationReport(id);
            return form;
        }
        public void SetMyCustomFormat(DateTimePicker p, String style)
        {
            try
            {
                p.Format = DateTimePickerFormat.Custom;
                p.CustomFormat = style;
                p.ShowUpDown = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void MutationReport_Load(object sender, EventArgs e)
        {
            try
            {
                dAwal.Format = DateTimePickerFormat.Custom;
                dAwal.CustomFormat = "MMMM";
                dAwal.ShowUpDown = true;
                dAkhir.Format = DateTimePickerFormat.Custom;
                dAkhir.CustomFormat = "yyyy";
                dAkhir.ShowUpDown = true;
                String tanggal = gettanggal(dAwal.Value.Month.ToString(), dAkhir.Value.Year.ToString());
                SqlCommand sql = searchData(tanggal);
                this.sql = sql;
                this.date = dAwal.Text + " " + dAkhir.Text;
                showDataU(sql);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void showDataU(SqlCommand sql)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter();
                data.SelectCommand = sql;
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "ListLoseTable");
                t.Clear();
                data.Fill(t);
                conn.Close();
                this.dataGU.DataSource = dataSet.Tables["ListLoseTable"].DefaultView;
                this.dataGU.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        

        private void bCari_Click(object sender, EventArgs e)
        {
            try
            {
                this.date = dAwal.Text + " " + dAkhir.Text;
                String tanggal = gettanggal(dAwal.Value.Month.ToString(), dAkhir.Value.Year.ToString());
                SqlCommand sql = searchData(tanggal);
                showDataU(sql);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        String gettanggal(String month, String year) {
            return month + "/1/" + year;
        }
        SqlCommand searchData(String tanggal)
        {
            SqlConnection conn = new SqlConnection(lib.param());
            conn.Open();
            SqlCommand sql = conn.CreateCommand();
            sql.CommandType = CommandType.StoredProcedure;
            sql.CommandText = "show_mutation_report";
            sql.Parameters.AddWithValue("@tanggal", tanggal);
            conn.Close();
            this.sql = sql;
            return sql;
        }

        private void dataGU_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex > 1)
            {
                if (dataGU.Rows[e.RowIndex].Cells[e.ColumnIndex].Value != null)
                {
                    String id = dataGU[0, e.RowIndex].Value.ToString();
                    String tAwal = dAwal.Text;
                    if (e.ColumnIndex == dataGU.Columns[2].Index)
                    {
                        String tanggal = gettanggal(dAwal.Value.Month.ToString(), dAkhir.Value.Year.ToString());
                        MutationListI m = new MutationListI(id, tanggal);
                        m.ShowDialog();
                        m.BringToFront();
                    }
                    else if (e.ColumnIndex == dataGU.Columns[3].Index)
                    {
                        String tanggal = gettanggal(dAwal.Value.Month.ToString(), dAkhir.Value.Year.ToString());
                        MutationList m = new MutationList(id, tanggal);
                        m.ShowDialog();
                        m.BringToFront();
                    }
                    else if (e.ColumnIndex == dataGU.Columns[4].Index)
                    {
                        String tanggal = gettanggal(dAwal.Value.Month.ToString(), dAkhir.Value.Year.ToString());
                        BrokenList m = new BrokenList(id, tanggal);
                        m.ShowDialog();
                        m.BringToFront();
                    }
                    else if (e.ColumnIndex == dataGU.Columns[6].Index)
                    {
                        String tanggal = gettanggal(dAwal.Value.Month.ToString(), dAkhir.Value.Year.ToString());
                        FixList m = new FixList(id, tanggal);
                        m.ShowDialog();
                        m.BringToFront();
                    }
                    else if (e.ColumnIndex == dataGU.Columns[5].Index)
                    {
                        String tanggal = gettanggal(dAwal.Value.Month.ToString(), dAkhir.Value.Year.ToString());
                        FixListP m = new FixListP(id, tanggal);
                        m.ShowDialog();
                        m.BringToFront();
                    }
                    else if (e.ColumnIndex == dataGU.Columns[7].Index)
                    {
                        String tanggal = gettanggal(dAwal.Value.Month.ToString(), dAkhir.Value.Year.ToString());
                        BreakList m = new BreakList(id, tanggal);
                        m.ShowDialog();
                        m.BringToFront();
                    }
                    else if (e.ColumnIndex == dataGU.Columns[8].Index)
                    {
                        String tanggal = gettanggal(dAwal.Value.Month.ToString(), dAkhir.Value.Year.ToString());
                        LostList m = new LostList(id, tanggal);
                        m.ShowDialog();
                        m.BringToFront();
                    }
                }
            }
        }

        private void bExport_Click(object sender, EventArgs e)
        {
            try
            {
                convertExcel(this.sql);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void bExport2_Click(object sender, EventArgs e)
        {
            try
            {
                convertPDF(this.sql);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void convertExcel(SqlCommand sql)
        {
            try
            {
                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                var file = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\Report\\" +
    "reportM" + id + date + ".xls";
                //MessageBox.Show(file);
                if (File.Exists(file)) File.Delete(file);
                SqlConnection cnn;
                cnn = new SqlConnection(lib.param());
                cnn.Open();
                
                SqlDataAdapter dscmd = new SqlDataAdapter();
                dscmd.SelectCommand = sql;
                Report.DSreportAsset ds = new Report.DSreportAsset();
                dscmd.Fill(ds, "Mutation");
                cnn.Close();
                Report.reportMutation objRpt = new Report.reportMutation();
                objRpt.SetDataSource(ds.Tables["Mutation"]);

                ParameterFieldDefinitions crParameterFieldDefinitions;
                ParameterFieldDefinition crParameterFieldDefinition;
                ParameterValues crParameterValues = new ParameterValues();
                ParameterDiscreteValue crParameterDiscreteValue = new ParameterDiscreteValue();

                crParameterDiscreteValue.Value = this.date;
                crParameterFieldDefinitions = objRpt.DataDefinition.ParameterFields;
                crParameterFieldDefinition = crParameterFieldDefinitions["date"];
                crParameterValues = crParameterFieldDefinition.CurrentValues;

                crParameterValues.Clear();
                crParameterValues.Add(crParameterDiscreteValue);
                crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);

                ExportOptions CrExportOptions;
                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                ExcelFormatOptions CrFormatTypeOptions = new ExcelFormatOptions();
                CrDiskFileDestinationOptions.DiskFileName = file;
                CrExportOptions = objRpt.ExportOptions;
                CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                CrExportOptions.ExportFormatType = ExportFormatType.Excel;
                CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                CrExportOptions.FormatOptions = CrFormatTypeOptions;
                objRpt.Export();
                Process.Start(file);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void convertPDF(SqlCommand sql)
        {
            try
            {
                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                var file = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\Report\\" +"reportM" + id + date + ".pdf";
                if (File.Exists(file)) File.Delete(file);
                SqlConnection cnn;

                
                cnn = new SqlConnection(lib.param());
                cnn.Open();
                
                SqlDataAdapter dscmd = new SqlDataAdapter();
                dscmd.SelectCommand = sql;
                Report.DSreportAsset ds = new Report.DSreportAsset();
                dscmd.Fill(ds, "Mutation");
                cnn.Close();
                Report.reportMutation objRpt = new Report.reportMutation();
                objRpt.SetDataSource(ds.Tables["Mutation"]);

                ParameterFieldDefinitions crParameterFieldDefinitions;
                ParameterFieldDefinition crParameterFieldDefinition;
                ParameterValues crParameterValues = new ParameterValues();
                ParameterDiscreteValue crParameterDiscreteValue = new ParameterDiscreteValue();

                crParameterDiscreteValue.Value = this.date;
                crParameterFieldDefinitions = objRpt.DataDefinition.ParameterFields;
                crParameterFieldDefinition = crParameterFieldDefinitions["date"];
                crParameterValues = crParameterFieldDefinition.CurrentValues;

                crParameterValues.Clear();
                crParameterValues.Add(crParameterDiscreteValue);
                crParameterFieldDefinition.ApplyCurrentValues(crParameterValues);

                ExportOptions CrExportOptions;
                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                CrDiskFileDestinationOptions.DiskFileName = file;
                CrExportOptions = objRpt.ExportOptions;
                {
                    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                    CrExportOptions.FormatOptions = CrFormatTypeOptions;
                }
                objRpt.Export();
                Process.Start(file);
            }
            catch (Exception)
            {
                MessageBox.Show("File masih terbuka", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void exportExcel(DataTable dt,String id, String tAwal, String tAkhir, String tNama)
        {
            try
            {
                String date = DateTime.Now.ToString("ddMMyy");
                var file =Path.Combine(
    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "reportM" + id + date + ".xls");
                if (File.Exists(file)) File.Delete(file);
                FileStream stream = new FileStream(file, FileMode.OpenOrCreate);
                ExcelWriter writer = new ExcelWriter(stream);
                writer.BeginWrite();
                int iRow = 0;
                //writer.WriteCell(iRow, iCol, "Laporan Mutasi/Kerusakan/Kehilangan"); iRow++;
                //writer.WriteCell(iRow, iCol, "Tanggal "); iRow++;
                int iCol = -1;
                foreach (DataColumn c in dt.Columns)
                {
                    iCol++;
                    writer.WriteCell(iRow, iCol, c.ColumnName);
                }
                iRow++;
                foreach (DataRow r in dt.Rows)
                {
                    iRow++;
                    iCol = -1;
                    foreach (DataColumn c in dt.Columns)
                    {
                        iCol++;
                        writer.WriteCell(iRow, iCol, r[c.ColumnName].ToString());
                    }
                }
                writer.EndWrite();
                stream.Close();
                Process.Start(file);
            }
            catch (Exception)
            {
                MessageBox.Show("File masih terbuka", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void MutationReport_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            } 
        }

        private void bPrint_Click(object sender, EventArgs e)
        {
            try
            {
                PreviewPrintMutation p = new PreviewPrintMutation(this.sql, this.date);
                p.ShowDialog();
                p.BringToFront();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void MutationReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }


    }
}
