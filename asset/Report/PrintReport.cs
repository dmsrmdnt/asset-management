﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class PrintReport : Form
    {
        lib lib = new lib();
        Class.Email.email mail = new Class.Email.email();
        private String id;
        public PrintReport(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
        }
        private static PrintReport form;
        public static PrintReport GetChildInstance(String id)
        {
            if (form == null)
                form = new PrintReport(id);
            return form;
        }
        private void PrintReport_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewButtonColumn a = new DataGridViewButtonColumn();
                dataGUser.Columns.Add(a);
                a.HeaderText = "Cetak Bukti";
                a.Text = "Preview";
                a.UseColumnTextForButtonValue = true;
                DataGridViewButtonColumn b = new DataGridViewButtonColumn();
                dataGUser.Columns.Add(b);
                b.HeaderText = "Kirim Email";
                b.Text = "Send";
                b.UseColumnTextForButtonValue = true;
                /*DataGridViewButtonColumn c = new DataGridViewButtonColumn();
                dataGUser.Columns.Add(c);
                c.HeaderText = "Cetak Excel";
                c.Text = "Excel";
                c.UseColumnTextForButtonValue = true;*/
                showdataGUser();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showdataGUser()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sql = conn.CreateCommand();
                sql.CommandType = CommandType.StoredProcedure;
                sql.CommandText = "show_data_user";
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = sql;
                data.Fill(dataSet, "UserTable");
                conn.Close();
                this.dataGUser.DataSource = dataSet.Tables["UserTable"].DefaultView;
                this.dataGUser.Columns["ID Karyawan"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void bSearch_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearch.Text;
                searchNama(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void searchNama(String str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "search_data_user";
                myCommand.Parameters.AddWithValue("@str", str);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "UserTable");
                conn.Close();
                this.dataGUser.DataSource = dataSet.Tables["UserTable"].DefaultView;
                this.dataGUser.Columns["ID Karyawan"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void PrintReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void dataGUser_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGUser.Columns[0].Index)
                {
                    //Cetak
                    try
                    {
                        String id = dataGUser[2, e.RowIndex].Value.ToString();
                        PreviewPrintReport p = new PreviewPrintReport(id);
                        p.ShowDialog();
                        p.BringToFront();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGUser.Columns[1].Index)
                {
                    //Email
                    try
                    {
                        String id = dataGUser[2, e.RowIndex].Value.ToString();
                        sendEmail(id);
                        //convertPDF(id);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                /*else if (e.ColumnIndex == dataGUser.Columns[2].Index)
                {
                    //Excel
                    try
                    {
                        String id = dataGUser[3, e.RowIndex].Value.ToString();
                        convertExcel(id);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }*/
            }
        }
        void sendEmail(String id_employee) 
        {
            View.SendEmail s = new View.SendEmail();
            s.id_employee = id_employee;
            s.stat = 2;
            List<String> file = new List<String>();
            file.Add(convertExcel(id_employee));
            file.Add(convertPDF(id_employee));
            s.file=file.ToArray();
            s.form = 4;
            s.ShowDialog();

            if (asset.Class.Email.email.cek == true)
            {
                MessageBox.Show("Laporan daftar aset berhasil dikirim", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Laporan daftar aset gagal dikirim", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        String convertExcel(String id_employee)
        {
            String file=null;
            try
            {
                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                file = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\Report\\"+
                            "reportP" + id_employee + date + ".xls";
                //MessageBox.Show(file);
                if (File.Exists(file)) File.Delete(file);
                SqlConnection cnn;
                cnn = new SqlConnection(lib.param());
                cnn.Open();
                SqlCommand myCommand = cnn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "print_report";
                myCommand.Parameters.AddWithValue("@id_employee", id_employee);
                SqlDataAdapter dscmd = new SqlDataAdapter();
                dscmd.SelectCommand = myCommand;
                Report.DSreportAsset ds = new Report.DSreportAsset();
                dscmd.Fill(ds, "Report");
                cnn.Close();
                Report.reportAsset objRpt = new Report.reportAsset();
                objRpt.SetDataSource(ds.Tables[0]);

                ExportOptions CrExportOptions;
                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                ExcelFormatOptions CrFormatTypeOptions = new ExcelFormatOptions();
                CrDiskFileDestinationOptions.DiskFileName = file;
                CrExportOptions = objRpt.ExportOptions;
                CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                CrExportOptions.ExportFormatType = ExportFormatType.Excel;
                CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                CrExportOptions.FormatOptions = CrFormatTypeOptions;
                objRpt.Export();

                /*View.SendEmail s = new View.SendEmail();
                s.id_employee = id_employee;
                s.stat = 2;
                s.file = file.ToString();
                s.form = 4;
                s.ShowDialog();

                if (asset.Class.Email.email.cek == true)
                {
                    Process.Start(file);
                    MessageBox.Show("Laporan daftar aset berhasil dikirim", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Laporan daftar aset gagal dikirim", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }*/

                //mail.mailReport(id_employee, 2,file.ToString());
                
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return file;
        }
        String convertPDF(String id_employee)
        {
            String file = null;
            try
            {
                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                file = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\Report\\"+
                            "reportP" + id_employee + date + ".pdf";
                if (File.Exists(file)) File.Delete(file);
                SqlConnection cnn;
                cnn = new SqlConnection(lib.param());
                cnn.Open();
                SqlCommand myCommand = cnn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "print_report";
                myCommand.Parameters.AddWithValue("@id_employee", id_employee);
                SqlDataAdapter dscmd = new SqlDataAdapter();
                dscmd.SelectCommand = myCommand;
                Report.DSreportAsset ds = new Report.DSreportAsset();
                dscmd.Fill(ds, "Report");
                cnn.Close();
                Report.reportAsset objRpt = new Report.reportAsset();
                objRpt.SetDataSource(ds.Tables[0]);

                ExportOptions CrExportOptions;
                DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                CrDiskFileDestinationOptions.DiskFileName = file;
                CrExportOptions = objRpt.ExportOptions;
                {
                    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                    CrExportOptions.FormatOptions = CrFormatTypeOptions;
                }
                objRpt.Export();
                /*View.SendEmail s = new View.SendEmail();
                s.id_employee = id_employee;
                s.stat = 2;
                s.file = file.ToString();
                s.form = 4;
                s.ShowDialog();

                if (asset.Class.Email.email.cek == true)
                {
                    Process.Start(file);
                    MessageBox.Show("Laporan daftar aset berhasil dikirim", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("Laporan daftar aset gagal dikirim", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }*/
                
            }
            catch (Exception)
            {
                MessageBox.Show("File masih terbuka", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return file;
        }
        private void PrintReport_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
    }
}
