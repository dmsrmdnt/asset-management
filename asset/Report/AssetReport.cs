﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;

namespace asset
{
    public partial class AssetReport : Form
    {
        private String id;
        private String n = "";
        lib lib = new lib();
        DataTable t = new DataTable();
        public AssetReport(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
        }
        private static AssetReport form;
        public static AssetReport GetChildInstance(String id)
        {
            if (form == null)
                form = new AssetReport(id);
            return form;
        }
        private void AssetReport_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void AssetReport_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

        private void AssetReport_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewButtonColumn c = new DataGridViewButtonColumn();
                dataGUser.Columns.Add(c);
                c.HeaderText = "Detail";
                c.Text = "Detail";
                c.UseColumnTextForButtonValue = true;
                showdataGAsset();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showdataGAsset()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_assetR";
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(t);
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGUser.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGUser.Columns["ID"].Visible = false;
                this.dataGUser.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void bSearch_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearch.Text;
                this.n = str;
                searchModel(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void searchModel(String str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "search_data_assetR";
                myCommand.Parameters.AddWithValue("@str", str);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(t);
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGUser.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGUser.Columns["ID"].Visible = false;
                this.dataGUser.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void dataGUser_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGUser.Columns[0].Index)
                {
                    //Detail
                    try
                    {
                        String id = dataGUser[4, e.RowIndex].Value.ToString();
                        AssetDetail asset = new AssetDetail(id);
                        asset.ShowDialog();
                        asset.BringToFront();
                        this.dataGUser.Refresh();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
        private void bExport2_Click(object sender, EventArgs e)
        {
            try
            {
                exportExcel2(this.t, this.id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void bExport_Click(object sender, EventArgs e)
        {
            try
            {
                exportExcel(this.t, this.id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void exportExcel(DataTable dt, String id)
        {
            try
            {
                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                var file = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\Report\\" +"reportA" + id + date + ".xls";
                if (File.Exists(file)) File.Delete(file);
                FileStream stream = new FileStream(file, FileMode.OpenOrCreate);
                ExcelWriter writer = new ExcelWriter(stream);
                writer.BeginWrite();
                //excel
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                int iRow = 0;
                SqlCommand myCommand3 = conn.CreateCommand();
                myCommand3.CommandType = CommandType.StoredProcedure;
                myCommand3.CommandText = "search_data_assetR";
                myCommand3.Parameters.AddWithValue("@str", this.n);
                SqlDataReader read3 = myCommand3.ExecuteReader();
                writer.WriteCell(iRow, 0, "Item");
                writer.WriteCell(iRow, 1, "Kategori");
                writer.WriteCell(iRow, 2, "Pemilik");
                writer.WriteCell(iRow, 3, "Brand");
                writer.WriteCell(iRow, 4, "Model");
                writer.WriteCell(iRow, 5, "No. Serial");
                writer.WriteCell(iRow, 6, "Harga");
                writer.WriteCell(iRow, 7, "Tanggal Pembelian");
                writer.WriteCell(iRow, 8, "Lokasi");
                writer.WriteCell(iRow, 9, "Status");
                writer.WriteCell(iRow, 10, "Deskripsi");
                iRow++;
                while (read3.Read())
                {
                    writer.WriteCell(iRow, 0, read3["Item"].ToString());
                    writer.WriteCell(iRow, 1, read3["Kategori"].ToString());
                    writer.WriteCell(iRow, 2, read3["Pemilik"].ToString());
                    writer.WriteCell(iRow, 3, read3["Brand"].ToString());
                    writer.WriteCell(iRow, 4, read3["Model"].ToString());
                    writer.WriteCell(iRow, 5, read3["No. Serial"].ToString());
                    writer.WriteCell(iRow, 6, read3["Harga"].ToString());
                    writer.WriteCell(iRow, 7, read3["Tanggal Pembelian"].ToString());
                    writer.WriteCell(iRow, 8, read3["Lokasi"].ToString());
                    writer.WriteCell(iRow, 9, read3["Status"].ToString());
                    writer.WriteCell(iRow, 10, read3["Deskripsi"].ToString());
                    iRow++;
                }
                myCommand3.Dispose();
                read3.Close();
                iRow += 1;
                writer.EndWrite();
                stream.Close();
                Process.Start(file);
            }
            catch (Exception)
            {
                MessageBox.Show("File masih terbuka", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void exportExcel2(DataTable dt, String id)
        {
            try
            {
                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                var file = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\Report\\" +"reportA2" + id + date + ".xls";
                if (File.Exists(file)) File.Delete(file);
                FileStream stream = new FileStream(file, FileMode.OpenOrCreate);
                ExcelWriter writer = new ExcelWriter(stream);
                writer.BeginWrite();
                //excel
                //int iCol = 0;
                int iRow = 0;
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataReader read = null;
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_category";
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    writer.WriteCell(iRow, 0, read["Kategori"].ToString());
                    iRow++;
                    SqlCommand myCommand2 = conn.CreateCommand();
                    myCommand2.CommandType = CommandType.StoredProcedure;
                    myCommand2.CommandText = "get_year_pur_date";
                    myCommand2.Parameters.AddWithValue("@id_category", read["ID"].ToString());
                    SqlDataReader read2 = myCommand2.ExecuteReader();
                    while (read2.Read())
                    {
                        writer.WriteCell(iRow, 0, read2["pur_date"].ToString());
                        iRow++;
                        SqlCommand myCommand3 = conn.CreateCommand();
                        myCommand3.CommandType = CommandType.StoredProcedure;
                        myCommand3.CommandText = "get_data_pur_date";
                        myCommand3.Parameters.AddWithValue("@id_category", read["ID"].ToString());
                        myCommand3.Parameters.AddWithValue("@pur_date", read2["pur_date"].ToString());
                        myCommand3.Parameters.AddWithValue("@nama", this.n);
                        SqlDataReader read3 = myCommand3.ExecuteReader();
                        writer.WriteCell(iRow, 0, "No. Asset");
                        writer.WriteCell(iRow, 1, "Item");
                        writer.WriteCell(iRow, 2, "Kategori");
                        writer.WriteCell(iRow, 3, "Pemilik");
                        writer.WriteCell(iRow, 4, "Brand");
                        writer.WriteCell(iRow, 5, "Model");
                        writer.WriteCell(iRow, 6, "No. Serial");
                        writer.WriteCell(iRow, 7, "Harga");
                        writer.WriteCell(iRow, 8, "Tanggal Pembelian");
                        writer.WriteCell(iRow, 9, "Lokasi");
                        writer.WriteCell(iRow, 10, "Status");
                        writer.WriteCell(iRow, 11, "Deskripsi");
                        iRow++;
                        while (read3.Read())
                        {
                            writer.WriteCell(iRow, 0, read3["Asset"].ToString());
                            writer.WriteCell(iRow, 1, read3["Item"].ToString());
                            writer.WriteCell(iRow, 2, read3["Kategori"].ToString());
                            writer.WriteCell(iRow, 3, read3["Pemilik"].ToString());
                            writer.WriteCell(iRow, 4, read3["Brand"].ToString());
                            writer.WriteCell(iRow, 5, read3["Model"].ToString());
                            writer.WriteCell(iRow, 6, read3["No. Serial"].ToString());
                            writer.WriteCell(iRow, 7, read3["Harga"].ToString());
                            writer.WriteCell(iRow, 8, read3["Tanggal Pembelian"].ToString());
                            writer.WriteCell(iRow, 9, read3["Lokasi"].ToString());
                            writer.WriteCell(iRow, 10, read3["Status"].ToString());
                            writer.WriteCell(iRow, 11, read3["Deskripsi"].ToString());
                            iRow++;
                        }
                        myCommand3.Dispose();
                        read3.Close();
                        iRow+=1;
                    }
                    myCommand2.Dispose();
                    read2.Close();
                    iRow += 2;
                }

                writer.EndWrite();
                stream.Close();
                Process.Start(file);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                var file = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\Report\\" + "reportA3" + id + date + ".xls";
                if (File.Exists(file)) File.Delete(file);
                FileStream stream = new FileStream(file, FileMode.OpenOrCreate);
                ExcelWriter writer = new ExcelWriter(stream);
                writer.BeginWrite();
                int iRow = 3;
                int count = 1;
                int iCell = 3;
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataReader read = null;
                SqlCommand myCommand = conn.CreateCommand();
                //header
                writer.WriteCell(0, 0, "No");
                writer.WriteCell(0, 1, "Nama");
                writer.WriteCell(0, 2, "Device");
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_label_software";
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    writer.WriteCell(0, iCell, read["category"].ToString());
                    writer.WriteCell(1, iCell, read["status"].ToString());
                    writer.WriteCell(2, iCell, read["software"].ToString());
                    iCell++;
                    
                }
                myCommand.Dispose();
                read.Close();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_user_soft";
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    //left label
                    writer.WriteCell(iRow, 0, count++);
                    writer.WriteCell(iRow, 1, read["nama"].ToString());
                    writer.WriteCell(iRow, 2, read["category"].ToString());

                    iCell = 3;
                    SqlCommand myCommand2 = conn.CreateCommand();
                    myCommand2.CommandType = CommandType.StoredProcedure;
                    myCommand2.CommandText = "count_software";
                    myCommand2.Parameters.AddWithValue("@id_employee", read["id_employee"].ToString());
                    myCommand2.Parameters.AddWithValue("@id_asset", read["id_asset"].ToString());
                    SqlDataReader read2 = myCommand2.ExecuteReader();
                    while (read2.Read())
                    {
                        //left label
                        writer.WriteCell(iRow, iCell++, read2["jml"].ToString());

                    }
                    myCommand2.Dispose();
                    read2.Close();
                    iRow++;

                }
                myCommand.Dispose();
                read.Close();
                writer.WriteCell(iRow, 0, "Total");
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_label_software";
                read = myCommand.ExecuteReader();
                iCell = 3;
                while (read.Read())
                {
                    //left label
                    
                    SqlCommand myCommand2 = conn.CreateCommand();
                    myCommand2.CommandType = CommandType.StoredProcedure;
                    myCommand2.CommandText = "count_all_software";
                    myCommand2.Parameters.AddWithValue("@id_software", read["id_software"].ToString());
                    myCommand2.Parameters.AddWithValue("@id_status", read["id_status"].ToString());
                    SqlDataReader read2 = myCommand2.ExecuteReader();
                    while (read2.Read())
                    {
                        //left label
                        writer.WriteCell(iRow, iCell++, read2[0].ToString());

                    }
                    myCommand2.Dispose();
                    read2.Close();
                }
                writer.EndWrite();
                stream.Close();
                Process.Start(file);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        
    }
}
