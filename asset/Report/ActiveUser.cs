﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset.Report
{
    public partial class ActiveUser : Form
    {
        lib lib = new lib();
        Class.Search.activeUser s = new Class.Search.activeUser();
        public ActiveUser()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }
        private static ActiveUser form;
        public static ActiveUser GetChildInstance()
        {
            if (form == null)
                form = new ActiveUser();
            return form;
        }

        private void ActiveUser_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void ActiveUser_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
        private void ActiveUser_Load(object sender, EventArgs e)
        {
            process();
        }

        private void dAwal_ValueChanged(object sender, EventArgs e)
        {
            process();
        }

        private void dAkhir_ValueChanged(object sender, EventArgs e)
        {
            process();
        }
        void process() 
        {
            DateTime date_start = Convert.ToDateTime(dAwal.Text.ToString());
            DateTime date_finish = Convert.ToDateTime(dAkhir.Text.ToString());
            if (s.cekTanggal(date_start, date_finish))
            {
                search(dAwal.Text.ToString(), dAkhir.Text.ToString()); 
            }
            else
            {
                MessageBox.Show("Format tanggal salah!", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void search(String awal, String akhir)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "active_user";
                myCommand.Parameters.AddWithValue("@awal", awal);
                myCommand.Parameters.AddWithValue("@akhir", akhir);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                DataTable t = new DataTable();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(t);
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGSearch.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGSearch.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
