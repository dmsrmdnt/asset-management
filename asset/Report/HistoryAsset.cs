﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class HistoryAsset : Form
    {
        lib lib = new lib();
        private String id;
        public HistoryAsset(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
        }
        private static HistoryAsset form;
        public static HistoryAsset GetChildInstance(String id)
        {
            if (form == null)
                form = new HistoryAsset(id);
            return form;
        }
        private void HistoryAsset_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewButtonColumn c = new DataGridViewButtonColumn();
                dataGAsset.Columns.Add(c);
                c.HeaderText = "Detail";
                c.Text = "Detail";
                c.UseColumnTextForButtonValue = true;

                showDataC();
                String str = "";
                searchAsset(getSql(), str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        String getSql()
        {
            return "select  g.id_employee \"ID\", g.nama as Pemilik, a.id_asset as \"ID Aset\", b.category as Kategori, a.asset as \"No. Aset\", c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                "from m_asset a, m_category b, m_brand c, m_location d, m_status e, t_asset f, m_employee g " +
                "where a.id_asset=f.id_asset and g.id_employee=f.id_employee and a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status ";
        }
        void showDataC()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sc = new SqlCommand("select id_category,category from m_category", conn);
                SqlDataReader read;
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(string));
                dt.Columns.Add("category", typeof(string));
                dt.Rows.Add(new object[] { "0", "All" });
                read = sc.ExecuteReader();
                while (read.Read())
                {
                    dt.Rows.Add(new object[] { read["id_category"].ToString(), read["category"].ToString() });
                }

                cKategori.ValueMember = "id";
                cKategori.DisplayMember = "category";
                cKategori.DataSource = dt;

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        void searchAsset(String sql, String str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter(sql + " and g.nama like '%" + str + "%'", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGAsset.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGAsset.Columns["ID"].Visible = false;
                this.dataGAsset.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void HistoryAsset_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

        private void HistoryAsset_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void dataGAsset_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGAsset.Columns[0].Index)
                {
                    //Detail
                    try
                    {
                        String id = dataGAsset[3, e.RowIndex].Value.ToString();
                        String id_employee = dataGAsset[1, e.RowIndex].Value.ToString();
                        AssetDetailH asset = new AssetDetailH(id,id_employee);
                        asset.ShowDialog();
                        asset.BringToFront();
                        this.dataGAsset.Refresh();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void cKategori_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                tSearch.Text = "";
                String s = getS(Convert.ToInt32(cKategori.SelectedValue.ToString()));
                searchAsset(getSql() + s, tSearch.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        String getS(int i) 
        {
            if (i == 0)
            {
                return "";
            }
            else
            {
                return " and a.id_category='" + cKategori.SelectedValue.ToString() + "'";
            }
        }

        private void bCari_Click(object sender, EventArgs e)
        {
            try
            {
                String s = getS(Convert.ToInt32(cKategori.SelectedValue.ToString()));
                searchAsset(getSql() + s, tSearch.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
