﻿namespace asset
{
    partial class PrintReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tSearch = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.bSearch = new System.Windows.Forms.Button();
            this.dataGUser = new System.Windows.Forms.DataGridView();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGUser)).BeginInit();
            this.SuspendLayout();
            // 
            // tSearch
            // 
            this.tSearch.Location = new System.Drawing.Point(6, 20);
            this.tSearch.Name = "tSearch";
            this.tSearch.Size = new System.Drawing.Size(901, 20);
            this.tSearch.TabIndex = 1;
            this.tSearch.TextChanged += new System.EventHandler(this.bSearch_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.tSearch);
            this.groupBox3.Controls.Add(this.bSearch);
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(12, 1);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(994, 47);
            this.groupBox3.TabIndex = 10;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Nama";
            // 
            // bSearch
            // 
            this.bSearch.ForeColor = System.Drawing.Color.Black;
            this.bSearch.Location = new System.Drawing.Point(913, 17);
            this.bSearch.Name = "bSearch";
            this.bSearch.Size = new System.Drawing.Size(75, 23);
            this.bSearch.TabIndex = 0;
            this.bSearch.Text = "Cari";
            this.bSearch.UseVisualStyleBackColor = true;
            this.bSearch.Click += new System.EventHandler(this.bSearch_Click);
            // 
            // dataGUser
            // 
            this.dataGUser.AllowUserToAddRows = false;
            this.dataGUser.AllowUserToDeleteRows = false;
            this.dataGUser.AllowUserToResizeColumns = false;
            this.dataGUser.AllowUserToResizeRows = false;
            this.dataGUser.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGUser.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGUser.Location = new System.Drawing.Point(12, 66);
            this.dataGUser.MultiSelect = false;
            this.dataGUser.Name = "dataGUser";
            this.dataGUser.ReadOnly = true;
            this.dataGUser.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGUser.Size = new System.Drawing.Size(994, 397);
            this.dataGUser.TabIndex = 8;
            this.dataGUser.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGUser_CellContentClick);
            // 
            // PrintReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 477);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.dataGUser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "PrintReport";
            this.Text = "Cetak Bukti Kepemilikan Aset";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.PrintReport_FormClosed);
            this.Load += new System.EventHandler(this.PrintReport_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.PrintReport_Paint);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGUser)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tSearch;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button bSearch;
        private System.Windows.Forms.DataGridView dataGUser;
    }
}