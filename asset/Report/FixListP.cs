﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class FixListP : Form
    {
        private String id;
        private String tanggal;
        lib lib = new lib();
        public FixListP(String id, String tanggal)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
            this.tanggal = tanggal;
        }
        String searchData(String id, String tanggal)
        {
            String sql="select h.note as Keterangan, a.id_asset as \"ID Aset\", h.date_app \"Tanggal Klaim\",b.category as Kategori, c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                "from m_asset a, m_category b, m_brand c, m_location d, m_status e, t_asset f, m_employee g, t_condition h " +
                "where h.id_employee='" + id + "' and a.id_asset=h.id_asset and a.id_asset=f.id_asset and g.id_employee=f.id_employee and a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status and h.acc='1' and h.stat='2' and h.date_app between "+
                "(SELECT DATEADD(dd,-(DAY(DATEADD(mm,-1,'"+ tanggal +"'))-1),DATEADD(mm,-1,'"+ tanggal +"'))) and (SELECT DATEADD(dd,-(DAY('"+ tanggal +"')),'"+ tanggal +"'))";
           
            return sql;
        }
        private void FixList_Load(object sender, EventArgs e)
        {
            try
            {
                String sql = searchData(this.id, this.tanggal);
                showDataF(this.id, sql);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void showDataF(String id, String sql)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter(sql, conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGL.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGL.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bSearch_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearch.Text;
                String sql = searchData(this.id, this.tanggal);
                searchDataF(this.id, sql, str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void searchDataF(String id, String sql, String str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter(sql + " and a.model like '%" + str + "%' ", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGL.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGL.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FixList_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
    }
}
