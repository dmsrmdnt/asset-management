﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;
namespace asset.Class.process
{
    public class mAddAsset
    {
        lib lib = new lib();
        public Boolean cekInput(String id_employee, String id_location, String id_brand, String id_category, String pur_date, String price, String description, String model)
        {
            if (
                id_employee == ""
                || id_location == ""
                || id_brand == ""
                || id_category == ""
                || pur_date == ""
                || price == ""
                || description == ""
                || model == ""
                || id_employee == null
                || id_location == null
                || id_brand == null
                || id_category == null
                || pur_date == null
                || price == null
                || description == null
                || model == null
                )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void insert(String id_employee, String id_location, String id_brand, String id_category, String pur_date, String price, String description, String model, String serial, String asset, String userid, Boolean c, String item, String dname, byte[] dcontent, String id_status = "1")
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = null;
                myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_m_asset";
                myCommand.Parameters.AddWithValue("@id_location", id_location);
                myCommand.Parameters.AddWithValue("@id_brand", id_brand);
                myCommand.Parameters.AddWithValue("@id_category", id_category);
                myCommand.Parameters.AddWithValue("@pur_date", pur_date);
                myCommand.Parameters.AddWithValue("@price", price);
                myCommand.Parameters.AddWithValue("@description", description);
                myCommand.Parameters.AddWithValue("@model", model);
                myCommand.Parameters.AddWithValue("@serial", serial);
                myCommand.Parameters.AddWithValue("@id_status", id_status);
                myCommand.Parameters.AddWithValue("@asset", asset);
                myCommand.Parameters.AddWithValue("@userid", userid);
                myCommand.Parameters.AddWithValue("@item", item);
                myCommand.Parameters.AddWithValue("@FN", dname);
                myCommand.Parameters.AddWithValue("@FB", dcontent);
                myCommand.Parameters.AddWithValue("@id_employee", id_employee);

                myCommand.ExecuteNonQuery();
                if (c == true)
                {
                    if (cekSoftware(userid) == true)
                    {
                        myCommand = conn.CreateCommand();
                        myCommand.CommandType = CommandType.StoredProcedure;
                        myCommand.CommandText = "get_id_license_temp";
                        myCommand.Parameters.AddWithValue("@userid", userid);
                        read = myCommand.ExecuteReader();
                        while (read.Read())
                        {
                            SqlCommand myCommand2 = conn.CreateCommand();
                            myCommand2.CommandType = CommandType.StoredProcedure;
                            myCommand2.CommandText = "insert_license_soft";
                            myCommand2.Parameters.AddWithValue("@id_license", read[0].ToString());
                            myCommand2.Parameters.AddWithValue("@id_software", read[1].ToString());
                            //myCommand2.Parameters.AddWithValue("@stat", '1');
                            //myCommand2.Parameters.AddWithValue("@stat_l", '1');
                            myCommand2.ExecuteNonQuery();
                        }
                    }
                }
                deleteTemp(userid);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void deleteTemp(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_temp_soft";
                myCommand.Parameters.AddWithValue("@userid", id);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void delete(String id_software, String id_license, String userid)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_temp_soft_id";
                myCommand.Parameters.AddWithValue("@userid", userid);
                myCommand.Parameters.AddWithValue("@id_software", id_software);
                myCommand.Parameters.AddWithValue("@id_license", id_license);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean cekSoftware(String id)
        {
            int count = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_temp_soft";
                myCommand.Parameters.AddWithValue("@userid", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    count = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public String getFileName(String file)
        {
            String filename = file.Substring(Convert.ToInt32(file.LastIndexOf("\\")) + 1, file.Length - (Convert.ToInt32(file.LastIndexOf("\\")) + 1));
            return filename;
        }
        public byte[] getFileBytes(String file)
        {
            byte[] FileBytes = null;

            try
            {
                // Open file to read using file path
                FileStream FS = new FileStream(file, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                // Add filestream to binary reader
                BinaryReader BR = new BinaryReader(FS);

                // get total byte length of the file
                long allbytes = new FileInfo(file).Length;

                // read entire file into buffer
                FileBytes = BR.ReadBytes((Int32)allbytes);

                // close all instances
                FS.Close();
                FS.Dispose();
                BR.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error during File Read " + ex.ToString());
            }
            return FileBytes;
        }
    }
    public class mAddBrand
    {
        lib lib = new lib();
        public Boolean cekInput(String brand)
        {
            if (brand == "" || brand == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public String cekData(String brand)
        {
            String hasil = "";
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_m_brand";
                myCommand.Parameters.AddWithValue("@brand", brand);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = read[0].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
        public void insert(String brand)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_m_brand";
                myCommand.Parameters.AddWithValue("@brand", brand);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
    public class mAddCategory
    {
        lib lib = new lib();
        public Boolean cekInput(String category)
        {
            if (category == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public String cekData(String category)
        {
            String hasil = "";
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_m_category";
                myCommand.Parameters.AddWithValue("@category", category);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = read[0].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
        public void insert(String category)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_m_category";
                myCommand.Parameters.AddWithValue("@category", category);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
    public class mAddLocation
    {
        lib lib = new lib();
        public Boolean cekInput(String location)
        {
            if (location == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public String cekData(String location)
        {
            String hasil = "";
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_m_location";
                myCommand.Parameters.AddWithValue("@location", location);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = read[0].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
        public void insert(String location)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_m_location";
                myCommand.Parameters.AddWithValue("@location", location);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
    public class mAddPIC
    {
        lib lib = new lib();
        public Boolean cekInput(String pic, String alamat, String telp)
        {
            if (pic == "" || pic == null ||
                telp == "" || telp == null ||
                alamat == "" || alamat == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public String cekData(String pic)
        {
            String hasil = "";
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_m_PIC";
                myCommand.Parameters.AddWithValue("@pic", pic);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = read[0].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
        public void insert(String pic, String alamat, String telp)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_m_PIC";
                myCommand.Parameters.AddWithValue("@pic", pic);
                myCommand.Parameters.AddWithValue("@alamat", alamat);
                myCommand.Parameters.AddWithValue("@telp", telp);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
    public class mAddSoftware
    {
        lib lib = new lib();
        public Boolean cekInput(String id_software, String license, String exp_date)
        {
            if (
                id_software == ""
                || license == ""
                || exp_date == ""
                || id_software == null
                || license == null
                || exp_date == null
                )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public Boolean cekInput(String id_software)
        {
            if (
                id_software == ""
                )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public Boolean cekData(String id_software, String license)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_m_soft";
                myCommand.Parameters.AddWithValue("@id_software", id_software);
                myCommand.Parameters.AddWithValue("@license", license);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (hasil == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public Boolean cekData(String id_software)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_m_soft_l";
                myCommand.Parameters.AddWithValue("@id_software", id_software);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (hasil == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void insert(String id_software, String license, String exp_date, String userid)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_m_soft_l";
                myCommand.Parameters.AddWithValue("@id_software", id_software);
                myCommand.Parameters.AddWithValue("@license", license);
                myCommand.Parameters.AddWithValue("@exp_date", exp_date);
                myCommand.Parameters.AddWithValue("@userid", userid);
                myCommand.Parameters.AddWithValue("@stat_u", '1');
                myCommand.Parameters.AddWithValue("@stat_l", '1');
                myCommand.Parameters.AddWithValue("@stat", '1');
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void insert(String id_software, String userid)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_m_soft";
                myCommand.Parameters.AddWithValue("@id_software", id_software);
                myCommand.Parameters.AddWithValue("@stat_u", '1');
                myCommand.Parameters.AddWithValue("@stat_l", '0');
                myCommand.Parameters.AddWithValue("@userid", userid);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
    public class mAddSoftwareDetail
    {
        lib lib = new lib();
        public Boolean cekInput(String brand)
        {
            if (brand == "" || brand == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public String cekData(String nama)
        {
            String hasil = "";
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_m_soft_d";
                myCommand.Parameters.AddWithValue("@nama", nama);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = read[0].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
        public void insert(String nama, String userid)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_m_soft_d";
                myCommand.Parameters.AddWithValue("@nama", nama);
                myCommand.Parameters.AddWithValue("@userid", userid);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
    public class mAddUser
    {
        lib lib = new lib();
        public Boolean cekInput(String id, String nama, String status)
        {
            if (id == "" || nama == "" || status == "" || id == null || nama == null || status == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public String cekData(String email)
        {
            String hasil = "";
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_m_employee";
                myCommand.Parameters.AddWithValue("@email", email);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = read[0].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
        public void insert(String email, String nama, String dept, String status)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_m_employee";
                myCommand.Parameters.AddWithValue("@id_dept", dept);
                myCommand.Parameters.AddWithValue("@nama", nama);
                myCommand.Parameters.AddWithValue("@email", email);
                myCommand.Parameters.AddWithValue("@password", lib.encrypt("123456"));
                myCommand.Parameters.AddWithValue("@id_auth", status);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
    public class mAssetBrand
    {
        lib lib = new lib();
        public void delete(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_brand";
                myCommand.Parameters.AddWithValue("@id_brand", id);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean CekData(String id)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_brand";
                myCommand.Parameters.AddWithValue("@id_brand", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (hasil == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public class mAssetCategory
    {
        lib lib = new lib();
        public void delete(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_category";
                myCommand.Parameters.AddWithValue("@id_category", id);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean CekData(String id)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_category";
                myCommand.Parameters.AddWithValue("@id_category", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (hasil == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public class mAssetLocation
    {
        lib lib = new lib();
        public void delete(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_location";
                myCommand.Parameters.AddWithValue("@id_location", id);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean CekData(String id)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_location";
                myCommand.Parameters.AddWithValue("@id_location", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (hasil == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public class mAssetManagement
    {
        lib lib = new lib();
        public void delete(String id_asset, String id_employee)
        {
            try
            {
                SqlDataReader read = null;
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_asset";
                myCommand.Parameters.AddWithValue("@id_asset", id_asset);
                myCommand.ExecuteNonQuery();
                //Normalisasi tabel t_software
                myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "get_id_license";
                myCommand.Parameters.AddWithValue("@id_asset", id_asset);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    SqlCommand myCommand2 = conn.CreateCommand();
                    myCommand2.CommandType = CommandType.StoredProcedure;
                    myCommand2.CommandText = "update_m_license";
                    myCommand2.Parameters.AddWithValue("@id_license", read[0].ToString());
                    myCommand2.ExecuteNonQuery();
                }
                myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_t_software";
                myCommand.Parameters.AddWithValue("@id_asset", id_asset);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean CekData(String id)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "get_count_data_asset";
                myCommand.Parameters.AddWithValue("@id_asset", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil += Convert.ToInt32(read[0].ToString());
                }
                read.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (hasil == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public class mAssetPIC
    {
        lib lib = new lib();
        public void delete(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_PIC";
                myCommand.Parameters.AddWithValue("@id_pic", id);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean CekData(String id)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_PIC";
                myCommand.Parameters.AddWithValue("@id_pic", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (hasil == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public class mAssetSoftware
    {
        lib lib = new lib();
        public void insert(String id_software, String id_license, String userid)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_temp_software";
                myCommand.Parameters.AddWithValue("@id_license", id_license);
                myCommand.Parameters.AddWithValue("@id_software", id_software);
                myCommand.Parameters.AddWithValue("@userid", userid);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void delete(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_m_license";
                myCommand.Parameters.AddWithValue("@id_license", id);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean CekData(String id)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "get_count_data_license";
                myCommand.Parameters.AddWithValue("@id_license", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil += Convert.ToInt32(read[0].ToString());
                }
                read.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (hasil == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public Boolean cekTable(String id_software, String id_license, String userid)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_temp_software";
                myCommand.Parameters.AddWithValue("@id_license", id_license);
                myCommand.Parameters.AddWithValue("@id_software", id_software);
                myCommand.Parameters.AddWithValue("@userid", userid);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (hasil == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public class mSoftware
    {
        lib lib = new lib();
        public void delete(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_data_AssetSoft";
                myCommand.Parameters.AddWithValue("@id_software", id);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean CekData(String id)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_data_AssetSoft";
                myCommand.Parameters.AddWithValue("@id_software", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (hasil == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public class mClaimManagement
    {
        lib lib = new lib();
        public void updateL(String id_employee, String id_asset, String id_user, String acc = "0")
        {
            try
            {
                String date = DateTime.Now.ToString();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("update t_lose set acc='" + acc + "', date_app='" + date + "', id_user='" + id_user + "' where id_employee='" + id_employee + "' and id_asset='" + id_asset + "' and acc='2'", conn);
                myCommand.ExecuteNonQuery();
                myCommand = new SqlCommand("update m_asset set id_status='4' where id_asset='" + id_asset + "'", conn);
                myCommand.ExecuteNonQuery();
                myCommand = new SqlCommand("delete from t_softAsset where id_asset='" + id_asset + "'", conn);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void deleteL(String id_employee, String id_asset, String acc = "0")
        {
            try
            {
                String date = DateTime.Now.ToString();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("delete from t_lose where id_employee='" + id_employee + "' and id_asset='" + id_asset + "' and acc='2'", conn);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void updateB(String id_employee, String id_asset, String id_user, int status, int acc)
        {
            try
            {
                String date = DateTime.Now.ToString();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("update t_condition set acc='" + acc + "', date_app='" + date + "', stat='" + status + "', id_user='" + id_user + "' where id_employee='" + id_employee + "' and id_asset='" + id_asset + "' and acc='2'", conn);
                myCommand.ExecuteNonQuery();
                if (status == 0)
                {
                    myCommand = new SqlCommand("update m_asset set id_status='3' where id_asset='" + id_asset + "'", conn);
                    myCommand.ExecuteNonQuery();
                    myCommand = new SqlCommand("delete from t_softAsset where id_asset='" + id_asset + "'", conn);
                    myCommand.ExecuteNonQuery();
                }
                else if (status == 1)
                {
                    myCommand = new SqlCommand("update m_asset set id_status='2' where id_asset='" + id_asset + "'", conn);
                    myCommand.ExecuteNonQuery();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void deleteB(String id_employee, String id_asset, String acc = "0")
        {
            try
            {
                String date = DateTime.Now.ToString();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("delete from t_condition where id_employee='" + id_employee + "' and id_asset='" + id_asset + "' and acc='2'", conn);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void deleteF(String id_employee, String id_asset, String acc = "1")
        {
            try
            {
                String date = DateTime.Now.ToString();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("delete from t_condition where id_employee='" + id_employee + "' and id_asset='" + id_asset + "' and acc='1' and h.stat='1'", conn);
                myCommand.ExecuteNonQuery();
                myCommand = new SqlCommand("update m_asset set id_status='1' where id_asset='" + id_asset + "'", conn);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void updateF(String id_employee, String id_asset, String userid, String acc = "1")
        {
            try
            {
                String date = DateTime.Now.ToString();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("update t_condition set stat='0', acc='0' , id_user='" + userid + "' where id_employee='" + id_employee + "' and id_asset='" + id_asset + "' and acc='1' and stat='1'", conn);
                myCommand.ExecuteNonQuery();
                myCommand = new SqlCommand("update m_asset set id_status='3' where id_asset='" + id_asset + "'", conn);
                myCommand.ExecuteNonQuery();
                myCommand = new SqlCommand("delete from t_softAsset where id_asset='" + id_asset + "'", conn);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void updateFi(String id_employee, String id_asset, String userid, String acc = "1")
        {
            try
            {
                String date = DateTime.Now.ToString();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("update t_condition set stat='0', acc='0' , id_user='" + userid + "' where id_employee='" + id_employee + "' and id_asset='" + id_asset + "' and acc='1' and stat='2'", conn);
                myCommand.ExecuteNonQuery();
                myCommand = new SqlCommand("update m_asset set id_status='3' where id_asset='" + id_asset + "'", conn);
                myCommand.ExecuteNonQuery();
                myCommand = new SqlCommand("delete from t_softAsset where id_asset='" + id_asset + "'", conn);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void deleteFi(String id_employee, String id_asset, String userid, String acc = "1")
        {
            try
            {
                String date = DateTime.Now.ToString();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("update t_condition set stat='1', id_user='" + userid + "' where id_employee='" + id_employee + "' and id_asset='" + id_asset + "' and acc='1' and stat='2'", conn);
                myCommand.ExecuteNonQuery();
                myCommand = new SqlCommand("update m_asset set id_status='2' where id_asset='" + id_asset + "'", conn);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
    public class mDepartmentManagement
    {
        lib lib = new lib();
        public void update(String id_dept, String dept, String userid)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "update_m_dept";
                myCommand.Parameters.AddWithValue("@dept", dept);
                myCommand.Parameters.AddWithValue("@userid", userid);
                myCommand.Parameters.AddWithValue("@id_dept", id_dept);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean cekInput(String dept)
        {
            if (dept == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
    public class mEditAsset
    {
        lib lib = new lib();
        public void delete(String id_software, String id_license, String userid)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_temp_soft_id";
                myCommand.Parameters.AddWithValue("@userid", userid);
                myCommand.Parameters.AddWithValue("@id_software", id_software);
                myCommand.Parameters.AddWithValue("@id_license", id_license);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void update(String id_employee, String id_location, String id_brand, String id_category, String pur_date, String price, String description, String model, String serial, String id_asset, String userid, String asset, Boolean c, String dname, byte[] dcontent, String item)
        {
            try
            {
                deleteSoftware(id_asset);
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataReader read = null;
                SqlCommand myCommand;
                if (dname == "" || dname == null)
                {
                    myCommand = new SqlCommand("update m_asset set item='" + item + "',  asset='" + asset + "', id_location='" + id_location + "', id_brand='" + id_brand + "', id_category='" + id_category + "', pur_date='" + pur_date + "', description='" + description + "', serial='" + serial + "', model='" + model + "' where id_asset='" + id_asset + "'", conn);
                    myCommand.ExecuteNonQuery();
                }
                else
                {
                    myCommand = new SqlCommand("update m_asset set item='" + item + "', asset='" + asset + "', id_location='" + id_location + "', id_brand='" + id_brand + "', id_category='" + id_category + "', pur_date='" + pur_date + "', description='" + description + "', serial='" + serial + "', model='" + model + "', dname=@FN, dcontent=@FB where id_asset='" + id_asset + "'", conn);
                    myCommand.Parameters.AddWithValue("@FN", dname);
                    myCommand.Parameters.AddWithValue("@FB", dcontent);
                    myCommand.ExecuteNonQuery();
                }

                myCommand = new SqlCommand("update t_asset set id_employee='" + id_employee + "' where id_asset='" + id_asset + "'", conn);
                myCommand.ExecuteNonQuery();
                if (c == true)
                {
                    if (cekSoftware(userid) == true)
                    {
                        myCommand = new SqlCommand("select id_license, id_software from temp_software where userid='" + userid + "'", conn);
                        read = myCommand.ExecuteReader();
                        while (read.Read())
                        {
                            SqlCommand myCommand2 = new SqlCommand("insert into t_softAsset(id_asset, id_license, id_assetsoft) values('" + id_asset + "','" + read[0].ToString() + "','" + read[1].ToString() + "')", conn);
                            myCommand2.ExecuteNonQuery();
                            //myCommand2 = new SqlCommand("update m_license set stat_u='0' where id_license='" + read[0].ToString() + "' and stat_l='1'", conn);
                            //myCommand2.ExecuteNonQuery();
                        }
                    }
                }
                deleteTemp(userid);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void deleteTemp(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("delete from temp_software where userid='" + id + "'", conn);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void deleteSoftware(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("delete from t_softAsset where id_asset='" + id + "'", conn);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean cekInput(String id_employee, String id_location, String id_brand, String id_category, String pur_date, String price, String description, String model)
        {
            if (
                id_employee == ""
                || id_location == ""
                || id_brand == ""
                || id_category == ""
                || pur_date == ""
                || price == ""
                || description == ""
                || model == ""
                )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public Boolean cekSoftwareCount(String id)
        {
            int count = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select count(*) from t_software where id_asset='" + id + "' and stat='1'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    count = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public Boolean cekSoftware(String id)
        {
            int count = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select count(*) from temp_software where userid='" + id + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    count = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public String getFileName(String file)
        {
            String filename = file.Substring(Convert.ToInt32(file.LastIndexOf("\\")) + 1, file.Length - (Convert.ToInt32(file.LastIndexOf("\\")) + 1));
            return filename;
        }
        public byte[] getFileBytes(String file)
        {
            byte[] FileBytes = null;

            try
            {
                // Open file to read using file path
                FileStream FS = new FileStream(file, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                // Add filestream to binary reader
                BinaryReader BR = new BinaryReader(FS);

                // get total byte length of the file
                long allbytes = new FileInfo(file).Length;

                // read entire file into buffer
                FileBytes = BR.ReadBytes((Int32)allbytes);

                // close all instances
                FS.Close();
                FS.Dispose();
                BR.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error during File Read " + ex.ToString());
            }
            return FileBytes;
        }
    }
    public class mEditBrand
    {
        lib lib = new lib();
        public void update(String id, String brand)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("update m_brand set brand='" + brand + "' where id_brand='" + id + "'", conn);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean cekInput(String id, String brand)
        {
            if (id == "" || brand == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public Int32 cekData(String brand)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select count(*) from m_brand where brand='" + brand + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
    }
    public class mEditCategory
    {
        lib lib = new lib();
        public void update(String id, String category)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("update m_category set category='" + category + "' where id_category='" + id + "'", conn);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean cekInput(String id, String category)
        {
            if (id == "" || category == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public Int32 cekData(String category)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select count(*) from m_category where category='" + category + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
    }
    public class mEditLocation
    {
        lib lib = new lib();
        public void update(String id, String location)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("update m_location set location='" + location + "' where id_location='" + id + "'", conn);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean cekInput(String id, String location)
        {
            if (id == "" || location == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public Int32 cekData(String location)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select count(*) from m_location where location='" + location + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
    }
    public class mEditUser
    {
        lib lib = new lib();
        public void update(String id, String nik, String nama, String status)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("update m_employee set nama='" + nama + "', email='" + nik + "', id_auth='" + status + "' where id_employee='" + id + "'", conn);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean cekInput(String id, String nama, String status)
        {
            if (id == "" || nama == "" || status == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public Int32 cekData(String id)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select count(*) from m_employee where email='" + id + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
    }
    public class mFixAsset
    {
        lib lib = new lib();
        public Boolean cekInput(String tAwal, String tHarga)
        {
            if (tAwal == "" || tHarga == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public Boolean cekTanggal(DateTime awal, String id_asset, String id_employee, String stat="2", String acc="1")
        {
            DateTime akhir = new DateTime();
            SqlConnection conn = new SqlConnection(lib.param());
            SqlDataReader read = null;
            conn.Open();
            SqlCommand myCommand = new SqlCommand("select date_start from t_condition where id_asset='" + id_asset + "' and id_employee='" + id_employee + "' and stat='" + stat + "' and acc='" + acc + "'", conn);
            read = myCommand.ExecuteReader();
            while (read.Read())
            {
                akhir = Convert.ToDateTime(read[0].ToString());
            }
            conn.Close();
            if (akhir.CompareTo(awal) > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void update(String date_finish, String price, String note_r, String id_asset, String id_employee, String id_user, String acc = "0")
        {
            try
            {
                String date = DateTime.Now.ToString();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "update_fixAsset";
                myCommand.Parameters.AddWithValue("@acc", acc);
                myCommand.Parameters.AddWithValue("@date_finish", date_finish);
                myCommand.Parameters.AddWithValue("@price", price);
                myCommand.Parameters.AddWithValue("@note_r", note_r);
                myCommand.Parameters.AddWithValue("@id_user", id_user);
                myCommand.Parameters.AddWithValue("@id_employee", id_employee);
                myCommand.Parameters.AddWithValue("@id_asset", id_asset);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
    public class mRepAsset
    {
        lib lib = new lib();
        public Boolean cekInput(String tAwal, String id_pic, String file)
        {
            if (tAwal == "" || id_pic == "" || file == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public Boolean cekTanggal(DateTime awal, DateTime akhir)
        {
            if (awal.CompareTo(akhir) > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void update(String date_start, String id_pic, String id_asset, String id_employee, String id_user, String filename, byte[] filebytes, String stat = "2")
        {
            try
            {
                String date = DateTime.Now.ToString();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "update_repAsset";
                myCommand.Parameters.AddWithValue("@stat", stat);
                myCommand.Parameters.AddWithValue("@date_start", date_start);
                myCommand.Parameters.AddWithValue("@id_user", id_user);
                myCommand.Parameters.AddWithValue("@id_employee", id_employee);
                myCommand.Parameters.AddWithValue("@id_asset", id_asset);
                myCommand.Parameters.AddWithValue("@FN", filename);
                myCommand.Parameters.AddWithValue("@FB", filebytes);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
    public class mUpdateLicense
    {
        lib lib = new lib();
        public Boolean cekInput(String license, String exp)
        {
            if (license == "" || exp == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public Boolean cekData(String license, String id_license)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_t_uplicense";
                myCommand.Parameters.AddWithValue("@id_license", id_license);
                myCommand.Parameters.AddWithValue("@license", license);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (hasil == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public Boolean cekTanggal(DateTime awal, DateTime akhir)
        {
            if (awal.CompareTo(akhir) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void insert(String license, String exp_date, String id_license)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "update_license";
                myCommand.Parameters.AddWithValue("@id_license", id_license);
                myCommand.Parameters.AddWithValue("@license", license);
                myCommand.Parameters.AddWithValue("@exp_date", exp_date);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
    public class mUserManagement
    {
        lib lib = new lib();
        public void reset(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "reset_password";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                myCommand.Parameters.AddWithValue("@password", lib.encrypt("123456"));
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void delete(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_m_employee";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void updateResign(String id_employee, String id_admin)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "resign_employee";
                myCommand.Parameters.AddWithValue("@id_employee", id_employee);
                myCommand.Parameters.AddWithValue("@id_admin", id_admin);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void insertMutation(String id_employee1, String id_employee2, String id_asset, string filename, byte[] fileFileBytes) 
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_t_mutation2";
                myCommand.Parameters.AddWithValue("@id_employee1", id_employee1);
                myCommand.Parameters.AddWithValue("@id_employee2", id_employee2);
                myCommand.Parameters.AddWithValue("@id_asset", id_asset);
                myCommand.Parameters.AddWithValue("@date", DateTime.Now.ToString());
                myCommand.Parameters.AddWithValue("@stat", 0);
                myCommand.Parameters.AddWithValue("@FN", filename);
                myCommand.Parameters.AddWithValue("@FB", fileFileBytes);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void resign(String id_employee, String id_admin, string filename, byte[] fileFileBytes)
        {
            try
            {
                
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "get_id_assetR";
                myCommand.Parameters.AddWithValue("@id_employee", id_employee);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    insertMutation(id_employee, id_admin, read[0].ToString(), filename, fileFileBytes);
                }
                read.Close();
                conn.Close();
                updateResign(id_employee, id_admin);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean CekData(String id)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "get_count_data_employee";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil += Convert.ToInt32(read[0].ToString());
                }
                read.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (hasil == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public class mAddNewSoftware
    {
        lib lib = new lib();
        public Boolean ceklisensi(int id_software) 
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "get_count_lisensi_software";
                myCommand.Parameters.AddWithValue("@id_software", id_software);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil += Convert.ToInt32(read[0].ToString());
                }
                read.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (hasil == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public Boolean cek(string software, string id_category, string id_status)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "get_count_asset_software";
                myCommand.Parameters.AddWithValue("@software", software);
                myCommand.Parameters.AddWithValue("@id_category", id_category);
                myCommand.Parameters.AddWithValue("@id_status", id_status);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil += Convert.ToInt32(read[0].ToString());
                }
                read.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (hasil == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void insertSoftware(string software, string id_category, string id_status) 
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_t_asset_software";
                myCommand.Parameters.AddWithValue("@software", software);
                myCommand.Parameters.AddWithValue("@id_category", id_category);
                myCommand.Parameters.AddWithValue("@id_status", id_status);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void updateSoftware(int id_software,string software, string id_category, string id_status)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "update_t_asset_software";
                myCommand.Parameters.AddWithValue("@software", software);
                myCommand.Parameters.AddWithValue("@id_category", id_category);
                myCommand.Parameters.AddWithValue("@id_status", id_status);
                myCommand.Parameters.AddWithValue("@id_software", id_software);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
    public class mSoftwareManagement 
    {
        lib lib = new lib();
        public void delete(string id_software)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_t_asset_software";
                myCommand.Parameters.AddWithValue("@id_software", id_software);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean CekData(string id_software)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "get_count_software";
                myCommand.Parameters.AddWithValue("@id_software", id_software);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil += Convert.ToInt32(read[0].ToString());
                }
                read.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (hasil == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public class mSoftwareLicenseManagement 
    {
        lib lib = new lib();
        public void delete(string id_license)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_t_license_software";
                myCommand.Parameters.AddWithValue("@id_license", id_license);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean CekData(string id_license)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "get_count_software_license";
                myCommand.Parameters.AddWithValue("@id_license", id_license);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil += Convert.ToInt32(read[0].ToString());
                }
                read.Close();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (hasil == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    public class mAddSoftwareLicense 
    {
        lib lib = new lib();
        public Boolean empty(string [] input) 
        {
            Boolean c = true;
            for (int i = 0; i < input.Length; i++) {
                if (input[i] == null || input[i] == "")
                {
                    c = false;
                    break;
                }
            }
            return c;
        }
        public void insertSoftwareLicense(string id_assetSoft, string license, string exp_date)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_t_license_software";
                myCommand.Parameters.AddWithValue("@id_assetSoft", id_assetSoft);
                myCommand.Parameters.AddWithValue("@license", license);
                myCommand.Parameters.AddWithValue("@exp_date", exp_date);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void updateSoftwareLicense(string id_assetSoft, string license, string exp_date, int id_license)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "update_t_license_software";
                myCommand.Parameters.AddWithValue("@id_assetSoft", id_assetSoft);
                myCommand.Parameters.AddWithValue("@license", license);
                myCommand.Parameters.AddWithValue("@exp_date", exp_date);
                myCommand.Parameters.AddWithValue("@id_license", id_license);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void updateLicense(string id_assetSoft, string license, string exp_date, int id_license)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "update_license_software";
                myCommand.Parameters.AddWithValue("@id_assetSoft", id_assetSoft);
                myCommand.Parameters.AddWithValue("@license", license);
                myCommand.Parameters.AddWithValue("@exp_date", exp_date);
                myCommand.Parameters.AddWithValue("@id_license", id_license);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
    public class mRequestAsset 
    {
        lib lib = new lib();
        public Boolean cekInput(String [] input) 
        {
            Boolean cek=true;
            for (int i = 0; i < input.Length; i++) {
                if (input[i] == "" || input[i] == null) {
                    cek = false;
                    break;
                }
            }
            return cek;
        }
        public void insertRequestAsset(String [] input) 
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_requestAsset";
                myCommand.Parameters.AddWithValue("@brand", input[0]);
                myCommand.Parameters.AddWithValue("@description", input[1]);
                myCommand.Parameters.AddWithValue("@specs", input[2]);
                myCommand.Parameters.AddWithValue("@id_category", input[3]);
                myCommand.Parameters.AddWithValue("@price", input[4]);
                myCommand.Parameters.AddWithValue("@id_employee", input[5]);
                myCommand.Parameters.AddWithValue("@date_req", DateTime.Now.ToString());
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
    public class mRequestManagement
    {
        lib lib = new lib();
        public void delete(String id_request)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_requestAsset";
                myCommand.Parameters.AddWithValue("@id_request", id_request);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void update(String id_request,String userid)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "update_requestAsset";
                myCommand.Parameters.AddWithValue("@id_request", id_request);
                myCommand.Parameters.AddWithValue("@userid", userid);
                myCommand.Parameters.AddWithValue("@date_app", DateTime.Now.ToString());
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }

}
