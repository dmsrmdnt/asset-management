﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;
using System.Configuration;

namespace asset.Class
{
    class login
    {
        public class mlogin
        {
            lib lib = new lib();
            public Boolean cekInput(String username, String password)
            {
                if (username == "" || password == "" || username == null || password == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            public String cekData(String username, String password)
            {
                String hasil = "";
                try
                {
                    password = lib.encrypt(password);
                    SqlConnection conn = new SqlConnection(lib.param());
                    SqlDataReader read = null;
                    conn.Open();
                    SqlCommand myCommand = conn.CreateCommand();
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.CommandText = "cek_data_login";
                    myCommand.Parameters.AddWithValue("@username", username);
                    myCommand.Parameters.AddWithValue("@password", password);
                    read = myCommand.ExecuteReader();
                    while (read.Read())
                    {
                        hasil = read[0].ToString();
                    }
                    conn.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Gagal terhubung ke database server", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return hasil;
            }
            public String getID(String username, String password)
            {
                String hasil = "";
                try
                {
                    password = lib.encrypt(password);
                    SqlConnection conn = new SqlConnection(lib.param());
                    SqlDataReader read = null;
                    conn.Open();
                    SqlCommand myCommand = conn.CreateCommand();
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.CommandText = "get_id_employee";
                    myCommand.Parameters.AddWithValue("@username", username);
                    myCommand.Parameters.AddWithValue("@password", password);
                    read = myCommand.ExecuteReader();
                    while (read.Read())
                    {
                        hasil = read[0].ToString();
                    }
                    conn.Close();
                }
                catch (Exception)
                {
                    MessageBox.Show("Gagal terhubung ke database server", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return hasil;
            }
            public void saveUsrPasswd(String username,String password)
            {
                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings.Remove("UsrPasswd");
                config.AppSettings.Settings.Add("UsrPasswd", "username=" + username + ";password=" + password + ";");
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
            }
            public void unsaveUsrPasswd()
            {
                System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings.Remove("UsrPasswd");
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
            }
        }
    }
}
