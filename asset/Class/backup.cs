﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace asset.Class.backupDB
{
    class backup
    {
        lib lib = new lib();
        public static Boolean cek;
        public String separator(String S)
        {
            if ((S.Length - 1)==S.LastIndexOf("\\")) {
                S.Replace("\\","");
            }
            return S;
        }
        public void backupdb(String file) {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = null;
                myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "backup_database";
                myCommand.Parameters.AddWithValue("@file", file);
                myCommand.ExecuteNonQuery();
                conn.Close();
                asset.Class.backupDB.backup.cek = true;
            }
            catch (Exception)
            {
                asset.Class.backupDB.backup.cek = false;
            }
        }
        public void restoredb(String file) 
        {
            try 
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = null;
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "restore_database";
                myCommand.Parameters.AddWithValue("@file", file);
                myCommand.ExecuteNonQuery();
                conn.Close();
                asset.Class.backupDB.backup.cek = true;
            }
            catch(Exception)
            {
                asset.Class.backupDB.backup.cek = false;
            }
        }
    }
}
