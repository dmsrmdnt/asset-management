﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace asset.Class.mutasi
{
    public class mSend
    {
        lib lib = new lib();
        public int cekTCondition(String idAsset)
        {
            int n = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_t_condition";
                myCommand.Parameters.AddWithValue("@id_asset", idAsset);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    n = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return n;
        }
        public int cekCondition(String idAsset)
        {
            int n = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "get_id_status";
                myCommand.Parameters.AddWithValue("@id_asset", idAsset);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    n = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return n;
        }
        public int cekAssetM(String idAsset)
        {
            int n = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_t_mutation";
                myCommand.Parameters.AddWithValue("@id_asset", idAsset);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    n = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return n;

        }
        public int cekAssetL(String idAsset)
        {
            int n = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_t_lose";
                myCommand.Parameters.AddWithValue("@id_asset", idAsset);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    n = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return n;

        }
        public int cekAssetB(String idAsset)
        {
            int n = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_t_conditionB";
                myCommand.Parameters.AddWithValue("@id_asset", idAsset);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    n = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return n;

        }
        public Boolean cekInput(String id, String id_asset, String tfile)
        {
            if (id == "" || id_asset == "" || tfile == "" || id == null || id_asset == null || tfile == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void insert(String id_employee1, String id_employee2, String id_asset, String dname, byte[] dcontent, String status = "1")
        {
            try
            {
                String date = DateTime.Now.ToString();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_t_mutation";
                myCommand.Parameters.AddWithValue("@id_asset", id_asset);
                myCommand.Parameters.AddWithValue("@id_employee1", id_employee1);
                myCommand.Parameters.AddWithValue("@id_employee2", id_employee2);
                myCommand.Parameters.AddWithValue("@date_req", date);
                myCommand.Parameters.AddWithValue("@stat", status);
                myCommand.Parameters.AddWithValue("@FN", dname);
                myCommand.Parameters.AddWithValue("@FB", dcontent);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
    public class mPendding
    {
        lib lib = new lib();
        public void delete(String id_employee1, String id_asset, String status = "1")
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_t_mutationP";
                myCommand.Parameters.AddWithValue("@id_asset", id_asset);
                myCommand.Parameters.AddWithValue("@id_employee1", id_employee1);
                myCommand.Parameters.AddWithValue("@stat", status);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
    public class mRequest
    {
        lib lib = new lib();
        public void update(String id_employee2, String id_asset, String status = "0")
        {
            try
            {
                String date = DateTime.Now.ToString();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "update_t_mutationP";
                myCommand.Parameters.AddWithValue("@id_asset", id_asset);
                myCommand.Parameters.AddWithValue("@stat", status);
                myCommand.Parameters.AddWithValue("@date_app", date);
                myCommand.Parameters.AddWithValue("@id_employee2", id_employee2);
                myCommand.ExecuteNonQuery();
                //updateLicenseStatus(id_asset, 1);
                //updateSoftware(id_asset);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void updateLicenseStatus(String id_asset, int status)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "update_licenseM";
                myCommand.Parameters.AddWithValue("@id_asset", id_asset);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    SqlCommand myCommand2 = conn.CreateCommand();
                    myCommand2.CommandType = CommandType.StoredProcedure;
                    myCommand2.CommandText = "update_licenseS";
                    myCommand2.Parameters.AddWithValue("@id_license", read[0].ToString());
                    myCommand2.Parameters.AddWithValue("@stat_u", id_asset);
                    myCommand2.ExecuteNonQuery();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void updateSoftware(String id_asset)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "update_software_mutation";
                myCommand.Parameters.AddWithValue("@id_asset", id_asset);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void delete(String id_employee2, String id_asset, String status = "1")
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_t_mutation";
                myCommand.Parameters.AddWithValue("@id_asset", id_asset);
                myCommand.Parameters.AddWithValue("@stat", status);
                myCommand.Parameters.AddWithValue("@id_employee2", id_employee2);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }

    public class mSendBreak
    {
        lib lib = new lib();
        public int cekTCondition(String idAsset)
        {
            int n = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_t_condition";
                myCommand.Parameters.AddWithValue("@id_asset", idAsset);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    n = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return n;
        }
        public int cekCondition(String idAsset)
        {
            int n = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "get_id_status";
                myCommand.Parameters.AddWithValue("@id_asset", idAsset);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    n = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return n;
        }
        public int cekAssetM(String idAsset)
        {
            int n = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_t_mutation";
                myCommand.Parameters.AddWithValue("@id_asset", idAsset);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    n = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return n;

        }
        public int cekAssetL(String idAsset)
        {
            int n = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_t_lose";
                myCommand.Parameters.AddWithValue("@id_asset", idAsset);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    n = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return n;

        }
        public int cekAssetB(String idAsset)
        {
            int n = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_t_conditionB";
                myCommand.Parameters.AddWithValue("@id_asset", idAsset);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    n = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return n;

        }
        public Boolean cekInput(String id, String id_asset, String file)
        {
            if (id == "" || id_asset == "" || file == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public void insertL(String id_employee, String id_asset, String note, String dname, byte [] dcontent, String status = "1", String acc = "2")
        {
            try
            {
                String date = DateTime.Now.ToString();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_t_lost";
                myCommand.Parameters.AddWithValue("@id_asset", id_asset);
                myCommand.Parameters.AddWithValue("@id_employee", id_employee);
                myCommand.Parameters.AddWithValue("@stat", status);
                myCommand.Parameters.AddWithValue("@note", note);
                myCommand.Parameters.AddWithValue("@date_req", date);
                myCommand.Parameters.AddWithValue("@acc", acc);
                myCommand.Parameters.AddWithValue("@FN", dname);
                myCommand.Parameters.AddWithValue("@FB", dcontent);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void insertB(String id_employee, String id_asset, String note, String dname, byte[] dcontent, String status = "3", String acc = "2")
        {
            try
            {
                String date = DateTime.Now.ToString();
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_t_condition";
                myCommand.Parameters.AddWithValue("@id_asset", id_asset);
                myCommand.Parameters.AddWithValue("@id_employee", id_employee);
                myCommand.Parameters.AddWithValue("@stat", status);
                myCommand.Parameters.AddWithValue("@note", note);
                myCommand.Parameters.AddWithValue("@date_req", date);
                myCommand.Parameters.AddWithValue("@acc", acc);
                myCommand.Parameters.AddWithValue("@FN", dname);
                myCommand.Parameters.AddWithValue("@FB", dcontent);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void deleteL(String id_employee, String id_asset, String acc = "2")
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_t_lose";
                myCommand.Parameters.AddWithValue("@id_asset", id_asset);
                myCommand.Parameters.AddWithValue("@id_employee", id_employee);
                myCommand.Parameters.AddWithValue("@acc", acc);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void deleteB(String id_employee, String id_asset, String acc = "2")
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "delete_t_condition";
                myCommand.Parameters.AddWithValue("@id_asset", id_asset);
                myCommand.Parameters.AddWithValue("@id_employee", id_employee);
                myCommand.Parameters.AddWithValue("@acc", acc);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
