﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Drawing.Drawing2D;
using System.Net.Mail;
using System.Data.OleDb;

namespace asset.Class.Excel
{
    class UploadMaster
    {
        lib lib = new lib();
        public static Boolean status =true;
        public void truncateTable() 
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = null;
                myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "truncate_all_table";
                myCommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void readExcelEmployee(String resource)
        {
            try
            {
                OleDbConnection con = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + resource + ";Extended Properties='Excel 8.0;HDR=Yes;'");
                OleDbDataAdapter da = new OleDbDataAdapter("select * from [Sheet1$]", con);
                DataSet ds = new DataSet();
                da.Fill(ds);
                DataTable t = new DataTable();
                da.Fill(t);


                //input user
                List<String> user = new List<String>();
                for (int i = 0; i < t.Rows.Count; i++)
                {
                    String[] temp = user.ToArray();
                    if (!temp.Contains(t.Rows[i][1].ToString().Trim()))
                    {
                        insertUser(t.Rows[i][1].ToString().Trim(), t.Rows[i][0].ToString().Trim(), "1", "2");
                        user.Add(t.Rows[i][1].ToString().Trim());
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                asset.Class.Excel.UploadMaster.status = false;
            }
        }
        public void readExcelAsset(String resource)
        {
            int count = 0;
            try
            {
                OleDbConnection con = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source="+resource+";Extended Properties='Excel 8.0;HDR=Yes;'");
                OleDbDataAdapter da = new OleDbDataAdapter("select * from [Sheet1$]", con);
                DataSet ds = new DataSet();
                da.Fill(ds);
                DataTable t = new DataTable();
                da.Fill(t);

                //input category
                List<String> category = new List<String>();
                for (int i = 0; i < t.Rows.Count; i++)
                {
                    String[] temp = category.ToArray();
                    if (!temp.Contains(t.Rows[i][2].ToString().Trim()))
                    {
                        insertCategory(t.Rows[i][2].ToString().Trim());
                        category.Add(t.Rows[i][2].ToString().Trim());
                    }
                }

                //input brand
                List<String> brand = new List<String>();
                for (int i = 0; i < t.Rows.Count; i++)
                {
                    String[] temp = brand.ToArray();
                    if (!temp.Contains(t.Rows[i][17].ToString().Trim()))
                    {
                        insertBrand(t.Rows[i][17].ToString().Trim());
                        brand.Add(t.Rows[i][17].ToString().Trim());
                    }
                }

                //input location
                List<String> location = new List<String>();
                for (int i = 0; i < t.Rows.Count; i++)
                {
                    String[] temp = location.ToArray();
                    if (!temp.Contains(t.Rows[i][4].ToString().Trim()))
                    {
                        insertLocation(t.Rows[i][4].ToString().Trim());
                        location.Add(t.Rows[i][4].ToString().Trim());
                    }
                }

                //input asset
                List<String> asset = new List<String>();
                for (int i = 0; i < t.Rows.Count; i++)
                {
                    count++;
                    String id_employee = getidEmployee(t.Rows[i][5].ToString().Trim());
                    String id_location = getidLocation(t.Rows[i][4].ToString().Trim());
                    String id_brand = getidBrand(t.Rows[i][17].ToString().Trim());
                    String id_category = getidCategory(t.Rows[i][2].ToString().Trim());
                    String pur_date = getidPurDate(t.Rows[i][9].ToString().Trim());
                    String price = "0";
                    String description = t.Rows[i][6].ToString().Trim();
                    String model = t.Rows[i][18].ToString().Trim();
                    String aset = t.Rows[i][9].ToString().Trim();
                    String serial = t.Rows[i][19].ToString().Trim();
                    String item = t.Rows[i][1].ToString().Trim();
                    String status = t.Rows[i][31].ToString().Trim();
                    String capacity = "-";
                    String filename = null;
                    byte[] filebytes = null;
                    insertAsset(item, capacity, id_employee, id_location, id_brand, id_category, pur_date, price, description, model, serial, aset, "1", true, filename, filebytes, status);
                    if (Convert.ToInt32(t.Rows[i][31].ToString().Trim()) == 3)
                    {
                        //rusak
                        String note_r = t.Rows[i][30].ToString().Trim();
                        String stat = "0";
                        String acc = "0";
                        String userid = "1";

                        insertAssetBroken(id_employee, note_r, stat, acc, userid);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                asset.Class.Excel.UploadMaster.status = false;
            }
        }
        private void getPurDate()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = null;
                myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "update_pur_date";
                myCommand.Parameters.AddWithValue("@date", "2003-01-01");
                myCommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void insertAssetBroken(String id_employee, String note_r, String stat, String acc, String userid)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = null;
                myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_t_condition2";
                myCommand.Parameters.AddWithValue("@id_employee", id_employee);
                myCommand.Parameters.AddWithValue("@note_r", note_r);
                myCommand.Parameters.AddWithValue("@stat", stat);
                myCommand.Parameters.AddWithValue("@acc", acc);
                myCommand.Parameters.AddWithValue("@userid", userid);
                myCommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private String getidEmployee(String str)
        {
            String hasil = "";
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select id_employee from m_employee where nama='" + str + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = read[0].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
        private String getidLocation(String str)
        {
            String hasil = "";
            try
            {

                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select id_location from m_location where location='" + str + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = read[0].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
        private String getidBrand(String str)
        {
            String hasil = "";
            try
            {

                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select id_brand from m_brand where brand='" + str + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = read[0].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
        private String getidCategory(String str)
        {
            String hasil = "";
            try
            {

                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select id_category from m_category where category='" + str + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = read[0].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
        private String getidPurDate(String str)
        {
            String hasil = null;
            if (!str.Equals("-") && !str.Equals(""))
            {
                hasil = (str.Substring(str.LastIndexOf("/") + 1).Trim()) +"/01/01";
            }
            else {
                hasil = "2003/01/01";
            }
            return hasil;
        }
        private void insertAsset(String item, String capacity, String id_employee, String id_location, String id_brand, String id_category, String pur_date, String price, String description, String model, String serial, String asset, String userid, Boolean c, String dname, byte[] dcontent, String id_status)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = null;
                myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_m_asset2";
                myCommand.Parameters.AddWithValue("@item", item);
                myCommand.Parameters.AddWithValue("@capacity", capacity);
                myCommand.Parameters.AddWithValue("@id_location", id_location);
                myCommand.Parameters.AddWithValue("@id_brand", id_brand);
                myCommand.Parameters.AddWithValue("@id_category", id_category);
                myCommand.Parameters.AddWithValue("@pur_date", pur_date);
                myCommand.Parameters.AddWithValue("@price", price);
                myCommand.Parameters.AddWithValue("@description", description);
                myCommand.Parameters.AddWithValue("@model", model);
                myCommand.Parameters.AddWithValue("@serial", serial);
                myCommand.Parameters.AddWithValue("@id_status", id_status);
                myCommand.Parameters.AddWithValue("@asset", asset);
                myCommand.Parameters.AddWithValue("@userid", userid);
                myCommand.Parameters.AddWithValue("@FN", dname);
                myCommand.Parameters.AddWithValue("@FB", dcontent);
                myCommand.Parameters.AddWithValue("@id_employee", id_employee);

                myCommand.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void insertUser(String email, String nama, String dept, String status)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_m_employee";
                myCommand.Parameters.AddWithValue("@id_dept", dept);
                myCommand.Parameters.AddWithValue("@nama", nama);
                myCommand.Parameters.AddWithValue("@email", email);
                myCommand.Parameters.AddWithValue("@password", lib.encrypt("123456"));
                myCommand.Parameters.AddWithValue("@id_auth", status);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void insertCategory(String category)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_m_category";
                myCommand.Parameters.AddWithValue("@category", category);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void insertBrand(String brand)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_m_brand";
                myCommand.Parameters.AddWithValue("@brand", brand);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void insertLocation(String location)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "insert_m_location";
                myCommand.Parameters.AddWithValue("@location", location);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
