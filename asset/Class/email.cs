﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Drawing.Drawing2D;
using System.Net.Mail;
using System.Data.OleDb;

namespace asset.Class.Email
{
    class email
    {
        public static Boolean cek;
        lib lib = new lib();
        private void mail(String mail, String subject, String to, String from = "noreply.amgis@wilmar.co.id")
        {
            try
            {
                String SMTP = "10.1.1.4";
                String username = "rizki.priyo";
                String password = "123456";
                SmtpClient client = new SmtpClient(SMTP);
                client.Credentials = new System.Net.NetworkCredential(username, password);
                MailMessage message = new MailMessage(from, to);
                message.Body = mail;
                message.Subject = subject;
                if ("25" != null)
                    client.Port = System.Convert.ToInt32("25");
                client.Send(message);
                asset.Class.Email.email.cek = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot send message: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                asset.Class.Email.email.cek = false;
            }
        }
        private void mailWithAttachment(String mail, String subject, String to, String [] file, String from = "noreply.amgis@wilmar.co.id")
        {
            try
            {
                //String SMTP = "10.1.1.2";
                //String username = "gisreport";
                //String password = "v1xl5";
                String SMTP = "10.1.1.4";
                String username = "rizki.priyo";
                String password = "123456";
                SmtpClient client = new SmtpClient(SMTP);
                client.Credentials = new System.Net.NetworkCredential(username, password);
                MailMessage message = new MailMessage(from, to);
                message.Body = mail;
                message.Subject = subject;

                System.Net.Mail.Attachment attachment;
                for (int i = 0; i < file.Length; i++)
                {
                    attachment = new System.Net.Mail.Attachment(file[i]);
                    message.Attachments.Add(attachment);
                }

                if ("25" != null)
                    client.Port = System.Convert.ToInt32("25");
                client.Send(message);
                asset.Class.Email.email.cek = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot send message: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                asset.Class.Email.email.cek = false;
            }
        }
        private String getDetailEmployee(String id) 
        {
            String detail = null;
            String nama = null;
            String email = null;
            String dept = null;
            SqlConnection conn = new SqlConnection(lib.param());
            conn.Open();
            SqlCommand sql = conn.CreateCommand();
            sql.CommandType = CommandType.StoredProcedure;
            sql.CommandText = "show_data_owner";
            sql.Parameters.AddWithValue("@id_employee", id);
            SqlDataAdapter data = new SqlDataAdapter();
            DataTable dt = new DataTable();
            dt.Clear();
            data.SelectCommand = sql;
            data.Fill(dt);
            conn.Close();
            if (dt.Rows.Count > 0)
            {
                nama = dt.Rows[0]["nama"].ToString();
                email = dt.Rows[0]["email"].ToString();
                dept = dt.Rows[0]["dept"].ToString();
            }
            detail = "Detil Informasi Karyawan\n" +
                "Nama : " + nama + "\n" +
                "Email : " + email + "\n" +
                "Department : " + dept + "\n\n";
                return detail;
        }
        private String getDetailEmailEmployee(String id)
        {
            String email = null;
            SqlConnection conn = new SqlConnection(lib.param());
            conn.Open();
            SqlCommand sql = conn.CreateCommand();
            sql.CommandType = CommandType.StoredProcedure;
            sql.CommandText = "show_data_owner";
            sql.Parameters.AddWithValue("@id_employee", id);
            SqlDataAdapter data = new SqlDataAdapter();
            DataTable dt = new DataTable();
            dt.Clear();
            data.SelectCommand = sql;
            data.Fill(dt);
            conn.Close();
            if (dt.Rows.Count > 0)
            {
                email = dt.Rows[0]["email"].ToString();
            }
            return email;
        }
        private String getListAsset(String id)
        {
            String detail = null;
            SqlConnection conn = new SqlConnection(lib.param());
            conn.Open();
            SqlCommand sql = conn.CreateCommand();
            sql.CommandType = CommandType.StoredProcedure;
            sql.CommandText = "print_report";
            sql.Parameters.AddWithValue("@id_employee", id);
            SqlDataAdapter data = new SqlDataAdapter();
            DataTable dt = new DataTable();
            dt.Clear();
            data.SelectCommand = sql;
            data.Fill(dt);
            conn.Close();
            for (int i = 0; i < dt.Rows.Count; i++) 
            {
                detail += (i+1) + ". " + dt.Rows[i]["asset"].ToString() + " (" + dt.Rows[i]["category"].ToString() + "/" + dt.Rows[i]["brand"].ToString() + "/" + dt.Rows[i]["model"].ToString() + "/" + dt.Rows[i]["serial"].ToString() + ")\n";
            }
            return detail;
        }
        private String getDetailAsset(String id)
        {
            String detail = null;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_asset_detail";
                myCommand.Parameters.AddWithValue("@id_asset", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    detail +=
                    "Nomor Aset : " + read["asset"].ToString() + "\n" +
                    "Item : " + read["item"].ToString() + "\n" +
                    "Kategori : " + read["category"].ToString() + "\n" +
                    "Brand : " + read["brand"].ToString() + "\n" +
                    "Model : " + read["model"].ToString() + "\n" +
                    "Lokasi : " + read["location"].ToString() + "\n" +
                    "Nomor Serial : " + read["serial"].ToString() + "\n" +
                    "Tanggal Pembelian : " + read["pur_date"].ToString() + "\n";
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return detail;
        }
        private DataTable getUserAdmin() 
        {
            SqlConnection conn = new SqlConnection(lib.param());
            conn.Open();
            SqlCommand sql = conn.CreateCommand();
            sql.CommandType = CommandType.StoredProcedure;
            sql.CommandText = "get_useradmin";
            SqlDataAdapter data = new SqlDataAdapter();
            DataTable dt = new DataTable();
            dt.Clear();
            data.SelectCommand = sql;
            data.Fill(dt);
            conn.Close();
            return dt;
        }
        private String getSubject(int i) 
        {
            String subject = null;
            if (i == 0) 
            {
                //kehilangan
                subject = "Laporan Kehilangan Aset";
            }
            else if (i == 1)
            {
                //kerusakan
                subject = "Laporan Kerusakan Aset";
            }
            else if (i == 2)
            {
                //laporan
                subject = "Laporan Kepemilikan Aset";
            }
            else if (i == 3)
            {
                //laporan
                subject = "Konfirmasi Laporan Kerusakan Aset";
            }
            else if (i == 4)
            {
                //laporan
                subject = "Konfirmasi Laporan Kehilangan Aset";
            }
            else if (i == 5)
            {
                //mutasi
                subject = "Permintaan Mutasi Aset";
            }
            else if (i == 6)
            {
                //mutasi
                subject = "Konfirmasi Permintaan Mutasi Aset";
            }
            else if (i == 7)
            {
                //penambahan aset
                subject = "Laporan Penambahan Aset";
            }
            else if (i == 8)
            {
                //penambahan aset
                subject = "Laporan Penghapusan Aset";
            }
            else if (i == 9)
            {
                //request aset baru
                subject = "Permohonan Aset Baru";
            }
            return subject;
        }
        private String getStatus(int i)
        {
            String status = null;
            if (i == 0) 
            {
                status= "diterima";
            }
            else if (i == 1) 
            {
                status= "ditolak";
            }
            else if (i == 2)
            {
                status = "dibatalkan";
            }
            else if (i == 3)
            {
                status = "penambahan";
            }
            else if (i == 4)
            {
                status = "penghapusan";
            }
            return status;
        }
        private String getfooter() 
        {
            return "Note: Ini adalah email otomatis dari sistem, silahkan anda login ke dalam aplikasi Asset Management GIS";
        }
        public void mailClaim(String id_employee, String id_asset, String desc, int stat, String[] file) 
        {
            String message = null;
            message =
                "----------------------------------------------------------------\n" +
                getSubject(stat) + "\n" +
                "----------------------------------------------------------------\n" +
                "Tanggal : " + DateTime.Now.ToString() + "\n\n" +
                getDetailEmployee(id_employee) +
                "Daftar Aset yang dilaporkan :\n" +
                getDetailAsset(id_asset) +
                "Alasan : "+desc+"\n"+
                "\n\n" + getfooter();
            DataTable dt = new DataTable();
            dt = getUserAdmin();
            for (int i = 0; i < dt.Rows.Count; i++) 
            {
                String to = getDetailEmailEmployee(dt.Rows[i]["id_employee"].ToString());
                mailWithAttachment(message, getSubject(stat), to, file);
                //mail(message, getSubject(stat), to);
            }
        }
        public void mailRequest(String [] data, int stat)
        {
            String message = null;
            message =
                "----------------------------------------------------------------\n" +
                getSubject(stat) + "\n" +
                "----------------------------------------------------------------\n" +
                "Tanggal : " + DateTime.Now.ToString() + "\n\n" +
                getDetailEmployee(data[5]) +
                "Detil aset yang diajukan :\n" +
                    "Kategori : " + data[6] + "\n" +
                    "Brand : " + data[0] + "\n" +
                    "Estimasi Harga : " + data[4] + "\n" +
                    "Keperluan : " + data[1] + "\n" +
                    "Specs : \n" + data[2] + "\n" +
                "\n\n" + getfooter();
            DataTable dt = new DataTable();
            dt = getUserAdmin();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                String to = getDetailEmailEmployee(dt.Rows[i]["id_employee"].ToString());
                mail(message, getSubject(stat), to);
                //mail(message, getSubject(stat), to);
            }
        }
        public void mailClaimResponse(String id_employee, String id_asset, int stat, int indexStatus) 
        {
            String message = null;
            message =
                "----------------------------------------------------------------\n" +
                getSubject(stat) + "\n" +
                "----------------------------------------------------------------\n" +
                "Tanggal : " + DateTime.Now.ToString() + "\n\n" +
                "Laporan kerusakan aset anda telah " +getStatus(indexStatus)+" dengan detil aset :\n"+
                getDetailAsset(id_asset) +
                "\n\n"+getfooter();
            String to = getDetailEmailEmployee(id_employee);
            mail(message, getSubject(stat), to);
        }
        public void mailDeleteAsset(String id_employee, String id_asset, int stat, int indexStatus)
        {
            String message = null;
            message =
                "----------------------------------------------------------------\n" +
                getSubject(stat) + "\n" +
                "----------------------------------------------------------------\n" +
                "Tanggal : " + DateTime.Now.ToString() + "\n\n" +
                "Laporan " + getStatus(indexStatus) + ", dengan detil aset :\n" +
                getDetailAsset(id_asset) +
                "\n\n" + getfooter();
            String to = getDetailEmailEmployee(id_employee);
            mail(message, getSubject(stat), to);
        }
        public void mailAddAsset(String id_employee, String [] assetInfo, int stat, int indexStatus)
        {
            String message = null;
            message =
                "----------------------------------------------------------------\n" +
                getSubject(stat) + "\n" +
                "----------------------------------------------------------------\n" +
                "Tanggal : " + DateTime.Now.ToString() + "\n\n" +
                "Laporan " + getStatus(indexStatus) + ", dengan detil aset :\n" +
                    "Nomor Aset : " + assetInfo[0].ToString() + "\n" +
                    "Item : " + assetInfo[1].ToString() + "\n" +
                    "Kategori : " + assetInfo[2].ToString() + "\n" +
                    "Brand : " + assetInfo[3].ToString() + "\n" +
                    "Model : " + assetInfo[4].ToString() + "\n" +
                    "Lokasi : " + assetInfo[5].ToString() + "\n" +
                    "Nomor Serial : " + assetInfo[6].ToString() + "\n" +
                    "Tanggal Pembelian : " + assetInfo[7].ToString() +
                "\n\n" + getfooter();
            String to = getDetailEmailEmployee(id_employee);
            mail(message, getSubject(stat), to);
        }
        public void mailMutation(String id_employee1,String id_employee2, String id_asset, int stat, String [] file)
        {
            String message = null;
            message =
                "----------------------------------------------------------------\n" +
                getSubject(stat) + "\n" +
                "----------------------------------------------------------------\n" +
                "Tanggal : " + DateTime.Now.ToString() + "\n\n" +
                getDetailEmployee(id_employee1) +
                "Daftar Aset yang dimutasikan :\n" +
                getDetailAsset(id_asset) +
                "\n\n" + getfooter();
            String to = getDetailEmailEmployee(id_employee2);
            mailWithAttachment(message, getSubject(stat), to, file);
        }
        public void mailMutationResponse(String id_employee, String id_asset, int stat, int indexStatus)
        {
            String message = null;
            message =
                "----------------------------------------------------------------\n" +
                getSubject(stat) + "\n" +
                "----------------------------------------------------------------\n" +
                "Tanggal : " + DateTime.Now.ToString() + "\n\n" +
                "Laporan kerusakan aset anda telah " + getStatus(indexStatus) + " dengan detil aset :\n" +
                getDetailAsset(id_asset) +
                "\n\n" + getfooter();
            String to = getDetailEmailEmployee(id_employee);
            mail(message, getSubject(stat), to);
        }
        public void mailReport(String id_employee, int stat, String [] file)
        {
            String message = null;
            message =
                "----------------------------------------------------------------\n" +
                getSubject(stat) + "\n" +
                "----------------------------------------------------------------\n" +
                "Tanggal : " + DateTime.Now.ToString() + "\n\n" +
                getDetailEmployee(id_employee) +
                "Daftar Aset yang dimiliki :\n" +
                getListAsset(id_employee) +
                "\n*Nomor Aset (Kategori/Brand/Model/Nomor Serial)\n\n" + getfooter();
            String to = getDetailEmailEmployee(id_employee);
            mailWithAttachment(message, getSubject(stat), to, file);
        }
    }
}
