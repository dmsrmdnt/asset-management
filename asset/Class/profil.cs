﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace asset.Class.profil
{
    public class mProfilManagement
    {
        lib lib = new lib();
        public void updatePassword(String id, String password)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "update_password";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                myCommand.Parameters.AddWithValue("@password", lib.encrypt(password));
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void update(String id, String nik, String nama)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "update_profil";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                myCommand.Parameters.AddWithValue("@email", nik);
                myCommand.Parameters.AddWithValue("@nama", nama);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean cekInputPassword(String p1, String p2, String p3)
        {
            if (p1 == "" || p2 == "" || p3 == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public Boolean cekInput(String id, String nama)
        {
            if (id == "" || nama == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public Int32 cekData(String id)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_m_employee";
                myCommand.Parameters.AddWithValue("@email", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
        public Int32 cekDataPassword(String id, String password)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "cek_data_employee";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                myCommand.Parameters.AddWithValue("@password", lib.encrypt(password));
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
    }
}
