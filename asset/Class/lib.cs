﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.IO;
using System.Diagnostics;
using System.Xml;
using System.Drawing.Drawing2D;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Configuration;

namespace asset
{
    public class ExcelWriter
    {
        private Stream stream;
        private BinaryWriter writer;

        private ushort[] clBegin = { 0x0809, 8, 0, 0x10, 0, 0 };
        private ushort[] clEnd = { 0x0A, 00 };


        private void WriteUshortArray(ushort[] value)
        {
            for (int i = 0; i < value.Length; i++)
                writer.Write(value[i]);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ExcelWriter"/> class.
        /// </summary>
        /// <param name="stream">The stream.</param>
        public ExcelWriter(Stream stream)
        {
            this.stream = stream;
            writer = new BinaryWriter(stream);
        }

        /// <summary>
        /// Writes the text cell value.
        /// </summary>
        /// <param name="row">The row.</param>
        /// <param name="col">The col.</param>
        /// <param name="value">The string value.</param>
        public void WriteCell(int row, int col, string value)
        {
            ushort[] clData = { 0x0204, 0, 0, 0, 0, 0 };
            int iLen = value.Length;
            byte[] plainText = Encoding.ASCII.GetBytes(value);
            clData[1] = (ushort)(8 + iLen);
            clData[2] = (ushort)row;
            clData[3] = (ushort)col;
            clData[5] = (ushort)iLen;
            WriteUshortArray(clData);
            writer.Write(plainText);
        }

        /// <summary>
        /// Writes the integer cell value.
        /// </summary>
        /// <param name="row">The row number.</param>
        /// <param name="col">The column number.</param>
        /// <param name="value">The value.</param>
        public void WriteCell(int row, int col, int value)
        {
            ushort[] clData = { 0x027E, 10, 0, 0, 0 };
            clData[2] = (ushort)row;
            clData[3] = (ushort)col;
            WriteUshortArray(clData);
            int iValue = (value << 2) | 2;
            writer.Write(iValue);
        }

        /// <summary>
        /// Writes the double cell value.
        /// </summary>
        /// <param name="row">The row number.</param>
        /// <param name="col">The column number.</param>
        /// <param name="value">The value.</param>
        public void WriteCell(int row, int col, double value)
        {
            ushort[] clData = { 0x0203, 14, 0, 0, 0 };
            clData[2] = (ushort)row;
            clData[3] = (ushort)col;
            WriteUshortArray(clData);
            writer.Write(value);
        }

        /// <summary>
        /// Writes the empty cell.
        /// </summary>
        /// <param name="row">The row number.</param>
        /// <param name="col">The column number.</param>
        public void WriteCell(int row, int col)
        {
            ushort[] clData = { 0x0201, 6, 0, 0, 0x17 };
            clData[2] = (ushort)row;
            clData[3] = (ushort)col;
            WriteUshortArray(clData);
        }

        /// <summary>
        /// Must be called once for creating XLS file header
        /// </summary>
        public void BeginWrite()
        {
            WriteUshortArray(clBegin);
        }

        /// <summary>
        /// Ends the writing operation, but do not close the stream
        /// </summary>
        public void EndWrite()
        {
            WriteUshortArray(clEnd);
            writer.Flush();
        }
    }

    class lib
    {
        private String emailServer = "wilmar.co.id";
        private String username = "sa";
        private String password = "123";
        //private String server = "GIS-JKT05-DEV\\SQLEXPRESS";
        private String database = "db.asset.management";
        private String ip = "10.7.15.32";
        public bool ValidateEmail(string sEmail)
        {
            Regex exp = new Regex(@"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            Match m = exp.Match(sEmail);
            if (m.Success && m.Value.Equals(sEmail)) return true;
            else return false;
        }
        public Color color1()
        {
            return Color.FromArgb(65, 131, 150);
        }
        public Color color2()
        {
            return Color.FromArgb(37, 66, 82);
        }
        private void getParam() 
        {
            foreach (string key in ConfigurationManager.AppSettings)
            {
                if (key == "dbConn")
                {
                    string value = ConfigurationManager.AppSettings[key];
                    string[] splitvalue = value.Split(';');
                    string[] databaseserversplit = splitvalue[0].Split('=');
                    this.ip = databaseserversplit[1];
                    string[] databasenamesplit = splitvalue[1].Split('=');
                    this.database = databasenamesplit[1];
                    string[] usernamenamesplit = splitvalue[2].Split('=');
                    this.username = usernamenamesplit[1];
                    string[] passwordnamesplit = splitvalue[3].Split('=');
                    this.password = passwordnamesplit[1];
                }
            }
        }
        public String param()
        {
            getParam();
            String param = "Data Source=" + this.ip + "\\SQLEXPRESS;" +
            "user id=" + this.username + ";" +
            "password=" + this.password + ";" +
            //"server=" + this.server + ";" +
            // "Trusted_Connection=yes;integrated security=false;" +
            "MultipleActiveResultSets=true;" +
            "database=" + this.database + ";" +
            "connection timeout=30";


            //String param = "Server=10.7.15.32\\SQLEXPRESS,2301;Database=db.asset.management;UID=sa;Password=123;Trusted_Connection=yes;";
            ////"; "Data Source=" + this.ip + ",3637;" +
            //// "Network Library=DBMSSOCN;" +
            //// "Initial Catalog=" + this.database + ";" +
            //// "user id=" + this.username + ";" +
            //// "password=" + this.password + ";";
            return param;
        }
        public String encrypt(String text)
        {
            MD5 encrypt = new MD5CryptoServiceProvider();
            encrypt.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            byte[] result = encrypt.Hash;
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                strBuilder.Append(result[i].ToString("x2"));
            }
            return strBuilder.ToString();
        }
        public String splitEmail(String email) 
        {
            String [] temp = email.Split('@');
            return temp[0];
        }
        public String getEmail(String email) 
        {
            String m = "";
            String [] temp = email.Split('@');
            if (temp.Length > 1)
            {
                if (temp[1].Equals(this.emailServer))
                {
                    m = temp[0] + "@" + this.emailServer;
                }
                else
                {
                    m = email;
                }
            }
            else
            {
                m = temp[0] + "@" + this.emailServer;
            }
            return m;
        }
        public Boolean cekEmail(String email) 
        {
            String[] temp = email.Split('@');
            if (temp.Length > 1)
            {
                if (temp[1].Equals(this.emailServer))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
