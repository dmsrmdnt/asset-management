﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class AssetDetailLB : Form
    {
        private String id_employee;
        private String id_asset;
        private String status;
        private String acc;
        private String stat;
        lib lib = new lib();
        public AssetDetailLB(String id_employee, String id_asset, String status, String acc, String stat)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id_employee = id_employee;
            this.id_asset = id_asset;
            this.status = status;
            this.acc = acc;
            this.stat = stat;
        }

        private void AssetDetailLB_Load(object sender, EventArgs e)
        {
            try
            {
                int year = Convert.ToInt32(DateTime.Now.Year.ToString());
                showBaru(year, this.id_asset);
                showDataOwner(this.id_employee);
                showDataAsset(this.id_asset);
                showDataMutation(this.id_asset);
                showDataClaim(this.id_employee, this.id_asset, this.status, this.acc,this.stat);
                showImage(this.id_asset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showBaru(int year, String id)
        {
            try
            {
                var pos = this.PointToScreen(lKondisi.Location);
                pos = pAsset.PointToClient(pos);
                pos.X = pAsset.Width - lKondisi.Width;
                pos.Y = 0;
                lKondisi.Parent = pAsset;
                lKondisi.Location = pos;
                lKondisi.BackColor = Color.Transparent;
                int y = 0;
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_baru";
                myCommand.Parameters.AddWithValue("@id_asset", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    if (read[0].ToString() != null && read[0].ToString() != "")
                    {
                        y = Convert.ToInt32(read[0].ToString());
                    }
                }
                if (y == year)
                {
                    lKondisi.Show();
                }
                else
                {
                    lKondisi.Hide();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showImage(String id)
        {
            try
            {
                byte[] bit = null;
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sqlcmd = new SqlCommand();
                SqlDataAdapter da = new SqlDataAdapter();
                DataTable dt = new DataTable();
                sqlcmd = conn.CreateCommand();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "show_image";
                sqlcmd.Parameters.AddWithValue("@id_asset", id);
                da = new SqlDataAdapter(sqlcmd);
                dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                if (dt.Rows.Count > 0)
                {
                    //Get a stored PDF bytes
                    String dname = dt.Rows[0]["dname"].ToString();
                    if (dname != "")
                    {

                        bit = (byte[])dt.Rows[0]["dcontent"];
                        MemoryStream bitStream = new MemoryStream(bit);
                        Image image = Image.FromStream(bitStream);
                        pAsset.Image = image;
                    }
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showDataClaim(String id_employee, String id_asset, String status, String acc, String stat)
        {
            try
            {
                //String table = null;
                if (status.Equals("0"))
                {
                    tKondisi.Text = "Rusak";
                    //table = "t_condition";
                }
                else
                {
                    tKondisi.Text = "Hilang";
                    //table = "t_lose";
                }
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_claim";
                myCommand.Parameters.AddWithValue("@status", status);
                myCommand.Parameters.AddWithValue("@id_employee", id_employee);
                myCommand.Parameters.AddWithValue("@id_asset", id_asset);
                myCommand.Parameters.AddWithValue("@acc", acc);
                myCommand.Parameters.AddWithValue("@stat", stat);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    tAlasan.Text = read["note"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showDataMutation(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataReader read = null;
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_mutation_id";
                myCommand.Parameters.AddWithValue("@id_asset", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    tNMutation.Text = read[0].ToString();
                }
                read.Close();
                myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_mutation";
                myCommand.Parameters.AddWithValue("@id_asset", id);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "MutationTable");
                conn.Close();
                this.dataGMutation.DataSource = dataSet.Tables["MutationTable"].DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showDataOwner(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_owner";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    tNIK.Text = read["email"].ToString();
                    tName.Text = read["nama"].ToString();
                    tDepartment.Text = read["dept"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showDataAsset(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_asset_detail";
                myCommand.Parameters.AddWithValue("@id_asset", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    tNoAsset.Text = read["asset"].ToString();
                    tItem.Text = read["item"].ToString();
                    tCategory.Text = read["category"].ToString();
                    tBrand.Text = read["brand"].ToString();
                    tLocation.Text = read["location"].ToString();
                    tModel.Text = read["model"].ToString();
                    tSerial.Text = read["serial"].ToString();
                    tPrice.Text = read["price"].ToString();
                    tDesc.Text = read["description"].ToString();
                    purDate.Text = read["pur_date"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AssetDetailLB_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
    }
}
