﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace asset.View
{
    public partial class SendEmail : Form
    {
        Class.Email.email mail = new Class.Email.email();
        public string[] file,infoAsset, data;
        public string id_employee = "", id_employee1="", id_employee2="", id_asset="", desc="", pdf="", excel="";
        public int form, stat, indexStatus;
        public SendEmail()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        } 
        void showProgress() 
        {
            ProgressBar p = new ProgressBar();
            p.Location = new Point(10, 30);
            p.Size = new Size(230, 15);
            p.MarqueeAnimationSpeed = 30;
            p.Style = ProgressBarStyle.Marquee;
            this.Controls.Add(p);
        }
        private void SendEmail_Load(object sender, EventArgs e)
        {
            showProgress();
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            if (form == 0)
            {
                //BreakManagement.cs
                mail.mailClaim(this.id_employee, this.id_asset, this.desc, this.stat, this.file);
            }
            else if (form == 1)
            {
                //ClaimManagement.cs
                mail.mailClaimResponse(this.id_employee, this.id_asset, this.stat, this.indexStatus);
            }
            else if (form == 2)
            {
                //MutationManagement.cs - send
                mail.mailMutation(this.id_employee1, this.id_employee2, this.id_asset, this.stat, this.file);
            }
            else if (form == 3)
            {
                //MutationManagement.cs - response
                mail.mailMutationResponse(this.id_employee, this.id_asset, this.stat, this.indexStatus);
            }
            else if (form == 4)
            {
                //PrintReport.cs
                mail.mailReport(this.id_employee, this.stat, this.file);
            }
            else if (form == 5)
            {
                //AddAsset.cs
                mail.mailAddAsset(this.id_employee, this.infoAsset, this.stat, this.indexStatus);
            }
            else if (form == 6)
            {
                //DeleteAsset.cs
                mail.mailDeleteAsset(this.id_employee, this.id_asset, this.stat, this.indexStatus);
            }
            else if (form == 7)
            {
                //DeleteAsset.cs
                mail.mailRequest(this.data, this.stat);
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Close();
        }
    }
}
