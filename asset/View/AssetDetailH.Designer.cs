﻿namespace asset
{
    partial class AssetDetailH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tNMutation = new System.Windows.Forms.TextBox();
            this.lNMutation = new System.Windows.Forms.Label();
            this.dataGMutation = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGSoft = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tTotalFix = new System.Windows.Forms.TextBox();
            this.dataGFix = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.gOwner = new System.Windows.Forms.GroupBox();
            this.tDepartment = new System.Windows.Forms.TextBox();
            this.tName = new System.Windows.Forms.TextBox();
            this.tNIK = new System.Windows.Forms.TextBox();
            this.lDepartment = new System.Windows.Forms.Label();
            this.lName = new System.Windows.Forms.Label();
            this.lNIK = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lBreak = new System.Windows.Forms.Label();
            this.lLost = new System.Windows.Forms.Label();
            this.lRepaired = new System.Windows.Forms.Label();
            this.lGood = new System.Windows.Forms.Label();
            this.gAsset = new System.Windows.Forms.GroupBox();
            this.tItem = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lKondisi = new System.Windows.Forms.Label();
            this.pAsset = new System.Windows.Forms.PictureBox();
            this.tNoAsset = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.purDate = new System.Windows.Forms.DateTimePicker();
            this.lDate = new System.Windows.Forms.Label();
            this.tDesc = new System.Windows.Forms.TextBox();
            this.tLocation = new System.Windows.Forms.TextBox();
            this.tPrice = new System.Windows.Forms.TextBox();
            this.tBrand = new System.Windows.Forms.TextBox();
            this.tCategory = new System.Windows.Forms.TextBox();
            this.tSerial = new System.Windows.Forms.TextBox();
            this.tModel = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lLocation = new System.Windows.Forms.Label();
            this.lPrice = new System.Windows.Forms.Label();
            this.lBrand = new System.Windows.Forms.Label();
            this.lCategory = new System.Windows.Forms.Label();
            this.lSerial = new System.Windows.Forms.Label();
            this.lModel = new System.Windows.Forms.Label();
            this.lUnused = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGMutation)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGSoft)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGFix)).BeginInit();
            this.gOwner.SuspendLayout();
            this.gAsset.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pAsset)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.tNMutation);
            this.groupBox1.Controls.Add(this.lNMutation);
            this.groupBox1.Controls.Add(this.dataGMutation);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(555, 227);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(504, 206);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Daftar Mutasi";
            // 
            // tNMutation
            // 
            this.tNMutation.Enabled = false;
            this.tNMutation.Location = new System.Drawing.Point(134, 28);
            this.tNMutation.Name = "tNMutation";
            this.tNMutation.ReadOnly = true;
            this.tNMutation.Size = new System.Drawing.Size(148, 20);
            this.tNMutation.TabIndex = 3;
            // 
            // lNMutation
            // 
            this.lNMutation.AutoSize = true;
            this.lNMutation.Location = new System.Drawing.Point(6, 31);
            this.lNMutation.Name = "lNMutation";
            this.lNMutation.Size = new System.Drawing.Size(38, 13);
            this.lNMutation.TabIndex = 2;
            this.lNMutation.Text = "Mutasi";
            // 
            // dataGMutation
            // 
            this.dataGMutation.AllowUserToAddRows = false;
            this.dataGMutation.AllowUserToDeleteRows = false;
            this.dataGMutation.AllowUserToResizeColumns = false;
            this.dataGMutation.AllowUserToResizeRows = false;
            this.dataGMutation.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGMutation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGMutation.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGMutation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGMutation.Location = new System.Drawing.Point(9, 62);
            this.dataGMutation.MultiSelect = false;
            this.dataGMutation.Name = "dataGMutation";
            this.dataGMutation.ReadOnly = true;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.dataGMutation.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGMutation.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGMutation.Size = new System.Drawing.Size(474, 138);
            this.dataGMutation.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.dataGSoft);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(555, 439);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(504, 215);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Daftar Software";
            // 
            // dataGSoft
            // 
            this.dataGSoft.AllowUserToAddRows = false;
            this.dataGSoft.AllowUserToDeleteRows = false;
            this.dataGSoft.AllowUserToResizeColumns = false;
            this.dataGSoft.AllowUserToResizeRows = false;
            this.dataGSoft.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGSoft.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGSoft.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGSoft.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGSoft.Location = new System.Drawing.Point(9, 26);
            this.dataGSoft.MultiSelect = false;
            this.dataGSoft.Name = "dataGSoft";
            this.dataGSoft.ReadOnly = true;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            this.dataGSoft.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGSoft.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGSoft.Size = new System.Drawing.Size(474, 138);
            this.dataGSoft.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.tTotalFix);
            this.groupBox3.Controls.Add(this.dataGFix);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(555, 52);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(504, 169);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Daftar Perbaikan";
            // 
            // tTotalFix
            // 
            this.tTotalFix.Enabled = false;
            this.tTotalFix.Location = new System.Drawing.Point(134, 143);
            this.tTotalFix.Name = "tTotalFix";
            this.tTotalFix.ReadOnly = true;
            this.tTotalFix.Size = new System.Drawing.Size(148, 20);
            this.tTotalFix.TabIndex = 3;
            // 
            // dataGFix
            // 
            this.dataGFix.AllowUserToAddRows = false;
            this.dataGFix.AllowUserToDeleteRows = false;
            this.dataGFix.AllowUserToResizeColumns = false;
            this.dataGFix.AllowUserToResizeRows = false;
            this.dataGFix.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGFix.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGFix.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGFix.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGFix.Location = new System.Drawing.Point(9, 20);
            this.dataGFix.MultiSelect = false;
            this.dataGFix.Name = "dataGFix";
            this.dataGFix.ReadOnly = true;
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            this.dataGFix.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGFix.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGFix.Size = new System.Drawing.Size(474, 117);
            this.dataGFix.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Total Biaya Perbaikan";
            // 
            // gOwner
            // 
            this.gOwner.BackColor = System.Drawing.Color.Transparent;
            this.gOwner.Controls.Add(this.tDepartment);
            this.gOwner.Controls.Add(this.tName);
            this.gOwner.Controls.Add(this.tNIK);
            this.gOwner.Controls.Add(this.lDepartment);
            this.gOwner.Controls.Add(this.lName);
            this.gOwner.Controls.Add(this.lNIK);
            this.gOwner.ForeColor = System.Drawing.Color.White;
            this.gOwner.Location = new System.Drawing.Point(15, 52);
            this.gOwner.Name = "gOwner";
            this.gOwner.Size = new System.Drawing.Size(534, 169);
            this.gOwner.TabIndex = 49;
            this.gOwner.TabStop = false;
            this.gOwner.Text = "Pemilik Saat Ini";
            // 
            // tDepartment
            // 
            this.tDepartment.Enabled = false;
            this.tDepartment.Location = new System.Drawing.Point(104, 75);
            this.tDepartment.Name = "tDepartment";
            this.tDepartment.ReadOnly = true;
            this.tDepartment.Size = new System.Drawing.Size(188, 20);
            this.tDepartment.TabIndex = 5;
            // 
            // tName
            // 
            this.tName.Enabled = false;
            this.tName.Location = new System.Drawing.Point(104, 49);
            this.tName.Name = "tName";
            this.tName.ReadOnly = true;
            this.tName.Size = new System.Drawing.Size(309, 20);
            this.tName.TabIndex = 4;
            // 
            // tNIK
            // 
            this.tNIK.Enabled = false;
            this.tNIK.Location = new System.Drawing.Point(104, 23);
            this.tNIK.Name = "tNIK";
            this.tNIK.ReadOnly = true;
            this.tNIK.Size = new System.Drawing.Size(309, 20);
            this.tNIK.TabIndex = 3;
            // 
            // lDepartment
            // 
            this.lDepartment.AutoSize = true;
            this.lDepartment.Location = new System.Drawing.Point(6, 73);
            this.lDepartment.Name = "lDepartment";
            this.lDepartment.Size = new System.Drawing.Size(65, 13);
            this.lDepartment.TabIndex = 2;
            this.lDepartment.Text = "Departemen";
            // 
            // lName
            // 
            this.lName.AutoSize = true;
            this.lName.Location = new System.Drawing.Point(6, 49);
            this.lName.Name = "lName";
            this.lName.Size = new System.Drawing.Size(35, 13);
            this.lName.TabIndex = 1;
            this.lName.Text = "Nama";
            // 
            // lNIK
            // 
            this.lNIK.AutoSize = true;
            this.lNIK.Location = new System.Drawing.Point(6, 26);
            this.lNIK.Name = "lNIK";
            this.lNIK.Size = new System.Drawing.Size(32, 13);
            this.lNIK.TabIndex = 0;
            this.lNIK.Text = "Email";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(12, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 25);
            this.label4.TabIndex = 50;
            this.label4.Text = "label4";
            // 
            // lBreak
            // 
            this.lBreak.BackColor = System.Drawing.Color.Transparent;
            this.lBreak.Image = global::asset.Properties.Resources._break;
            this.lBreak.Location = new System.Drawing.Point(892, 0);
            this.lBreak.Name = "lBreak";
            this.lBreak.Size = new System.Drawing.Size(182, 40);
            this.lBreak.TabIndex = 51;
            // 
            // lLost
            // 
            this.lLost.BackColor = System.Drawing.Color.Transparent;
            this.lLost.Image = global::asset.Properties.Resources.lost;
            this.lLost.Location = new System.Drawing.Point(892, 0);
            this.lLost.Name = "lLost";
            this.lLost.Size = new System.Drawing.Size(182, 40);
            this.lLost.TabIndex = 52;
            // 
            // lRepaired
            // 
            this.lRepaired.BackColor = System.Drawing.Color.Transparent;
            this.lRepaired.Image = global::asset.Properties.Resources.repaired;
            this.lRepaired.Location = new System.Drawing.Point(892, 0);
            this.lRepaired.Name = "lRepaired";
            this.lRepaired.Size = new System.Drawing.Size(182, 40);
            this.lRepaired.TabIndex = 53;
            // 
            // lGood
            // 
            this.lGood.BackColor = System.Drawing.Color.Transparent;
            this.lGood.Image = global::asset.Properties.Resources.good1;
            this.lGood.Location = new System.Drawing.Point(892, 0);
            this.lGood.Name = "lGood";
            this.lGood.Size = new System.Drawing.Size(182, 40);
            this.lGood.TabIndex = 54;
            // 
            // gAsset
            // 
            this.gAsset.BackColor = System.Drawing.Color.Transparent;
            this.gAsset.Controls.Add(this.tItem);
            this.gAsset.Controls.Add(this.label1);
            this.gAsset.Controls.Add(this.lKondisi);
            this.gAsset.Controls.Add(this.pAsset);
            this.gAsset.Controls.Add(this.tNoAsset);
            this.gAsset.Controls.Add(this.label2);
            this.gAsset.Controls.Add(this.purDate);
            this.gAsset.Controls.Add(this.lDate);
            this.gAsset.Controls.Add(this.tDesc);
            this.gAsset.Controls.Add(this.tLocation);
            this.gAsset.Controls.Add(this.tPrice);
            this.gAsset.Controls.Add(this.tBrand);
            this.gAsset.Controls.Add(this.tCategory);
            this.gAsset.Controls.Add(this.tSerial);
            this.gAsset.Controls.Add(this.tModel);
            this.gAsset.Controls.Add(this.label5);
            this.gAsset.Controls.Add(this.lLocation);
            this.gAsset.Controls.Add(this.lPrice);
            this.gAsset.Controls.Add(this.lBrand);
            this.gAsset.Controls.Add(this.lCategory);
            this.gAsset.Controls.Add(this.lSerial);
            this.gAsset.Controls.Add(this.lModel);
            this.gAsset.ForeColor = System.Drawing.Color.White;
            this.gAsset.Location = new System.Drawing.Point(14, 227);
            this.gAsset.Name = "gAsset";
            this.gAsset.Size = new System.Drawing.Size(534, 427);
            this.gAsset.TabIndex = 55;
            this.gAsset.TabStop = false;
            this.gAsset.Text = "Informasi Aset";
            // 
            // tItem
            // 
            this.tItem.Enabled = false;
            this.tItem.Location = new System.Drawing.Point(113, 54);
            this.tItem.Name = "tItem";
            this.tItem.ReadOnly = true;
            this.tItem.Size = new System.Drawing.Size(220, 20);
            this.tItem.TabIndex = 51;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 13);
            this.label1.TabIndex = 50;
            this.label1.Text = "Item";
            // 
            // lKondisi
            // 
            this.lKondisi.Image = global::asset.Properties.Resources.baru;
            this.lKondisi.Location = new System.Drawing.Point(444, 29);
            this.lKondisi.Name = "lKondisi";
            this.lKondisi.Size = new System.Drawing.Size(74, 77);
            this.lKondisi.TabIndex = 49;
            // 
            // pAsset
            // 
            this.pAsset.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pAsset.Image = global::asset.Properties.Resources.noimage;
            this.pAsset.Location = new System.Drawing.Point(339, 28);
            this.pAsset.Name = "pAsset";
            this.pAsset.Size = new System.Drawing.Size(180, 150);
            this.pAsset.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pAsset.TabIndex = 34;
            this.pAsset.TabStop = false;
            // 
            // tNoAsset
            // 
            this.tNoAsset.Enabled = false;
            this.tNoAsset.Location = new System.Drawing.Point(113, 28);
            this.tNoAsset.Name = "tNoAsset";
            this.tNoAsset.ReadOnly = true;
            this.tNoAsset.Size = new System.Drawing.Size(220, 20);
            this.tNoAsset.TabIndex = 48;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 47;
            this.label2.Text = "No. Aset";
            // 
            // purDate
            // 
            this.purDate.Enabled = false;
            this.purDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.purDate.Location = new System.Drawing.Point(113, 185);
            this.purDate.Name = "purDate";
            this.purDate.Size = new System.Drawing.Size(148, 20);
            this.purDate.TabIndex = 46;
            // 
            // lDate
            // 
            this.lDate.AutoSize = true;
            this.lDate.Location = new System.Drawing.Point(15, 189);
            this.lDate.Name = "lDate";
            this.lDate.Size = new System.Drawing.Size(98, 13);
            this.lDate.TabIndex = 45;
            this.lDate.Text = "Tanggal Pembelian";
            // 
            // tDesc
            // 
            this.tDesc.Enabled = false;
            this.tDesc.Location = new System.Drawing.Point(113, 263);
            this.tDesc.Multiline = true;
            this.tDesc.Name = "tDesc";
            this.tDesc.ReadOnly = true;
            this.tDesc.Size = new System.Drawing.Size(400, 150);
            this.tDesc.TabIndex = 41;
            // 
            // tLocation
            // 
            this.tLocation.Enabled = false;
            this.tLocation.Location = new System.Drawing.Point(113, 237);
            this.tLocation.Name = "tLocation";
            this.tLocation.ReadOnly = true;
            this.tLocation.Size = new System.Drawing.Size(148, 20);
            this.tLocation.TabIndex = 40;
            // 
            // tPrice
            // 
            this.tPrice.Enabled = false;
            this.tPrice.Location = new System.Drawing.Point(113, 211);
            this.tPrice.Name = "tPrice";
            this.tPrice.ReadOnly = true;
            this.tPrice.Size = new System.Drawing.Size(148, 20);
            this.tPrice.TabIndex = 39;
            // 
            // tBrand
            // 
            this.tBrand.Enabled = false;
            this.tBrand.Location = new System.Drawing.Point(113, 159);
            this.tBrand.Name = "tBrand";
            this.tBrand.ReadOnly = true;
            this.tBrand.Size = new System.Drawing.Size(148, 20);
            this.tBrand.TabIndex = 38;
            // 
            // tCategory
            // 
            this.tCategory.Enabled = false;
            this.tCategory.Location = new System.Drawing.Point(113, 133);
            this.tCategory.Name = "tCategory";
            this.tCategory.ReadOnly = true;
            this.tCategory.Size = new System.Drawing.Size(148, 20);
            this.tCategory.TabIndex = 37;
            // 
            // tSerial
            // 
            this.tSerial.Enabled = false;
            this.tSerial.Location = new System.Drawing.Point(113, 107);
            this.tSerial.Name = "tSerial";
            this.tSerial.ReadOnly = true;
            this.tSerial.Size = new System.Drawing.Size(220, 20);
            this.tSerial.TabIndex = 36;
            // 
            // tModel
            // 
            this.tModel.Enabled = false;
            this.tModel.Location = new System.Drawing.Point(113, 81);
            this.tModel.Name = "tModel";
            this.tModel.ReadOnly = true;
            this.tModel.Size = new System.Drawing.Size(220, 20);
            this.tModel.TabIndex = 35;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(15, 263);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(50, 13);
            this.label5.TabIndex = 34;
            this.label5.Text = "Deskripsi";
            // 
            // lLocation
            // 
            this.lLocation.AutoSize = true;
            this.lLocation.Location = new System.Drawing.Point(15, 240);
            this.lLocation.Name = "lLocation";
            this.lLocation.Size = new System.Drawing.Size(38, 13);
            this.lLocation.TabIndex = 33;
            this.lLocation.Text = "Lokasi";
            // 
            // lPrice
            // 
            this.lPrice.AutoSize = true;
            this.lPrice.Location = new System.Drawing.Point(15, 214);
            this.lPrice.Name = "lPrice";
            this.lPrice.Size = new System.Drawing.Size(36, 13);
            this.lPrice.TabIndex = 32;
            this.lPrice.Text = "Harga";
            // 
            // lBrand
            // 
            this.lBrand.AutoSize = true;
            this.lBrand.Location = new System.Drawing.Point(15, 162);
            this.lBrand.Name = "lBrand";
            this.lBrand.Size = new System.Drawing.Size(35, 13);
            this.lBrand.TabIndex = 31;
            this.lBrand.Text = "Brand";
            // 
            // lCategory
            // 
            this.lCategory.AutoSize = true;
            this.lCategory.Location = new System.Drawing.Point(15, 136);
            this.lCategory.Name = "lCategory";
            this.lCategory.Size = new System.Drawing.Size(46, 13);
            this.lCategory.TabIndex = 30;
            this.lCategory.Text = "Kategori";
            // 
            // lSerial
            // 
            this.lSerial.AutoSize = true;
            this.lSerial.Location = new System.Drawing.Point(15, 110);
            this.lSerial.Name = "lSerial";
            this.lSerial.Size = new System.Drawing.Size(53, 13);
            this.lSerial.TabIndex = 29;
            this.lSerial.Text = "No. Serial";
            // 
            // lModel
            // 
            this.lModel.AutoSize = true;
            this.lModel.Location = new System.Drawing.Point(15, 84);
            this.lModel.Name = "lModel";
            this.lModel.Size = new System.Drawing.Size(36, 13);
            this.lModel.TabIndex = 28;
            this.lModel.Text = "Model";
            // 
            // lUnused
            // 
            this.lUnused.BackColor = System.Drawing.Color.Transparent;
            this.lUnused.Image = global::asset.Properties.Resources.unused;
            this.lUnused.Location = new System.Drawing.Point(892, 0);
            this.lUnused.Name = "lUnused";
            this.lUnused.Size = new System.Drawing.Size(182, 40);
            this.lUnused.TabIndex = 56;
            this.lUnused.Visible = false;
            // 
            // AssetDetailH
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1071, 666);
            this.Controls.Add(this.lUnused);
            this.Controls.Add(this.gAsset);
            this.Controls.Add(this.lGood);
            this.Controls.Add(this.lRepaired);
            this.Controls.Add(this.lLost);
            this.Controls.Add(this.lBreak);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.gOwner);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AssetDetailH";
            this.Text = "Detail Aset";
            this.Load += new System.EventHandler(this.AssetDetail_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AssetDetail_Paint);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGMutation)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGSoft)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGFix)).EndInit();
            this.gOwner.ResumeLayout(false);
            this.gOwner.PerformLayout();
            this.gAsset.ResumeLayout(false);
            this.gAsset.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pAsset)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tNMutation;
        private System.Windows.Forms.Label lNMutation;
        private System.Windows.Forms.DataGridView dataGMutation;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGSoft;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tTotalFix;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGFix;
        private System.Windows.Forms.GroupBox gOwner;
        private System.Windows.Forms.TextBox tDepartment;
        private System.Windows.Forms.TextBox tName;
        private System.Windows.Forms.TextBox tNIK;
        private System.Windows.Forms.Label lDepartment;
        private System.Windows.Forms.Label lName;
        private System.Windows.Forms.Label lNIK;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lBreak;
        private System.Windows.Forms.Label lLost;
        private System.Windows.Forms.Label lRepaired;
        private System.Windows.Forms.Label lGood;
        private System.Windows.Forms.GroupBox gAsset;
        private System.Windows.Forms.TextBox tItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lKondisi;
        private System.Windows.Forms.PictureBox pAsset;
        private System.Windows.Forms.TextBox tNoAsset;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker purDate;
        private System.Windows.Forms.Label lDate;
        private System.Windows.Forms.TextBox tDesc;
        private System.Windows.Forms.TextBox tLocation;
        private System.Windows.Forms.TextBox tPrice;
        private System.Windows.Forms.TextBox tBrand;
        private System.Windows.Forms.TextBox tCategory;
        private System.Windows.Forms.TextBox tSerial;
        private System.Windows.Forms.TextBox tModel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lLocation;
        private System.Windows.Forms.Label lPrice;
        private System.Windows.Forms.Label lBrand;
        private System.Windows.Forms.Label lCategory;
        private System.Windows.Forms.Label lSerial;
        private System.Windows.Forms.Label lModel;
        private System.Windows.Forms.Label lUnused;
    }
}