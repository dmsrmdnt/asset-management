﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace asset.View
{
    public partial class UploadMaster : Form
    {
        lib lib = new lib();
        Class.Excel.UploadMaster p = new Class.Excel.UploadMaster();
        public string fileEmployee, fileAsset;
        public UploadMaster()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        } 
        void showProgress() 
        {
            ProgressBar p = new ProgressBar();
            p.Location = new Point(10, 30);
            p.Size = new Size(230, 15);
            p.MarqueeAnimationSpeed = 30;
            p.Style = ProgressBarStyle.Marquee;
            this.Controls.Add(p);
        }
        private void SendEmail_Load(object sender, EventArgs e)
        {
            showProgress();
            backgroundWorker1.RunWorkerAsync();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            p.readExcelEmployee(fileEmployee);
            p.readExcelAsset(fileAsset);
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Close();
        }
    }
}
