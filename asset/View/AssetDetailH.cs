﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class AssetDetailH : Form
    {
        private String id;
        private String id_employee;
        lib lib = new lib();
        public AssetDetailH(String id,String id_employee)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
            this.id_employee = id_employee;
        }

        private void AssetDetail_Load(object sender, EventArgs e)
        {
            try
            {
                lBreak.Visible = false;
                lLost.Visible = false;
                lGood.Visible = false;
                lRepaired.Visible = false;
                showDataFix(this.id);
                showDataOwner(this.id_employee);
                int year = Convert.ToInt32(DateTime.Now.Year.ToString());
                showData(this.id);
                showMutation(this.id);
                showSoft(this.id);
                showImage(this.id);
                showBaru(year, this.id);
                showKondisi(this.id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showKondisi(String id)
        {
            try
            {
                int st = 0;
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_asset_status";
                myCommand.Parameters.AddWithValue("@id_asset", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    st = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
                switch (st)
                {
                    case 1:
                        lGood.Visible = true;
                        break;
                    case 2:
                        lBreak.Visible = true;
                        break;
                    case 3:
                        lUnused.Visible = true;
                        break;
                    case 4:
                        lLost.Visible = true;
                        break;
                    case 5:
                        lRepaired.Visible = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showDataFix(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataReader read = null;
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "sum_fix_price";
                myCommand.Parameters.AddWithValue("@id_asset", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    tTotalFix.Text = read[0].ToString();
                }
                read.Close();
                myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_fix";
                myCommand.Parameters.AddWithValue("@id_asset", id);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "MutationTable");
                conn.Close();
                this.dataGFix.DataSource = dataSet.Tables["MutationTable"].DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showDataOwner(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_owner";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    tNIK.Text = read["email"].ToString();
                    tName.Text = read["nama"].ToString();
                    tDepartment.Text = read["dept"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showBaru(int year, String id)
        {
            try
            {
                var pos = this.PointToScreen(lKondisi.Location);
                pos = pAsset.PointToClient(pos);
                pos.X = pAsset.Width - lKondisi.Width;
                pos.Y = 0;
                lKondisi.Parent = pAsset;
                lKondisi.Location = pos;
                lKondisi.BackColor = Color.Transparent;
                int y = 0;
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_baru";
                myCommand.Parameters.AddWithValue("@id_asset", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    if (read[0].ToString() != null && read[0].ToString() != "")
                    {
                        y = Convert.ToInt32(read[0].ToString());
                    }
                }
                if (y == year)
                {
                    lKondisi.Show();
                }
                else
                {
                    lKondisi.Hide();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showImage(String id)
        {
            try
            {
                byte[] bit = null;
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sqlcmd = new SqlCommand();
                SqlDataAdapter da = new SqlDataAdapter();
                DataTable dt = new DataTable();
                sqlcmd = conn.CreateCommand();
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandText = "show_image";
                sqlcmd.Parameters.AddWithValue("@id_asset", id);
                da = new SqlDataAdapter(sqlcmd);
                dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                if (dt.Rows.Count > 0)
                {
                    //Get a stored PDF bytes
                    String dname = dt.Rows[0]["dname"].ToString();
                    if (dname != "")
                    {

                        bit = (byte[])dt.Rows[0]["dcontent"];
                        MemoryStream bitStream = new MemoryStream(bit);
                        Image image = Image.FromStream(bitStream);
                        pAsset.Image = image;
                    }
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showSoft(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_soft";
                myCommand.Parameters.AddWithValue("@id_asset", id);
                SqlDataAdapter data = new SqlDataAdapter();
                data.SelectCommand = myCommand;
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "locationTable");
                conn.Close();
                this.dataGSoft.DataSource = dataSet.Tables["locationTable"].DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showData(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_asset_detail";
                myCommand.Parameters.AddWithValue("@id_asset", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    tNoAsset.Text = read["asset"].ToString();
                    tItem.Text = read["item"].ToString();
                    tCategory.Text = read["category"].ToString();
                    label4.Text = read["category"].ToString();
                    tBrand.Text = read["brand"].ToString();
                    tLocation.Text = read["location"].ToString();
                    tModel.Text = read["model"].ToString();
                    tSerial.Text = read["serial"].ToString();
                    tPrice.Text = read["price"].ToString();
                    tDesc.Text = read["description"].ToString();
                    purDate.Text = read["pur_date"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showMutation(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataReader read = null;
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "count_mutation_id";
                myCommand.Parameters.AddWithValue("@id_asset", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    tNMutation.Text = read[0].ToString();
                }
                read.Close();
                myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_mutation";
                myCommand.Parameters.AddWithValue("@id_asset", id);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "MutationTable");
                conn.Close();
                this.dataGMutation.DataSource = dataSet.Tables["MutationTable"].DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void AssetDetail_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            } 
        }

    }
}
