﻿namespace asset
{
    partial class Option
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.tServer = new System.Windows.Forms.TextBox();
            this.tDb = new System.Windows.Forms.TextBox();
            this.tUsr = new System.Windows.Forms.TextBox();
            this.tPasswd = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP Server";
            // 
            // tServer
            // 
            this.tServer.Location = new System.Drawing.Point(87, 12);
            this.tServer.Name = "tServer";
            this.tServer.Size = new System.Drawing.Size(255, 20);
            this.tServer.TabIndex = 1;
            this.tServer.Text = "10.7.15.32";
            // 
            // tDb
            // 
            this.tDb.Location = new System.Drawing.Point(87, 38);
            this.tDb.Name = "tDb";
            this.tDb.Size = new System.Drawing.Size(255, 20);
            this.tDb.TabIndex = 2;
            this.tDb.Text = "db.asset.management";
            // 
            // tUsr
            // 
            this.tUsr.Location = new System.Drawing.Point(87, 64);
            this.tUsr.Name = "tUsr";
            this.tUsr.Size = new System.Drawing.Size(255, 20);
            this.tUsr.TabIndex = 3;
            this.tUsr.Text = "sa";
            // 
            // tPasswd
            // 
            this.tPasswd.Location = new System.Drawing.Point(87, 90);
            this.tPasswd.Name = "tPasswd";
            this.tPasswd.Size = new System.Drawing.Size(255, 20);
            this.tPasswd.TabIndex = 4;
            this.tPasswd.Text = "123";
            this.tPasswd.UseSystemPasswordChar = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(12, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Database";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(12, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Username";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(12, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Password";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 122);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(330, 53);
            this.button1.TabIndex = 8;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Option
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 208);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tPasswd);
            this.Controls.Add(this.tUsr);
            this.Controls.Add(this.tDb);
            this.Controls.Add(this.tServer);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Option";
            this.Text = "Setting Database Connection";
            this.Load += new System.EventHandler(this.Option_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Option_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tServer;
        private System.Windows.Forms.TextBox tDb;
        private System.Windows.Forms.TextBox tUsr;
        private System.Windows.Forms.TextBox tPasswd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button1;
    }
}