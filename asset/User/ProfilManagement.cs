﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class ProfilManagement : Form
    {
        private String id;
        lib lib = new lib();
        Class.profil.mProfilManagement edit = new Class.profil.mProfilManagement();
        public ProfilManagement(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
        }
        private static ProfilManagement form;
        public static ProfilManagement GetChildInstance(String id)
        {
            if (form == null)
                form = new ProfilManagement(id);
            return form;
        }
        private void ProfilManagement_Load(object sender, EventArgs e)
        {
            try
            {
                showDataUser(this.id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void showDataUser(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_profil";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                //SqlCommand myCommand = new SqlCommand("select a.email, a.nama, b.auth, c.dept from m_employee a, m_auth b, m_dept c where a.id_employee='" + id + "' and a.id_auth=b.id_auth and a.id_dept=c.id_dept", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    tNIK.Text = read["email"].ToString();
                    tName.Text = read["nama"].ToString();
                    tAuth.Text = read["auth"].ToString();
                    tDepartment.Text = read["dept"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void ProfilManagement_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }
        public void clearForm() 
        {
            tPassNew.Clear();
            tPassNewR.Clear();
            tPassOld.Clear();
            lNotify.Text = "";
            lNotifyP.Text = "";
        }
        /////////////////////////////////profil///////////////////////////////////////
        private void bCancel_Click(object sender, EventArgs e)
        {
            try
            {
                showDataUser(this.id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            try
            {
                String nama = tName.Text;
                String nik = tNIK.Text;
                String id = this.id;

                Boolean cek = edit.cekInput(id, nama);

                if (cek == true)
                {
                    if (lib.cekEmail(tNIK.Text))
                    {
                        if (edit.cekData(nik) <= 1)
                        {
                            edit.update(id, nik, nama);
                            MessageBox.Show("Update profil berhasil");
                        }
                        else
                        {
                            lNotify.Text = "Email sudah terpakai!";
                        }
                    }
                    else
                    {
                        lNotify.Text = "Format email salah! Contoh : coba@wilmar.co.id";
                    }
                }
                else
                {
                    lNotify.Text = "Salah input!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /////////////////////////////////password///////////////////////////////////////
        private void bCancelP_Click(object sender, EventArgs e)
        {
            try
            {
                clearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                String p1 = tPassOld.Text;
                String p2 = tPassNew.Text;
                String p3 = tPassNewR.Text;

                Boolean cek = edit.cekInputPassword(p1, p2, p3);

                if (cek == true)
                {
                    if (tPassNew.Text.Equals(tPassNewR.Text))
                    {
                        if (edit.cekDataPassword(this.id, p1) == 1)
                        {
                            edit.updatePassword(this.id, p2);
                            clearForm();
                            MessageBox.Show("Update kata sandi berhasil");
                        }
                        else
                        {
                            lNotifyP.Text = "Kata sandi salah!";
                        }
                    }
                    else
                    {
                        lNotifyP.Text = "Password baru tidak sama!";
                    }
                }
                else
                {
                    lNotifyP.Text = "Salah input!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tPassNewR_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (tPassNew.Text.Equals(tPassNewR.Text))
                {
                    lNotifyP.Text = "";
                }
                else
                {
                    lNotifyP.Text = "Password baru tidak sama!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                clearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ProfilManagement_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }


    }
}
