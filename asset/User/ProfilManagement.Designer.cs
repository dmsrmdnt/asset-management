﻿namespace asset
{
    partial class ProfilManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabProfil = new System.Windows.Forms.TabPage();
            this.tAuth = new System.Windows.Forms.TextBox();
            this.lNotify = new System.Windows.Forms.Label();
            this.tDepartment = new System.Windows.Forms.TextBox();
            this.tName = new System.Windows.Forms.TextBox();
            this.tNIK = new System.Windows.Forms.TextBox();
            this.bCancel = new System.Windows.Forms.Button();
            this.bOK = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lName = new System.Windows.Forms.Label();
            this.lNIK = new System.Windows.Forms.Label();
            this.tabPassword = new System.Windows.Forms.TabPage();
            this.label4 = new System.Windows.Forms.Label();
            this.tPassNewR = new System.Windows.Forms.TextBox();
            this.lNotifyP = new System.Windows.Forms.Label();
            this.tPassNew = new System.Windows.Forms.TextBox();
            this.tPassOld = new System.Windows.Forms.TextBox();
            this.bCancelP = new System.Windows.Forms.Button();
            this.bUpdate = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabProfil.SuspendLayout();
            this.tabPassword.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabProfil);
            this.tabControl1.Controls.Add(this.tabPassword);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(659, 400);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabProfil
            // 
            this.tabProfil.Controls.Add(this.tAuth);
            this.tabProfil.Controls.Add(this.lNotify);
            this.tabProfil.Controls.Add(this.tDepartment);
            this.tabProfil.Controls.Add(this.tName);
            this.tabProfil.Controls.Add(this.tNIK);
            this.tabProfil.Controls.Add(this.bCancel);
            this.tabProfil.Controls.Add(this.bOK);
            this.tabProfil.Controls.Add(this.label2);
            this.tabProfil.Controls.Add(this.label1);
            this.tabProfil.Controls.Add(this.lName);
            this.tabProfil.Controls.Add(this.lNIK);
            this.tabProfil.Location = new System.Drawing.Point(4, 22);
            this.tabProfil.Name = "tabProfil";
            this.tabProfil.Padding = new System.Windows.Forms.Padding(3);
            this.tabProfil.Size = new System.Drawing.Size(651, 374);
            this.tabProfil.TabIndex = 0;
            this.tabProfil.Text = "Profil";
            this.tabProfil.UseVisualStyleBackColor = true;
            // 
            // tAuth
            // 
            this.tAuth.Enabled = false;
            this.tAuth.Location = new System.Drawing.Point(142, 104);
            this.tAuth.Name = "tAuth";
            this.tAuth.ReadOnly = true;
            this.tAuth.Size = new System.Drawing.Size(173, 20);
            this.tAuth.TabIndex = 22;
            // 
            // lNotify
            // 
            this.lNotify.AutoSize = true;
            this.lNotify.ForeColor = System.Drawing.Color.Red;
            this.lNotify.Location = new System.Drawing.Point(51, 180);
            this.lNotify.Name = "lNotify";
            this.lNotify.Size = new System.Drawing.Size(0, 13);
            this.lNotify.TabIndex = 21;
            // 
            // tDepartment
            // 
            this.tDepartment.Enabled = false;
            this.tDepartment.Location = new System.Drawing.Point(142, 133);
            this.tDepartment.Name = "tDepartment";
            this.tDepartment.ReadOnly = true;
            this.tDepartment.Size = new System.Drawing.Size(173, 20);
            this.tDepartment.TabIndex = 18;
            // 
            // tName
            // 
            this.tName.Location = new System.Drawing.Point(142, 72);
            this.tName.Name = "tName";
            this.tName.Size = new System.Drawing.Size(316, 20);
            this.tName.TabIndex = 13;
            // 
            // tNIK
            // 
            this.tNIK.Location = new System.Drawing.Point(142, 38);
            this.tNIK.Name = "tNIK";
            this.tNIK.Size = new System.Drawing.Size(316, 20);
            this.tNIK.TabIndex = 11;
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(499, 220);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(120, 40);
            this.bCancel.TabIndex = 20;
            this.bCancel.Text = "Batal";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bOK
            // 
            this.bOK.Location = new System.Drawing.Point(362, 220);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(120, 40);
            this.bOK.TabIndex = 19;
            this.bOK.Text = "Update";
            this.bOK.UseVisualStyleBackColor = true;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 136);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Departemen";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 107);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 15;
            this.label1.Text = "Authority";
            // 
            // lName
            // 
            this.lName.AutoSize = true;
            this.lName.Location = new System.Drawing.Point(51, 75);
            this.lName.Name = "lName";
            this.lName.Size = new System.Drawing.Size(35, 13);
            this.lName.TabIndex = 14;
            this.lName.Text = "Nama";
            // 
            // lNIK
            // 
            this.lNIK.AutoSize = true;
            this.lNIK.Location = new System.Drawing.Point(51, 41);
            this.lNIK.Name = "lNIK";
            this.lNIK.Size = new System.Drawing.Size(32, 13);
            this.lNIK.TabIndex = 12;
            this.lNIK.Text = "Email";
            // 
            // tabPassword
            // 
            this.tabPassword.Controls.Add(this.label4);
            this.tabPassword.Controls.Add(this.tPassNewR);
            this.tabPassword.Controls.Add(this.lNotifyP);
            this.tabPassword.Controls.Add(this.tPassNew);
            this.tabPassword.Controls.Add(this.tPassOld);
            this.tabPassword.Controls.Add(this.bCancelP);
            this.tabPassword.Controls.Add(this.bUpdate);
            this.tabPassword.Controls.Add(this.label6);
            this.tabPassword.Controls.Add(this.label7);
            this.tabPassword.Location = new System.Drawing.Point(4, 22);
            this.tabPassword.Name = "tabPassword";
            this.tabPassword.Padding = new System.Windows.Forms.Padding(3);
            this.tabPassword.Size = new System.Drawing.Size(651, 374);
            this.tabPassword.TabIndex = 1;
            this.tabPassword.Text = "Ubah Kata Sandi";
            this.tabPassword.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(51, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 13);
            this.label4.TabIndex = 34;
            this.label4.Text = "Ulangi Kata Sandi Baru";
            // 
            // tPassNewR
            // 
            this.tPassNewR.Location = new System.Drawing.Point(183, 106);
            this.tPassNewR.Name = "tPassNewR";
            this.tPassNewR.Size = new System.Drawing.Size(316, 20);
            this.tPassNewR.TabIndex = 33;
            this.tPassNewR.UseSystemPasswordChar = true;
            this.tPassNewR.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tPassNewR_KeyUp);
            // 
            // lNotifyP
            // 
            this.lNotifyP.AutoSize = true;
            this.lNotifyP.ForeColor = System.Drawing.Color.Red;
            this.lNotifyP.Location = new System.Drawing.Point(53, 176);
            this.lNotifyP.Name = "lNotifyP";
            this.lNotifyP.Size = new System.Drawing.Size(0, 13);
            this.lNotifyP.TabIndex = 32;
            // 
            // tPassNew
            // 
            this.tPassNew.Location = new System.Drawing.Point(183, 72);
            this.tPassNew.Name = "tPassNew";
            this.tPassNew.Size = new System.Drawing.Size(316, 20);
            this.tPassNew.TabIndex = 25;
            this.tPassNew.UseSystemPasswordChar = true;
            // 
            // tPassOld
            // 
            this.tPassOld.Location = new System.Drawing.Point(183, 38);
            this.tPassOld.Name = "tPassOld";
            this.tPassOld.Size = new System.Drawing.Size(316, 20);
            this.tPassOld.TabIndex = 23;
            this.tPassOld.UseSystemPasswordChar = true;
            // 
            // bCancelP
            // 
            this.bCancelP.Location = new System.Drawing.Point(501, 216);
            this.bCancelP.Name = "bCancelP";
            this.bCancelP.Size = new System.Drawing.Size(120, 40);
            this.bCancelP.TabIndex = 31;
            this.bCancelP.Text = "Batal";
            this.bCancelP.UseVisualStyleBackColor = true;
            this.bCancelP.Click += new System.EventHandler(this.bCancelP_Click);
            // 
            // bUpdate
            // 
            this.bUpdate.Location = new System.Drawing.Point(364, 216);
            this.bUpdate.Name = "bUpdate";
            this.bUpdate.Size = new System.Drawing.Size(120, 40);
            this.bUpdate.TabIndex = 30;
            this.bUpdate.Text = "Update";
            this.bUpdate.UseVisualStyleBackColor = true;
            this.bUpdate.Click += new System.EventHandler(this.bUpdate_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(51, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 13);
            this.label6.TabIndex = 26;
            this.label6.Text = "Kata Sandi Baru";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(51, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 13);
            this.label7.TabIndex = 24;
            this.label7.Text = "Kata Sandi Lama";
            // 
            // ProfilManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(683, 424);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ProfilManagement";
            this.Text = "Profil Pengguna";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ProfilManagement_FormClosed);
            this.Load += new System.EventHandler(this.ProfilManagement_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ProfilManagement_Paint);
            this.tabControl1.ResumeLayout(false);
            this.tabProfil.ResumeLayout(false);
            this.tabProfil.PerformLayout();
            this.tabPassword.ResumeLayout(false);
            this.tabPassword.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabProfil;
        private System.Windows.Forms.TabPage tabPassword;
        private System.Windows.Forms.Label lNotify;
        private System.Windows.Forms.TextBox tDepartment;
        private System.Windows.Forms.TextBox tName;
        private System.Windows.Forms.TextBox tNIK;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lName;
        private System.Windows.Forms.Label lNIK;
        private System.Windows.Forms.TextBox tAuth;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tPassNewR;
        private System.Windows.Forms.Label lNotifyP;
        private System.Windows.Forms.TextBox tPassNew;
        private System.Windows.Forms.TextBox tPassOld;
        private System.Windows.Forms.Button bCancelP;
        private System.Windows.Forms.Button bUpdate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}