﻿namespace asset.User
{
    partial class requestAsset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cKategori = new System.Windows.Forms.ComboBox();
            this.tBrand = new System.Windows.Forms.TextBox();
            this.Brand = new System.Windows.Forms.Label();
            this.nHarga = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.tSpec = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tNeeds = new System.Windows.Forms.TextBox();
            this.lNotify = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nHarga)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Kategori";
            // 
            // cKategori
            // 
            this.cKategori.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cKategori.FormattingEnabled = true;
            this.cKategori.Location = new System.Drawing.Point(95, 6);
            this.cKategori.Name = "cKategori";
            this.cKategori.Size = new System.Drawing.Size(293, 21);
            this.cKategori.TabIndex = 1;
            // 
            // tBrand
            // 
            this.tBrand.Location = new System.Drawing.Point(95, 33);
            this.tBrand.Name = "tBrand";
            this.tBrand.Size = new System.Drawing.Size(293, 20);
            this.tBrand.TabIndex = 2;
            // 
            // Brand
            // 
            this.Brand.AutoSize = true;
            this.Brand.BackColor = System.Drawing.Color.Transparent;
            this.Brand.ForeColor = System.Drawing.Color.White;
            this.Brand.Location = new System.Drawing.Point(12, 36);
            this.Brand.Name = "Brand";
            this.Brand.Size = new System.Drawing.Size(35, 13);
            this.Brand.TabIndex = 3;
            this.Brand.Text = "Brand";
            // 
            // nHarga
            // 
            this.nHarga.Location = new System.Drawing.Point(95, 59);
            this.nHarga.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.nHarga.Name = "nHarga";
            this.nHarga.Size = new System.Drawing.Size(192, 20);
            this.nHarga.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(12, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Estimasi Harga";
            // 
            // tSpec
            // 
            this.tSpec.Location = new System.Drawing.Point(95, 111);
            this.tSpec.Multiline = true;
            this.tSpec.Name = "tSpec";
            this.tSpec.Size = new System.Drawing.Size(522, 257);
            this.tSpec.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(12, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Keperluan";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(513, 382);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 40);
            this.button1.TabIndex = 8;
            this.button1.Text = "Kirim";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(12, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Spesifikasi";
            // 
            // tNeeds
            // 
            this.tNeeds.Location = new System.Drawing.Point(95, 85);
            this.tNeeds.Name = "tNeeds";
            this.tNeeds.Size = new System.Drawing.Size(293, 20);
            this.tNeeds.TabIndex = 10;
            // 
            // lNotify
            // 
            this.lNotify.AutoSize = true;
            this.lNotify.BackColor = System.Drawing.Color.Transparent;
            this.lNotify.ForeColor = System.Drawing.Color.Red;
            this.lNotify.Location = new System.Drawing.Point(12, 382);
            this.lNotify.Name = "lNotify";
            this.lNotify.Size = new System.Drawing.Size(0, 13);
            this.lNotify.TabIndex = 39;
            // 
            // requestAsset
            // 
            this.AcceptButton = this.button1;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(689, 459);
            this.Controls.Add(this.lNotify);
            this.Controls.Add(this.tNeeds);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tSpec);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.nHarga);
            this.Controls.Add(this.Brand);
            this.Controls.Add(this.tBrand);
            this.Controls.Add(this.cKategori);
            this.Controls.Add(this.label1);
            this.Name = "requestAsset";
            this.Text = "Permohonan Aset Baru";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.requestAsset_FormClosed);
            this.Load += new System.EventHandler(this.requestAsset_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.requestAsset_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.nHarga)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cKategori;
        private System.Windows.Forms.TextBox tBrand;
        private System.Windows.Forms.Label Brand;
        private System.Windows.Forms.NumericUpDown nHarga;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tSpec;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tNeeds;
        private System.Windows.Forms.Label lNotify;
    }
}