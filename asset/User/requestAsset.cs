﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace asset.User
{
    public partial class requestAsset : Form
    {
        String id;
        lib lib = new lib();
        Class.process.mRequestAsset req = new Class.process.mRequestAsset();
        public requestAsset(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
        }
        private static requestAsset form;
        public static requestAsset GetChildInstance(String id)
        {
            if (form == null)
                form = new requestAsset(id);
            return form;
        }
        private void requestAsset_Load(object sender, EventArgs e)
        {
            showCategory();
        }
        void showCategory() 
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sc = new SqlCommand("select id_category,category from m_category", conn);
                SqlDataReader read;
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(string));
                dt.Columns.Add("category", typeof(string));
                read = sc.ExecuteReader();
                while (read.Read())
                {
                    dt.Rows.Add(new object[] { read["id_category"].ToString(), read["category"].ToString() });
                }

                cKategori.ValueMember = "id";
                cKategori.DisplayMember = "category";
                cKategori.DataSource = dt;

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void requestAsset_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void requestAsset_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            String brand = tBrand.Text;
            String description = tNeeds.Text;
            String specs = tSpec.Text;
            String id_category = cKategori.SelectedValue.ToString();
            String price = nHarga.Value.ToString();
            String id_employee = this.id;
            
            List<String> input = new List<String>();
            input.Add(brand);
            input.Add(description);
            input.Add(specs);
            input.Add(id_category);
            input.Add(price);
            input.Add(id_employee);
            input.Add(cKategori.Text);
            if (req.cekInput(input.ToArray()))
            {
                View.SendEmail s = new View.SendEmail();
                s.data = input.ToArray();
                s.stat = 9;
                s.form = 7;
                s.ShowDialog();

                if (asset.Class.Email.email.cek == true)
                {
                    req.insertRequestAsset(input.ToArray());
                    MessageBox.Show("Permohonan aset baru berhasil dikirim", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    clearField();
                }
                else
                {
                    MessageBox.Show("Permohonan aset baru gagal dikirim", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else {
                lNotify.Text = "Salah input!";
            }
        }
        void clearField() 
        {
            tBrand.Clear();
            tNeeds.Clear();
            tSpec.Clear();
            nHarga.Value = 0;
            showCategory();
        }
    }
}
