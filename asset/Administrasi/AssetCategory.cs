﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class AssetCategory : Form
    {
        lib lib = new lib();
        private String idC = "";
        private String namaC = "";
        Class.process.mAssetCategory asset = new Class.process.mAssetCategory();
        public AssetCategory()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }
        public String idCategory{
            get {
                return this.idC;
            }
        }
        public String namaCategory
        {
            get
            {
                return this.namaC;
            }
        }
        private void AssetCategory_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewButtonColumn a = new DataGridViewButtonColumn();
                dataGCategory.Columns.Add(a);
                a.HeaderText = "Pilih";
                a.Text = "Pilih";
                a.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn b = new DataGridViewButtonColumn();
                dataGCategory.Columns.Add(b);
                b.HeaderText = "Ubah";
                b.Text = "Ubah";
                b.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn c = new DataGridViewButtonColumn();
                dataGCategory.Columns.Add(c);
                c.HeaderText = "Hapus";
                c.Text = "Hapus";
                c.UseColumnTextForButtonValue = true;
                showdataGCategory();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showdataGCategory()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_category";
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "CategoryTable");
                conn.Close();
                this.dataGCategory.DataSource = dataSet.Tables["CategoryTable"].DefaultView;
                this.dataGCategory.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void searchCategory(String str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "search_category";
                myCommand.Parameters.AddWithValue("@str", str);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "CategoryTable");
                conn.Close();
                this.dataGCategory.DataSource = dataSet.Tables["CategoryTable"].DefaultView;
                this.dataGCategory.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void bSearch_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearch.Text;
                searchCategory(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                showdataGCategory();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            try
            {
                AddCategory add = new AddCategory();
                add.ShowDialog();
                showdataGCategory();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGCategory_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGCategory.Columns[0].Index)
                {
                    //Pilih
                    try
                    {
                        String id = dataGCategory[3, e.RowIndex].Value.ToString();
                        String nama = dataGCategory[4, e.RowIndex].Value.ToString();
                        this.idC = id;
                        this.namaC = nama;
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGCategory.Columns[1].Index)
                {
                    //Ubah
                    try
                    {
                        String id = dataGCategory[3, e.RowIndex].Value.ToString();
                        EditCategory edit = new EditCategory(id);
                        edit.ShowDialog();
                        showdataGCategory();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGCategory.Columns[2].Index)
                {
                    //Hapus
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menghapus data ini? ",
            "Konfirmasi Penghapusan", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (dr == DialogResult.Yes)
                        {

                            String id = dataGCategory[3, e.RowIndex].Value.ToString();
                            Boolean cek = asset.CekData(id);
                            if (cek == true)
                            {
                                asset.delete(id);
                                showdataGCategory();
                            }
                            else
                            {
                                MessageBox.Show("Data ini mempunyai relasi dengan tabel lain!",
                       "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void AssetCategory_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            } 
        }
    }
}
