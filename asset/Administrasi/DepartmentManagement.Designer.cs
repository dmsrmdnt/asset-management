﻿namespace asset
{
    partial class DepartmentManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tDept = new System.Windows.Forms.TextBox();
            this.lNIK = new System.Windows.Forms.Label();
            this.bCancel = new System.Windows.Forms.Button();
            this.bOK = new System.Windows.Forms.Button();
            this.lNotify = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tDept
            // 
            this.tDept.Location = new System.Drawing.Point(145, 46);
            this.tDept.Name = "tDept";
            this.tDept.Size = new System.Drawing.Size(316, 20);
            this.tDept.TabIndex = 13;
            // 
            // lNIK
            // 
            this.lNIK.AutoSize = true;
            this.lNIK.BackColor = System.Drawing.Color.Transparent;
            this.lNIK.ForeColor = System.Drawing.Color.White;
            this.lNIK.Location = new System.Drawing.Point(54, 49);
            this.lNIK.Name = "lNIK";
            this.lNIK.Size = new System.Drawing.Size(65, 13);
            this.lNIK.TabIndex = 14;
            this.lNIK.Text = "Departemen";
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(403, 147);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(120, 40);
            this.bCancel.TabIndex = 22;
            this.bCancel.Text = "Batal";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bOK
            // 
            this.bOK.Location = new System.Drawing.Point(266, 147);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(120, 40);
            this.bOK.TabIndex = 21;
            this.bOK.Text = "Update";
            this.bOK.UseVisualStyleBackColor = true;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // lNotify
            // 
            this.lNotify.AutoSize = true;
            this.lNotify.ForeColor = System.Drawing.Color.Red;
            this.lNotify.Location = new System.Drawing.Point(54, 87);
            this.lNotify.Name = "lNotify";
            this.lNotify.Size = new System.Drawing.Size(0, 13);
            this.lNotify.TabIndex = 23;
            // 
            // DepartmentManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(544, 223);
            this.Controls.Add(this.lNotify);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.tDept);
            this.Controls.Add(this.lNIK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "DepartmentManagement";
            this.Text = "Update Nama Departemen";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.DepartmentManagement_FormClosed);
            this.Load += new System.EventHandler(this.DepartmentManagement_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.DepartmentManagement_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tDept;
        private System.Windows.Forms.Label lNIK;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Label lNotify;
    }
}