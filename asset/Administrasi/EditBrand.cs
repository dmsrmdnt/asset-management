﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class EditBrand : Form
    {
        lib lib=new lib();
        Class.process.mEditBrand edit=new Class.process.mEditBrand();
        private String id=null;
        public EditBrand(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            showData(id);
        }
        public void showData(String id) {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select id_brand,brand from m_brand where id_brand='" + id + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    this.id = read["id_brand"].ToString();
                    tBrand.Text = read["brand"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            try
            {
                String category = tBrand.Text;
                String id = this.id;
                Boolean cek = edit.cekInput(id, category);

                if (cek == true)
                {
                    if (edit.cekData(category) < 1)
                    {
                        edit.update(id, category);
                        this.Close();
                    }
                    else
                    {
                        lNotify.Text = "Brand sudah ada!";
                    }
                }
                else
                {
                    lNotify.Text = "Salah input!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EditBrand_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
    }
}
