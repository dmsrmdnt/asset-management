﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class AssetListD : Form
    {
        lib lib = new lib();
        private String id;
        private String year;
        private int cat;
        public AssetListD(String id, String year, int cat)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
            this.year = year;
            this.cat = cat;
        }

        private void bSearchU_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearchU.Text;
                searchData(this.id, this.year, this.cat, str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGAsset_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGAsset.Columns[0].Index)
                {
                    //Lihat
                    try
                    {
                        String id = dataGAsset[1, e.RowIndex].Value.ToString();
                        AssetDetail asset = new AssetDetail(id);
                        asset.ShowDialog();
                        asset.BringToFront();
                        this.dataGAsset.Refresh();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void AssetListD_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewButtonColumn a = new DataGridViewButtonColumn();
                dataGAsset.Columns.Add(a);
                a.HeaderText = "Detail";
                a.Text = "Detail";
                a.UseColumnTextForButtonValue = true;

                showData(this.id, this.year, this.cat);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void searchData(String id, String year, int cat, String str)
        {
            try
            {
                String sql = "";
                switch (cat)
                {
                    case 1:
                        sql = "select a.id_asset as \"ID Aset\", g.nama as Pemilik, a.asset as \"No. Aset\", b.category as Kategori, c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                    "from m_asset a, m_category b, m_brand c, m_location d, m_status e, t_asset f, m_employee g " +
                    "where a.asset like '%" + str + "%' and LEFT(a.pur_date,4)='" + year + "' and a.id_category='" + id + "' and a.id_asset=f.id_asset and g.id_employee=f.id_employee and a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status";
                        break;
                    case 2:
                        sql = "select a.id_asset as \"ID Aset\", g.nama as Pemilik, a.asset as \"No. Aset\", b.category as Kategori, c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                    "from m_asset a, m_category b, m_brand c, m_location d, m_status e, t_asset f, m_employee g " +
                    "where a.asset like '%" + str + "%' and  LEFT(a.pur_date,4)='" + year + "' and g.id_employee='" + id + "' and a.id_asset=f.id_asset and g.id_employee=f.id_employee and a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status";
                        break;
                    case 3:
                        sql = "select a.id_asset as \"ID Aset\", g.nama as Pemilik, a.asset as \"No. Aset\", b.category as Kategori, c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                    "from m_asset a, m_category b, m_brand c, m_location d, m_status e, t_asset f, m_employee g " +
                    "where a.asset like '%" + str + "%' and LEFT(a.pur_date,4)='" + year + "' and a.id_brand='" + id + "' and a.id_asset=f.id_asset and g.id_employee=f.id_employee and a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status";
                        break;
                    case 4:
                        sql = "select a.id_asset as \"ID Aset\", g.nama as Pemilik, a.asset as \"No. Aset\", b.category as Kategori, c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                    "from m_asset a, m_category b, m_brand c, m_location d, m_status e, t_asset f, m_employee g " +
                    "where a.asset like '%" + str + "%' and LEFT(a.pur_date,4)='" + year + "' and a.id_location='" + id + "' and a.id_asset=f.id_asset and g.id_employee=f.id_employee and a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status";
                        break;
                }
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter(sql, conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGAsset.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGAsset.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void showData(String id, String year, int cat) 
        {
            try
            {
                String sql = "";
                switch (cat)
                {
                    case 1:
                        sql = "select a.id_asset as \"ID Aset\", g.nama as Pemilik, a.asset as \"No. Aset\", b.category as Kategori, c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                    "from m_asset a, m_category b, m_brand c, m_location d, m_status e, t_asset f, m_employee g " +
                    "where LEFT(a.pur_date,4)='" + year + "' and a.id_category='" + id + "' and a.id_asset=f.id_asset and g.id_employee=f.id_employee and a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status";
                        break;
                    case 2:
                        sql = "select a.id_asset as \"ID Aset\", g.nama as Pemilik, a.asset as \"No. Aset\", b.category as Kategori, c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                    "from m_asset a, m_category b, m_brand c, m_location d, m_status e, t_asset f, m_employee g " +
                    "where LEFT(a.pur_date,4)='" + year + "' and g.id_employee='" + id + "' and a.id_asset=f.id_asset and g.id_employee=f.id_employee and a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status";
                        break;
                    case 3:
                        sql = "select a.id_asset as \"ID Aset\", g.nama as Pemilik, a.asset as \"No. Aset\", b.category as Kategori, c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                    "from m_asset a, m_category b, m_brand c, m_location d, m_status e, t_asset f, m_employee g " +
                    "where LEFT(a.pur_date,4)='" + year + "' and a.id_brand='" + id + "' and a.id_asset=f.id_asset and g.id_employee=f.id_employee and a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status";
                        break;
                    case 4:
                        sql = "select a.id_asset as \"ID Aset\", g.nama as Pemilik, a.asset as \"No. Aset\", b.category as Kategori, c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                    "from m_asset a, m_category b, m_brand c, m_location d, m_status e, t_asset f, m_employee g " +
                    "where LEFT(a.pur_date,4)='" + year + "' and a.id_location='" + id + "' and a.id_asset=f.id_asset and g.id_employee=f.id_employee and a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status";
                        break;
                }
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter(sql, conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGAsset.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGAsset.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AssetListD_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            } 
        }
    }
}
