﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class AssetPIC : Form
    {
        Class.process.mAssetPIC asset = new Class.process.mAssetPIC();
        private String idB = "";
        private String namaB = "";
        private String alamatB = "";
        private String telpB = "";
        lib lib = new lib();
        public AssetPIC()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }
        public String idPIC
        {
            get
            {
                return this.idB; 
            }
        }
        public String namaPIC
        {
            get
            {
                return this.namaB; 
            }
        }
        public String alamatPIC
        {
            get
            {
                return this.alamatB;
            }
        }
        public String telpPIC
        {
            get
            {
                return this.telpB;
            }
        }
        private void bSearch_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearch.Text;
                searchBrand(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void searchBrand(String str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "search_PIC";
                myCommand.Parameters.AddWithValue("@str", str);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "BrandTable");
                conn.Close();
                this.dataGBrand.DataSource = dataSet.Tables["BrandTable"].DefaultView;
                this.dataGBrand.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AssetBrand_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewButtonColumn a = new DataGridViewButtonColumn();
                dataGBrand.Columns.Add(a);
                a.HeaderText = "Pilih";
                a.Text = "Pilih";
                a.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn b = new DataGridViewButtonColumn();
                dataGBrand.Columns.Add(b);
                b.HeaderText = "Ubah";
                b.Text = "Ubah";
                b.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn c = new DataGridViewButtonColumn();
                dataGBrand.Columns.Add(c);
                c.HeaderText = "Hapus";
                c.Text = "Hapus";
                c.UseColumnTextForButtonValue = true;

                showdataGBrand();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showdataGBrand()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_PIC";
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "BrandTable");
                conn.Close();
                this.dataGBrand.DataSource = dataSet.Tables["BrandTable"].DefaultView;
                this.dataGBrand.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                showdataGBrand();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            try
            {
                AddPIC add = new AddPIC();
                add.ShowDialog();
                showdataGBrand();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGBrand_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGBrand.Columns[0].Index)
                {
                    //Pilih
                    try
                    {
                        String id = dataGBrand[3, e.RowIndex].Value.ToString();
                        String nama = dataGBrand[4, e.RowIndex].Value.ToString();
                        String alamat = dataGBrand[5, e.RowIndex].Value.ToString();
                        String telp = dataGBrand[6, e.RowIndex].Value.ToString();
                        this.idB = id;
                        this.namaB = nama;
                        this.alamatB = alamat;
                        this.telpB = telp;
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGBrand.Columns[1].Index)
                {
                    //Ubah
                    try
                    {
                        String id = dataGBrand[3, e.RowIndex].Value.ToString();
                        EditPIC edit = new EditPIC(id);
                        edit.ShowDialog();
                        showdataGBrand();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGBrand.Columns[2].Index)
                {
                    //Hapus
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menghapus data ini? ",
            "Konfirmasi Penghapusan", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (dr == DialogResult.Yes)
                        {

                            String id = dataGBrand[3, e.RowIndex].Value.ToString();
                            Boolean cek = asset.CekData(id);
                            if (cek == true)
                            {
                                asset.delete(id);
                                showdataGBrand();
                            }
                            else
                            {
                                MessageBox.Show("Data ini mempunyai relasi dengan tabel lain!",
                       "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void AssetBrand_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            } 
        }
    }
}
