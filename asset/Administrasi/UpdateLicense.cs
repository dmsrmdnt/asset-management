﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class UpdateLicense : Form
    {
        lib lib = new lib();
        Class.process.mUpdateLicense add = new Class.process.mUpdateLicense();
        private String id;
        public UpdateLicense(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            try
            {
                String license = tNLicenseB.Text;
                String exp_date = expDateB.Text;
                String exp_date2 = expDateL.Text;
                Boolean cek = add.cekInput(license, exp_date);
                if (cek == true)
                {
                    if (add.cekData(license, this.id))
                    {
                        if (add.cekTanggal(Convert.ToDateTime(exp_date), Convert.ToDateTime(exp_date2)))
                        {
                            add.insert(license, exp_date, this.id);
                            this.Close();
                        }
                        else
                        {
                            lNotify.Text = "Expired date tidak valid!";
                        }
                    }
                    else
                    {
                        lNotify.Text = "Nomor Lisensi sudah ada!";
                    }
                }
                else
                {
                    lNotify.Text = "Salah input!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UpdateLicense_Load(object sender, EventArgs e)
        {
            try
            {
                showData(this.id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showData(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_license";
                myCommand.Parameters.AddWithValue("@id_license", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    tSoftL.Text = read["nama"].ToString();
                    tSoftB.Text = read["nama"].ToString();
                    tNLicenseL.Text = read["license"].ToString();
                    expDateL.Text = read["exp_date"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UpdateLicense_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
    }
}
