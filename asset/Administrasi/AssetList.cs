﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{

    public partial class AssetList : Form
    {
        lib lib = new lib();
        private String id;
        public AssetList(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
        }
        private static AssetList form;
        public static AssetList GetChildInstance(String id)
        {
            if (form == null)
                form = new AssetList(id);
            return form;
        }

        private void AssetList_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewButtonColumn a = new DataGridViewButtonColumn();
                dataGUser.Columns.Add(a);
                a.HeaderText = "Daftar Aset";
                a.Text = "Lihat";
                a.UseColumnTextForButtonValue = true;

                showdataGUser();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showdataGUser()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_user";
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "UserTable");
                conn.Close();
                this.dataGUser.DataSource = dataSet.Tables["UserTable"].DefaultView;
                this.dataGUser.Columns["ID Karyawan"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void bSearch_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearch.Text;
                searchUser(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void searchUser(String str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "search_data_user";
                myCommand.Parameters.AddWithValue("@str", str);
                SqlDataAdapter data = new SqlDataAdapter();//"Select a.id_employee as \"ID Karyawan\", a.email as Email, a.nama as Nama, b.dept as Departemen from m_employee a, m_dept b where a.id_auth<>'0' and a.nama like'%" + str + "%'", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "UserTable");
                conn.Close();
                this.dataGUser.DataSource = dataSet.Tables["UserTable"].DefaultView;
                this.dataGUser.Columns["ID Karyawan"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void bMyAsset_Click(object sender, EventArgs e)
        {
            try
            {
                AssetListDetail asset = new AssetListDetail(this.id);
                asset.ShowDialog();
                asset.BringToFront();
                this.dataGUser.Refresh();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AssetList_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void dataGUser_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGUser.Columns[0].Index)
                {
                    //Lihat
                    try
                    {
                        String id = dataGUser[1, e.RowIndex].Value.ToString();
                        AssetListDetail asset = new AssetListDetail(id);
                        asset.ShowDialog();
                        asset.BringToFront();
                        this.dataGUser.Refresh();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void AssetList_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            } 
        }
    }

}
