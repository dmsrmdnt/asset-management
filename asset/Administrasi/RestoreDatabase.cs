﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset.Administrasi
{
    public partial class RestoreDatabase : Form
    {
        lib lib = new lib();
        public RestoreDatabase()
        {
            InitializeComponent();
        }
        private static RestoreDatabase form;
        public static RestoreDatabase GetChildInstance()
        {
            if (form == null)
                form = new RestoreDatabase();
            return form;
        }

        private void RestoreDatabase_Load(object sender, EventArgs e)
        {

        }

        private void RestoreDatabase_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            } 
        }

        private void RestoreDatabase_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fDialog = new OpenFileDialog();
                fDialog.Title = "Pilih file untuk diunggah";
                fDialog.Filter = "File Backup|*.bak";
                if (fDialog.ShowDialog() == DialogResult.OK)
                {
                    tFile.Text = fDialog.FileName.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (tFile.Text != null && tFile.Text != "")
            {
                View.BackupDatabase backup = new View.BackupDatabase();
                backup.file = tFile.Text;
                backup.title = "Restore Database";
                backup.label = "Proses Restore Database...";
                backup.form = 1;
                backup.ShowDialog();
                backup.BringToFront();

                if (asset.Class.backupDB.backup.cek == true)
                {
                    MessageBox.Show("Restore database berhasil", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    tFile.Clear();
                }
                else
                {
                    MessageBox.Show("Restore database gagal. Proses restore hanya dapat dilakukan di komputer server dan di luar partisi sistem", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else 
            {
                MessageBox.Show("Input belum lengkap!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
