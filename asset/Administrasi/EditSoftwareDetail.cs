﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class EditSoftwareDetail : Form
    {
        lib lib = new lib();
        mEditSoftwareDetail edit = new mEditSoftwareDetail();
        private String id = null;
        private String userid = null;
        public EditSoftwareDetail(String id, String userid)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
            this.userid = userid;
        }

        private void EditSoftwareDetail_Load(object sender, EventArgs e)
        {
            try
            {
                showData(id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showData(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select id_software,nama from m_software where id_software='" + id + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    this.id = read["id_software"].ToString();
                    tSoft.Text = read["nama"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            try
            {
                String category = tSoft.Text;
                String id = this.id;
                Boolean cek = edit.cekInput(id, category);

                if (cek == true)
                {
                    if (edit.cekData(category) <= 2)
                    {
                        edit.update(id, category, this.userid);
                        this.Close();
                    }
                    else
                    {
                        lNotify.Text = "Nama Software sudah ada!";
                    }
                }
                else
                {
                    lNotify.Text = "Salah input!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EditSoftwareDetail_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
    }
    public class mEditSoftwareDetail
    {
        lib lib = new lib();
        public void update(String id, String brand, String userid)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("update m_software set nama='" + brand + "',userid='" + userid + "' where id_software='" + id + "'", conn);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean cekInput(String id, String brand)
        {
            if (id == "" || brand == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public Int32 cekData(String brand)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select count(*) from m_software where nama='" + brand + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
    }
}
