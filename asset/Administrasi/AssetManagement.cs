﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class AssetManagement : Form
    {
        lib lib = new lib();
        Class.process.mAssetManagement asset = new Class.process.mAssetManagement();
        private String id;
        public AssetManagement(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
        }
        private static AssetManagement form;
        public static AssetManagement GetChildInstance(String id)
        {
            if (form == null)
                form = new AssetManagement(id);
            return form;
        }
        private void AssetManagement_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewButtonColumn a = new DataGridViewButtonColumn();
                dataGAsset.Columns.Add(a);
                a.HeaderText = "Ubah";
                a.Text = "Ubah";
                a.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn b = new DataGridViewButtonColumn();
                dataGAsset.Columns.Add(b);
                b.HeaderText = "Hapus";
                b.Text = "Hapus";
                b.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn c = new DataGridViewButtonColumn();
                dataGAsset.Columns.Add(c);
                c.HeaderText = "Detail";
                c.Text = "Detail";
                c.UseColumnTextForButtonValue = true;

                showdataGAsset();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showdataGAsset()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_assetmng";
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGAsset.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGAsset.Columns["ID"].Visible = false;
                this.dataGAsset.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                showdataGAsset();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            try
            {
                AddAsset fA = new AddAsset(this.id);
                fA.ShowDialog();
                showdataGAsset();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void AssetManagement_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                form = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bSearch_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearch.Text;
                searchModel(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void searchModel(String str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "search_data_assetmng";
                myCommand.Parameters.AddWithValue("@str", str);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGAsset.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGAsset.Columns["ID"].Visible = false;
                this.dataGAsset.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGAsset_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGAsset.Columns[0].Index)
                {
                    //Ubah
                    try
                    {
                        String id = dataGAsset[5, e.RowIndex].Value.ToString();
                        EditAsset edit = new EditAsset(id,this.id);
                        edit.ShowDialog();
                        showdataGAsset();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAsset.Columns[1].Index)
                {
                    //Hapus
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menghapus data ini ? ",
                        "Konfirmasi Penghapusan", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (dr == DialogResult.Yes)
                        {
                            String id = dataGAsset[5, e.RowIndex].Value.ToString();
                            String id_employee = dataGAsset[3, e.RowIndex].Value.ToString();
                            Boolean cek = asset.CekData(id);
                            if (cek == true)
                            {

                                View.SendEmail s = new View.SendEmail();
                                s.id_employee = id_employee;
                                s.id_asset = id;
                                s.stat = 8;
                                s.indexStatus = 4;
                                s.form = 6;
                                s.ShowDialog();
                                if (Class.Email.email.cek == true)
                                {
                                    asset.delete(id, id_employee);
                                    showdataGAsset();
                                    MessageBox.Show("Aset berhasil dihapus", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    MessageBox.Show("Aset gagal dihapus", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                
                            }
                            else
                            {
                                MessageBox.Show("Data ini mempunyai relasi dengan tabel lain!",
                       "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAsset.Columns[2].Index)
                {
                    //Detail
                    try
                    {
                        String id = dataGAsset[5, e.RowIndex].Value.ToString();
                        AssetDetail asset = new AssetDetail(id);
                        asset.ShowDialog();
                        asset.BringToFront();
                        this.dataGAsset.Refresh();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void AssetManagement_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

    }
}
