﻿namespace asset.Administrasi
{
    partial class SearchAsset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tNama = new System.Windows.Forms.TextBox();
            this.tEmail = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bSearch = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.cStatus = new System.Windows.Forms.ComboBox();
            this.cLokasi = new System.Windows.Forms.ComboBox();
            this.cBrand = new System.Windows.Forms.ComboBox();
            this.cKategori = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.dAkhir = new System.Windows.Forms.DateTimePicker();
            this.dAwal = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tHarga2 = new System.Windows.Forms.NumericUpDown();
            this.tHarga1 = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tNoSeri = new System.Windows.Forms.TextBox();
            this.tModel = new System.Windows.Forms.TextBox();
            this.tItem = new System.Windows.Forms.TextBox();
            this.tNoAsset = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGSearch = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tHarga2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tHarga1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.tNama);
            this.groupBox1.Controls.Add(this.tEmail);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(912, 73);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pemilik Aset";
            // 
            // tNama
            // 
            this.tNama.Location = new System.Drawing.Point(130, 39);
            this.tNama.Name = "tNama";
            this.tNama.Size = new System.Drawing.Size(421, 20);
            this.tNama.TabIndex = 2;
            this.tNama.TextChanged += new System.EventHandler(this.tEmail_TextChanged);
            // 
            // tEmail
            // 
            this.tEmail.Location = new System.Drawing.Point(130, 13);
            this.tEmail.Name = "tEmail";
            this.tEmail.Size = new System.Drawing.Size(421, 20);
            this.tEmail.TabIndex = 1;
            this.tEmail.TextChanged += new System.EventHandler(this.tEmail_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nama";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Email";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.bSearch);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.cStatus);
            this.groupBox2.Controls.Add(this.cLokasi);
            this.groupBox2.Controls.Add(this.cBrand);
            this.groupBox2.Controls.Add(this.cKategori);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.dAkhir);
            this.groupBox2.Controls.Add(this.dAwal);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.tHarga2);
            this.groupBox2.Controls.Add(this.tHarga1);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.tNoSeri);
            this.groupBox2.Controls.Add(this.tModel);
            this.groupBox2.Controls.Add(this.tItem);
            this.groupBox2.Controls.Add(this.tNoAsset);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(12, 91);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(912, 193);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Informasi Aset";
            // 
            // bSearch
            // 
            this.bSearch.ForeColor = System.Drawing.Color.Black;
            this.bSearch.Location = new System.Drawing.Point(588, 126);
            this.bSearch.Name = "bSearch";
            this.bSearch.Size = new System.Drawing.Size(299, 46);
            this.bSearch.TabIndex = 27;
            this.bSearch.Text = "Cari";
            this.bSearch.UseVisualStyleBackColor = true;
            this.bSearch.Click += new System.EventHandler(this.bSearch_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(587, 103);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(76, 13);
            this.label14.TabIndex = 26;
            this.label14.Text = "Status/Kondisi";
            // 
            // cStatus
            // 
            this.cStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cStatus.FormattingEnabled = true;
            this.cStatus.Location = new System.Drawing.Point(666, 99);
            this.cStatus.Name = "cStatus";
            this.cStatus.Size = new System.Drawing.Size(221, 21);
            this.cStatus.TabIndex = 25;
            this.cStatus.SelectedIndexChanged += new System.EventHandler(this.cStatus_SelectedIndexChanged);
            // 
            // cLokasi
            // 
            this.cLokasi.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cLokasi.FormattingEnabled = true;
            this.cLokasi.Location = new System.Drawing.Point(666, 73);
            this.cLokasi.Name = "cLokasi";
            this.cLokasi.Size = new System.Drawing.Size(221, 21);
            this.cLokasi.TabIndex = 24;
            this.cLokasi.SelectedIndexChanged += new System.EventHandler(this.cLokasi_SelectedIndexChanged);
            // 
            // cBrand
            // 
            this.cBrand.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cBrand.FormattingEnabled = true;
            this.cBrand.Location = new System.Drawing.Point(666, 47);
            this.cBrand.Name = "cBrand";
            this.cBrand.Size = new System.Drawing.Size(221, 21);
            this.cBrand.TabIndex = 23;
            this.cBrand.SelectedIndexChanged += new System.EventHandler(this.cBrand_SelectedIndexChanged);
            // 
            // cKategori
            // 
            this.cKategori.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cKategori.FormattingEnabled = true;
            this.cKategori.Location = new System.Drawing.Point(666, 22);
            this.cKategori.Name = "cKategori";
            this.cKategori.Size = new System.Drawing.Size(221, 21);
            this.cKategori.TabIndex = 22;
            this.cKategori.SelectedIndexChanged += new System.EventHandler(this.cKategori_SelectedIndexChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(587, 77);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "Lokasi";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(587, 51);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "Brand";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(587, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 13);
            this.label11.TabIndex = 19;
            this.label11.Text = "Kategori";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(334, 154);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 18;
            this.label10.Text = "s/d";
            // 
            // dAkhir
            // 
            this.dAkhir.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dAkhir.Location = new System.Drawing.Point(363, 152);
            this.dAkhir.Name = "dAkhir";
            this.dAkhir.Size = new System.Drawing.Size(188, 20);
            this.dAkhir.TabIndex = 17;
            this.dAkhir.ValueChanged += new System.EventHandler(this.dAwal_ValueChanged);
            // 
            // dAwal
            // 
            this.dAwal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dAwal.Location = new System.Drawing.Point(130, 152);
            this.dAwal.Name = "dAwal";
            this.dAwal.Size = new System.Drawing.Size(188, 20);
            this.dAwal.TabIndex = 16;
            this.dAwal.Value = new System.DateTime(2000, 12, 25, 0, 0, 0, 0);
            this.dAwal.ValueChanged += new System.EventHandler(this.dAwal_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 154);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 13);
            this.label9.TabIndex = 15;
            this.label9.Text = "Tanggal Pembelian";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 128);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(36, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Harga";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(334, 128);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "s/d";
            // 
            // tHarga2
            // 
            this.tHarga2.Location = new System.Drawing.Point(363, 126);
            this.tHarga2.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.tHarga2.Name = "tHarga2";
            this.tHarga2.Size = new System.Drawing.Size(188, 20);
            this.tHarga2.TabIndex = 12;
            this.tHarga2.Value = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.tHarga2.ValueChanged += new System.EventHandler(this.tHarga1_ValueChanged);
            // 
            // tHarga1
            // 
            this.tHarga1.Location = new System.Drawing.Point(130, 126);
            this.tHarga1.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.tHarga1.Name = "tHarga1";
            this.tHarga1.Size = new System.Drawing.Size(188, 20);
            this.tHarga1.TabIndex = 11;
            this.tHarga1.ValueChanged += new System.EventHandler(this.tHarga1_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(59, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Nomor Seri";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Model";
            // 
            // tNoSeri
            // 
            this.tNoSeri.Location = new System.Drawing.Point(130, 100);
            this.tNoSeri.Name = "tNoSeri";
            this.tNoSeri.Size = new System.Drawing.Size(421, 20);
            this.tNoSeri.TabIndex = 7;
            this.tNoSeri.TextChanged += new System.EventHandler(this.tEmail_TextChanged);
            // 
            // tModel
            // 
            this.tModel.Location = new System.Drawing.Point(130, 74);
            this.tModel.Name = "tModel";
            this.tModel.Size = new System.Drawing.Size(421, 20);
            this.tModel.TabIndex = 6;
            this.tModel.TextChanged += new System.EventHandler(this.tEmail_TextChanged);
            // 
            // tItem
            // 
            this.tItem.Location = new System.Drawing.Point(130, 48);
            this.tItem.Name = "tItem";
            this.tItem.Size = new System.Drawing.Size(421, 20);
            this.tItem.TabIndex = 5;
            this.tItem.TextChanged += new System.EventHandler(this.tEmail_TextChanged);
            // 
            // tNoAsset
            // 
            this.tNoAsset.Location = new System.Drawing.Point(130, 22);
            this.tNoAsset.Name = "tNoAsset";
            this.tNoAsset.Size = new System.Drawing.Size(421, 20);
            this.tNoAsset.TabIndex = 3;
            this.tNoAsset.TextChanged += new System.EventHandler(this.tEmail_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Item";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Nomor Aset";
            // 
            // dataGSearch
            // 
            this.dataGSearch.AllowUserToAddRows = false;
            this.dataGSearch.AllowUserToDeleteRows = false;
            this.dataGSearch.AllowUserToResizeColumns = false;
            this.dataGSearch.AllowUserToResizeRows = false;
            this.dataGSearch.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGSearch.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGSearch.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGSearch.Location = new System.Drawing.Point(12, 299);
            this.dataGSearch.MultiSelect = false;
            this.dataGSearch.Name = "dataGSearch";
            this.dataGSearch.ReadOnly = true;
            this.dataGSearch.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGSearch.Size = new System.Drawing.Size(912, 197);
            this.dataGSearch.TabIndex = 19;
            this.dataGSearch.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGSearch_CellContentClick);
            // 
            // SearchAsset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(936, 508);
            this.Controls.Add(this.dataGSearch);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "SearchAsset";
            this.Text = "Informasi Aset";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SearchAsset_FormClosed);
            this.Load += new System.EventHandler(this.SearchAsset_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.SearchAsset_Paint);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tHarga2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tHarga1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGSearch)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tNama;
        private System.Windows.Forms.TextBox tEmail;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tNoSeri;
        private System.Windows.Forms.TextBox tModel;
        private System.Windows.Forms.TextBox tItem;
        private System.Windows.Forms.TextBox tNoAsset;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown tHarga1;
        private System.Windows.Forms.NumericUpDown tHarga2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cLokasi;
        private System.Windows.Forms.ComboBox cBrand;
        private System.Windows.Forms.ComboBox cKategori;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dAkhir;
        private System.Windows.Forms.DateTimePicker dAwal;
        private System.Windows.Forms.DataGridView dataGSearch;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cStatus;
        private System.Windows.Forms.Button bSearch;
    }
}