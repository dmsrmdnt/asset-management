﻿namespace asset
{
    partial class AssetList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGUser = new System.Windows.Forms.DataGridView();
            this.gSearch = new System.Windows.Forms.GroupBox();
            this.tSearch = new System.Windows.Forms.TextBox();
            this.bSearch = new System.Windows.Forms.Button();
            this.bMyAsset = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGUser)).BeginInit();
            this.gSearch.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGUser
            // 
            this.dataGUser.AllowUserToAddRows = false;
            this.dataGUser.AllowUserToDeleteRows = false;
            this.dataGUser.AllowUserToResizeColumns = false;
            this.dataGUser.AllowUserToResizeRows = false;
            this.dataGUser.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGUser.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGUser.Location = new System.Drawing.Point(12, 61);
            this.dataGUser.MultiSelect = false;
            this.dataGUser.Name = "dataGUser";
            this.dataGUser.ReadOnly = true;
            this.dataGUser.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGUser.Size = new System.Drawing.Size(994, 380);
            this.dataGUser.TabIndex = 0;
            this.dataGUser.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGUser_CellContentClick);
            // 
            // gSearch
            // 
            this.gSearch.BackColor = System.Drawing.Color.Transparent;
            this.gSearch.Controls.Add(this.tSearch);
            this.gSearch.Controls.Add(this.bSearch);
            this.gSearch.ForeColor = System.Drawing.Color.White;
            this.gSearch.Location = new System.Drawing.Point(296, 11);
            this.gSearch.Name = "gSearch";
            this.gSearch.Size = new System.Drawing.Size(710, 44);
            this.gSearch.TabIndex = 1;
            this.gSearch.TabStop = false;
            this.gSearch.Text = "Pemilik";
            // 
            // tSearch
            // 
            this.tSearch.Location = new System.Drawing.Point(6, 17);
            this.tSearch.Name = "tSearch";
            this.tSearch.Size = new System.Drawing.Size(604, 20);
            this.tSearch.TabIndex = 1;
            this.tSearch.TextChanged += new System.EventHandler(this.bSearch_Click);
            // 
            // bSearch
            // 
            this.bSearch.ForeColor = System.Drawing.Color.Black;
            this.bSearch.Location = new System.Drawing.Point(616, 15);
            this.bSearch.Name = "bSearch";
            this.bSearch.Size = new System.Drawing.Size(75, 23);
            this.bSearch.TabIndex = 2;
            this.bSearch.Text = "Cari";
            this.bSearch.UseVisualStyleBackColor = true;
            this.bSearch.Click += new System.EventHandler(this.bSearch_Click);
            // 
            // bMyAsset
            // 
            this.bMyAsset.Location = new System.Drawing.Point(12, 11);
            this.bMyAsset.Name = "bMyAsset";
            this.bMyAsset.Size = new System.Drawing.Size(278, 44);
            this.bMyAsset.TabIndex = 0;
            this.bMyAsset.Text = "Daftar Aset";
            this.bMyAsset.UseVisualStyleBackColor = true;
            this.bMyAsset.Click += new System.EventHandler(this.bMyAsset_Click);
            // 
            // AssetList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 456);
            this.Controls.Add(this.bMyAsset);
            this.Controls.Add(this.gSearch);
            this.Controls.Add(this.dataGUser);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AssetList";
            this.Text = "Daftar Aset";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AssetList_FormClosed);
            this.Load += new System.EventHandler(this.AssetList_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AssetList_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.dataGUser)).EndInit();
            this.gSearch.ResumeLayout(false);
            this.gSearch.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGUser;
        private System.Windows.Forms.GroupBox gSearch;
        private System.Windows.Forms.TextBox tSearch;
        private System.Windows.Forms.Button bSearch;
        private System.Windows.Forms.Button bMyAsset;
    }
}