﻿namespace asset
{
    partial class LicenseManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tSearch = new System.Windows.Forms.TextBox();
            this.bSearch = new System.Windows.Forms.Button();
            this.dataGSoft = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGSoft)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.dataGSoft);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(950, 292);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Daftar Software";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tSearch);
            this.groupBox5.Controls.Add(this.bSearch);
            this.groupBox5.ForeColor = System.Drawing.Color.White;
            this.groupBox5.Location = new System.Drawing.Point(6, 19);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(938, 57);
            this.groupBox5.TabIndex = 33;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Nama Software";
            // 
            // tSearch
            // 
            this.tSearch.Location = new System.Drawing.Point(6, 19);
            this.tSearch.Name = "tSearch";
            this.tSearch.Size = new System.Drawing.Size(845, 20);
            this.tSearch.TabIndex = 1;
            this.tSearch.TextChanged += new System.EventHandler(this.bSearch_Click);
            // 
            // bSearch
            // 
            this.bSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bSearch.ForeColor = System.Drawing.Color.Black;
            this.bSearch.Location = new System.Drawing.Point(857, 17);
            this.bSearch.Name = "bSearch";
            this.bSearch.Size = new System.Drawing.Size(75, 23);
            this.bSearch.TabIndex = 0;
            this.bSearch.Text = "Cari";
            this.bSearch.UseVisualStyleBackColor = true;
            this.bSearch.Click += new System.EventHandler(this.bSearch_Click);
            // 
            // dataGSoft
            // 
            this.dataGSoft.AllowUserToAddRows = false;
            this.dataGSoft.AllowUserToDeleteRows = false;
            this.dataGSoft.AllowUserToResizeColumns = false;
            this.dataGSoft.AllowUserToResizeRows = false;
            this.dataGSoft.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGSoft.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGSoft.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGSoft.Location = new System.Drawing.Point(6, 82);
            this.dataGSoft.MultiSelect = false;
            this.dataGSoft.Name = "dataGSoft";
            this.dataGSoft.ReadOnly = true;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.dataGSoft.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGSoft.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGSoft.Size = new System.Drawing.Size(938, 204);
            this.dataGSoft.TabIndex = 7;
            this.dataGSoft.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGSoft_CellContentClick);
            // 
            // LicenseManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 316);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "LicenseManagement";
            this.Text = "Update Lisensi Software";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.LicenseManagement_FormClosed);
            this.Load += new System.EventHandler(this.LicenseManagement_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.LicenseManagement_Paint);
            this.groupBox1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGSoft)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGSoft;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox tSearch;
        private System.Windows.Forms.Button bSearch;
    }
}