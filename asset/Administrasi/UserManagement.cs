﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class UserManagement : Form
    {
        lib lib = new lib();
        public string id="";
        Class.process.mUserManagement Usr = new Class.process.mUserManagement();
        public UserManagement()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }
        private static UserManagement form;
        public static UserManagement GetChildInstance()
        {
            if (form == null)
                form = new UserManagement();
            return form;
        }
        private void UserManagement_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewButtonColumn a = new DataGridViewButtonColumn();
                dataGUser.Columns.Add(a);
                a.HeaderText = "Edit";
                a.Text = "Edit";
                a.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn b = new DataGridViewButtonColumn();
                dataGUser.Columns.Add(b);
                b.HeaderText = "Delete";
                b.Text = "Delete";
                b.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn c = new DataGridViewButtonColumn();
                dataGUser.Columns.Add(c);
                c.HeaderText = "Reset Password";
                c.Text = "Reset";
                c.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn d = new DataGridViewButtonColumn();
                dataGUser.Columns.Add(d);
                d.HeaderText = "Resign Employee";
                d.Text = "Resign";
                d.UseColumnTextForButtonValue = true;

                showdataGUser();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showdataGUser()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_user";
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "UserTable");
                conn.Close();
                this.dataGUser.DataSource = dataSet.Tables["UserTable"].DefaultView;
                this.dataGUser.Columns["ID Karyawan"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            try
            {
                AddUser fA = new AddUser();
                fA.ShowDialog();
                showdataGUser();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                showdataGUser();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UserManagement_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void bSearch_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearch.Text;
                searchNama(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void searchNama(String str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "search_data_user";
                myCommand.Parameters.AddWithValue("@str", str);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "UserTable");
                conn.Close();
                this.dataGUser.DataSource = dataSet.Tables["UserTable"].DefaultView;
                this.dataGUser.Columns["ID Karyawan"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGUser_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGUser.Columns[0].Index)
                {
                    //Ubah
                    try
                    {
                        String id = dataGUser[4, e.RowIndex].Value.ToString();
                        EditUser edit = new EditUser(id);
                        edit.ShowDialog();
                        showdataGUser();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGUser.Columns[1].Index)
                {
                    //Hapus
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menghapus data ini ? ",
                        "Konfirmasi Penghapusan", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (dr == DialogResult.Yes)
                        {
                            String id = dataGUser[4, e.RowIndex].Value.ToString();
                            Boolean cek = Usr.CekData(id);
                            if (cek == true)
                            {
                                Usr.delete(id);
                                showdataGUser();
                            }
                            else
                            {
                                MessageBox.Show("Data ini mempunyai relasi dengan tabel lain!",
                       "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGUser.Columns[2].Index)
                {
                    //Reset
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk me-reset kata sandi baris ini ? ",
                        "Konfirmasi reset kata sandi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            String id = dataGUser[4, e.RowIndex].Value.ToString();
                            Usr.reset(id);
                            showdataGUser();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGUser.Columns[3].Index)
                {
                    //Resign
                    try
                    {
                            String id = dataGUser[4, e.RowIndex].Value.ToString();
                            if (id == this.id) {
                                MessageBox.Show("Anda tidak dapat resign diri anda sendiri", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            else {
                                Administrasi.UserResign s = new Administrasi.UserResign();
                                s.id_employee = id;
                                s.id_admin = this.id;
                                s.ShowDialog();
                                s.BringToFront();
                            }
                            showdataGUser();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void UserManagement_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r=this.ClientRectangle;
            if(r.Width>0 && r.Height>0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
            e.Graphics.FillRectangle(br, r); 
            }
        }

    }
}
