﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class EditPIC : Form
    {
        lib lib=new lib();
        mEditPIC edit=new mEditPIC();
        private String id=null;
        public EditPIC(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            showData(id);
        }
        public void showData(String id) {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select id_pic,pic,address,phone from m_pic where id_pic='" + id + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    this.id = read["id_pic"].ToString();
                    tPIC.Text = read["pic"].ToString();
                    tAlamat.Text = read["address"].ToString();
                    tTelp.Text = read["phone"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            try
            {
                String pic = tPIC.Text;
                String alamat = tAlamat.Text;
                String telp = tTelp.Text;
                String id = this.id;
                Boolean cek = edit.cekInput(id, pic,alamat,telp);

                if (cek == true)
                {
                    if (edit.cekData(pic) < 1)
                    {
                        edit.update(id, pic, alamat, telp);
                        this.Close();
                    }
                    else
                    {
                        lNotify.Text = "PIC sudah ada!";
                    }
                }
                else
                {
                    lNotify.Text = "Salah input!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EditBrand_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
    }
    public class mEditPIC
    {
        lib lib = new lib();
        public void update(String id, String pic, String alamat, String telp)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("update m_pic set pic='" + pic + "',address='" + alamat + "',phone='" + telp + "' where id_pic='" + id + "'", conn);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public Boolean cekInput(String id, String pic, String alamat, String telp)
        {
            if (id == "" || pic == "" || alamat == "" || telp == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public Int32 cekData(String pic)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select count(*) from m_pic where pic='" + pic + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
    }
}
