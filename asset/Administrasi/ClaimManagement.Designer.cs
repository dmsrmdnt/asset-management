﻿namespace asset
{
    partial class ClaimManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Kehilangan = new System.Windows.Forms.TabPage();
            this.dataGAssetL = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tSearchL = new System.Windows.Forms.TextBox();
            this.bSearchL = new System.Windows.Forms.Button();
            this.Kerusakan = new System.Windows.Forms.TabPage();
            this.dataGAssetB = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tSearchB = new System.Windows.Forms.TextBox();
            this.bSearchB = new System.Windows.Forms.Button();
            this.Perbaikan = new System.Windows.Forms.TabPage();
            this.dataGAssetF = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tSearchF = new System.Windows.Forms.TextBox();
            this.bSearchF = new System.Windows.Forms.Button();
            this.selesaiPerbaikan = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tSearchFi = new System.Windows.Forms.TextBox();
            this.bSearchFi = new System.Windows.Forms.Button();
            this.dataGAssetFi = new System.Windows.Forms.DataGridView();
            this.tabControl1.SuspendLayout();
            this.Kehilangan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGAssetL)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.Kerusakan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGAssetB)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.Perbaikan.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGAssetF)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.selesaiPerbaikan.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGAssetFi)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Kehilangan);
            this.tabControl1.Controls.Add(this.Kerusakan);
            this.tabControl1.Controls.Add(this.Perbaikan);
            this.tabControl1.Controls.Add(this.selesaiPerbaikan);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(994, 514);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // Kehilangan
            // 
            this.Kehilangan.Controls.Add(this.dataGAssetL);
            this.Kehilangan.Controls.Add(this.groupBox5);
            this.Kehilangan.Location = new System.Drawing.Point(4, 22);
            this.Kehilangan.Name = "Kehilangan";
            this.Kehilangan.Padding = new System.Windows.Forms.Padding(3);
            this.Kehilangan.Size = new System.Drawing.Size(986, 488);
            this.Kehilangan.TabIndex = 0;
            this.Kehilangan.Text = "Klaim Kehilangan";
            this.Kehilangan.UseVisualStyleBackColor = true;
            // 
            // dataGAssetL
            // 
            this.dataGAssetL.AllowUserToAddRows = false;
            this.dataGAssetL.AllowUserToDeleteRows = false;
            this.dataGAssetL.AllowUserToResizeColumns = false;
            this.dataGAssetL.AllowUserToResizeRows = false;
            this.dataGAssetL.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGAssetL.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGAssetL.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGAssetL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGAssetL.Location = new System.Drawing.Point(6, 69);
            this.dataGAssetL.MultiSelect = false;
            this.dataGAssetL.Name = "dataGAssetL";
            this.dataGAssetL.ReadOnly = true;
            this.dataGAssetL.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGAssetL.Size = new System.Drawing.Size(974, 413);
            this.dataGAssetL.TabIndex = 33;
            this.dataGAssetL.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGAssetL_CellContentClick);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tSearchL);
            this.groupBox5.Controls.Add(this.bSearchL);
            this.groupBox5.Location = new System.Drawing.Point(6, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(974, 57);
            this.groupBox5.TabIndex = 32;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Pemilik";
            // 
            // tSearchL
            // 
            this.tSearchL.Location = new System.Drawing.Point(6, 19);
            this.tSearchL.Name = "tSearchL";
            this.tSearchL.Size = new System.Drawing.Size(427, 20);
            this.tSearchL.TabIndex = 1;
            this.tSearchL.TextChanged += new System.EventHandler(this.bSearchL_Click);
            // 
            // bSearchL
            // 
            this.bSearchL.Location = new System.Drawing.Point(439, 17);
            this.bSearchL.Name = "bSearchL";
            this.bSearchL.Size = new System.Drawing.Size(75, 23);
            this.bSearchL.TabIndex = 0;
            this.bSearchL.Text = "Cari";
            this.bSearchL.UseVisualStyleBackColor = true;
            this.bSearchL.Click += new System.EventHandler(this.bSearchL_Click);
            // 
            // Kerusakan
            // 
            this.Kerusakan.Controls.Add(this.dataGAssetB);
            this.Kerusakan.Controls.Add(this.groupBox1);
            this.Kerusakan.Location = new System.Drawing.Point(4, 22);
            this.Kerusakan.Name = "Kerusakan";
            this.Kerusakan.Padding = new System.Windows.Forms.Padding(3);
            this.Kerusakan.Size = new System.Drawing.Size(986, 488);
            this.Kerusakan.TabIndex = 1;
            this.Kerusakan.Text = "Klaim Kerusakan";
            this.Kerusakan.UseVisualStyleBackColor = true;
            // 
            // dataGAssetB
            // 
            this.dataGAssetB.AllowUserToAddRows = false;
            this.dataGAssetB.AllowUserToDeleteRows = false;
            this.dataGAssetB.AllowUserToResizeColumns = false;
            this.dataGAssetB.AllowUserToResizeRows = false;
            this.dataGAssetB.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGAssetB.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGAssetB.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGAssetB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGAssetB.Location = new System.Drawing.Point(6, 69);
            this.dataGAssetB.MultiSelect = false;
            this.dataGAssetB.Name = "dataGAssetB";
            this.dataGAssetB.ReadOnly = true;
            this.dataGAssetB.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGAssetB.Size = new System.Drawing.Size(974, 413);
            this.dataGAssetB.TabIndex = 37;
            this.dataGAssetB.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGAssetB_CellContentClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tSearchB);
            this.groupBox1.Controls.Add(this.bSearchB);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 57);
            this.groupBox1.TabIndex = 36;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pemilik";
            // 
            // tSearchB
            // 
            this.tSearchB.Location = new System.Drawing.Point(6, 19);
            this.tSearchB.Name = "tSearchB";
            this.tSearchB.Size = new System.Drawing.Size(427, 20);
            this.tSearchB.TabIndex = 1;
            this.tSearchB.TextChanged += new System.EventHandler(this.bSearchB_Click);
            // 
            // bSearchB
            // 
            this.bSearchB.Location = new System.Drawing.Point(439, 17);
            this.bSearchB.Name = "bSearchB";
            this.bSearchB.Size = new System.Drawing.Size(75, 23);
            this.bSearchB.TabIndex = 0;
            this.bSearchB.Text = "Cari";
            this.bSearchB.UseVisualStyleBackColor = true;
            this.bSearchB.Click += new System.EventHandler(this.bSearchB_Click);
            // 
            // Perbaikan
            // 
            this.Perbaikan.Controls.Add(this.dataGAssetF);
            this.Perbaikan.Controls.Add(this.groupBox2);
            this.Perbaikan.Location = new System.Drawing.Point(4, 22);
            this.Perbaikan.Name = "Perbaikan";
            this.Perbaikan.Size = new System.Drawing.Size(986, 488);
            this.Perbaikan.TabIndex = 2;
            this.Perbaikan.Text = "Kirim Perbaikan";
            this.Perbaikan.UseVisualStyleBackColor = true;
            // 
            // dataGAssetF
            // 
            this.dataGAssetF.AllowUserToAddRows = false;
            this.dataGAssetF.AllowUserToDeleteRows = false;
            this.dataGAssetF.AllowUserToResizeColumns = false;
            this.dataGAssetF.AllowUserToResizeRows = false;
            this.dataGAssetF.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGAssetF.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGAssetF.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGAssetF.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGAssetF.Location = new System.Drawing.Point(6, 69);
            this.dataGAssetF.MultiSelect = false;
            this.dataGAssetF.Name = "dataGAssetF";
            this.dataGAssetF.ReadOnly = true;
            this.dataGAssetF.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGAssetF.Size = new System.Drawing.Size(974, 413);
            this.dataGAssetF.TabIndex = 37;
            this.dataGAssetF.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGAssetF_CellContentClick);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tSearchF);
            this.groupBox2.Controls.Add(this.bSearchF);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(974, 57);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pemilik";
            // 
            // tSearchF
            // 
            this.tSearchF.Location = new System.Drawing.Point(6, 19);
            this.tSearchF.Name = "tSearchF";
            this.tSearchF.Size = new System.Drawing.Size(427, 20);
            this.tSearchF.TabIndex = 1;
            this.tSearchF.TextChanged += new System.EventHandler(this.bSearchF_Click);
            this.tSearchF.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tSearchF_KeyUp);
            // 
            // bSearchF
            // 
            this.bSearchF.Location = new System.Drawing.Point(439, 17);
            this.bSearchF.Name = "bSearchF";
            this.bSearchF.Size = new System.Drawing.Size(75, 23);
            this.bSearchF.TabIndex = 0;
            this.bSearchF.Text = "Cari";
            this.bSearchF.UseVisualStyleBackColor = true;
            this.bSearchF.Click += new System.EventHandler(this.bSearchF_Click);
            // 
            // selesaiPerbaikan
            // 
            this.selesaiPerbaikan.Controls.Add(this.groupBox3);
            this.selesaiPerbaikan.Controls.Add(this.dataGAssetFi);
            this.selesaiPerbaikan.Location = new System.Drawing.Point(4, 22);
            this.selesaiPerbaikan.Name = "selesaiPerbaikan";
            this.selesaiPerbaikan.Size = new System.Drawing.Size(986, 488);
            this.selesaiPerbaikan.TabIndex = 3;
            this.selesaiPerbaikan.Text = "Selesai Perbaikan";
            this.selesaiPerbaikan.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tSearchFi);
            this.groupBox3.Controls.Add(this.bSearchFi);
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(974, 57);
            this.groupBox3.TabIndex = 38;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Pemilik";
            // 
            // tSearchFi
            // 
            this.tSearchFi.Location = new System.Drawing.Point(6, 19);
            this.tSearchFi.Name = "tSearchFi";
            this.tSearchFi.Size = new System.Drawing.Size(427, 20);
            this.tSearchFi.TabIndex = 1;
            this.tSearchFi.TextChanged += new System.EventHandler(this.bSearchFi_Click);
            // 
            // bSearchFi
            // 
            this.bSearchFi.Location = new System.Drawing.Point(439, 17);
            this.bSearchFi.Name = "bSearchFi";
            this.bSearchFi.Size = new System.Drawing.Size(75, 23);
            this.bSearchFi.TabIndex = 0;
            this.bSearchFi.Text = "Cari";
            this.bSearchFi.UseVisualStyleBackColor = true;
            this.bSearchFi.Click += new System.EventHandler(this.bSearchFi_Click);
            // 
            // dataGAssetFi
            // 
            this.dataGAssetFi.AllowUserToAddRows = false;
            this.dataGAssetFi.AllowUserToDeleteRows = false;
            this.dataGAssetFi.AllowUserToResizeColumns = false;
            this.dataGAssetFi.AllowUserToResizeRows = false;
            this.dataGAssetFi.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGAssetFi.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGAssetFi.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGAssetFi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGAssetFi.Location = new System.Drawing.Point(6, 69);
            this.dataGAssetFi.MultiSelect = false;
            this.dataGAssetFi.Name = "dataGAssetFi";
            this.dataGAssetFi.ReadOnly = true;
            this.dataGAssetFi.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGAssetFi.Size = new System.Drawing.Size(974, 413);
            this.dataGAssetFi.TabIndex = 39;
            this.dataGAssetFi.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGAssetFi_CellContentClick);
            // 
            // ClaimManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 538);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ClaimManagement";
            this.Text = "Klaim Kerusakan/Kehilangan";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ClaimManagement_FormClosed);
            this.Load += new System.EventHandler(this.ClaimManagement_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ClaimManagement_Paint);
            this.tabControl1.ResumeLayout(false);
            this.Kehilangan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGAssetL)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.Kerusakan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGAssetB)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Perbaikan.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGAssetF)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.selesaiPerbaikan.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGAssetFi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Kehilangan;
        private System.Windows.Forms.TabPage Kerusakan;
        private System.Windows.Forms.TabPage Perbaikan;
        private System.Windows.Forms.DataGridView dataGAssetL;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox tSearchL;
        private System.Windows.Forms.Button bSearchL;
        private System.Windows.Forms.DataGridView dataGAssetB;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tSearchB;
        private System.Windows.Forms.Button bSearchB;
        private System.Windows.Forms.DataGridView dataGAssetF;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tSearchF;
        private System.Windows.Forms.Button bSearchF;
        private System.Windows.Forms.TabPage selesaiPerbaikan;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tSearchFi;
        private System.Windows.Forms.Button bSearchFi;
        private System.Windows.Forms.DataGridView dataGAssetFi;
    }
}