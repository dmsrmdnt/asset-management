﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace asset.Administrasi
{
    public partial class UserResign : Form
    {
        lib lib = new lib();
        public string id_employee = "", id_admin = "";
        public UserResign()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }

        private void UserResign_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

        private void bOk_Click(object sender, EventArgs e)
        {
            //Upload file
            string filetype;
            string filename;

            filetype = tFile.Text.Substring(Convert.ToInt32(tFile.Text.LastIndexOf(".")) + 1, tFile.Text.Length - (Convert.ToInt32(tFile.Text.LastIndexOf(".")) + 1));
            filename = "BA" + DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ') + this.id_employee + this.id_admin + "." + filetype;
            byte[] FileBytes = null;

            try
            {
                // Open file to read using file path
                FileStream FS = new FileStream(tFile.Text, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                // Add filestream to binary reader
                BinaryReader BR = new BinaryReader(FS);

                // get total byte length of the file
                long allbytes = new FileInfo(tFile.Text).Length;

                // read entire file into buffer
                FileBytes = BR.ReadBytes((Int32)allbytes);

                // close all instances
                FS.Close();
                FS.Dispose();
                BR.Close();

                //Store File Bytes in database image filed
                asset.Class.process.mUserManagement usr = new asset.Class.process.mUserManagement();
                usr.resign(this.id_employee, this.id_admin, filename, FileBytes);
                MessageBox.Show("Proses pemindahan aset berhasil", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error during File Read " + ex.ToString());
            }
        }

        private void bCari_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fDialog = new OpenFileDialog();
                fDialog.Title = "Pilih file untuk diunggah";
                fDialog.Filter = "All Files|*.pdf;*.doc;*.docx;*.jpg;*.jpeg;*.png|PDF Files|*.pdf|Word Document|*.doc;*.docx|Image|*.jpg;*.jpeg;*.png";
                if (fDialog.ShowDialog() == DialogResult.OK)
                {
                    tFile.Text = fDialog.FileName.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
