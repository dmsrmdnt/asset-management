﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class LicenseManagement : Form
    {
        private String id;
        lib lib = new lib();
        public LicenseManagement(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
        }
        private static LicenseManagement form;
        public static LicenseManagement GetChildInstance(String id)
        {
            if (form == null)
                form = new LicenseManagement(id);
            return form;
        }
        private void LicenseManagement_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewButtonColumn a = new DataGridViewButtonColumn();
                dataGSoft.Columns.Add(a);
                a.HeaderText = "Update";
                a.Text = "Update";
                a.UseColumnTextForButtonValue = true;
                showDataGSoft();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showDataGSoft()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_soft_license";
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "locationTable");
                conn.Close();
                this.dataGSoft.DataSource = dataSet.Tables["locationTable"].DefaultView;
                this.dataGSoft.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void dataGSoft_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGSoft.Columns[0].Index)
                {
                    //Update
                    try
                    {
                        String id = dataGSoft[1, e.RowIndex].Value.ToString();
                        UpdateLicense asset = new UpdateLicense(id);
                        asset.ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void bSearch_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearch.Text;
                searchDataGSoft(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void searchDataGSoft(String str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "search_data_soft_license";
                myCommand.Parameters.AddWithValue("@str", str);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "locationTable");
                conn.Close();
                this.dataGSoft.DataSource = dataSet.Tables["locationTable"].DefaultView;
                this.dataGSoft.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void LicenseManagement_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void LicenseManagement_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

    }
}
