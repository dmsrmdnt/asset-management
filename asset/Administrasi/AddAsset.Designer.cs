﻿namespace asset
{
    partial class AddAsset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.bCancel = new System.Windows.Forms.Button();
            this.bOK = new System.Windows.Forms.Button();
            this.lModel = new System.Windows.Forms.Label();
            this.lSerial = new System.Windows.Forms.Label();
            this.lCategory = new System.Windows.Forms.Label();
            this.lBrand = new System.Windows.Forms.Label();
            this.lPrice = new System.Windows.Forms.Label();
            this.lLocation = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tModel = new System.Windows.Forms.TextBox();
            this.tSerial = new System.Windows.Forms.TextBox();
            this.tCategory = new System.Windows.Forms.TextBox();
            this.tBrand = new System.Windows.Forms.TextBox();
            this.tPrice = new System.Windows.Forms.TextBox();
            this.tLocation = new System.Windows.Forms.TextBox();
            this.tDesc = new System.Windows.Forms.TextBox();
            this.baCategory = new System.Windows.Forms.Button();
            this.baBrand = new System.Windows.Forms.Button();
            this.baLocation = new System.Windows.Forms.Button();
            this.groupBoxAsset = new System.Windows.Forms.GroupBox();
            this.tItem = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bCancelUp = new System.Windows.Forms.Button();
            this.pAsset = new System.Windows.Forms.PictureBox();
            this.bUpload = new System.Windows.Forms.Button();
            this.checkSoft = new System.Windows.Forms.CheckBox();
            this.dataGSoft = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.baSoft = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tNoAsset = new System.Windows.Forms.TextBox();
            this.purDate = new System.Windows.Forms.DateTimePicker();
            this.lDate = new System.Windows.Forms.Label();
            this.lNotify = new System.Windows.Forms.Label();
            this.groupBoxOwner = new System.Windows.Forms.GroupBox();
            this.tOwner = new System.Windows.Forms.TextBox();
            this.lName = new System.Windows.Forms.Label();
            this.baOwner = new System.Windows.Forms.Button();
            this.tNIK = new System.Windows.Forms.TextBox();
            this.lNIK = new System.Windows.Forms.Label();
            this.groupBoxAsset.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pAsset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGSoft)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBoxOwner.SuspendLayout();
            this.SuspendLayout();
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(509, 666);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(120, 40);
            this.bCancel.TabIndex = 13;
            this.bCancel.Text = "Batal";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bOK
            // 
            this.bOK.Location = new System.Drawing.Point(372, 666);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(120, 40);
            this.bOK.TabIndex = 12;
            this.bOK.Text = "Simpan";
            this.bOK.UseVisualStyleBackColor = true;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // lModel
            // 
            this.lModel.AutoSize = true;
            this.lModel.Location = new System.Drawing.Point(11, 70);
            this.lModel.Name = "lModel";
            this.lModel.Size = new System.Drawing.Size(36, 13);
            this.lModel.TabIndex = 8;
            this.lModel.Text = "Model";
            // 
            // lSerial
            // 
            this.lSerial.AutoSize = true;
            this.lSerial.Location = new System.Drawing.Point(11, 96);
            this.lSerial.Name = "lSerial";
            this.lSerial.Size = new System.Drawing.Size(53, 13);
            this.lSerial.TabIndex = 9;
            this.lSerial.Text = "No. Serial";
            // 
            // lCategory
            // 
            this.lCategory.AutoSize = true;
            this.lCategory.Location = new System.Drawing.Point(11, 122);
            this.lCategory.Name = "lCategory";
            this.lCategory.Size = new System.Drawing.Size(46, 13);
            this.lCategory.TabIndex = 10;
            this.lCategory.Text = "Kategori";
            // 
            // lBrand
            // 
            this.lBrand.AutoSize = true;
            this.lBrand.Location = new System.Drawing.Point(12, 148);
            this.lBrand.Name = "lBrand";
            this.lBrand.Size = new System.Drawing.Size(35, 13);
            this.lBrand.TabIndex = 11;
            this.lBrand.Text = "Brand";
            // 
            // lPrice
            // 
            this.lPrice.AutoSize = true;
            this.lPrice.Location = new System.Drawing.Point(12, 200);
            this.lPrice.Name = "lPrice";
            this.lPrice.Size = new System.Drawing.Size(36, 13);
            this.lPrice.TabIndex = 12;
            this.lPrice.Text = "Harga";
            // 
            // lLocation
            // 
            this.lLocation.AutoSize = true;
            this.lLocation.Location = new System.Drawing.Point(11, 226);
            this.lLocation.Name = "lLocation";
            this.lLocation.Size = new System.Drawing.Size(38, 13);
            this.lLocation.TabIndex = 13;
            this.lLocation.Text = "Lokasi";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 249);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Deskripsi";
            // 
            // tModel
            // 
            this.tModel.Location = new System.Drawing.Point(109, 67);
            this.tModel.Name = "tModel";
            this.tModel.Size = new System.Drawing.Size(220, 20);
            this.tModel.TabIndex = 2;
            // 
            // tSerial
            // 
            this.tSerial.Location = new System.Drawing.Point(109, 93);
            this.tSerial.Name = "tSerial";
            this.tSerial.Size = new System.Drawing.Size(220, 20);
            this.tSerial.TabIndex = 3;
            // 
            // tCategory
            // 
            this.tCategory.Location = new System.Drawing.Point(109, 119);
            this.tCategory.Name = "tCategory";
            this.tCategory.Size = new System.Drawing.Size(148, 20);
            this.tCategory.TabIndex = 11;
            // 
            // tBrand
            // 
            this.tBrand.Location = new System.Drawing.Point(109, 145);
            this.tBrand.Name = "tBrand";
            this.tBrand.Size = new System.Drawing.Size(148, 20);
            this.tBrand.TabIndex = 12;
            // 
            // tPrice
            // 
            this.tPrice.Location = new System.Drawing.Point(109, 197);
            this.tPrice.Name = "tPrice";
            this.tPrice.Size = new System.Drawing.Size(148, 20);
            this.tPrice.TabIndex = 7;
            this.tPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tPrice_KeyPress);
            // 
            // tLocation
            // 
            this.tLocation.Location = new System.Drawing.Point(109, 223);
            this.tLocation.Name = "tLocation";
            this.tLocation.Size = new System.Drawing.Size(148, 20);
            this.tLocation.TabIndex = 13;
            // 
            // tDesc
            // 
            this.tDesc.Location = new System.Drawing.Point(109, 249);
            this.tDesc.Multiline = true;
            this.tDesc.Name = "tDesc";
            this.tDesc.Size = new System.Drawing.Size(500, 150);
            this.tDesc.TabIndex = 9;
            this.tDesc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tNoAsset_KeyPress);
            // 
            // baCategory
            // 
            this.baCategory.ForeColor = System.Drawing.Color.Black;
            this.baCategory.Location = new System.Drawing.Point(263, 117);
            this.baCategory.Name = "baCategory";
            this.baCategory.Size = new System.Drawing.Size(66, 23);
            this.baCategory.TabIndex = 4;
            this.baCategory.Text = "Cari";
            this.baCategory.UseVisualStyleBackColor = true;
            this.baCategory.Click += new System.EventHandler(this.baCategory_Click);
            // 
            // baBrand
            // 
            this.baBrand.ForeColor = System.Drawing.Color.Black;
            this.baBrand.Location = new System.Drawing.Point(263, 143);
            this.baBrand.Name = "baBrand";
            this.baBrand.Size = new System.Drawing.Size(66, 23);
            this.baBrand.TabIndex = 5;
            this.baBrand.Text = "Cari";
            this.baBrand.UseVisualStyleBackColor = true;
            this.baBrand.Click += new System.EventHandler(this.baBrand_Click);
            // 
            // baLocation
            // 
            this.baLocation.ForeColor = System.Drawing.Color.Black;
            this.baLocation.Location = new System.Drawing.Point(263, 221);
            this.baLocation.Name = "baLocation";
            this.baLocation.Size = new System.Drawing.Size(66, 23);
            this.baLocation.TabIndex = 8;
            this.baLocation.Text = "Cari";
            this.baLocation.UseVisualStyleBackColor = true;
            this.baLocation.Click += new System.EventHandler(this.baLocation_Click);
            // 
            // groupBoxAsset
            // 
            this.groupBoxAsset.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxAsset.Controls.Add(this.tItem);
            this.groupBoxAsset.Controls.Add(this.label3);
            this.groupBoxAsset.Controls.Add(this.groupBox2);
            this.groupBoxAsset.Controls.Add(this.checkSoft);
            this.groupBoxAsset.Controls.Add(this.dataGSoft);
            this.groupBoxAsset.Controls.Add(this.groupBox1);
            this.groupBoxAsset.Controls.Add(this.label2);
            this.groupBoxAsset.Controls.Add(this.tNoAsset);
            this.groupBoxAsset.Controls.Add(this.purDate);
            this.groupBoxAsset.Controls.Add(this.lDate);
            this.groupBoxAsset.Controls.Add(this.baLocation);
            this.groupBoxAsset.Controls.Add(this.baBrand);
            this.groupBoxAsset.Controls.Add(this.baCategory);
            this.groupBoxAsset.Controls.Add(this.tDesc);
            this.groupBoxAsset.Controls.Add(this.tLocation);
            this.groupBoxAsset.Controls.Add(this.tPrice);
            this.groupBoxAsset.Controls.Add(this.tBrand);
            this.groupBoxAsset.Controls.Add(this.tCategory);
            this.groupBoxAsset.Controls.Add(this.tSerial);
            this.groupBoxAsset.Controls.Add(this.tModel);
            this.groupBoxAsset.Controls.Add(this.label1);
            this.groupBoxAsset.Controls.Add(this.lLocation);
            this.groupBoxAsset.Controls.Add(this.lPrice);
            this.groupBoxAsset.Controls.Add(this.lBrand);
            this.groupBoxAsset.Controls.Add(this.lCategory);
            this.groupBoxAsset.Controls.Add(this.lSerial);
            this.groupBoxAsset.Controls.Add(this.lModel);
            this.groupBoxAsset.ForeColor = System.Drawing.Color.White;
            this.groupBoxAsset.Location = new System.Drawing.Point(14, 112);
            this.groupBoxAsset.Name = "groupBoxAsset";
            this.groupBoxAsset.Size = new System.Drawing.Size(615, 548);
            this.groupBoxAsset.TabIndex = 25;
            this.groupBoxAsset.TabStop = false;
            this.groupBoxAsset.Text = "Informasi Aset";
            // 
            // tItem
            // 
            this.tItem.Location = new System.Drawing.Point(109, 41);
            this.tItem.Name = "tItem";
            this.tItem.Size = new System.Drawing.Size(220, 20);
            this.tItem.TabIndex = 33;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 32;
            this.label3.Text = "Item";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bCancelUp);
            this.groupBox2.Controls.Add(this.pAsset);
            this.groupBox2.Controls.Add(this.bUpload);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(380, 15);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(195, 204);
            this.groupBox2.TabIndex = 31;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Upload Foto";
            // 
            // bCancelUp
            // 
            this.bCancelUp.ForeColor = System.Drawing.Color.Black;
            this.bCancelUp.Location = new System.Drawing.Point(100, 175);
            this.bCancelUp.Name = "bCancelUp";
            this.bCancelUp.Size = new System.Drawing.Size(89, 23);
            this.bCancelUp.TabIndex = 34;
            this.bCancelUp.Text = "Cancel";
            this.bCancelUp.UseVisualStyleBackColor = true;
            this.bCancelUp.Click += new System.EventHandler(this.bCancelUp_Click);
            // 
            // pAsset
            // 
            this.pAsset.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pAsset.Location = new System.Drawing.Point(9, 19);
            this.pAsset.Name = "pAsset";
            this.pAsset.Size = new System.Drawing.Size(180, 150);
            this.pAsset.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pAsset.TabIndex = 33;
            this.pAsset.TabStop = false;
            // 
            // bUpload
            // 
            this.bUpload.ForeColor = System.Drawing.Color.Black;
            this.bUpload.Location = new System.Drawing.Point(9, 175);
            this.bUpload.Name = "bUpload";
            this.bUpload.Size = new System.Drawing.Size(89, 23);
            this.bUpload.TabIndex = 32;
            this.bUpload.Text = "Upload";
            this.bUpload.UseVisualStyleBackColor = true;
            this.bUpload.Click += new System.EventHandler(this.bUpload_Click);
            // 
            // checkSoft
            // 
            this.checkSoft.AutoSize = true;
            this.checkSoft.Location = new System.Drawing.Point(14, 412);
            this.checkSoft.Name = "checkSoft";
            this.checkSoft.Size = new System.Drawing.Size(160, 17);
            this.checkSoft.TabIndex = 10;
            this.checkSoft.Text = "Check bila terinstall software";
            this.checkSoft.UseVisualStyleBackColor = true;
            this.checkSoft.CheckedChanged += new System.EventHandler(this.checkSoft_CheckedChanged);
            // 
            // dataGSoft
            // 
            this.dataGSoft.AllowUserToAddRows = false;
            this.dataGSoft.AllowUserToDeleteRows = false;
            this.dataGSoft.AllowUserToResizeColumns = false;
            this.dataGSoft.AllowUserToResizeRows = false;
            this.dataGSoft.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGSoft.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGSoft.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGSoft.Enabled = false;
            this.dataGSoft.Location = new System.Drawing.Point(23, 454);
            this.dataGSoft.MultiSelect = false;
            this.dataGSoft.Name = "dataGSoft";
            this.dataGSoft.ReadOnly = true;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.dataGSoft.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGSoft.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGSoft.Size = new System.Drawing.Size(504, 79);
            this.dataGSoft.TabIndex = 8;
            this.dataGSoft.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGSoft_CellContentClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.baSoft);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(17, 435);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(592, 104);
            this.groupBox1.TabIndex = 29;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Software";
            // 
            // baSoft
            // 
            this.baSoft.Enabled = false;
            this.baSoft.ForeColor = System.Drawing.Color.Black;
            this.baSoft.Location = new System.Drawing.Point(520, 19);
            this.baSoft.Name = "baSoft";
            this.baSoft.Size = new System.Drawing.Size(66, 23);
            this.baSoft.TabIndex = 11;
            this.baSoft.Text = "Tambah";
            this.baSoft.UseVisualStyleBackColor = true;
            this.baSoft.Click += new System.EventHandler(this.baSoft_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 28;
            this.label2.Text = "No. Aset";
            // 
            // tNoAsset
            // 
            this.tNoAsset.Location = new System.Drawing.Point(109, 16);
            this.tNoAsset.Name = "tNoAsset";
            this.tNoAsset.Size = new System.Drawing.Size(220, 20);
            this.tNoAsset.TabIndex = 27;
            // 
            // purDate
            // 
            this.purDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.purDate.Location = new System.Drawing.Point(109, 171);
            this.purDate.Name = "purDate";
            this.purDate.Size = new System.Drawing.Size(148, 20);
            this.purDate.TabIndex = 6;
            // 
            // lDate
            // 
            this.lDate.AutoSize = true;
            this.lDate.Location = new System.Drawing.Point(11, 177);
            this.lDate.Name = "lDate";
            this.lDate.Size = new System.Drawing.Size(98, 13);
            this.lDate.TabIndex = 26;
            this.lDate.Text = "Tanggal Pembelian";
            // 
            // lNotify
            // 
            this.lNotify.AutoSize = true;
            this.lNotify.BackColor = System.Drawing.Color.Transparent;
            this.lNotify.ForeColor = System.Drawing.Color.Red;
            this.lNotify.Location = new System.Drawing.Point(25, 666);
            this.lNotify.Name = "lNotify";
            this.lNotify.Size = new System.Drawing.Size(0, 13);
            this.lNotify.TabIndex = 25;
            // 
            // groupBoxOwner
            // 
            this.groupBoxOwner.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxOwner.Controls.Add(this.tOwner);
            this.groupBoxOwner.Controls.Add(this.lName);
            this.groupBoxOwner.Controls.Add(this.baOwner);
            this.groupBoxOwner.Controls.Add(this.tNIK);
            this.groupBoxOwner.Controls.Add(this.lNIK);
            this.groupBoxOwner.ForeColor = System.Drawing.Color.White;
            this.groupBoxOwner.Location = new System.Drawing.Point(14, 10);
            this.groupBoxOwner.Name = "groupBoxOwner";
            this.groupBoxOwner.Size = new System.Drawing.Size(615, 96);
            this.groupBoxOwner.TabIndex = 26;
            this.groupBoxOwner.TabStop = false;
            this.groupBoxOwner.Text = "Pemilik";
            // 
            // tOwner
            // 
            this.tOwner.Location = new System.Drawing.Point(109, 54);
            this.tOwner.Name = "tOwner";
            this.tOwner.Size = new System.Drawing.Size(346, 20);
            this.tOwner.TabIndex = 10;
            // 
            // lName
            // 
            this.lName.AutoSize = true;
            this.lName.Location = new System.Drawing.Point(11, 57);
            this.lName.Name = "lName";
            this.lName.Size = new System.Drawing.Size(35, 13);
            this.lName.TabIndex = 26;
            this.lName.Text = "Nama";
            // 
            // baOwner
            // 
            this.baOwner.ForeColor = System.Drawing.Color.Black;
            this.baOwner.Location = new System.Drawing.Point(461, 26);
            this.baOwner.Name = "baOwner";
            this.baOwner.Size = new System.Drawing.Size(66, 23);
            this.baOwner.TabIndex = 0;
            this.baOwner.Text = "Cari";
            this.baOwner.UseVisualStyleBackColor = true;
            this.baOwner.Click += new System.EventHandler(this.baOwner_Click);
            // 
            // tNIK
            // 
            this.tNIK.Location = new System.Drawing.Point(109, 28);
            this.tNIK.Name = "tNIK";
            this.tNIK.Size = new System.Drawing.Size(346, 20);
            this.tNIK.TabIndex = 9;
            // 
            // lNIK
            // 
            this.lNIK.AutoSize = true;
            this.lNIK.Location = new System.Drawing.Point(11, 31);
            this.lNIK.Name = "lNIK";
            this.lNIK.Size = new System.Drawing.Size(32, 13);
            this.lNIK.TabIndex = 25;
            this.lNIK.Text = "Email";
            // 
            // AddAsset
            // 
            this.AcceptButton = this.bOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 718);
            this.Controls.Add(this.groupBoxOwner);
            this.Controls.Add(this.groupBoxAsset);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.lNotify);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddAsset";
            this.Text = "Tambah Aset";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AddAsset_FormClosed);
            this.Load += new System.EventHandler(this.AddAsset_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AddAsset_Paint);
            this.groupBoxAsset.ResumeLayout(false);
            this.groupBoxAsset.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pAsset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGSoft)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBoxOwner.ResumeLayout(false);
            this.groupBoxOwner.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Label lModel;
        private System.Windows.Forms.Label lSerial;
        private System.Windows.Forms.Label lCategory;
        private System.Windows.Forms.Label lBrand;
        private System.Windows.Forms.Label lPrice;
        private System.Windows.Forms.Label lLocation;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tModel;
        private System.Windows.Forms.TextBox tSerial;
        private System.Windows.Forms.TextBox tCategory;
        private System.Windows.Forms.TextBox tBrand;
        private System.Windows.Forms.TextBox tPrice;
        private System.Windows.Forms.TextBox tLocation;
        private System.Windows.Forms.TextBox tDesc;
        private System.Windows.Forms.Button baCategory;
        private System.Windows.Forms.Button baBrand;
        private System.Windows.Forms.Button baLocation;
        private System.Windows.Forms.GroupBox groupBoxAsset;
        private System.Windows.Forms.GroupBox groupBoxOwner;
        private System.Windows.Forms.Button baOwner;
        private System.Windows.Forms.TextBox tNIK;
        private System.Windows.Forms.Label lNIK;
        private System.Windows.Forms.TextBox tOwner;
        private System.Windows.Forms.Label lName;
        private System.Windows.Forms.Label lNotify;
        private System.Windows.Forms.DateTimePicker purDate;
        private System.Windows.Forms.Label lDate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tNoAsset;
        private System.Windows.Forms.DataGridView dataGSoft;
        private System.Windows.Forms.Button baSoft;
        private System.Windows.Forms.CheckBox checkSoft;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pAsset;
        private System.Windows.Forms.Button bUpload;
        private System.Windows.Forms.Button bCancelUp;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tItem;
    }
}