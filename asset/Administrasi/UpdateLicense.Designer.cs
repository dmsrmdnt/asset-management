﻿namespace asset
{
    partial class UpdateLicense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lNotify = new System.Windows.Forms.Label();
            this.bCancel = new System.Windows.Forms.Button();
            this.bOK = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.expDateL = new System.Windows.Forms.DateTimePicker();
            this.tNLicenseL = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tSoftL = new System.Windows.Forms.TextBox();
            this.lNIK = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.expDateB = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.tSoftB = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tNLicenseB = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lNotify
            // 
            this.lNotify.AutoSize = true;
            this.lNotify.ForeColor = System.Drawing.Color.Red;
            this.lNotify.Location = new System.Drawing.Point(12, 250);
            this.lNotify.Name = "lNotify";
            this.lNotify.Size = new System.Drawing.Size(0, 13);
            this.lNotify.TabIndex = 47;
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(529, 268);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(120, 40);
            this.bCancel.TabIndex = 46;
            this.bCancel.Text = "Batal";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bOK
            // 
            this.bOK.Location = new System.Drawing.Point(392, 268);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(120, 40);
            this.bOK.TabIndex = 45;
            this.bOK.Text = "Simpan";
            this.bOK.UseVisualStyleBackColor = true;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 44;
            this.label2.Text = "Tanggal Exp";
            // 
            // expDateL
            // 
            this.expDateL.Enabled = false;
            this.expDateL.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.expDateL.Location = new System.Drawing.Point(112, 76);
            this.expDateL.Name = "expDateL";
            this.expDateL.Size = new System.Drawing.Size(148, 20);
            this.expDateL.TabIndex = 43;
            // 
            // tNLicenseL
            // 
            this.tNLicenseL.Enabled = false;
            this.tNLicenseL.Location = new System.Drawing.Point(112, 50);
            this.tNLicenseL.Name = "tNLicenseL";
            this.tNLicenseL.Size = new System.Drawing.Size(298, 20);
            this.tNLicenseL.TabIndex = 41;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 42;
            this.label1.Text = "No. Lisensi";
            // 
            // tSoftL
            // 
            this.tSoftL.Enabled = false;
            this.tSoftL.Location = new System.Drawing.Point(112, 24);
            this.tSoftL.Name = "tSoftL";
            this.tSoftL.Size = new System.Drawing.Size(298, 20);
            this.tSoftL.TabIndex = 38;
            // 
            // lNIK
            // 
            this.lNIK.AutoSize = true;
            this.lNIK.Location = new System.Drawing.Point(14, 27);
            this.lNIK.Name = "lNIK";
            this.lNIK.Size = new System.Drawing.Size(49, 13);
            this.lNIK.TabIndex = 39;
            this.lNIK.Text = "Software";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.expDateL);
            this.groupBox1.Controls.Add(this.lNIK);
            this.groupBox1.Controls.Add(this.tSoftL);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tNLicenseL);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(15, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(634, 112);
            this.groupBox1.TabIndex = 52;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Lisensi Lama";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.expDateB);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.tSoftB);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.tNLicenseB);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(15, 130);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(634, 112);
            this.groupBox2.TabIndex = 53;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Lisensi Baru";
            // 
            // expDateB
            // 
            this.expDateB.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.expDateB.Location = new System.Drawing.Point(112, 76);
            this.expDateB.Name = "expDateB";
            this.expDateB.Size = new System.Drawing.Size(148, 20);
            this.expDateB.TabIndex = 43;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 39;
            this.label3.Text = "Software";
            // 
            // tSoftB
            // 
            this.tSoftB.Enabled = false;
            this.tSoftB.Location = new System.Drawing.Point(112, 24);
            this.tSoftB.Name = "tSoftB";
            this.tSoftB.Size = new System.Drawing.Size(298, 20);
            this.tSoftB.TabIndex = 38;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 42;
            this.label4.Text = "No. Lisensi";
            // 
            // tNLicenseB
            // 
            this.tNLicenseB.Location = new System.Drawing.Point(112, 50);
            this.tNLicenseB.Name = "tNLicenseB";
            this.tNLicenseB.Size = new System.Drawing.Size(298, 20);
            this.tNLicenseB.TabIndex = 41;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 44;
            this.label5.Text = "Tanggal Exp";
            // 
            // UpdateLicense
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(661, 320);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lNotify);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UpdateLicense";
            this.Text = "Update Lisensi Software";
            this.Load += new System.EventHandler(this.UpdateLicense_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.UpdateLicense_Paint);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lNotify;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker expDateL;
        private System.Windows.Forms.TextBox tNLicenseL;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tSoftL;
        private System.Windows.Forms.Label lNIK;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DateTimePicker expDateB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tSoftB;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tNLicenseB;
        private System.Windows.Forms.Label label5;
    }
}