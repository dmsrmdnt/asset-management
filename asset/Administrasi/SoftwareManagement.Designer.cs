﻿namespace asset.Administrasi
{
    partial class SoftwareManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bSearch = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.tSoft = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cStatus = new System.Windows.Forms.ComboBox();
            this.cKategori = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGSoft = new System.Windows.Forms.DataGridView();
            this.bAdd = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGSoft)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.bSearch);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tSoft);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cStatus);
            this.groupBox1.Controls.Add(this.cKategori);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1041, 120);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cari Software";
            // 
            // bSearch
            // 
            this.bSearch.ForeColor = System.Drawing.Color.Black;
            this.bSearch.Location = new System.Drawing.Point(380, 24);
            this.bSearch.Name = "bSearch";
            this.bSearch.Size = new System.Drawing.Size(202, 48);
            this.bSearch.TabIndex = 6;
            this.bSearch.Text = "Cari";
            this.bSearch.UseVisualStyleBackColor = true;
            this.bSearch.Click += new System.EventHandler(this.bSearch_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Software";
            // 
            // tSoft
            // 
            this.tSoft.Location = new System.Drawing.Point(116, 78);
            this.tSoft.Name = "tSoft";
            this.tSoft.Size = new System.Drawing.Size(466, 20);
            this.tSoft.TabIndex = 4;
            this.tSoft.TextChanged += new System.EventHandler(this.bSearch_Click);
            this.tSoft.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tSoft_KeyUp);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Status Software";
            // 
            // cStatus
            // 
            this.cStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cStatus.FormattingEnabled = true;
            this.cStatus.Location = new System.Drawing.Point(116, 51);
            this.cStatus.Name = "cStatus";
            this.cStatus.Size = new System.Drawing.Size(258, 21);
            this.cStatus.TabIndex = 2;
            this.cStatus.SelectedIndexChanged += new System.EventHandler(this.cStatus_SelectedIndexChanged);
            // 
            // cKategori
            // 
            this.cKategori.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cKategori.FormattingEnabled = true;
            this.cKategori.Location = new System.Drawing.Point(116, 24);
            this.cKategori.Name = "cKategori";
            this.cKategori.Size = new System.Drawing.Size(258, 21);
            this.cKategori.TabIndex = 1;
            this.cKategori.SelectedIndexChanged += new System.EventHandler(this.cKategori_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Kategori Software";
            // 
            // dataGSoft
            // 
            this.dataGSoft.AllowUserToAddRows = false;
            this.dataGSoft.AllowUserToDeleteRows = false;
            this.dataGSoft.AllowUserToResizeColumns = false;
            this.dataGSoft.AllowUserToResizeRows = false;
            this.dataGSoft.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGSoft.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGSoft.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGSoft.Location = new System.Drawing.Point(12, 138);
            this.dataGSoft.MultiSelect = false;
            this.dataGSoft.Name = "dataGSoft";
            this.dataGSoft.ReadOnly = true;
            this.dataGSoft.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGSoft.Size = new System.Drawing.Size(1041, 233);
            this.dataGSoft.TabIndex = 9;
            this.dataGSoft.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGSoft_CellContentClick);
            // 
            // bAdd
            // 
            this.bAdd.Location = new System.Drawing.Point(12, 377);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(1041, 41);
            this.bAdd.TabIndex = 7;
            this.bAdd.Text = "Tambah Software Baru";
            this.bAdd.UseVisualStyleBackColor = true;
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // SoftwareManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1065, 430);
            this.Controls.Add(this.bAdd);
            this.Controls.Add(this.dataGSoft);
            this.Controls.Add(this.groupBox1);
            this.Name = "SoftwareManagement";
            this.Text = "Kelola Software";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SoftwareManagement_FormClosed);
            this.Load += new System.EventHandler(this.SoftwareManagement_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.SoftwareManagement_Paint);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGSoft)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bSearch;
        private System.Windows.Forms.Button bAdd;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tSoft;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cStatus;
        private System.Windows.Forms.ComboBox cKategori;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGSoft;
    }
}