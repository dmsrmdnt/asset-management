﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class EditAsset : Form
    {
        private String imgFile = null;
        private String idCategory = null;
        private String idBrand = null;
        private String idLocation = null;
        private String idOwner = null;
        private String idAsset = null;
        private String userid = null;
        lib lib = new lib();
        Class.process.mEditAsset edit = new Class.process.mEditAsset();
        public EditAsset(String id, String userid)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            deleteTemp(userid);
            insertTemp(id, userid);

            InitializeComponent();
            tCategory.Enabled = false;
            tBrand.Enabled = false;
            tLocation.Enabled = false;
            tNIK.Enabled = false;
            tOwner.Enabled = false;
            this.idAsset = id;
            this.userid=userid;
        }
        public void updateLicenseStatus(String id_asset, int status) 
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select id_license from t_software where id_asset='" + id_asset + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    SqlCommand myCommand2 = new SqlCommand("update m_license set stat_u='" + status + "' where id_license='" + read[0].ToString() + "' and stat_l='1'", conn);
                    myCommand2.ExecuteNonQuery();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void insertTemp(String id_asset, String userid)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select id_license,id_assetsoft from t_softAsset where id_asset='" + id_asset + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    SqlCommand myCommand2 = new SqlCommand("insert into temp_software(id_license,id_software,userid) values('" + read[0].ToString() + "','" + read[1].ToString() + "','" + userid + "')", conn);
                    myCommand2.ExecuteNonQuery();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showSoftware(String id,String userid)
        {
            try
            {
                if (edit.cekSoftwareCount(id) == true)
                {
                    checkSoft.Checked = true;
                    baSoft.Enabled = true;
                    dataGSoft.Enabled = true;

                }
                showDataGSoft(userid);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showDataGSoft(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_datagrid_soft";
                myCommand.Parameters.AddWithValue("@userid", id);
                SqlDataAdapter data = new SqlDataAdapter();
                data.SelectCommand = myCommand;
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "locationTable");
                conn.Close();
                this.dataGSoft.DataSource = dataSet.Tables["locationTable"].DefaultView;
                this.dataGSoft.Columns["IDSoftware"].Visible = false;
                this.dataGSoft.Columns["IDLicense"].Visible = false;
                this.dataGSoft.Columns["IDCategory"].Visible = false;
                this.dataGSoft.Columns["IDStatus"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showData(String id) {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select a.item,a.asset, g.id_employee, a.id_location, a.id_brand, a.id_category, a.id_asset , g.nama, g.email , b.category , c.brand , a.model , a.serial, a.price, a.pur_date, d.location, e.status, a.description " +
                    "from m_asset a, m_category b, m_brand c, m_location d, m_status e, t_asset f, m_employee g " +
                    "where a.id_asset='" + id + "'and a.id_asset=f.id_asset and g.id_employee=f.id_employee and a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    this.idCategory = read["id_category"].ToString();
                    this.idBrand = read["id_brand"].ToString();
                    this.idLocation = read["id_location"].ToString();
                    this.idOwner = read["id_employee"].ToString();
                    tNoAsset.Text = read["asset"].ToString();
                    tItem.Text = read["item"].ToString();
                    tCategory.Text = read["category"].ToString();
                    tBrand.Text = read["brand"].ToString();
                    tLocation.Text = read["location"].ToString();
                    tNIK.Text = read["email"].ToString();
                    tOwner.Text = read["nama"].ToString();
                    tModel.Text = read["model"].ToString();
                    tSerial.Text = read["serial"].ToString();
                    tPrice.Text = read["price"].ToString();
                    tDesc.Text = read["description"].ToString();
                    purDate.Text = read["pur_date"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void bOK_Click(object sender, EventArgs e)
        {
            try
            {
                String id_employee = this.idOwner;
                String id_location = this.idLocation;
                String id_brand = this.idBrand;
                String id_category = this.idCategory;
                String pur_date = purDate.Text;
                String price = tPrice.Text;
                String description = tDesc.Text;
                String model = tModel.Text;
                String serial = tSerial.Text;
                String id_asset = this.idAsset;
                String asset = tNoAsset.Text;
                String item = tItem.Text;
                Boolean cek = edit.cekInput(id_employee, id_location, id_brand, id_category, pur_date, price, description, model);
                Boolean c = checkSoft.Checked;
                if (cek == true)
                {
                    String filename = null;
                    byte[] filebytes = null;
                    if (this.imgFile != null && this.imgFile != "")
                    {
                        filename = edit.getFileName(this.imgFile);
                        filebytes = edit.getFileBytes(this.imgFile);
                    }
                    edit.update(id_employee, id_location, id_brand, id_category, pur_date, price, description, model, serial, id_asset, this.userid, asset, c, filename, filebytes, item);
                    this.Close();
                }
                else
                {
                    lNotify.Text = "Salah input!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void baOwner_Click(object sender, EventArgs e)
        {
            try
            {
                AssetOwner cat = new AssetOwner();
                cat.ShowDialog();
                this.tOwner.Text = cat.namaOwner;
                this.tNIK.Text = cat.nikOwner;
                this.idOwner = cat.idOwner;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void baCategory_Click(object sender, EventArgs e)
        {
            try
            {
                AssetCategory cat = new AssetCategory();
                cat.ShowDialog();
                this.tCategory.Text = cat.namaCategory;
                this.idCategory = cat.idCategory;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void baBrand_Click(object sender, EventArgs e)
        {
            try
            {
                AssetBrand cat = new AssetBrand();
                cat.ShowDialog();
                this.tBrand.Text = cat.namaBrand;
                this.idBrand = cat.idBrand;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void baLocation_Click(object sender, EventArgs e)
        {
            try
            {
                AssetLocation cat = new AssetLocation();
                cat.ShowDialog();
                this.tLocation.Text = cat.namaLocation;
                this.idLocation = cat.idLocation;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EditAsset_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                //update m_license
                updateLicenseStatus(this.idAsset, 0);
                //insert temp_software
                deleteTemp(userid);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void deleteTemp(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("delete from temp_software where userid='" + id + "'", conn);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void checkSoft_CheckedChanged(object sender, EventArgs e)
        {
            if (checkSoft.Checked == true)
            {
                baSoft.Enabled = true;
                dataGSoft.Enabled = true;
            }
            else
            {
                baSoft.Enabled = false;
                dataGSoft.Enabled = false;
            }
        }

        private void baSoft_Click(object sender, EventArgs e)
        {
            try
            {
                AssetSoftware cat = new AssetSoftware(this.userid,this.idAsset);
                cat.ShowDialog();
                showDataGSoft(this.userid);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGSoft_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGSoft.Columns[0].Index)
                {
                    //Hapus
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menghapus data ini ? ",
                        "Konfirmasi Penghapusan", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (dr == DialogResult.Yes)
                        {
                            String id_software = dataGSoft[1, e.RowIndex].Value.ToString();
                            String id_license = dataGSoft[3, e.RowIndex].Value.ToString();
                            String id_employee = this.userid;
                            edit.delete(id_software, id_license, id_employee);
                            showDataGSoft(this.userid);

                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void EditAsset_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewButtonColumn c = new DataGridViewButtonColumn();
                dataGSoft.Columns.Add(c);
                c.HeaderText = "Hapus";
                c.Text = "Hapus";
                c.UseColumnTextForButtonValue = true;

                showData(this.idAsset);
                showSoftware(this.idAsset, this.userid);
                showImage(this.idAsset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showImage(String id)
        {
            try
            {
                byte[] bit = null;
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sqlcmd = new SqlCommand();
                SqlDataAdapter da = new SqlDataAdapter();
                DataTable dt = new DataTable();
                sqlcmd = new SqlCommand("select * from m_asset where id_asset='" + id + "'", conn);
                da = new SqlDataAdapter(sqlcmd);
                dt = new DataTable();
                da.Fill(dt);
                conn.Close();
                if (dt.Rows.Count > 0)
                {
                    //Get a stored PDF bytes
                    String dname = dt.Rows[0]["dname"].ToString();
                    if (dname != "")
                    {

                        bit = (byte[])dt.Rows[0]["dcontent"];
                        MemoryStream bitStream = new MemoryStream(bit);
                        Image image = Image.FromStream(bitStream);
                        pAsset.Image = image;
                    }
                    else
                    {
                        pAsset.Image = null;
                    }
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCancelUp_Click(object sender, EventArgs e)
        {
            try
            {
                showImage(this.idAsset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bUpload_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fDialog = new OpenFileDialog();
                fDialog.Title = "Pilih file untuk diunggah";
                fDialog.Filter = "Image|*.jpg;*.jpeg;*.png";
                if (fDialog.ShowDialog() == DialogResult.OK)
                {
                    this.imgFile = fDialog.FileName.ToString();
                    Image image = Image.FromFile(this.imgFile);
                    pAsset.Image = image;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EditAsset_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

        private void tPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }
    }
    
}
