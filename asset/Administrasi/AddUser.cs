﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class AddUser : Form
    {
        lib lib = new lib();
        Class.process.mAddUser add = new Class.process.mAddUser();
        public AddUser()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            try
            {
                String nama = tName.Text;
                String id = tNIK.Text;
                String dept = "1";// tDepartment.Text;
                String status = cAuth.SelectedValue.ToString();
                Boolean cek = add.cekInput(id, nama, status);

                if (cek == true)
                {
                    if (lib.ValidateEmail(tNIK.Text) && lib.cekEmail(tNIK.Text))
                    {
                        if (add.cekData(id) == "0")
                        {
                            add.insert(id, nama, dept, status);
                            UserManagement usr = new UserManagement();
                            usr.showdataGUser();
                            usr.Refresh();
                            this.Close();
                        }
                        else
                        {
                            lNotify.Text = "Email sudah dipakai sebelumnya!";
                        }
                    }
                    else
                    {
                        lNotify.Text = "Format email salah! Contoh : coba@wilmar.co.id";
                    }
                }
                else
                {
                    lNotify.Text = "Salah input!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AddUser_Load(object sender, EventArgs e)
        {
            showDepartment();
            showAuth();
        }
        public void showAuth() {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sc = conn.CreateCommand();
                sc.CommandType = CommandType.StoredProcedure;
                sc.CommandText = "show_auth";
                SqlDataReader reader;

                reader = sc.ExecuteReader();
                DataTable dt = new DataTable();

                dt.Columns.Add("id_auth", typeof(string));
                dt.Columns.Add("auth", typeof(string));
                dt.Load(reader);

                cAuth.ValueMember = "id_auth";
                cAuth.DisplayMember = "auth";
                cAuth.DataSource = dt;

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        public void showDepartment(){
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_dept";
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    tDepartment.Text = read[0].ToString();
                }
                tDepartment.Enabled = false;
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AddUser_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
    }
}
