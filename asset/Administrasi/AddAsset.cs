﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class AddAsset : Form
    {
        private String imgFile = null;
        private String idCategory = null;
        private String idBrand = null;
        private String idLocation = null;
        private String idOwner = null;
        private String Category = null;
        private String Brand = null;
        private String Loc = null;
        private String id;
        lib lib = new lib();
        Class.process.mAddAsset add = new Class.process.mAddAsset();
        public AddAsset(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            add.deleteTemp(id);
            InitializeComponent();
            this.id = id;
            tCategory.Enabled = false;
            tBrand.Enabled = false;  
            tLocation.Enabled = false;
            tNIK.Enabled = false;
            tOwner.Enabled = false;
        }
        
        private void bCancel_Click(object sender, EventArgs e)
        {
            try
            {
                add.deleteTemp(this.id);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            try
            {
                String id_employee = this.idOwner;
                String id_location = this.idLocation;
                String id_brand = this.idBrand;
                String id_category = this.idCategory;
                String pur_date = purDate.Text;
                String price = tPrice.Text;
                String description = tDesc.Text;
                String model = tModel.Text;
                String asset = tNoAsset.Text;
                String serial = tSerial.Text;
                String item = tItem.Text;
                Boolean cek = add.cekInput(id_employee, id_location, id_brand, id_category, pur_date, price, description, model);
                Boolean c = checkSoft.Checked;
                if (cek == true)
                {
                    View.SendEmail s = new View.SendEmail();
                    s.id_employee = id_employee;
                    List<string> infoAsset = new List<string>();
                    infoAsset.Add(asset);
                    infoAsset.Add(item);
                    infoAsset.Add(this.Category);
                    infoAsset.Add(this.Brand);
                    infoAsset.Add(model);
                    infoAsset.Add(this.Loc);
                    infoAsset.Add(serial);
                    infoAsset.Add(pur_date);
                    s.infoAsset = infoAsset.ToArray();
                    s.stat = 7;
                    s.indexStatus = 3;
                    s.form = 5;
                    s.ShowDialog();

                    if (Class.Email.email.cek == true)
                    {
                        String filename = null;
                        byte[] filebytes = null;
                        if (this.imgFile != null && this.imgFile != "")
                        {
                            filename = add.getFileName(this.imgFile);
                            filebytes = add.getFileBytes(this.imgFile);
                        }
                        add.insert(id_employee, id_location, id_brand, id_category, pur_date, price, description, model, serial, asset, this.id, c, item, filename, filebytes);
                        this.Close();
                        MessageBox.Show("Aset berhasil ditambahkan", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("Aset gagal ditambahkan", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    
                }
                else
                {
                    lNotify.Text = "Salah input!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void baCategory_Click(object sender, EventArgs e)
        {
            try
            {
                AssetCategory cat = new AssetCategory();
                cat.ShowDialog();
                this.Category = cat.namaCategory;
                this.tCategory.Text = cat.namaCategory;
                this.idCategory = cat.idCategory;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void baBrand_Click(object sender, EventArgs e)
        {
            try
            {
                AssetBrand cat = new AssetBrand();
                cat.ShowDialog();
                this.Brand = cat.namaBrand;
                this.tBrand.Text = cat.namaBrand;
                this.idBrand = cat.idBrand;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void baLocation_Click(object sender, EventArgs e)
        {
            try
            {
                AssetLocation cat = new AssetLocation();
                cat.ShowDialog();
                this.Loc = cat.namaLocation;
                this.tLocation.Text = cat.namaLocation;
                this.idLocation = cat.idLocation;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void baOwner_Click(object sender, EventArgs e)
        {
            try
            {
                AssetOwner cat = new AssetOwner();
                cat.ShowDialog();
                this.tOwner.Text = cat.namaOwner;
                this.tNIK.Text = cat.nikOwner;
                this.idOwner = cat.idOwner;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void baSoft_Click(object sender, EventArgs e)
        {
            try
            {
                AssetSoftware cat = new AssetSoftware(this.id,"0");
                cat.ShowDialog();
                showDataGSoft(this.id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AddAsset_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                add.deleteTemp(this.id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void checkSoft_CheckedChanged(object sender, EventArgs e)
        {
            if (checkSoft.Checked == true)
            {
                baSoft.Enabled = true;
                dataGSoft.Enabled = true;
            }
            else
            {
                baSoft.Enabled = false;
                dataGSoft.Enabled = false;
            }
        }
        public void showDataGSoft(String id) 
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_datagrid_soft";
                myCommand.Parameters.AddWithValue("@userid", id);
                SqlDataAdapter data = new SqlDataAdapter();
                data.SelectCommand = myCommand;
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "locationTable");
                conn.Close();
                this.dataGSoft.DataSource = dataSet.Tables["locationTable"].DefaultView;
                this.dataGSoft.Columns["IDSoftware"].Visible = false;
                this.dataGSoft.Columns["IDLicense"].Visible = false;
                this.dataGSoft.Columns["IDCategory"].Visible = false;
                this.dataGSoft.Columns["IDStatus"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AddAsset_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewButtonColumn c = new DataGridViewButtonColumn();
                dataGSoft.Columns.Add(c);
                c.HeaderText = "Hapus";
                c.Text = "Hapus";
                c.UseColumnTextForButtonValue = true;
                showDataGSoft(this.id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGSoft_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGSoft.Columns[0].Index)
                {
                    //Hapus
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menghapus data ini ? ",
                        "Konfirmasi Penghapusan", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (dr == DialogResult.Yes)
                        {
                            String id_software = dataGSoft[1, e.RowIndex].Value.ToString();
                            String id_license = dataGSoft[3, e.RowIndex].Value.ToString();
                            String id_employee = this.id;
                            add.delete(id_software,id_license, id_employee);
                            showDataGSoft(this.id);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void bUpload_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fDialog = new OpenFileDialog();
                fDialog.Title = "Pilih file untuk diunggah";
                fDialog.Filter = "Image|*.jpg;*.jpeg;*.png";
                if (fDialog.ShowDialog() == DialogResult.OK)
                {
                    this.imgFile = fDialog.FileName.ToString();
                    Image image = Image.FromFile(this.imgFile);
                    pAsset.Image = image;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCancelUp_Click(object sender, EventArgs e)
        {
            this.imgFile = null;
            pAsset.Image = null;
        }

        private void AddAsset_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

        private void tPrice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void tNoAsset_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regexItem = new Regex("^[a-zA-Z0-9 ]*$");
            if (!char.IsControl(e.KeyChar)&&!regexItem.IsMatch(e.KeyChar.ToString())) { e.Handled = true; }
        }

    }
    
}
