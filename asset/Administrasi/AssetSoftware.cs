﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class AssetSoftware : Form
    {
        lib lib = new lib();
        Class.process.mAssetSoftware s = new Class.process.mAssetSoftware();
        private String id;
        private String id_asset;
        public AssetSoftware(String id, String id_asset)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
            this.id_asset = id_asset;
        }

        private void AssetSoftware_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewButtonColumn a = new DataGridViewButtonColumn();
                dataGSoft.Columns.Add(a);
                a.HeaderText = "Pilih";
                a.Text = "Pilih";
                a.UseColumnTextForButtonValue = true;

                String str = tSearch.Text;
                searchDataSoftware(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void showDataSoftware()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_software";
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "locationTable");
                conn.Close();
                this.dataGSoft.DataSource = dataSet.Tables["locationTable"].DefaultView;
                this.dataGSoft.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bSearch_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearch.Text;
                searchDataSoftware(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void searchDataSoftware(String str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "search_data_software";
                myCommand.Parameters.AddWithValue("@str", str);
                myCommand.Parameters.AddWithValue("@id_asset", this.id_asset);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "locationTable");
                conn.Close();
                this.dataGSoft.DataSource = dataSet.Tables["locationTable"].DefaultView;
                this.dataGSoft.Columns["IDSoftware"].Visible = false;
                this.dataGSoft.Columns["IDLicense"].Visible = false;
                this.dataGSoft.Columns["IDCategory"].Visible = false;
                this.dataGSoft.Columns["IDStatus"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                showDataSoftware();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            try
            {
                AddSoftware add = new AddSoftware(this.id);
                add.ShowDialog();
                showDataSoftware();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGSoft_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGSoft.Columns[0].Index)
                {
                    //Pilih
                    try
                    {
                        String idSoftware = dataGSoft[1, e.RowIndex].Value.ToString();
                        String idLicense = dataGSoft[3, e.RowIndex].Value.ToString();
                        if (s.cekTable(idSoftware,idLicense, this.id))
                        {
                            s.insert(idSoftware, idLicense, this.id);
                            this.Close();
                        }
                        else
                        {
                            MessageBox.Show("Software sudah dipilih sebelumnya!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void AssetSoftware_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

        private void tSearch_TextChanged(object sender, EventArgs e)
        {
            String str = tSearch.Text;
            searchDataSoftware(str);
        }
    }
}
