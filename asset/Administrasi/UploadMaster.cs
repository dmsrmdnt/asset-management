﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace asset.Administrasi
{
    public partial class UploadMaster : Form
    {
        lib lib = new lib();
        public UploadMaster()
        {
            InitializeComponent();
        }
        private static UploadMaster form;
        public static UploadMaster GetChildInstance()
        {
            if (form == null)
                form = new UploadMaster();
            return form;
        }
        private void UploadMaster_Load(object sender, EventArgs e)
        {

        }

        private void UploadMaster_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fDialog = new OpenFileDialog();
                fDialog.Title = "Pilih file untuk diunggah";
                fDialog.Filter = "Excel Files|*.xls;";
                if (fDialog.ShowDialog() == DialogResult.OK)
                {
                    tMasterEmployee.Text = fDialog.FileName.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fDialog = new OpenFileDialog();
                fDialog.Title = "Pilih file untuk diunggah";
                fDialog.Filter = "Excel Files|*.xls;";
                if (fDialog.ShowDialog() == DialogResult.OK)
                {
                    tMasterAsset.Text = fDialog.FileName.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UploadMaster_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Class.Excel.UploadMaster p = new Class.Excel.UploadMaster();
            String fileMasterEmployee = tMasterEmployee.Text;
            String fileMasterAsset = tMasterAsset.Text;
            if (
                fileMasterEmployee != "" && fileMasterEmployee != null &&
                fileMasterAsset != "" && fileMasterAsset != null
                )
            {
                DialogResult dr = MessageBox.Show("Apakah anda yakin untuk mengunggah data baru dan menghapus seluruh data yang lama ? ",
                        "Konfirmasi Persetujuan", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.Yes)
                {
                    p.truncateTable();

                    string filetype;
                    string filename1;
                    string filename2;
                    string appPath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\Master\\";
                    System.IO.FileInfo fi;

                    //upload master employee
                    filetype = tMasterEmployee.Text.Substring(Convert.ToInt32(tMasterEmployee.Text.LastIndexOf(".")) + 1, tMasterEmployee.Text.Length - (Convert.ToInt32(tMasterEmployee.Text.LastIndexOf(".")) + 1));
                    filename1 = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ') + "MasterEmployee" + "." + filetype;

                    fi = new System.IO.FileInfo(tMasterEmployee.Text);
                    fi.Refresh();
                    fi.CopyTo(appPath + filename1);
                    

                    //upload master asset
                    filetype = tMasterAsset.Text.Substring(Convert.ToInt32(tMasterAsset.Text.LastIndexOf(".")) + 1, tMasterAsset.Text.Length - (Convert.ToInt32(tMasterAsset.Text.LastIndexOf(".")) + 1));
                    filename2 = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ') + "MasterAsset" + "." + filetype;

                    fi = new System.IO.FileInfo(tMasterAsset.Text);
                    fi.Refresh();
                    fi.CopyTo(appPath + filename2);

                    View.UploadMaster upload = new View.UploadMaster();
                    upload.fileEmployee = appPath + filename1;
                    upload.fileAsset = appPath + filename2;
                    upload.ShowDialog();
                    upload.BringToFront();

                    if (asset.Class.Excel.UploadMaster.status == true)
                    {
                        MessageBox.Show("Upload data master berhasil", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        clearField();
                    }
                    else
                    {
                        p.truncateTable();
                        MessageBox.Show("Upload data master gagal", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }                      
                }
            }
            else 
            {
                MessageBox.Show("Pilih file master untuk diunggah!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void clearField() 
        {
            tMasterAsset.Clear();
            tMasterEmployee.Clear();
        }
    }
}
