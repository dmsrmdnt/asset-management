﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset.Administrasi
{
    public partial class SoftwareManagement : Form
    {
        Class.Search.search s = new Class.Search.search();
        Class.process.mSoftwareManagement soft = new Class.process.mSoftwareManagement();
        lib lib = new lib();
        public string id = "";
        public SoftwareManagement()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }
        private static SoftwareManagement form;
        public static SoftwareManagement GetChildInstance()
        {
            if (form == null)
                form = new SoftwareManagement();
            return form;
        }
        private void SoftwareManagement_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void SoftwareManagement_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

        private void bSearch_Click(object sender, EventArgs e)
        {
            process();
        }
        void showDataC()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sc = new SqlCommand("select id_category,category from m_softwareCat", conn);
                SqlDataReader read;
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(string));
                dt.Columns.Add("category", typeof(string));
                dt.Rows.Add(new object[] { "0", "All" });
                read = sc.ExecuteReader();
                while (read.Read())
                {
                    dt.Rows.Add(new object[] { read["id_category"].ToString(), read["category"].ToString() });
                }

                cKategori.ValueMember = "id";
                cKategori.DisplayMember = "category";
                cKategori.DataSource = dt;

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        void showDataS()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sc = new SqlCommand("select id_status,status from m_softwareStat", conn);
                SqlDataReader read;
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(string));
                dt.Columns.Add("status", typeof(string));
                dt.Rows.Add(new object[] { "0", "All" });
                read = sc.ExecuteReader();
                while (read.Read())
                {
                    dt.Rows.Add(new object[] { read["id_status"].ToString(), read["status"].ToString() });
                }

                cStatus.ValueMember = "id";
                cStatus.DisplayMember = "status";
                cStatus.DataSource = dt;

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void SoftwareManagement_Load(object sender, EventArgs e)
        {
            DataGridViewButtonColumn a = new DataGridViewButtonColumn();
            dataGSoft.Columns.Add(a);
            a.HeaderText = "Edit";
            a.Text = "Edit";
            a.UseColumnTextForButtonValue = true;
            DataGridViewButtonColumn b = new DataGridViewButtonColumn();
            dataGSoft.Columns.Add(b);
            b.HeaderText = "Delete";
            b.Text = "Delete";
            b.UseColumnTextForButtonValue = true;
            showDataC();
            showDataS();
            process();
        }

        private void cKategori_SelectedIndexChanged(object sender, EventArgs e)
        {
            process();
        }

        private void cStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            process();
        }

        private void tSoft_KeyUp(object sender, KeyEventArgs e)
        {
            process();
        }
        private void process()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "search_software";
                myCommand.Parameters.AddWithValue("@category", s.getIndex(cKategori.Text,cKategori.SelectedIndex.ToString()));
                myCommand.Parameters.AddWithValue("@status", s.getIndex(cStatus.Text,cStatus.SelectedIndex.ToString()));
                myCommand.Parameters.AddWithValue("@software", tSoft.Text);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                DataTable t = new DataTable();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(t);
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGSoft.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGSoft.Columns["ID"].Visible = false;
                this.dataGSoft.Columns["IDKategori"].Visible = false;
                this.dataGSoft.Columns["IDStatus"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGSoft_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGSoft.Columns[0].Index)
                {
                    //Edit
                    try
                    {
                        Administrasi.AddNewSoftware s = new Administrasi.AddNewSoftware();
                        s.tittle = "Update Software";
                        s.status = 1;
                        String id_software = dataGSoft[2, e.RowIndex].Value.ToString();
                        String id_category = dataGSoft[4, e.RowIndex].Value.ToString();
                        String id_status = dataGSoft[6, e.RowIndex].Value.ToString();
                        String software = dataGSoft[3, e.RowIndex].Value.ToString();
                        s.id_software = Convert.ToInt32(id_software);
                        s.id_status = Convert.ToInt32(id_status);
                        s.id_category = Convert.ToInt32(id_category);
                        s.software = software;
                        s.ShowDialog();
                        s.BringToFront();
                        process();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGSoft.Columns[1].Index)
                {
                    //Delete
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menghapus data ini ? ",
                        "Konfirmasi Penghapusan", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (dr == DialogResult.Yes)
                        {
                            String id_software = dataGSoft[2, e.RowIndex].Value.ToString();
                            Boolean cek = soft.CekData(id_software);
                            if (cek)
                            {
                                soft.delete(id_software);
                                process();
                            }
                            else
                            {
                                MessageBox.Show("Data ini mempunyai relasi dengan tabel lain!",
                       "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            Administrasi.AddNewSoftware s = new Administrasi.AddNewSoftware();
            s.tittle = "Tambah Software";
            s.status = 0;
            s.ShowDialog();
            s.BringToFront();
            process();
        }
    }
}
