﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace asset.Administrasi
{
    public partial class RequestManagement : Form
    {
        lib lib = new lib();
        String id;
        Class.process.mRequestManagement req = new Class.process.mRequestManagement();
        public RequestManagement(String id)
        {
            InitializeComponent();
            this.id = id;
        }
        private static RequestManagement form;
        public static RequestManagement GetChildInstance(String id)
        {
            if (form == null)
                form = new RequestManagement(id);
            return form;
        }
        void listStatus()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(string));
            dt.Columns.Add("status", typeof(string));
            dt.Rows.Add(new object[] { "", "All" });
            dt.Rows.Add(new object[] { "1", "Unread" });
            dt.Rows.Add(new object[] { "0", "Read" });
            cStatus.ValueMember = "id";
            cStatus.DisplayMember = "status";
            cStatus.DataSource = dt;
        }
        private void bSearch_Click(object sender, EventArgs e)
        {
            process();
        }
        void process()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "request_history";
                myCommand.Parameters.AddWithValue("@awalReq", dAwal.Text);
                myCommand.Parameters.AddWithValue("@akhirReq", dAkhir.Text);
                myCommand.Parameters.AddWithValue("@acc", cStatus.SelectedValue.ToString());
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                DataTable t = new DataTable();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(t);
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGAssetL.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGAssetL.Columns["IDEmployee"].Visible = false;
                this.dataGAssetL.Columns["IDRequest"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RequestManagement_Load(object sender, EventArgs e)
        {
            DataGridViewButtonColumn g = new DataGridViewButtonColumn();
            dataGAssetL.Columns.Add(g);
            g.HeaderText = "Print";
            g.Text = "Print";
            g.UseColumnTextForButtonValue = true;

            DataGridViewButtonColumn h = new DataGridViewButtonColumn();
            dataGAssetL.Columns.Add(h);
            h.HeaderText = "Delete";
            h.Text = "Delete";
            h.UseColumnTextForButtonValue = true;

            listStatus();
            process();
        }

        private void RequestManagement_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

        private void RequestManagement_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void cStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            process();
        }

        private void dataGAssetL_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGAssetL.Columns[0].Index)
                {
                    //Print
                    try
                    {
                        String id_request = dataGAssetL[2, e.RowIndex].Value.ToString();
                        req.update(id_request,this.id);
                        Report.PreviewPrintRequest p = new Report.PreviewPrintRequest();
                        p.id_request = id_request;
                        p.ShowDialog();
                        p.BringToFront();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetL.Columns[1].Index)
                {
                    //Delete
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menghapus permohonan ini ? ",
                        "Konfirmasi Persetujuan", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            String id_request = dataGAssetL[2, e.RowIndex].Value.ToString();
                            req.delete(id_request);
                            process();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
    }
}
