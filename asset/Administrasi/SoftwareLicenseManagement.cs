﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset.Administrasi
{
    public partial class SoftwareLicenseManagement : Form
    {
        Class.Search.search s = new Class.Search.search();
        Class.process.mSoftwareLicenseManagement soft = new Class.process.mSoftwareLicenseManagement();
        lib lib = new lib();
        public string id = "";
        public SoftwareLicenseManagement()
        {
            InitializeComponent();
        }
        private static SoftwareLicenseManagement form;
        public static SoftwareLicenseManagement GetChildInstance()
        {
            if (form == null)
                form = new SoftwareLicenseManagement();
            return form;
        }
        private void SoftwareLicenseManagement_Load(object sender, EventArgs e)
        {
            DataGridViewButtonColumn a = new DataGridViewButtonColumn();
            dataGSoft.Columns.Add(a);
            a.HeaderText = "Edit";
            a.Text = "Edit";
            a.UseColumnTextForButtonValue = true;
            DataGridViewButtonColumn b = new DataGridViewButtonColumn();
            dataGSoft.Columns.Add(b);
            b.HeaderText = "Delete";
            b.Text = "Delete";
            b.UseColumnTextForButtonValue = true;
            DataGridViewButtonColumn c = new DataGridViewButtonColumn();
            dataGSoft.Columns.Add(c);
            c.HeaderText = "License Update";
            c.Text = "Update";
            c.UseColumnTextForButtonValue = true;
            showDataC();
            process();
        }
        private void process()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "search_software_license";
                myCommand.Parameters.AddWithValue("@category", s.getIndex(cKategori.Text, cKategori.SelectedIndex.ToString()));
                myCommand.Parameters.AddWithValue("@software", tSoft.Text);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                DataTable t = new DataTable();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(t);
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGSoft.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGSoft.Columns["IDAsset"].Visible = false;
                this.dataGSoft.Columns["IDSoftware"].Visible = false;
                this.dataGSoft.Columns["IDLicense"].Visible = false;
                this.dataGSoft.Columns["IDKategori"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void SoftwareLicenseManagement_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void SoftwareLicenseManagement_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
        void showDataC()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sc = new SqlCommand("select id_category,category from m_softwareCat", conn);
                SqlDataReader read;
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(string));
                dt.Columns.Add("category", typeof(string));
                dt.Rows.Add(new object[] { "0", "All" });
                read = sc.ExecuteReader();
                while (read.Read())
                {
                    dt.Rows.Add(new object[] { read["id_category"].ToString(), read["category"].ToString() });
                }

                cKategori.ValueMember = "id";
                cKategori.DisplayMember = "category";
                cKategori.DataSource = dt;

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void cKategori_SelectedIndexChanged(object sender, EventArgs e)
        {
            process();
        }

        private void tSoft_TextChanged(object sender, EventArgs e)
        {
            process();
        }

        private void bSearch_Click(object sender, EventArgs e)
        {
            process();
        }

        private void dataGSoft_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGSoft.Columns[0].Index)
                {
                    //Edit
                    try
                    {
                        Administrasi.AddSoftwareLicense s = new Administrasi.AddSoftwareLicense();
                        s.tittle = "Edit Lisensi Software";
                        s.status = 1;
                        String id_assetSoft = dataGSoft[7, e.RowIndex].Value.ToString();
                        String id_license = dataGSoft[10, e.RowIndex].Value.ToString();
                        String id_category = dataGSoft[13, e.RowIndex].Value.ToString();
                        String license = dataGSoft[11, e.RowIndex].Value.ToString();
                        String exp_date = dataGSoft[12, e.RowIndex].Value.ToString();
                        s.id_assetSoft = Convert.ToInt32(id_assetSoft);
                        s.id_license = Convert.ToInt32(id_license);
                        s.license = license;
                        s.id_category = Convert.ToInt32(id_category);
                        s.exp_date = exp_date;
                        s.ShowDialog();
                        s.BringToFront();
                        process();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGSoft.Columns[1].Index)
                {
                    //Delete
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menghapus data ini ? ",
                        "Konfirmasi Penghapusan", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (dr == DialogResult.Yes)
                        {
                            String id_license = dataGSoft[10, e.RowIndex].Value.ToString();
                            Boolean cek = soft.CekData(id_license);
                            if (cek)
                            {
                                soft.delete(id_license);
                                process();
                            }
                            else
                            {
                                MessageBox.Show("Data ini mempunyai relasi dengan tabel lain!",
                       "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGSoft.Columns[2].Index)
                {
                    //Update License
                    try
                    {
                        Administrasi.AddSoftwareLicense s = new Administrasi.AddSoftwareLicense();
                        s.tittle = "Update Lisensi Software";
                        s.status = 2;
                        String id_assetSoft = dataGSoft[7, e.RowIndex].Value.ToString();
                        String id_license = dataGSoft[10, e.RowIndex].Value.ToString();
                        String id_category = dataGSoft[13, e.RowIndex].Value.ToString();
                        s.id_assetSoft = Convert.ToInt32(id_assetSoft);
                        s.id_license = Convert.ToInt32(id_license);
                        s.id_category = Convert.ToInt32(id_category);
                        s.ShowDialog();
                        s.BringToFront();
                        process();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            Administrasi.AddSoftwareLicense s = new Administrasi.AddSoftwareLicense();
            s.tittle = "Tambah Lisensi Software";
            s.status = 0;
            s.ShowDialog();
            s.BringToFront();
            process();
        }
    }
}
