﻿namespace asset
{
    partial class AssetListD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tSearchU = new System.Windows.Forms.TextBox();
            this.bSearchU = new System.Windows.Forms.Button();
            this.dataGAsset = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGAsset)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.groupBox1.Controls.Add(this.tSearchU);
            this.groupBox1.Controls.Add(this.bSearchU);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(12, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(954, 44);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cari Nomor Aset";
            // 
            // tSearchU
            // 
            this.tSearchU.Location = new System.Drawing.Point(6, 17);
            this.tSearchU.Name = "tSearchU";
            this.tSearchU.Size = new System.Drawing.Size(843, 20);
            this.tSearchU.TabIndex = 1;
            this.tSearchU.TextChanged += new System.EventHandler(this.bSearchU_Click);
            // 
            // bSearchU
            // 
            this.bSearchU.ForeColor = System.Drawing.Color.Black;
            this.bSearchU.Location = new System.Drawing.Point(855, 15);
            this.bSearchU.Name = "bSearchU";
            this.bSearchU.Size = new System.Drawing.Size(75, 23);
            this.bSearchU.TabIndex = 2;
            this.bSearchU.Text = "Cari";
            this.bSearchU.UseVisualStyleBackColor = true;
            this.bSearchU.Click += new System.EventHandler(this.bSearchU_Click);
            // 
            // dataGAsset
            // 
            this.dataGAsset.AllowUserToAddRows = false;
            this.dataGAsset.AllowUserToDeleteRows = false;
            this.dataGAsset.AllowUserToResizeColumns = false;
            this.dataGAsset.AllowUserToResizeRows = false;
            this.dataGAsset.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGAsset.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGAsset.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGAsset.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGAsset.Location = new System.Drawing.Point(12, 53);
            this.dataGAsset.MultiSelect = false;
            this.dataGAsset.Name = "dataGAsset";
            this.dataGAsset.ReadOnly = true;
            this.dataGAsset.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGAsset.Size = new System.Drawing.Size(954, 392);
            this.dataGAsset.TabIndex = 21;
            this.dataGAsset.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGAsset_CellContentClick);
            // 
            // AssetListD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(978, 457);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGAsset);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AssetListD";
            this.Text = "Daftar Aset";
            this.Load += new System.EventHandler(this.AssetListD_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AssetListD_Paint);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGAsset)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tSearchU;
        private System.Windows.Forms.Button bSearchU;
        private System.Windows.Forms.DataGridView dataGAsset;
    }
}