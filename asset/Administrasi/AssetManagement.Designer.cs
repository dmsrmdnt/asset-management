﻿namespace asset
{
    partial class AssetManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bRefresh = new System.Windows.Forms.Button();
            this.bAdd = new System.Windows.Forms.Button();
            this.dataGAsset = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tSearch = new System.Windows.Forms.TextBox();
            this.bSearch = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGAsset)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // bRefresh
            // 
            this.bRefresh.ForeColor = System.Drawing.Color.Black;
            this.bRefresh.Location = new System.Drawing.Point(132, 22);
            this.bRefresh.Name = "bRefresh";
            this.bRefresh.Size = new System.Drawing.Size(120, 40);
            this.bRefresh.TabIndex = 3;
            this.bRefresh.Text = "Refresh";
            this.bRefresh.UseVisualStyleBackColor = true;
            this.bRefresh.Click += new System.EventHandler(this.bRefresh_Click);
            // 
            // bAdd
            // 
            this.bAdd.ForeColor = System.Drawing.Color.Black;
            this.bAdd.Location = new System.Drawing.Point(6, 22);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(120, 40);
            this.bAdd.TabIndex = 0;
            this.bAdd.Text = "Tambah Aset Baru";
            this.bAdd.UseVisualStyleBackColor = true;
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // dataGAsset
            // 
            this.dataGAsset.AllowUserToAddRows = false;
            this.dataGAsset.AllowUserToDeleteRows = false;
            this.dataGAsset.AllowUserToResizeColumns = false;
            this.dataGAsset.AllowUserToResizeRows = false;
            this.dataGAsset.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGAsset.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGAsset.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGAsset.Location = new System.Drawing.Point(20, 71);
            this.dataGAsset.MultiSelect = false;
            this.dataGAsset.Name = "dataGAsset";
            this.dataGAsset.ReadOnly = true;
            this.dataGAsset.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGAsset.Size = new System.Drawing.Size(986, 393);
            this.dataGAsset.TabIndex = 6;
            this.dataGAsset.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGAsset_CellContentClick);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.bRefresh);
            this.groupBox1.Controls.Add(this.bAdd);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(20, 470);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(986, 76);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Operasi";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.tSearch);
            this.groupBox3.Controls.Add(this.bSearch);
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(20, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(986, 53);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Cari";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "No. Aset";
            // 
            // tSearch
            // 
            this.tSearch.Location = new System.Drawing.Point(65, 20);
            this.tSearch.Name = "tSearch";
            this.tSearch.Size = new System.Drawing.Size(429, 20);
            this.tSearch.TabIndex = 1;
            this.tSearch.TextChanged += new System.EventHandler(this.bSearch_Click);
            // 
            // bSearch
            // 
            this.bSearch.ForeColor = System.Drawing.Color.Black;
            this.bSearch.Location = new System.Drawing.Point(905, 18);
            this.bSearch.Name = "bSearch";
            this.bSearch.Size = new System.Drawing.Size(75, 23);
            this.bSearch.TabIndex = 0;
            this.bSearch.Text = "Cari";
            this.bSearch.UseVisualStyleBackColor = true;
            this.bSearch.Click += new System.EventHandler(this.bSearch_Click);
            // 
            // AssetManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1018, 558);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGAsset);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "AssetManagement";
            this.Text = "Kelola Aset";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AssetManagement_FormClosed);
            this.Load += new System.EventHandler(this.AssetManagement_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AssetManagement_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.dataGAsset)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button bRefresh;
        private System.Windows.Forms.Button bAdd;
        private System.Windows.Forms.DataGridView dataGAsset;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tSearch;
        private System.Windows.Forms.Button bSearch;
        private System.Windows.Forms.Label label1;
    }
}