﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class AddPIC : Form
    {
        lib lib = new lib();
        Class.process.mAddPIC add = new Class.process.mAddPIC();
        public AddPIC()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            try
            {
                String pic = tPIC.Text;
                String alamat = tAlamat.Text;
                String telp = tTelp.Text;
                Boolean cek = add.cekInput(pic,alamat,telp);
                if (cek == true)
                {
                    if (add.cekData(pic) == "0")
                    {
                        add.insert(pic, alamat, telp);
                        this.Close();
                    }
                    else
                    {
                        lNotify.Text = "PIC sudah ada!";
                    }
                }
                else
                {
                    lNotify.Text = "Salah input!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AddBrand_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
    }
}
