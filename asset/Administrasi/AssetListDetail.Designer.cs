﻿namespace asset
{
    partial class AssetListDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.gOwner = new System.Windows.Forms.GroupBox();
            this.tDepartment = new System.Windows.Forms.TextBox();
            this.tName = new System.Windows.Forms.TextBox();
            this.tNIK = new System.Windows.Forms.TextBox();
            this.lDepartment = new System.Windows.Forms.Label();
            this.lName = new System.Windows.Forms.Label();
            this.lNIK = new System.Windows.Forms.Label();
            this.gAsset = new System.Windows.Forms.GroupBox();
            this.tSearch = new System.Windows.Forms.TextBox();
            this.bSearch = new System.Windows.Forms.Button();
            this.dataGAsset = new System.Windows.Forms.DataGridView();
            this.gOwner.SuspendLayout();
            this.gAsset.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGAsset)).BeginInit();
            this.SuspendLayout();
            // 
            // gOwner
            // 
            this.gOwner.BackColor = System.Drawing.Color.Transparent;
            this.gOwner.Controls.Add(this.tDepartment);
            this.gOwner.Controls.Add(this.tName);
            this.gOwner.Controls.Add(this.tNIK);
            this.gOwner.Controls.Add(this.lDepartment);
            this.gOwner.Controls.Add(this.lName);
            this.gOwner.Controls.Add(this.lNIK);
            this.gOwner.ForeColor = System.Drawing.Color.White;
            this.gOwner.Location = new System.Drawing.Point(12, 12);
            this.gOwner.Name = "gOwner";
            this.gOwner.Size = new System.Drawing.Size(898, 109);
            this.gOwner.TabIndex = 0;
            this.gOwner.TabStop = false;
            this.gOwner.Text = "Pemilik";
            // 
            // tDepartment
            // 
            this.tDepartment.Enabled = false;
            this.tDepartment.Location = new System.Drawing.Point(104, 75);
            this.tDepartment.Name = "tDepartment";
            this.tDepartment.ReadOnly = true;
            this.tDepartment.Size = new System.Drawing.Size(188, 20);
            this.tDepartment.TabIndex = 5;
            // 
            // tName
            // 
            this.tName.Enabled = false;
            this.tName.Location = new System.Drawing.Point(104, 49);
            this.tName.Name = "tName";
            this.tName.ReadOnly = true;
            this.tName.Size = new System.Drawing.Size(309, 20);
            this.tName.TabIndex = 4;
            // 
            // tNIK
            // 
            this.tNIK.Enabled = false;
            this.tNIK.Location = new System.Drawing.Point(104, 23);
            this.tNIK.Name = "tNIK";
            this.tNIK.ReadOnly = true;
            this.tNIK.Size = new System.Drawing.Size(309, 20);
            this.tNIK.TabIndex = 3;
            // 
            // lDepartment
            // 
            this.lDepartment.AutoSize = true;
            this.lDepartment.Location = new System.Drawing.Point(6, 73);
            this.lDepartment.Name = "lDepartment";
            this.lDepartment.Size = new System.Drawing.Size(65, 13);
            this.lDepartment.TabIndex = 2;
            this.lDepartment.Text = "Departemen";
            // 
            // lName
            // 
            this.lName.AutoSize = true;
            this.lName.Location = new System.Drawing.Point(6, 49);
            this.lName.Name = "lName";
            this.lName.Size = new System.Drawing.Size(35, 13);
            this.lName.TabIndex = 1;
            this.lName.Text = "Nama";
            // 
            // lNIK
            // 
            this.lNIK.AutoSize = true;
            this.lNIK.Location = new System.Drawing.Point(6, 26);
            this.lNIK.Name = "lNIK";
            this.lNIK.Size = new System.Drawing.Size(32, 13);
            this.lNIK.TabIndex = 0;
            this.lNIK.Text = "Email";
            // 
            // gAsset
            // 
            this.gAsset.BackColor = System.Drawing.Color.Transparent;
            this.gAsset.Controls.Add(this.tSearch);
            this.gAsset.Controls.Add(this.bSearch);
            this.gAsset.Controls.Add(this.dataGAsset);
            this.gAsset.ForeColor = System.Drawing.Color.White;
            this.gAsset.Location = new System.Drawing.Point(12, 127);
            this.gAsset.Name = "gAsset";
            this.gAsset.Size = new System.Drawing.Size(904, 292);
            this.gAsset.TabIndex = 1;
            this.gAsset.TabStop = false;
            this.gAsset.Text = "Daftar Aset";
            // 
            // tSearch
            // 
            this.tSearch.Location = new System.Drawing.Point(9, 24);
            this.tSearch.Name = "tSearch";
            this.tSearch.Size = new System.Drawing.Size(283, 20);
            this.tSearch.TabIndex = 0;
            this.tSearch.TextChanged += new System.EventHandler(this.bSearch_Click);
            // 
            // bSearch
            // 
            this.bSearch.ForeColor = System.Drawing.Color.Black;
            this.bSearch.Location = new System.Drawing.Point(298, 22);
            this.bSearch.Name = "bSearch";
            this.bSearch.Size = new System.Drawing.Size(75, 23);
            this.bSearch.TabIndex = 1;
            this.bSearch.Text = "Cari";
            this.bSearch.UseVisualStyleBackColor = true;
            this.bSearch.Click += new System.EventHandler(this.bSearch_Click);
            // 
            // dataGAsset
            // 
            this.dataGAsset.AllowUserToAddRows = false;
            this.dataGAsset.AllowUserToDeleteRows = false;
            this.dataGAsset.AllowUserToResizeColumns = false;
            this.dataGAsset.AllowUserToResizeRows = false;
            this.dataGAsset.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGAsset.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGAsset.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGAsset.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGAsset.Location = new System.Drawing.Point(12, 51);
            this.dataGAsset.MultiSelect = false;
            this.dataGAsset.Name = "dataGAsset";
            this.dataGAsset.ReadOnly = true;
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            this.dataGAsset.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGAsset.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGAsset.Size = new System.Drawing.Size(886, 235);
            this.dataGAsset.TabIndex = 8;
            this.dataGAsset.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGAsset_CellContentClick);
            // 
            // AssetListDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(928, 431);
            this.Controls.Add(this.gAsset);
            this.Controls.Add(this.gOwner);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AssetListDetail";
            this.Text = "Detail Daftar Aset ";
            this.Load += new System.EventHandler(this.AssetListDetail_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AssetListDetail_Paint);
            this.gOwner.ResumeLayout(false);
            this.gOwner.PerformLayout();
            this.gAsset.ResumeLayout(false);
            this.gAsset.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGAsset)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gOwner;
        private System.Windows.Forms.TextBox tDepartment;
        private System.Windows.Forms.TextBox tName;
        private System.Windows.Forms.TextBox tNIK;
        private System.Windows.Forms.Label lDepartment;
        private System.Windows.Forms.Label lName;
        private System.Windows.Forms.Label lNIK;
        private System.Windows.Forms.GroupBox gAsset;
        private System.Windows.Forms.TextBox tSearch;
        private System.Windows.Forms.Button bSearch;
        private System.Windows.Forms.DataGridView dataGAsset;
    }
}