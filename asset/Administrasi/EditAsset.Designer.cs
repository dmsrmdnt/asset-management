﻿namespace asset
{
    partial class EditAsset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.purDate = new System.Windows.Forms.DateTimePicker();
            this.lDate = new System.Windows.Forms.Label();
            this.groupBoxAsset = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tItem = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.bCancelUp = new System.Windows.Forms.Button();
            this.pAsset = new System.Windows.Forms.PictureBox();
            this.bUpload = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tNoAsset = new System.Windows.Forms.TextBox();
            this.checkSoft = new System.Windows.Forms.CheckBox();
            this.dataGSoft = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.baSoft = new System.Windows.Forms.Button();
            this.lNotify = new System.Windows.Forms.Label();
            this.baLocation = new System.Windows.Forms.Button();
            this.baBrand = new System.Windows.Forms.Button();
            this.baCategory = new System.Windows.Forms.Button();
            this.tDesc = new System.Windows.Forms.TextBox();
            this.tLocation = new System.Windows.Forms.TextBox();
            this.tPrice = new System.Windows.Forms.TextBox();
            this.tBrand = new System.Windows.Forms.TextBox();
            this.tCategory = new System.Windows.Forms.TextBox();
            this.tSerial = new System.Windows.Forms.TextBox();
            this.tModel = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lLocation = new System.Windows.Forms.Label();
            this.lPrice = new System.Windows.Forms.Label();
            this.lBrand = new System.Windows.Forms.Label();
            this.lCategory = new System.Windows.Forms.Label();
            this.lSerial = new System.Windows.Forms.Label();
            this.lModel = new System.Windows.Forms.Label();
            this.bCancel = new System.Windows.Forms.Button();
            this.tOwner = new System.Windows.Forms.TextBox();
            this.baOwner = new System.Windows.Forms.Button();
            this.lName = new System.Windows.Forms.Label();
            this.groupBoxOwner = new System.Windows.Forms.GroupBox();
            this.tNIK = new System.Windows.Forms.TextBox();
            this.lNIK = new System.Windows.Forms.Label();
            this.bOK = new System.Windows.Forms.Button();
            this.groupBoxAsset.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pAsset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGSoft)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBoxOwner.SuspendLayout();
            this.SuspendLayout();
            // 
            // purDate
            // 
            this.purDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.purDate.Location = new System.Drawing.Point(109, 181);
            this.purDate.Name = "purDate";
            this.purDate.Size = new System.Drawing.Size(148, 20);
            this.purDate.TabIndex = 5;
            // 
            // lDate
            // 
            this.lDate.AutoSize = true;
            this.lDate.Location = new System.Drawing.Point(11, 187);
            this.lDate.Name = "lDate";
            this.lDate.Size = new System.Drawing.Size(98, 13);
            this.lDate.TabIndex = 26;
            this.lDate.Text = "Tanggal Pembelian";
            // 
            // groupBoxAsset
            // 
            this.groupBoxAsset.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxAsset.Controls.Add(this.label3);
            this.groupBoxAsset.Controls.Add(this.tItem);
            this.groupBoxAsset.Controls.Add(this.groupBox2);
            this.groupBoxAsset.Controls.Add(this.label2);
            this.groupBoxAsset.Controls.Add(this.tNoAsset);
            this.groupBoxAsset.Controls.Add(this.checkSoft);
            this.groupBoxAsset.Controls.Add(this.dataGSoft);
            this.groupBoxAsset.Controls.Add(this.groupBox1);
            this.groupBoxAsset.Controls.Add(this.purDate);
            this.groupBoxAsset.Controls.Add(this.lDate);
            this.groupBoxAsset.Controls.Add(this.baLocation);
            this.groupBoxAsset.Controls.Add(this.baBrand);
            this.groupBoxAsset.Controls.Add(this.baCategory);
            this.groupBoxAsset.Controls.Add(this.tDesc);
            this.groupBoxAsset.Controls.Add(this.tLocation);
            this.groupBoxAsset.Controls.Add(this.tPrice);
            this.groupBoxAsset.Controls.Add(this.tBrand);
            this.groupBoxAsset.Controls.Add(this.tCategory);
            this.groupBoxAsset.Controls.Add(this.tSerial);
            this.groupBoxAsset.Controls.Add(this.tModel);
            this.groupBoxAsset.Controls.Add(this.label1);
            this.groupBoxAsset.Controls.Add(this.lLocation);
            this.groupBoxAsset.Controls.Add(this.lPrice);
            this.groupBoxAsset.Controls.Add(this.lBrand);
            this.groupBoxAsset.Controls.Add(this.lCategory);
            this.groupBoxAsset.Controls.Add(this.lSerial);
            this.groupBoxAsset.Controls.Add(this.lModel);
            this.groupBoxAsset.ForeColor = System.Drawing.Color.White;
            this.groupBoxAsset.Location = new System.Drawing.Point(13, 113);
            this.groupBoxAsset.Name = "groupBoxAsset";
            this.groupBoxAsset.Size = new System.Drawing.Size(616, 548);
            this.groupBoxAsset.TabIndex = 29;
            this.groupBoxAsset.TabStop = false;
            this.groupBoxAsset.Text = "Aset";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 38;
            this.label3.Text = "Item";
            // 
            // tItem
            // 
            this.tItem.Location = new System.Drawing.Point(109, 52);
            this.tItem.Name = "tItem";
            this.tItem.Size = new System.Drawing.Size(220, 20);
            this.tItem.TabIndex = 37;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.bCancelUp);
            this.groupBox2.Controls.Add(this.pAsset);
            this.groupBox2.Controls.Add(this.bUpload);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(380, 15);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(195, 204);
            this.groupBox2.TabIndex = 36;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Upload Foto";
            // 
            // bCancelUp
            // 
            this.bCancelUp.ForeColor = System.Drawing.Color.Black;
            this.bCancelUp.Location = new System.Drawing.Point(100, 175);
            this.bCancelUp.Name = "bCancelUp";
            this.bCancelUp.Size = new System.Drawing.Size(89, 23);
            this.bCancelUp.TabIndex = 34;
            this.bCancelUp.Text = "Cancel";
            this.bCancelUp.UseVisualStyleBackColor = true;
            this.bCancelUp.Click += new System.EventHandler(this.bCancelUp_Click);
            // 
            // pAsset
            // 
            this.pAsset.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pAsset.Location = new System.Drawing.Point(9, 19);
            this.pAsset.Name = "pAsset";
            this.pAsset.Size = new System.Drawing.Size(180, 150);
            this.pAsset.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pAsset.TabIndex = 33;
            this.pAsset.TabStop = false;
            // 
            // bUpload
            // 
            this.bUpload.ForeColor = System.Drawing.Color.Black;
            this.bUpload.Location = new System.Drawing.Point(9, 175);
            this.bUpload.Name = "bUpload";
            this.bUpload.Size = new System.Drawing.Size(89, 23);
            this.bUpload.TabIndex = 32;
            this.bUpload.Text = "Upload";
            this.bUpload.UseVisualStyleBackColor = true;
            this.bUpload.Click += new System.EventHandler(this.bUpload_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "No. Aset";
            // 
            // tNoAsset
            // 
            this.tNoAsset.Location = new System.Drawing.Point(109, 26);
            this.tNoAsset.Name = "tNoAsset";
            this.tNoAsset.Size = new System.Drawing.Size(220, 20);
            this.tNoAsset.TabIndex = 34;
            // 
            // checkSoft
            // 
            this.checkSoft.AutoSize = true;
            this.checkSoft.Location = new System.Drawing.Point(15, 415);
            this.checkSoft.Name = "checkSoft";
            this.checkSoft.Size = new System.Drawing.Size(160, 17);
            this.checkSoft.TabIndex = 33;
            this.checkSoft.Text = "Check bila terinstall software";
            this.checkSoft.UseVisualStyleBackColor = true;
            this.checkSoft.CheckedChanged += new System.EventHandler(this.checkSoft_CheckedChanged);
            // 
            // dataGSoft
            // 
            this.dataGSoft.AllowUserToAddRows = false;
            this.dataGSoft.AllowUserToDeleteRows = false;
            this.dataGSoft.AllowUserToResizeColumns = false;
            this.dataGSoft.AllowUserToResizeRows = false;
            this.dataGSoft.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGSoft.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGSoft.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGSoft.Enabled = false;
            this.dataGSoft.Location = new System.Drawing.Point(24, 457);
            this.dataGSoft.MultiSelect = false;
            this.dataGSoft.Name = "dataGSoft";
            this.dataGSoft.ReadOnly = true;
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            this.dataGSoft.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGSoft.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGSoft.Size = new System.Drawing.Size(507, 79);
            this.dataGSoft.TabIndex = 31;
            this.dataGSoft.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGSoft_CellContentClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.baSoft);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(18, 438);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(591, 104);
            this.groupBox1.TabIndex = 32;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Software";
            // 
            // baSoft
            // 
            this.baSoft.Enabled = false;
            this.baSoft.ForeColor = System.Drawing.Color.Black;
            this.baSoft.Location = new System.Drawing.Point(519, 19);
            this.baSoft.Name = "baSoft";
            this.baSoft.Size = new System.Drawing.Size(66, 23);
            this.baSoft.TabIndex = 30;
            this.baSoft.Text = "Tambah";
            this.baSoft.UseVisualStyleBackColor = true;
            this.baSoft.Click += new System.EventHandler(this.baSoft_Click);
            // 
            // lNotify
            // 
            this.lNotify.AutoSize = true;
            this.lNotify.BackColor = System.Drawing.Color.Transparent;
            this.lNotify.ForeColor = System.Drawing.Color.Red;
            this.lNotify.Location = new System.Drawing.Point(25, 666);
            this.lNotify.Name = "lNotify";
            this.lNotify.Size = new System.Drawing.Size(0, 13);
            this.lNotify.TabIndex = 25;
            // 
            // baLocation
            // 
            this.baLocation.ForeColor = System.Drawing.Color.Black;
            this.baLocation.Location = new System.Drawing.Point(263, 231);
            this.baLocation.Name = "baLocation";
            this.baLocation.Size = new System.Drawing.Size(66, 23);
            this.baLocation.TabIndex = 7;
            this.baLocation.Text = "Cari";
            this.baLocation.UseVisualStyleBackColor = true;
            this.baLocation.Click += new System.EventHandler(this.baLocation_Click);
            // 
            // baBrand
            // 
            this.baBrand.ForeColor = System.Drawing.Color.Black;
            this.baBrand.Location = new System.Drawing.Point(263, 153);
            this.baBrand.Name = "baBrand";
            this.baBrand.Size = new System.Drawing.Size(66, 23);
            this.baBrand.TabIndex = 4;
            this.baBrand.Text = "Cari";
            this.baBrand.UseVisualStyleBackColor = true;
            this.baBrand.Click += new System.EventHandler(this.baBrand_Click);
            // 
            // baCategory
            // 
            this.baCategory.ForeColor = System.Drawing.Color.Black;
            this.baCategory.Location = new System.Drawing.Point(263, 127);
            this.baCategory.Name = "baCategory";
            this.baCategory.Size = new System.Drawing.Size(66, 23);
            this.baCategory.TabIndex = 3;
            this.baCategory.Text = "Cari";
            this.baCategory.UseVisualStyleBackColor = true;
            this.baCategory.Click += new System.EventHandler(this.baCategory_Click);
            // 
            // tDesc
            // 
            this.tDesc.Location = new System.Drawing.Point(109, 259);
            this.tDesc.Multiline = true;
            this.tDesc.Name = "tDesc";
            this.tDesc.Size = new System.Drawing.Size(500, 150);
            this.tDesc.TabIndex = 8;
            // 
            // tLocation
            // 
            this.tLocation.Location = new System.Drawing.Point(109, 233);
            this.tLocation.Name = "tLocation";
            this.tLocation.Size = new System.Drawing.Size(148, 20);
            this.tLocation.TabIndex = 15;
            // 
            // tPrice
            // 
            this.tPrice.Location = new System.Drawing.Point(109, 207);
            this.tPrice.Name = "tPrice";
            this.tPrice.Size = new System.Drawing.Size(148, 20);
            this.tPrice.TabIndex = 6;
            this.tPrice.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tPrice_KeyPress);
            // 
            // tBrand
            // 
            this.tBrand.Location = new System.Drawing.Point(109, 155);
            this.tBrand.Name = "tBrand";
            this.tBrand.Size = new System.Drawing.Size(148, 20);
            this.tBrand.TabIndex = 14;
            // 
            // tCategory
            // 
            this.tCategory.Location = new System.Drawing.Point(109, 129);
            this.tCategory.Name = "tCategory";
            this.tCategory.Size = new System.Drawing.Size(148, 20);
            this.tCategory.TabIndex = 13;
            // 
            // tSerial
            // 
            this.tSerial.Location = new System.Drawing.Point(109, 103);
            this.tSerial.Name = "tSerial";
            this.tSerial.Size = new System.Drawing.Size(220, 20);
            this.tSerial.TabIndex = 2;
            // 
            // tModel
            // 
            this.tModel.Location = new System.Drawing.Point(109, 77);
            this.tModel.Name = "tModel";
            this.tModel.Size = new System.Drawing.Size(220, 20);
            this.tModel.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 259);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Deskripsi";
            // 
            // lLocation
            // 
            this.lLocation.AutoSize = true;
            this.lLocation.Location = new System.Drawing.Point(11, 236);
            this.lLocation.Name = "lLocation";
            this.lLocation.Size = new System.Drawing.Size(38, 13);
            this.lLocation.TabIndex = 13;
            this.lLocation.Text = "Lokasi";
            // 
            // lPrice
            // 
            this.lPrice.AutoSize = true;
            this.lPrice.Location = new System.Drawing.Point(12, 210);
            this.lPrice.Name = "lPrice";
            this.lPrice.Size = new System.Drawing.Size(36, 13);
            this.lPrice.TabIndex = 12;
            this.lPrice.Text = "Harga";
            // 
            // lBrand
            // 
            this.lBrand.AutoSize = true;
            this.lBrand.Location = new System.Drawing.Point(12, 158);
            this.lBrand.Name = "lBrand";
            this.lBrand.Size = new System.Drawing.Size(35, 13);
            this.lBrand.TabIndex = 11;
            this.lBrand.Text = "Brand";
            // 
            // lCategory
            // 
            this.lCategory.AutoSize = true;
            this.lCategory.Location = new System.Drawing.Point(11, 132);
            this.lCategory.Name = "lCategory";
            this.lCategory.Size = new System.Drawing.Size(46, 13);
            this.lCategory.TabIndex = 10;
            this.lCategory.Text = "Kategori";
            // 
            // lSerial
            // 
            this.lSerial.AutoSize = true;
            this.lSerial.Location = new System.Drawing.Point(11, 107);
            this.lSerial.Name = "lSerial";
            this.lSerial.Size = new System.Drawing.Size(53, 13);
            this.lSerial.TabIndex = 9;
            this.lSerial.Text = "No. Serial";
            // 
            // lModel
            // 
            this.lModel.AutoSize = true;
            this.lModel.Location = new System.Drawing.Point(11, 80);
            this.lModel.Name = "lModel";
            this.lModel.Size = new System.Drawing.Size(36, 13);
            this.lModel.TabIndex = 8;
            this.lModel.Text = "Model";
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(421, 666);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(120, 40);
            this.bCancel.TabIndex = 10;
            this.bCancel.Text = "Batal";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // tOwner
            // 
            this.tOwner.Location = new System.Drawing.Point(109, 54);
            this.tOwner.Name = "tOwner";
            this.tOwner.Size = new System.Drawing.Size(346, 20);
            this.tOwner.TabIndex = 12;
            // 
            // baOwner
            // 
            this.baOwner.ForeColor = System.Drawing.Color.Black;
            this.baOwner.Location = new System.Drawing.Point(461, 26);
            this.baOwner.Name = "baOwner";
            this.baOwner.Size = new System.Drawing.Size(66, 23);
            this.baOwner.TabIndex = 0;
            this.baOwner.Text = "Cari";
            this.baOwner.UseVisualStyleBackColor = true;
            this.baOwner.Click += new System.EventHandler(this.baOwner_Click);
            // 
            // lName
            // 
            this.lName.AutoSize = true;
            this.lName.Location = new System.Drawing.Point(11, 57);
            this.lName.Name = "lName";
            this.lName.Size = new System.Drawing.Size(35, 13);
            this.lName.TabIndex = 26;
            this.lName.Text = "Nama";
            // 
            // groupBoxOwner
            // 
            this.groupBoxOwner.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxOwner.Controls.Add(this.tOwner);
            this.groupBoxOwner.Controls.Add(this.lName);
            this.groupBoxOwner.Controls.Add(this.baOwner);
            this.groupBoxOwner.Controls.Add(this.tNIK);
            this.groupBoxOwner.Controls.Add(this.lNIK);
            this.groupBoxOwner.ForeColor = System.Drawing.Color.White;
            this.groupBoxOwner.Location = new System.Drawing.Point(13, 11);
            this.groupBoxOwner.Name = "groupBoxOwner";
            this.groupBoxOwner.Size = new System.Drawing.Size(616, 96);
            this.groupBoxOwner.TabIndex = 30;
            this.groupBoxOwner.TabStop = false;
            this.groupBoxOwner.Text = "Pemilik";
            // 
            // tNIK
            // 
            this.tNIK.Location = new System.Drawing.Point(109, 28);
            this.tNIK.Name = "tNIK";
            this.tNIK.Size = new System.Drawing.Size(346, 20);
            this.tNIK.TabIndex = 11;
            // 
            // lNIK
            // 
            this.lNIK.AutoSize = true;
            this.lNIK.Location = new System.Drawing.Point(11, 31);
            this.lNIK.Name = "lNIK";
            this.lNIK.Size = new System.Drawing.Size(32, 13);
            this.lNIK.TabIndex = 25;
            this.lNIK.Text = "Email";
            // 
            // bOK
            // 
            this.bOK.Location = new System.Drawing.Point(284, 666);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(120, 40);
            this.bOK.TabIndex = 9;
            this.bOK.Text = "Simpan";
            this.bOK.UseVisualStyleBackColor = true;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // EditAsset
            // 
            this.AcceptButton = this.bOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 718);
            this.Controls.Add(this.groupBoxAsset);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.groupBoxOwner);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.lNotify);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditAsset";
            this.Text = "Ubah Aset";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.EditAsset_FormClosed);
            this.Load += new System.EventHandler(this.EditAsset_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.EditAsset_Paint);
            this.groupBoxAsset.ResumeLayout(false);
            this.groupBoxAsset.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pAsset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGSoft)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBoxOwner.ResumeLayout(false);
            this.groupBoxOwner.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker purDate;
        private System.Windows.Forms.Label lDate;
        private System.Windows.Forms.GroupBox groupBoxAsset;
        private System.Windows.Forms.Label lNotify;
        private System.Windows.Forms.Button baLocation;
        private System.Windows.Forms.Button baBrand;
        private System.Windows.Forms.Button baCategory;
        private System.Windows.Forms.TextBox tDesc;
        private System.Windows.Forms.TextBox tLocation;
        private System.Windows.Forms.TextBox tPrice;
        private System.Windows.Forms.TextBox tBrand;
        private System.Windows.Forms.TextBox tCategory;
        private System.Windows.Forms.TextBox tSerial;
        private System.Windows.Forms.TextBox tModel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lLocation;
        private System.Windows.Forms.Label lPrice;
        private System.Windows.Forms.Label lBrand;
        private System.Windows.Forms.Label lCategory;
        private System.Windows.Forms.Label lSerial;
        private System.Windows.Forms.Label lModel;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.TextBox tOwner;
        private System.Windows.Forms.Button baOwner;
        private System.Windows.Forms.Label lName;
        private System.Windows.Forms.GroupBox groupBoxOwner;
        private System.Windows.Forms.TextBox tNIK;
        private System.Windows.Forms.Label lNIK;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.CheckBox checkSoft;
        private System.Windows.Forms.DataGridView dataGSoft;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button baSoft;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tNoAsset;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button bCancelUp;
        private System.Windows.Forms.PictureBox pAsset;
        private System.Windows.Forms.Button bUpload;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tItem;
    }
}