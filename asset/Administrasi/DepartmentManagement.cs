﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class DepartmentManagement : Form
    {
        private String id;
        private String id_dept;
        lib lib = new lib();
        Class.process.mDepartmentManagement edit = new Class.process.mDepartmentManagement();
        public DepartmentManagement(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
        }
        private static DepartmentManagement form;
        public static DepartmentManagement GetChildInstance(String id)
        {
            if (form == null)
                form = new DepartmentManagement(id);
            return form;
        }
        private void DepartmentManagement_Load(object sender, EventArgs e)
        {
            try
            {
                showDataDept();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void showDataDept()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_dept";
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    this.id_dept = read["id_dept"].ToString();
                    tDept.Text = read["dept"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void DepartmentManagement_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            try
            {
                showDataDept();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            try
            {
                String dept = tDept.Text;
                String id_dept = this.id_dept;
                String userid = this.id;

                Boolean cek = edit.cekInput(dept);

                if (cek == true)
                {
                    edit.update(id_dept, dept, userid);
                    MessageBox.Show("Update departemen berhasil");
                }
                else
                {
                    lNotify.Text = "Salah input!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void DepartmentManagement_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
    }
}
