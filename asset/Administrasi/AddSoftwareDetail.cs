﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class AddSoftwareDetail : Form
    {
        private String id;
        lib lib=new lib();
        Class.process.mAddSoftwareDetail add = new Class.process.mAddSoftwareDetail();
        public AddSoftwareDetail(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            try
            {
                String soft = tSoft.Text;
                Boolean cek = add.cekInput(soft);
                if (cek == true)
                {
                    if (add.cekData(soft) == "0")
                    {
                        add.insert(soft, this.id);
                        this.Close();
                    }
                    else
                    {
                        lNotify.Text = "Nama Software sudah ada!";
                    }
                }
                else
                {
                    lNotify.Text = "Salah input!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AddSoftwareDetail_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            } 
        }
    }
}
