﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset.Administrasi
{
    public partial class AddNewSoftware : Form
    {
        lib lib = new lib();
        public string tittle = "",software="";
        public int status=0,id_category=0,id_status=0,id_software ;
        Class.process.mAddNewSoftware m = new Class.process.mAddNewSoftware();
        public AddNewSoftware()
        {

            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }
        void showDataC()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sc = new SqlCommand("select id_category,category from m_softwareCat", conn);
                SqlDataReader read;
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(string));
                dt.Columns.Add("category", typeof(string));
                read = sc.ExecuteReader();
                while (read.Read())
                {
                    dt.Rows.Add(new object[] { read["id_category"].ToString(), read["category"].ToString() });
                }

                cKategori.ValueMember = "id";
                cKategori.DisplayMember = "category";
                cKategori.DataSource = dt;

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        void showDataS()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sc = new SqlCommand("select id_status,status from m_softwareStat", conn);
                SqlDataReader read;
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(string));
                dt.Columns.Add("status", typeof(string));
                read = sc.ExecuteReader();
                while (read.Read())
                {
                    dt.Rows.Add(new object[] { read["id_status"].ToString(), read["status"].ToString() });
                }

                cStatus.ValueMember = "id";
                cStatus.DisplayMember = "status";
                cStatus.DataSource = dt;

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void AddNewSoftware_Load(object sender, EventArgs e)
        {
            this.Text = this.tittle;
            showDataC();
            showDataS();
            cKategori.SelectedValue = this.id_category;
            cStatus.SelectedValue = this.id_status;
            tSoft.Text = this.software;
        }

        private void AddNewSoftware_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

        private void bOk_Click(object sender, EventArgs e)
        {
            if (tSoft.Text != "" || tSoft.Text != null)
            {
                Boolean cek = m.cek(tSoft.Text, cKategori.SelectedValue.ToString(), cStatus.SelectedValue.ToString());
                Boolean ceklisensi = m.ceklisensi(this.id_software);
                if (cek)
                {
                    if (this.status == 0)
                    {
                        m.insertSoftware(tSoft.Text, cKategori.SelectedValue.ToString(), cStatus.SelectedValue.ToString());
                    }
                    else if (this.status == 1)
                    {
                        m.updateSoftware(this.id_software, tSoft.Text, cKategori.SelectedValue.ToString(), cStatus.SelectedValue.ToString());
                    }
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Software sudah ada!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Harap isi nama software", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tSoft_KeyPress(object sender, KeyPressEventArgs e)
        {
            var regexItem = new Regex("^[a-zA-Z0-9 ]*$");
            if (!char.IsControl(e.KeyChar) && !regexItem.IsMatch(e.KeyChar.ToString())) { e.Handled = true; }
        }
    }
}
