﻿namespace asset
{
    partial class AssetSoftwareDetail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bRefresh = new System.Windows.Forms.Button();
            this.bAdd = new System.Windows.Forms.Button();
            this.dataGSoft = new System.Windows.Forms.DataGridView();
            this.bSearch = new System.Windows.Forms.Button();
            this.tSearch = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGSoft)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.bRefresh);
            this.groupBox1.Controls.Add(this.bAdd);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(10, 253);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(403, 79);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Operasi";
            // 
            // bRefresh
            // 
            this.bRefresh.ForeColor = System.Drawing.Color.Black;
            this.bRefresh.Location = new System.Drawing.Point(143, 26);
            this.bRefresh.Name = "bRefresh";
            this.bRefresh.Size = new System.Drawing.Size(120, 40);
            this.bRefresh.TabIndex = 5;
            this.bRefresh.Text = "Refresh";
            this.bRefresh.UseVisualStyleBackColor = true;
            this.bRefresh.Click += new System.EventHandler(this.bRefresh_Click);
            // 
            // bAdd
            // 
            this.bAdd.ForeColor = System.Drawing.Color.Black;
            this.bAdd.Location = new System.Drawing.Point(17, 26);
            this.bAdd.Name = "bAdd";
            this.bAdd.Size = new System.Drawing.Size(120, 40);
            this.bAdd.TabIndex = 2;
            this.bAdd.Text = "Tambah";
            this.bAdd.UseVisualStyleBackColor = true;
            this.bAdd.Click += new System.EventHandler(this.bAdd_Click);
            // 
            // dataGSoft
            // 
            this.dataGSoft.AllowUserToAddRows = false;
            this.dataGSoft.AllowUserToDeleteRows = false;
            this.dataGSoft.AllowUserToResizeColumns = false;
            this.dataGSoft.AllowUserToResizeRows = false;
            this.dataGSoft.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGSoft.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGSoft.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGSoft.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGSoft.Location = new System.Drawing.Point(12, 67);
            this.dataGSoft.MultiSelect = false;
            this.dataGSoft.Name = "dataGSoft";
            this.dataGSoft.ReadOnly = true;
            this.dataGSoft.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGSoft.Size = new System.Drawing.Size(401, 180);
            this.dataGSoft.TabIndex = 8;
            this.dataGSoft.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGSoft_CellContentClick);
            // 
            // bSearch
            // 
            this.bSearch.ForeColor = System.Drawing.Color.Black;
            this.bSearch.Location = new System.Drawing.Point(320, 17);
            this.bSearch.Name = "bSearch";
            this.bSearch.Size = new System.Drawing.Size(75, 23);
            this.bSearch.TabIndex = 1;
            this.bSearch.Text = "Cari";
            this.bSearch.UseVisualStyleBackColor = true;
            this.bSearch.Click += new System.EventHandler(this.bSearch_Click);
            // 
            // tSearch
            // 
            this.tSearch.Location = new System.Drawing.Point(6, 19);
            this.tSearch.Name = "tSearch";
            this.tSearch.Size = new System.Drawing.Size(308, 20);
            this.tSearch.TabIndex = 0;
            this.tSearch.TextChanged += new System.EventHandler(this.bSearch_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.bSearch);
            this.groupBox2.Controls.Add(this.tSearch);
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(401, 49);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Software";
            // 
            // AssetSoftwareDetail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 373);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGSoft);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AssetSoftwareDetail";
            this.Text = "Software";
            this.Load += new System.EventHandler(this.AssetSoftwareDetail_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AssetSoftwareDetail_Paint);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGSoft)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bRefresh;
        private System.Windows.Forms.Button bAdd;
        private System.Windows.Forms.DataGridView dataGSoft;
        private System.Windows.Forms.Button bSearch;
        private System.Windows.Forms.TextBox tSearch;
        private System.Windows.Forms.GroupBox groupBox2;
    }
}