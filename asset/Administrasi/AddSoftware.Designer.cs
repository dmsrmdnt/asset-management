﻿namespace asset
{
    partial class AddSoftware
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.baSoft = new System.Windows.Forms.Button();
            this.tSoft = new System.Windows.Forms.TextBox();
            this.lNIK = new System.Windows.Forms.Label();
            this.checkL = new System.Windows.Forms.CheckBox();
            this.tNLicense = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.expDate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.bCancel = new System.Windows.Forms.Button();
            this.bOK = new System.Windows.Forms.Button();
            this.lNotify = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // baSoft
            // 
            this.baSoft.Location = new System.Drawing.Point(415, 10);
            this.baSoft.Name = "baSoft";
            this.baSoft.Size = new System.Drawing.Size(66, 23);
            this.baSoft.TabIndex = 26;
            this.baSoft.Text = "Cari";
            this.baSoft.UseVisualStyleBackColor = true;
            this.baSoft.Click += new System.EventHandler(this.baSoft_Click);
            // 
            // tSoft
            // 
            this.tSoft.Enabled = false;
            this.tSoft.Location = new System.Drawing.Point(111, 12);
            this.tSoft.Name = "tSoft";
            this.tSoft.Size = new System.Drawing.Size(298, 20);
            this.tSoft.TabIndex = 27;
            // 
            // lNIK
            // 
            this.lNIK.AutoSize = true;
            this.lNIK.BackColor = System.Drawing.Color.Transparent;
            this.lNIK.ForeColor = System.Drawing.Color.Transparent;
            this.lNIK.Location = new System.Drawing.Point(13, 15);
            this.lNIK.Name = "lNIK";
            this.lNIK.Size = new System.Drawing.Size(49, 13);
            this.lNIK.TabIndex = 28;
            this.lNIK.Text = "Software";
            // 
            // checkL
            // 
            this.checkL.AutoSize = true;
            this.checkL.BackColor = System.Drawing.Color.Transparent;
            this.checkL.ForeColor = System.Drawing.Color.Transparent;
            this.checkL.Location = new System.Drawing.Point(16, 40);
            this.checkL.Name = "checkL";
            this.checkL.Size = new System.Drawing.Size(114, 17);
            this.checkL.TabIndex = 29;
            this.checkL.Text = "Licensed Software";
            this.checkL.UseVisualStyleBackColor = false;
            this.checkL.CheckedChanged += new System.EventHandler(this.checkL_CheckedChanged);
            // 
            // tNLicense
            // 
            this.tNLicense.Enabled = false;
            this.tNLicense.Location = new System.Drawing.Point(111, 61);
            this.tNLicense.Name = "tNLicense";
            this.tNLicense.Size = new System.Drawing.Size(298, 20);
            this.tNLicense.TabIndex = 30;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(13, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "No. Lisensi";
            // 
            // expDate
            // 
            this.expDate.Enabled = false;
            this.expDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.expDate.Location = new System.Drawing.Point(111, 87);
            this.expDate.Name = "expDate";
            this.expDate.Size = new System.Drawing.Size(148, 20);
            this.expDate.TabIndex = 32;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(13, 93);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 33;
            this.label2.Text = "Tanggal Exp";
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(426, 196);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(120, 40);
            this.bCancel.TabIndex = 35;
            this.bCancel.Text = "Batal";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bOK
            // 
            this.bOK.Location = new System.Drawing.Point(289, 196);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(120, 40);
            this.bOK.TabIndex = 34;
            this.bOK.Text = "Simpan";
            this.bOK.UseVisualStyleBackColor = true;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // lNotify
            // 
            this.lNotify.AutoSize = true;
            this.lNotify.ForeColor = System.Drawing.Color.Red;
            this.lNotify.Location = new System.Drawing.Point(13, 142);
            this.lNotify.Name = "lNotify";
            this.lNotify.Size = new System.Drawing.Size(0, 13);
            this.lNotify.TabIndex = 36;
            // 
            // AddSoftware
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 270);
            this.Controls.Add(this.lNotify);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.expDate);
            this.Controls.Add(this.tNLicense);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkL);
            this.Controls.Add(this.baSoft);
            this.Controls.Add(this.tSoft);
            this.Controls.Add(this.lNIK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddSoftware";
            this.Text = "Tambah Software";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AddSoftware_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button baSoft;
        private System.Windows.Forms.TextBox tSoft;
        private System.Windows.Forms.Label lNIK;
        private System.Windows.Forms.CheckBox checkL;
        private System.Windows.Forms.TextBox tNLicense;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker expDate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Label lNotify;
    }
}