﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class EditUser : Form
    {
        lib lib = new lib();
        Class.process.mEditUser edit = new Class.process.mEditUser();
        private String ID;
        public EditUser(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            showDepartment();
            showAuth();
            showData(id);
        }

        private void showData(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select * from m_employee where id_employee='" + id + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    ID = read["id_employee"].ToString();
                    tNIK.Text = read["email"].ToString();
                    tName.Text = read["nama"].ToString();
                    cAuth.SelectedValue = read["id_auth"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showAuth()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sc = new SqlCommand("select id_auth,auth from m_auth", conn);
                SqlDataReader reader;

                reader = sc.ExecuteReader();
                DataTable dt = new DataTable();

                dt.Columns.Add("id_auth", typeof(string));
                dt.Columns.Add("auth", typeof(string));
                dt.Load(reader);

                cAuth.ValueMember = "id_auth";
                cAuth.DisplayMember = "auth";
                cAuth.DataSource = dt;

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showDepartment()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select dept from m_dept", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    tDepartment.Text = read[0].ToString();
                }
                tDepartment.Enabled = false;
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void bOK_Click(object sender, EventArgs e)
        {
            try
            {
                String nama = tName.Text;
                String nik = tNIK.Text;
                String id = this.ID;
                String status = cAuth.SelectedValue.ToString();
                Boolean cek = edit.cekInput(id, nama, status);

                if (cek == true)
                {
                    if (lib.cekEmail(tNIK.Text))
                    {
                        if (edit.cekData(nik) <= 1)
                        {
                            edit.update(id, nik, nama, status);
                            this.Close();
                        }
                        else
                        {
                            lNotify.Text = "Email sudah terpakai!";
                        }
                    }
                    else
                    {
                        lNotify.Text = "Format email salah! Contoh : coba@wilmar.co.id";
                    }
                }
                else
                {
                    lNotify.Text = "Salah input!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void EditUser_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        } 
    }
}
