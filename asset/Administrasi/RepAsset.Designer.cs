﻿namespace asset
{
    partial class RepAsset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gAsset = new System.Windows.Forms.GroupBox();
            this.tNAsset = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.purDate = new System.Windows.Forms.DateTimePicker();
            this.lDate = new System.Windows.Forms.Label();
            this.tDesc = new System.Windows.Forms.TextBox();
            this.tLocation = new System.Windows.Forms.TextBox();
            this.tPrice = new System.Windows.Forms.TextBox();
            this.tBrand = new System.Windows.Forms.TextBox();
            this.tCategory = new System.Windows.Forms.TextBox();
            this.tSerial = new System.Windows.Forms.TextBox();
            this.tModel = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lLocation = new System.Windows.Forms.Label();
            this.lPrice = new System.Windows.Forms.Label();
            this.lBrand = new System.Windows.Forms.Label();
            this.lCategory = new System.Windows.Forms.Label();
            this.lSerial = new System.Windows.Forms.Label();
            this.lModel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.bSearchF = new System.Windows.Forms.Button();
            this.tAlamat = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tTelp = new System.Windows.Forms.TextBox();
            this.lNotify = new System.Windows.Forms.Label();
            this.bCancel = new System.Windows.Forms.Button();
            this.bOK = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tPIC = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dAwal = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.bCari = new System.Windows.Forms.Button();
            this.tFile = new System.Windows.Forms.TextBox();
            this.gAsset.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gAsset
            // 
            this.gAsset.BackColor = System.Drawing.Color.Transparent;
            this.gAsset.Controls.Add(this.tNAsset);
            this.gAsset.Controls.Add(this.label6);
            this.gAsset.Controls.Add(this.purDate);
            this.gAsset.Controls.Add(this.lDate);
            this.gAsset.Controls.Add(this.tDesc);
            this.gAsset.Controls.Add(this.tLocation);
            this.gAsset.Controls.Add(this.tPrice);
            this.gAsset.Controls.Add(this.tBrand);
            this.gAsset.Controls.Add(this.tCategory);
            this.gAsset.Controls.Add(this.tSerial);
            this.gAsset.Controls.Add(this.tModel);
            this.gAsset.Controls.Add(this.label1);
            this.gAsset.Controls.Add(this.lLocation);
            this.gAsset.Controls.Add(this.lPrice);
            this.gAsset.Controls.Add(this.lBrand);
            this.gAsset.Controls.Add(this.lCategory);
            this.gAsset.Controls.Add(this.lSerial);
            this.gAsset.Controls.Add(this.lModel);
            this.gAsset.ForeColor = System.Drawing.Color.White;
            this.gAsset.Location = new System.Drawing.Point(12, 12);
            this.gAsset.Name = "gAsset";
            this.gAsset.Size = new System.Drawing.Size(469, 369);
            this.gAsset.TabIndex = 1;
            this.gAsset.TabStop = false;
            this.gAsset.Text = "Informasi Aset";
            // 
            // tNAsset
            // 
            this.tNAsset.Enabled = false;
            this.tNAsset.Location = new System.Drawing.Point(114, 24);
            this.tNAsset.Name = "tNAsset";
            this.tNAsset.ReadOnly = true;
            this.tNAsset.Size = new System.Drawing.Size(220, 20);
            this.tNAsset.TabIndex = 48;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 13);
            this.label6.TabIndex = 47;
            this.label6.Text = "No. Aset";
            // 
            // purDate
            // 
            this.purDate.Enabled = false;
            this.purDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.purDate.Location = new System.Drawing.Point(114, 152);
            this.purDate.Name = "purDate";
            this.purDate.Size = new System.Drawing.Size(148, 20);
            this.purDate.TabIndex = 46;
            // 
            // lDate
            // 
            this.lDate.AutoSize = true;
            this.lDate.Location = new System.Drawing.Point(16, 158);
            this.lDate.Name = "lDate";
            this.lDate.Size = new System.Drawing.Size(98, 13);
            this.lDate.TabIndex = 45;
            this.lDate.Text = "Tanggal Pembelian";
            // 
            // tDesc
            // 
            this.tDesc.Enabled = false;
            this.tDesc.Location = new System.Drawing.Point(114, 230);
            this.tDesc.Multiline = true;
            this.tDesc.Name = "tDesc";
            this.tDesc.ReadOnly = true;
            this.tDesc.Size = new System.Drawing.Size(349, 133);
            this.tDesc.TabIndex = 41;
            // 
            // tLocation
            // 
            this.tLocation.Enabled = false;
            this.tLocation.Location = new System.Drawing.Point(114, 204);
            this.tLocation.Name = "tLocation";
            this.tLocation.ReadOnly = true;
            this.tLocation.Size = new System.Drawing.Size(148, 20);
            this.tLocation.TabIndex = 40;
            // 
            // tPrice
            // 
            this.tPrice.Enabled = false;
            this.tPrice.Location = new System.Drawing.Point(114, 178);
            this.tPrice.Name = "tPrice";
            this.tPrice.ReadOnly = true;
            this.tPrice.Size = new System.Drawing.Size(148, 20);
            this.tPrice.TabIndex = 39;
            // 
            // tBrand
            // 
            this.tBrand.Enabled = false;
            this.tBrand.Location = new System.Drawing.Point(114, 126);
            this.tBrand.Name = "tBrand";
            this.tBrand.ReadOnly = true;
            this.tBrand.Size = new System.Drawing.Size(148, 20);
            this.tBrand.TabIndex = 38;
            // 
            // tCategory
            // 
            this.tCategory.Enabled = false;
            this.tCategory.Location = new System.Drawing.Point(114, 100);
            this.tCategory.Name = "tCategory";
            this.tCategory.ReadOnly = true;
            this.tCategory.Size = new System.Drawing.Size(148, 20);
            this.tCategory.TabIndex = 37;
            // 
            // tSerial
            // 
            this.tSerial.Enabled = false;
            this.tSerial.Location = new System.Drawing.Point(114, 74);
            this.tSerial.Name = "tSerial";
            this.tSerial.ReadOnly = true;
            this.tSerial.Size = new System.Drawing.Size(220, 20);
            this.tSerial.TabIndex = 36;
            // 
            // tModel
            // 
            this.tModel.Enabled = false;
            this.tModel.Location = new System.Drawing.Point(114, 48);
            this.tModel.Name = "tModel";
            this.tModel.ReadOnly = true;
            this.tModel.Size = new System.Drawing.Size(220, 20);
            this.tModel.TabIndex = 35;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 230);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 34;
            this.label1.Text = "Deskripsi";
            // 
            // lLocation
            // 
            this.lLocation.AutoSize = true;
            this.lLocation.Location = new System.Drawing.Point(16, 207);
            this.lLocation.Name = "lLocation";
            this.lLocation.Size = new System.Drawing.Size(38, 13);
            this.lLocation.TabIndex = 33;
            this.lLocation.Text = "Lokasi";
            // 
            // lPrice
            // 
            this.lPrice.AutoSize = true;
            this.lPrice.Location = new System.Drawing.Point(16, 181);
            this.lPrice.Name = "lPrice";
            this.lPrice.Size = new System.Drawing.Size(36, 13);
            this.lPrice.TabIndex = 32;
            this.lPrice.Text = "Harga";
            // 
            // lBrand
            // 
            this.lBrand.AutoSize = true;
            this.lBrand.Location = new System.Drawing.Point(16, 129);
            this.lBrand.Name = "lBrand";
            this.lBrand.Size = new System.Drawing.Size(35, 13);
            this.lBrand.TabIndex = 31;
            this.lBrand.Text = "Brand";
            // 
            // lCategory
            // 
            this.lCategory.AutoSize = true;
            this.lCategory.Location = new System.Drawing.Point(16, 103);
            this.lCategory.Name = "lCategory";
            this.lCategory.Size = new System.Drawing.Size(46, 13);
            this.lCategory.TabIndex = 30;
            this.lCategory.Text = "Kategori";
            // 
            // lSerial
            // 
            this.lSerial.AutoSize = true;
            this.lSerial.Location = new System.Drawing.Point(16, 77);
            this.lSerial.Name = "lSerial";
            this.lSerial.Size = new System.Drawing.Size(53, 13);
            this.lSerial.TabIndex = 29;
            this.lSerial.Text = "No. Serial";
            // 
            // lModel
            // 
            this.lModel.AutoSize = true;
            this.lModel.Location = new System.Drawing.Point(16, 51);
            this.lModel.Name = "lModel";
            this.lModel.Size = new System.Drawing.Size(36, 13);
            this.lModel.TabIndex = 28;
            this.lModel.Text = "Model";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.bCari);
            this.groupBox1.Controls.Add(this.tFile);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.bSearchF);
            this.groupBox1.Controls.Add(this.tAlamat);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tTelp);
            this.groupBox1.Controls.Add(this.lNotify);
            this.groupBox1.Controls.Add(this.bCancel);
            this.groupBox1.Controls.Add(this.bOK);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tPIC);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dAwal);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(487, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(402, 369);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Data Perbaikan";
            // 
            // bSearchF
            // 
            this.bSearchF.ForeColor = System.Drawing.Color.Black;
            this.bSearchF.Location = new System.Drawing.Point(321, 48);
            this.bSearchF.Name = "bSearchF";
            this.bSearchF.Size = new System.Drawing.Size(75, 23);
            this.bSearchF.TabIndex = 54;
            this.bSearchF.Text = "Cari";
            this.bSearchF.UseVisualStyleBackColor = true;
            this.bSearchF.Click += new System.EventHandler(this.bSearchF_Click);
            // 
            // tAlamat
            // 
            this.tAlamat.Location = new System.Drawing.Point(109, 103);
            this.tAlamat.Multiline = true;
            this.tAlamat.Name = "tAlamat";
            this.tAlamat.ReadOnly = true;
            this.tAlamat.Size = new System.Drawing.Size(231, 95);
            this.tAlamat.TabIndex = 53;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 52;
            this.label3.Text = "Alamat";
            // 
            // tTelp
            // 
            this.tTelp.Location = new System.Drawing.Point(109, 76);
            this.tTelp.Name = "tTelp";
            this.tTelp.ReadOnly = true;
            this.tTelp.Size = new System.Drawing.Size(206, 20);
            this.tTelp.TabIndex = 51;
            // 
            // lNotify
            // 
            this.lNotify.AutoSize = true;
            this.lNotify.ForeColor = System.Drawing.Color.Red;
            this.lNotify.Location = new System.Drawing.Point(6, 289);
            this.lNotify.Name = "lNotify";
            this.lNotify.Size = new System.Drawing.Size(0, 13);
            this.lNotify.TabIndex = 50;
            // 
            // bCancel
            // 
            this.bCancel.ForeColor = System.Drawing.Color.Black;
            this.bCancel.Location = new System.Drawing.Point(276, 316);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(120, 40);
            this.bCancel.TabIndex = 49;
            this.bCancel.Text = "Batal";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bOK
            // 
            this.bOK.CausesValidation = false;
            this.bOK.ForeColor = System.Drawing.Color.Black;
            this.bOK.Location = new System.Drawing.Point(139, 316);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(120, 40);
            this.bOK.TabIndex = 48;
            this.bOK.Text = "Simpan";
            this.bOK.UseVisualStyleBackColor = true;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Telepon";
            // 
            // tPIC
            // 
            this.tPIC.Location = new System.Drawing.Point(109, 50);
            this.tPIC.Name = "tPIC";
            this.tPIC.ReadOnly = true;
            this.tPIC.Size = new System.Drawing.Size(206, 20);
            this.tPIC.TabIndex = 5;
            this.tPIC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tHarga_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Vendor";
            // 
            // dAwal
            // 
            this.dAwal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dAwal.Location = new System.Drawing.Point(109, 24);
            this.dAwal.Name = "dAwal";
            this.dAwal.Size = new System.Drawing.Size(98, 20);
            this.dAwal.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tanggal Perbaikan";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 207);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 13);
            this.label7.TabIndex = 55;
            this.label7.Text = "Upload Quotation";
            // 
            // bCari
            // 
            this.bCari.ForeColor = System.Drawing.Color.Black;
            this.bCari.Location = new System.Drawing.Point(321, 202);
            this.bCari.Name = "bCari";
            this.bCari.Size = new System.Drawing.Size(75, 23);
            this.bCari.TabIndex = 56;
            this.bCari.Text = "Cari";
            this.bCari.UseVisualStyleBackColor = true;
            this.bCari.Click += new System.EventHandler(this.bCari_Click);
            // 
            // tFile
            // 
            this.tFile.Location = new System.Drawing.Point(109, 204);
            this.tFile.Name = "tFile";
            this.tFile.ReadOnly = true;
            this.tFile.Size = new System.Drawing.Size(206, 20);
            this.tFile.TabIndex = 57;
            // 
            // RepAsset
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(901, 394);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gAsset);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RepAsset";
            this.Text = "Perbaikan Aset";
            this.Load += new System.EventHandler(this.FixAsset_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.FixAsset_Paint);
            this.gAsset.ResumeLayout(false);
            this.gAsset.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gAsset;
        private System.Windows.Forms.DateTimePicker purDate;
        private System.Windows.Forms.Label lDate;
        private System.Windows.Forms.TextBox tDesc;
        private System.Windows.Forms.TextBox tLocation;
        private System.Windows.Forms.TextBox tPrice;
        private System.Windows.Forms.TextBox tBrand;
        private System.Windows.Forms.TextBox tCategory;
        private System.Windows.Forms.TextBox tSerial;
        private System.Windows.Forms.TextBox tModel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lLocation;
        private System.Windows.Forms.Label lPrice;
        private System.Windows.Forms.Label lBrand;
        private System.Windows.Forms.Label lCategory;
        private System.Windows.Forms.Label lSerial;
        private System.Windows.Forms.Label lModel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dAwal;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tPIC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Label lNotify;
        private System.Windows.Forms.TextBox tNAsset;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tAlamat;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tTelp;
        private System.Windows.Forms.Button bSearchF;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button bCari;
        private System.Windows.Forms.TextBox tFile;
    }
}