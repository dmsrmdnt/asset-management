﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class EditLocation : Form
    {
        private String id = null;
        Class.process.mEditLocation edit = new Class.process.mEditLocation();
        lib lib = new lib();
        public EditLocation(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            showData(id);
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            try
            {
                String location = tLocation.Text;
                String id = this.id;
                Boolean cek = edit.cekInput(id, location);

                if (cek == true)
                {
                    if (edit.cekData(location) <= 2)
                    {
                        edit.update(id, location);
                        this.Close();
                    }
                    else
                    {
                        lNotify.Text = "Lokasi sudah ada!";
                    }
                }
                else
                {
                    lNotify.Text = "Salah input!";

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void showData(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select id_location,location from m_location where id_location='" + id + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    this.id = read["id_location"].ToString();
                    tLocation.Text = read["location"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EditLocation_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
    }
}
