﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class RepAsset : Form
    {
        private String id_employee;
        private String id_asset;
        private String id_user;
        private String id_pic;
        Class.process.mRepAsset fix = new Class.process.mRepAsset();
        lib lib = new lib();
        public RepAsset(String id_employee, String id_asset, String id_user)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id_asset = id_asset;
            this.id_employee = id_employee;
            this.id_user = id_user;
        }

        private void FixAsset_Load(object sender, EventArgs e)
        {
            try
            {
                showData(this.id_asset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showData(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select a.asset, g.id_employee, a.id_location, a.id_brand, a.id_category, a.id_asset , g.nama, g.email , b.category , c.brand , a.model , a.serial, a.price, a.pur_date, d.location, e.status, a.description " +
                    "from m_asset a, m_category b, m_brand c, m_location d, m_status e, t_asset f, m_employee g " +
                    "where a.id_asset='" + id + "'and a.id_asset=f.id_asset and g.id_employee=f.id_employee and a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    tNAsset.Text = read["asset"].ToString();
                    tCategory.Text = read["category"].ToString();
                    tBrand.Text = read["brand"].ToString();
                    tLocation.Text = read["location"].ToString();
                    tModel.Text = read["model"].ToString();
                    tSerial.Text = read["serial"].ToString();
                    tPrice.Text = read["price"].ToString();
                    tDesc.Text = read["description"].ToString();
                    purDate.Text = read["pur_date"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            try
            {
                String a = dAwal.Text;
                String file = tFile.Text;
                String id_pic = this.id_pic;
                DateTime date_start = Convert.ToDateTime(a);
                Boolean cek = fix.cekInput(a, id_pic, file);
                if (cek == true)
                {
                    //Upload file
                                string filetype;
                                string filename;
                                
                                filetype = tFile.Text.Substring(Convert.ToInt32(tFile.Text.LastIndexOf(".")) + 1, tFile.Text.Length - (Convert.ToInt32(tFile.Text.LastIndexOf(".")) + 1));
                                filename = "QUOT" + DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ') +this.id_asset+"."+filetype;
                                
                                byte[] FileBytes = null;

                                try
                                {
                                    // Open file to read using file path
                                    FileStream FS = new FileStream(tFile.Text, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                                    // Add filestream to binary reader
                                    BinaryReader BR = new BinaryReader(FS);

                                    // get total byte length of the file
                                    long allbytes = new FileInfo(tFile.Text).Length;

                                    // read entire file into buffer
                                    FileBytes = BR.ReadBytes((Int32)allbytes);

                                    // close all instances
                                    FS.Close();
                                    FS.Dispose();
                                    BR.Close();

                                    fix.update(a, id_pic, this.id_asset, this.id_employee, this.id_user, filename, FileBytes);
                                    MessageBox.Show("Data perbaikan aset tersimpan");
                                    this.Close();
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("Error during File Read " + ex.ToString());
                                }
                }
                else
                {
                    lNotify.Text = "Salah input!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FixAsset_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            } 
        }

        private void tHarga_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar)
            && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void bSearchF_Click(object sender, EventArgs e)
        {
            try
            {
                AssetPIC cat = new AssetPIC();
                cat.ShowDialog();
                this.tPIC.Text = cat.namaPIC;
                this.tAlamat.Text = cat.alamatPIC;
                this.tTelp.Text = cat.telpPIC;
                this.id_pic = cat.idPIC;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCari_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fDialog = new OpenFileDialog();
                fDialog.Title = "Pilih file untuk diunggah";
                fDialog.Filter = "All Files|*.pdf;*.doc;*.docx;*.jpg;*.jpeg;*.png|PDF Files|*.pdf|Word Document|*.doc;*.docx|Image|*.jpg;*.jpeg;*.png";
                if (fDialog.ShowDialog() == DialogResult.OK)
                {
                    tFile.Text = fDialog.FileName.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


    }
}
