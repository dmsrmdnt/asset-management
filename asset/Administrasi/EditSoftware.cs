﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class EditSoftware : Form
    {
        lib lib = new lib();
        mEditSoftware m = new mEditSoftware();
        private String id;
        private String id_software;
        private String userid;
        public EditSoftware(String id, String userid)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
            this.userid = userid;
        }

        private void EditSoftware_Load(object sender, EventArgs e)
        {
            try
            {
                showDataSoft(this.id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void showDataSoft(String id)
        {
            try
            {
                int c = 0;
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select a.id_software, b.nama, a.license, a.stat_l from m_license a, m_software b where a.id_software=b.id_software and a.id_license='" + id + "' ", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    this.id_software = read["id_software"].ToString();
                    tSoft.Text = read["nama"].ToString();
                    c = Convert.ToInt32(read["stat_l"].ToString());
                }
                read.Close();
                if (c == 1)
                {
                    myCommand = new SqlCommand("select a.license, b.exp_date from m_license a, t_uplicense b where a.id_license=b.id_license and a.id_license='" + id + "' and b.stat='1'", conn);
                    read = myCommand.ExecuteReader();
                    while (read.Read())
                    {
                        checkL.Checked = true;
                        tNLicense.Text = read["license"].ToString();
                        expDate.Text = read["exp_date"].ToString();
                    }
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void checkL_CheckedChanged(object sender, EventArgs e)
        {
            if (checkL.Checked == true)
            {
                tNLicense.Enabled = true;
                expDate.Enabled = true;
            }
            else
            {
                tNLicense.Enabled = false;
                expDate.Enabled = false;
            }
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (checkL.Checked)
                {
                    String id_software = this.id_software;
                    String license = tNLicense.Text;
                    String exp_date = expDate.Text;
                    if (m.cekInput(id_software, license, exp_date))
                    {
                        if (m.cekData(id_software, license))
                        {
                            m.update(id_software, license, exp_date, this.userid, this.id);
                            this.Close();
                        }
                        else
                        {
                            lNotify.Text = "Data sudah ada!";
                        }
                    }
                    else
                    {
                        lNotify.Text = "Salah input!";
                    }
                }
                else
                {
                    String id_software = this.id_software;
                    if (m.cekInput(id_software))
                    {
                        if (m.cekData(id_software))
                        {
                            if (m.cekLisensi(this.id) <= 1)
                            {
                                m.update(id_software, this.userid, this.id);
                                this.Close();
                            }
                            else
                            {
                                lNotify.Text = "Software ini berlisensi sebelumnya!";
                            }
                        }
                        else
                        {
                            lNotify.Text = "Data sudah ada!";
                        }
                    }
                    else
                    {
                        lNotify.Text = "Salah input!";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EditSoftware_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

    }
    public class mEditSoftware
    {
        lib lib = new lib();
        public Boolean cekInput(String id_software, String license, String exp_date)
        {
            if (
                id_software == ""
                || license == ""
                || exp_date == ""
                )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public Boolean cekInput(String id_software)
        {
            if (
                id_software == ""
                )
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public int cekLisensi(String id_license) 
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select count(*) from t_uplicense where id_license='" + id_license + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return hasil;
        }
        public Boolean cekData(String id_software, String license)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select count(*) from m_license where stat_l='1' and license='" + license + "' and id_software='" + id_software + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (hasil == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public Boolean cekData(String id_software)
        {
            int hasil = 0;
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select count(*) from m_license where stat_l='0' and id_software='" + id_software + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    hasil = Convert.ToInt32(read[0].ToString());
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (hasil == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void update(String id_software, String license, String exp_date, String userid, String id_license) 
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("update m_license set id_software='" + id_software + "', license='" + license + "', stat_l='1', userid='" + userid + "' where id_license='" + id_license + "'", conn);
                myCommand.ExecuteNonQuery();
                if (cekLisensi(id_license) == 0)
                {
                    myCommand = new SqlCommand("insert into t_uplicense(id_license, exp_date, stat) values('" + id_license + "','" + exp_date + "', '1')", conn);
                    myCommand.ExecuteNonQuery();
                }
                else
                {
                    myCommand = new SqlCommand("update t_uplicense set exp_date='" + exp_date + "', license='" + license + "' where id_license = '" + id_license + "' and stat='1'", conn);
                    myCommand.ExecuteNonQuery();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void update(String id_software, String userid, String id_license)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = new SqlCommand("update m_license set id_software='" + id_software + "', stat_l='0', license='', userid='" + userid + "' where id_license='" + id_license + "'", conn);
                myCommand.ExecuteNonQuery();

                myCommand = new SqlCommand("delete from t_uplicense where id_license='" + id_license + "'", conn);
                myCommand.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
    }
}
