﻿namespace asset
{
    partial class EditBrand
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lNotify = new System.Windows.Forms.Label();
            this.tBrand = new System.Windows.Forms.TextBox();
            this.bCancel = new System.Windows.Forms.Button();
            this.bOK = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lNotify
            // 
            this.lNotify.AutoSize = true;
            this.lNotify.ForeColor = System.Drawing.Color.Red;
            this.lNotify.Location = new System.Drawing.Point(27, 53);
            this.lNotify.Name = "lNotify";
            this.lNotify.Size = new System.Drawing.Size(0, 13);
            this.lNotify.TabIndex = 19;
            // 
            // tBrand
            // 
            this.tBrand.Location = new System.Drawing.Point(27, 17);
            this.tBrand.Name = "tBrand";
            this.tBrand.Size = new System.Drawing.Size(310, 20);
            this.tBrand.TabIndex = 0;
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(217, 90);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(120, 40);
            this.bCancel.TabIndex = 2;
            this.bCancel.Text = "Batal";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bOK
            // 
            this.bOK.Location = new System.Drawing.Point(80, 90);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(120, 40);
            this.bOK.TabIndex = 1;
            this.bOK.Text = "Simpan";
            this.bOK.UseVisualStyleBackColor = true;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // EditBrand
            // 
            this.AcceptButton = this.bOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 147);
            this.Controls.Add(this.lNotify);
            this.Controls.Add(this.tBrand);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditBrand";
            this.Text = "Ubah Brand";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.EditBrand_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lNotify;
        private System.Windows.Forms.TextBox tBrand;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bOK;
    }
}