﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class AddSoftware : Form
    {
        lib lib = new lib();
        Class.process.mAddSoftware m = new Class.process.mAddSoftware();
        private String id;
        private String id_software;
        public AddSoftware(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
        }

        private void checkL_CheckedChanged(object sender, EventArgs e)
        {
            if (checkL.Checked == true) 
            {
                tNLicense.Enabled = true;
                expDate.Enabled = true;
            }
            else
            {
                tNLicense.Enabled = false;
                expDate.Enabled = false;
            }
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            try
            {
                if (checkL.Checked)
                {
                    String id_software = this.id_software;
                    String license = tNLicense.Text;
                    String exp_date = expDate.Text;
                    if (m.cekInput(id_software, license, exp_date))
                    {
                        if (m.cekData(id_software, license))
                        {
                            m.insert(id_software, license, exp_date, this.id);
                            this.Close();
                        }
                        else
                        {
                            lNotify.Text = "Data sudah ada!";
                        }
                    }
                    else
                    {
                        lNotify.Text = "Salah input!";
                    }
                }
                else
                {
                    String id_software = this.id_software;
                    if (m.cekInput(id_software))
                    {
                        if (m.cekData(id_software))
                        {
                            m.insert(id_software, this.id);
                            this.Close();
                        }
                        else
                        {
                            lNotify.Text = "Data sudah ada!";
                        }
                    }
                    else
                    {
                        lNotify.Text = "Salah input!";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void baSoft_Click(object sender, EventArgs e)
        {
            try
            {
                AssetSoftwareDetail cat = new AssetSoftwareDetail(this.id);
                cat.ShowDialog();
                this.tSoft.Text = cat.namaSoftware;
                this.id_software = cat.idSoftware;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void AddSoftware_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
    }
}
