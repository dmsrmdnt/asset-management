﻿namespace asset.Administrasi
{
    partial class AddSoftwareLicense
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.cSoftware = new System.Windows.Forms.ComboBox();
            this.cKategori = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dAwal = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.tLicense = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.bOk = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(21, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Software";
            // 
            // cSoftware
            // 
            this.cSoftware.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cSoftware.FormattingEnabled = true;
            this.cSoftware.Location = new System.Drawing.Point(131, 39);
            this.cSoftware.Name = "cSoftware";
            this.cSoftware.Size = new System.Drawing.Size(339, 21);
            this.cSoftware.TabIndex = 10;
            // 
            // cKategori
            // 
            this.cKategori.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cKategori.FormattingEnabled = true;
            this.cKategori.Location = new System.Drawing.Point(131, 12);
            this.cKategori.Name = "cKategori";
            this.cKategori.Size = new System.Drawing.Size(339, 21);
            this.cKategori.TabIndex = 9;
            this.cKategori.SelectedIndexChanged += new System.EventHandler(this.cKategori_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(21, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Kategori Software";
            // 
            // dAwal
            // 
            this.dAwal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dAwal.Location = new System.Drawing.Point(131, 66);
            this.dAwal.Name = "dAwal";
            this.dAwal.Size = new System.Drawing.Size(98, 20);
            this.dAwal.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(21, 70);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Expired Date";
            // 
            // tLicense
            // 
            this.tLicense.Location = new System.Drawing.Point(131, 92);
            this.tLicense.Name = "tLicense";
            this.tLicense.Size = new System.Drawing.Size(339, 20);
            this.tLicense.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(21, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Nomor Lisensi";
            // 
            // bOk
            // 
            this.bOk.Location = new System.Drawing.Point(19, 136);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(451, 48);
            this.bOk.TabIndex = 16;
            this.bOk.Text = "Simpan";
            this.bOk.UseVisualStyleBackColor = true;
            this.bOk.Click += new System.EventHandler(this.bOk_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(235, 63);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(111, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "Lifetime License";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // AddSoftwareLicense
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 220);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bOk);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tLicense);
            this.Controls.Add(this.dAwal);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cSoftware);
            this.Controls.Add(this.cKategori);
            this.Controls.Add(this.label1);
            this.Name = "AddSoftwareLicense";
            this.Text = "Tambah Lisensi Software";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AddSoftwareLicense_FormClosed);
            this.Load += new System.EventHandler(this.AddSoftwareLicense_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AddSoftwareLicense_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cSoftware;
        private System.Windows.Forms.ComboBox cKategori;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dAwal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tLicense;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button bOk;
        private System.Windows.Forms.Button button1;
    }
}