﻿namespace asset.Administrasi
{
    partial class RequestManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.bSearch = new System.Windows.Forms.Button();
            this.cStatus = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.dAkhir = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dAwal = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGAssetL = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGAssetL)).BeginInit();
            this.SuspendLayout();
            // 
            // bSearch
            // 
            this.bSearch.Location = new System.Drawing.Point(378, 11);
            this.bSearch.Name = "bSearch";
            this.bSearch.Size = new System.Drawing.Size(162, 46);
            this.bSearch.TabIndex = 48;
            this.bSearch.Text = "Cari";
            this.bSearch.UseVisualStyleBackColor = true;
            this.bSearch.Click += new System.EventHandler(this.bSearch_Click);
            // 
            // cStatus
            // 
            this.cStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cStatus.FormattingEnabled = true;
            this.cStatus.Location = new System.Drawing.Point(93, 41);
            this.cStatus.Name = "cStatus";
            this.cStatus.Size = new System.Drawing.Size(98, 21);
            this.cStatus.TabIndex = 47;
            this.cStatus.SelectedIndexChanged += new System.EventHandler(this.cStatus_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(20, 44);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 46;
            this.label3.Text = "Status";
            // 
            // dAkhir
            // 
            this.dAkhir.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dAkhir.Location = new System.Drawing.Point(244, 12);
            this.dAkhir.Name = "dAkhir";
            this.dAkhir.Size = new System.Drawing.Size(98, 20);
            this.dAkhir.TabIndex = 45;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(206, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 13);
            this.label2.TabIndex = 44;
            this.label2.Text = "s/d";
            // 
            // dAwal
            // 
            this.dAwal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dAwal.Location = new System.Drawing.Point(93, 12);
            this.dAwal.Name = "dAwal";
            this.dAwal.Size = new System.Drawing.Size(98, 20);
            this.dAwal.TabIndex = 43;
            this.dAwal.Value = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(20, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 42;
            this.label1.Text = "Tanggal";
            // 
            // dataGAssetL
            // 
            this.dataGAssetL.AllowUserToAddRows = false;
            this.dataGAssetL.AllowUserToDeleteRows = false;
            this.dataGAssetL.AllowUserToResizeColumns = false;
            this.dataGAssetL.AllowUserToResizeRows = false;
            this.dataGAssetL.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGAssetL.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGAssetL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGAssetL.Location = new System.Drawing.Point(12, 68);
            this.dataGAssetL.MultiSelect = false;
            this.dataGAssetL.Name = "dataGAssetL";
            this.dataGAssetL.ReadOnly = true;
            this.dataGAssetL.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGAssetL.Size = new System.Drawing.Size(900, 319);
            this.dataGAssetL.TabIndex = 49;
            this.dataGAssetL.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGAssetL_CellContentClick);
            // 
            // RequestManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(924, 399);
            this.Controls.Add(this.dataGAssetL);
            this.Controls.Add(this.bSearch);
            this.Controls.Add(this.cStatus);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dAkhir);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dAwal);
            this.Controls.Add(this.label1);
            this.Name = "RequestManagement";
            this.Text = "Kelola Permintaan Aset Baru";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.RequestManagement_FormClosed);
            this.Load += new System.EventHandler(this.RequestManagement_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.RequestManagement_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.dataGAssetL)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bSearch;
        private System.Windows.Forms.ComboBox cStatus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dAkhir;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dAwal;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGAssetL;
    }
}