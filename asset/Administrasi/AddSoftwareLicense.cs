﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset.Administrasi
{
    public partial class AddSoftwareLicense : Form
    {
        Class.process.mAddSoftwareLicense s = new Class.process.mAddSoftwareLicense();
        lib lib = new lib();
        public string tittle = "", license = "", exp_date="";
        public int status = 0, id_category = 0, id_assetSoft,id_license;
        public AddSoftwareLicense()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }
        private static AddSoftwareLicense form;
        public static AddSoftwareLicense GetChildInstance()
        {
            if (form == null)
                form = new AddSoftwareLicense();
            return form;
        }
        private void AddSoftwareLicense_Load(object sender, EventArgs e)
        {
            this.Text = this.tittle;
            showDataC();
            if(status==1){
                cKategori.SelectedValue = this.id_category;
                cSoftware.SelectedValue = this.id_assetSoft;
                tLicense.Text = this.license;
                dAwal.Text = this.exp_date;
            }
            else if (status == 2)
            {
                cKategori.SelectedValue = this.id_category;
                cSoftware.SelectedValue = this.id_assetSoft;
                cKategori.Enabled = false;
                cSoftware.Enabled = false;
            }
        }

        private void AddSoftwareLicense_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

        private void AddSoftwareLicense_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void bOk_Click(object sender, EventArgs e)
        {
            List<String> input = new List<String>();
            input.Add(cKategori.SelectedValue.ToString());
            input.Add(cSoftware.SelectedValue.ToString());
            input.Add(dAwal.Text);
            input.Add(tLicense.Text);
            if (s.empty(input.ToArray()))
            {
                if (this.status == 0)
                {
                    string id_assetSoft=cSoftware.SelectedValue.ToString();
                    string license=tLicense.Text;
                    string exp_date=dAwal.Text;
                    s.insertSoftwareLicense(id_assetSoft,license,exp_date);
                }
                else if (this.status == 1)
                {
                    string id_assetSoft = cSoftware.SelectedValue.ToString();
                    string license = tLicense.Text;
                    string exp_date = dAwal.Text;
                    int id_license = this.id_license;
                    s.updateSoftwareLicense(id_assetSoft, license, exp_date,id_license);
                }
                else if (this.status == 2)
                {
                    string id_assetSoft = this.id_assetSoft.ToString();
                    string license = tLicense.Text;
                    string exp_date = dAwal.Text;
                    int id_license = this.id_license;
                    s.updateLicense(id_assetSoft, license, exp_date, id_license);
                }
                this.Close();
            }
            else 
            {
                MessageBox.Show("Input tidak lengkap!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cKategori_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cSoftware.DataSource = null;
            this.cSoftware.Items.Clear();
            showDataS(cKategori.SelectedValue.ToString());
        }
        void showDataC()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sc = new SqlCommand("select id_category,category from m_softwareCat", conn);
                SqlDataReader read;
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(string));
                dt.Columns.Add("category", typeof(string));
                read = sc.ExecuteReader();
                while (read.Read())
                {
                    dt.Rows.Add(new object[] { read["id_category"].ToString(), read["category"].ToString() });
                }

                cKategori.ValueMember = "id";
                cKategori.DisplayMember = "category";
                cKategori.DataSource = dt;

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        void showDataS(string id_category)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sc = new SqlCommand("select b.id_software, b.software from t_asset_software b where b.id_category='"+id_category+"' and b.id_status=1", conn);
                SqlDataReader read;
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(string));
                dt.Columns.Add("software", typeof(string));
                read = sc.ExecuteReader();
                while (read.Read())
                {
                    dt.Rows.Add(new object[] { read["id_software"].ToString(), read["software"].ToString() });
                }

                cSoftware.ValueMember = "id";
                cSoftware.DisplayMember = "software";
                cSoftware.DataSource = dt;

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            dAwal.Value = new DateTime(9998, 12, 31);
        }
    }
}
