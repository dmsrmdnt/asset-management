﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace asset.Administrasi
{
    public partial class BackupDatabase : Form
    {
        lib lib = new lib();
        Class.backupDB.backup b = new Class.backupDB.backup();
        public BackupDatabase()
        {
            InitializeComponent();
        }
        private static BackupDatabase form;
        public static BackupDatabase GetChildInstance()
        {
            if (form == null)
                form = new BackupDatabase();
            return form;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();
            if (result == DialogResult.OK)
            {
                tFolder.Text = fbd.SelectedPath;
            }
        }

        private void BackupDatabase_Load(object sender, EventArgs e)
        {

        }

        private void BackupDatabase_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            } 
        }

        private void BackupDatabase_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (tFolder.Text != null && tFolder.Text != "" && tFile.Text != null && tFile.Text != "")
            {
                if (File.Exists(b.separator(tFolder.Text) + "\\" + tFile.Text + ".bak"))
                {
                    DialogResult dr = MessageBox.Show("File already exist, are you sure want to replace ?",
                            "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dr == DialogResult.Yes)
                    {
                        File.Delete(b.separator(tFolder.Text) + "\\" + tFile.Text + ".bak");
                        View.BackupDatabase backup = new View.BackupDatabase();
                        backup.file = b.separator(tFolder.Text) + "\\" + tFile.Text + ".bak";
                        backup.title = "Backup Database";
                        backup.label = "Proses Backup Database...";
                        backup.form = 0;
                        backup.ShowDialog();
                        backup.BringToFront();
                    }
                }
                else
                {
                    View.BackupDatabase backup = new View.BackupDatabase();
                    backup.file = b.separator(tFolder.Text) + "\\" + tFile.Text + ".bak";
                    backup.title = "Backup Database";
                    backup.label = "Proses Backup Database...";
                    backup.form = 0;
                    backup.ShowDialog();
                    backup.BringToFront();
                }

                if (asset.Class.backupDB.backup.cek == true)
                {
                    MessageBox.Show("Backup database berhasil", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    clearField();
                }
                else
                {
                    MessageBox.Show("Backup database gagal. Proses backup hanya dapat dilakukan di komputer server dan di luar partisi sistem", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else {
                MessageBox.Show("Input belum lengkap!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        void clearField()
        {
            tFile.Clear();
            tFolder.Clear();
        }
    }
}
