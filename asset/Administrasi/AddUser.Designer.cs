﻿namespace asset
{
    partial class AddUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lNIK = new System.Windows.Forms.Label();
            this.lName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.bOK = new System.Windows.Forms.Button();
            this.bCancel = new System.Windows.Forms.Button();
            this.tNIK = new System.Windows.Forms.TextBox();
            this.tName = new System.Windows.Forms.TextBox();
            this.tDepartment = new System.Windows.Forms.TextBox();
            this.cAuth = new System.Windows.Forms.ComboBox();
            this.lNotify = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lNIK
            // 
            this.lNIK.AutoSize = true;
            this.lNIK.BackColor = System.Drawing.Color.Transparent;
            this.lNIK.ForeColor = System.Drawing.Color.White;
            this.lNIK.Location = new System.Drawing.Point(12, 29);
            this.lNIK.Name = "lNIK";
            this.lNIK.Size = new System.Drawing.Size(32, 13);
            this.lNIK.TabIndex = 0;
            this.lNIK.Text = "Email";
            // 
            // lName
            // 
            this.lName.AutoSize = true;
            this.lName.BackColor = System.Drawing.Color.Transparent;
            this.lName.ForeColor = System.Drawing.Color.White;
            this.lName.Location = new System.Drawing.Point(12, 63);
            this.lName.Name = "lName";
            this.lName.Size = new System.Drawing.Size(35, 13);
            this.lName.TabIndex = 1;
            this.lName.Text = "Nama";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 95);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Authority";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(12, 124);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Departemen";
            // 
            // bOK
            // 
            this.bOK.Location = new System.Drawing.Point(323, 208);
            this.bOK.Name = "bOK";
            this.bOK.Size = new System.Drawing.Size(120, 40);
            this.bOK.TabIndex = 4;
            this.bOK.Text = "Simpan";
            this.bOK.UseVisualStyleBackColor = true;
            this.bOK.Click += new System.EventHandler(this.bOK_Click);
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(460, 208);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(120, 40);
            this.bCancel.TabIndex = 5;
            this.bCancel.Text = "Batal";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // tNIK
            // 
            this.tNIK.Location = new System.Drawing.Point(103, 26);
            this.tNIK.Name = "tNIK";
            this.tNIK.Size = new System.Drawing.Size(316, 20);
            this.tNIK.TabIndex = 0;
            // 
            // tName
            // 
            this.tName.Location = new System.Drawing.Point(103, 60);
            this.tName.Name = "tName";
            this.tName.Size = new System.Drawing.Size(316, 20);
            this.tName.TabIndex = 1;
            // 
            // tDepartment
            // 
            this.tDepartment.Location = new System.Drawing.Point(103, 121);
            this.tDepartment.Name = "tDepartment";
            this.tDepartment.Size = new System.Drawing.Size(173, 20);
            this.tDepartment.TabIndex = 3;
            // 
            // cAuth
            // 
            this.cAuth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cAuth.FormattingEnabled = true;
            this.cAuth.Location = new System.Drawing.Point(103, 92);
            this.cAuth.Name = "cAuth";
            this.cAuth.Size = new System.Drawing.Size(121, 21);
            this.cAuth.TabIndex = 2;
            // 
            // lNotify
            // 
            this.lNotify.AutoSize = true;
            this.lNotify.ForeColor = System.Drawing.Color.Red;
            this.lNotify.Location = new System.Drawing.Point(12, 168);
            this.lNotify.Name = "lNotify";
            this.lNotify.Size = new System.Drawing.Size(0, 13);
            this.lNotify.TabIndex = 10;
            // 
            // AddUser
            // 
            this.AcceptButton = this.bOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(592, 266);
            this.Controls.Add(this.lNotify);
            this.Controls.Add(this.cAuth);
            this.Controls.Add(this.tDepartment);
            this.Controls.Add(this.tName);
            this.Controls.Add(this.tNIK);
            this.Controls.Add(this.bCancel);
            this.Controls.Add(this.bOK);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lName);
            this.Controls.Add(this.lNIK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AddUser";
            this.Text = "Tambah Pengguna";
            this.Load += new System.EventHandler(this.AddUser_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AddUser_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lNIK;
        private System.Windows.Forms.Label lName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button bOK;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.TextBox tNIK;
        private System.Windows.Forms.TextBox tName;
        private System.Windows.Forms.TextBox tDepartment;
        private System.Windows.Forms.ComboBox cAuth;
        private System.Windows.Forms.Label lNotify;
    }
}