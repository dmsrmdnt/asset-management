﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class EditCategory : Form
    {
        private String id = null;
        Class.process.mEditCategory edit = new Class.process.mEditCategory();
        lib lib = new lib();
        public EditCategory(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            showData(id);
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bOK_Click(object sender, EventArgs e)
        {
            try
            {
                String category = tCategory.Text;
                String id = this.id;
                Boolean cek = edit.cekInput(id, category);

                if (cek == true)
                {
                    if (edit.cekData(category) <= 2)
                    {
                        edit.update(id, category);
                        this.Close();
                    }
                    else
                    {
                        lNotify.Text = "Kategori sudah ada!";
                    }
                }
                else
                {
                    lNotify.Text = "Salah input!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showData(String id) {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = new SqlCommand("select id_category,category from m_category where id_category='" + id + "'", conn);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    this.id = read["id_category"].ToString();
                    tCategory.Text = read["category"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EditCategory_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
    }
}
