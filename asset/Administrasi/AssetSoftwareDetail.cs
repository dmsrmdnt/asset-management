﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class AssetSoftwareDetail : Form
    {
        private String id;
        Class.process.mSoftware s = new Class.process.mSoftware();
        lib lib = new lib();
        private String idS = "";
        private String namaS = "";
        public AssetSoftwareDetail(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
        }
        public String idSoftware
        {
            get
            {
                return this.idS;
            }
        }
        public String namaSoftware
        {
            get
            {
                return this.namaS;
            }
        }
        private void AssetSoftwareDetail_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewButtonColumn a = new DataGridViewButtonColumn();
                dataGSoft.Columns.Add(a);
                a.HeaderText = "Pilih";
                a.Text = "Pilih";
                a.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn b = new DataGridViewButtonColumn();
                dataGSoft.Columns.Add(b);
                b.HeaderText = "Ubah";
                b.Text = "Ubah";
                b.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn c = new DataGridViewButtonColumn();
                dataGSoft.Columns.Add(c);
                c.HeaderText = "Hapus";
                c.Text = "Hapus";
                c.UseColumnTextForButtonValue = true;
                showdataGSoft();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void showdataGSoft()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_AssetSoft";
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "SoftTable");
                conn.Close();
                this.dataGSoft.DataSource = dataSet.Tables["SoftTable"].DefaultView;
                this.dataGSoft.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bSearch_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearch.Text;
                searchDataGSoft(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void searchDataGSoft(String str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "search_data_AssetSoft";
                myCommand.Parameters.AddWithValue("@str", str);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "CategoryTable");
                conn.Close();
                this.dataGSoft.DataSource = dataSet.Tables["CategoryTable"].DefaultView;
                this.dataGSoft.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                showdataGSoft();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            try
            {
                AddSoftwareDetail add = new AddSoftwareDetail(this.id);
                add.ShowDialog();
                showdataGSoft();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGSoft_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGSoft.Columns[0].Index)
                {
                    //Pilih
                    try
                    {
                        String id = dataGSoft[3, e.RowIndex].Value.ToString();
                        String nama = dataGSoft[4, e.RowIndex].Value.ToString();
                        this.idS = id;
                        this.namaS = nama;
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGSoft.Columns[1].Index)
                {
                    //Ubah
                    try
                    {
                        String id = dataGSoft[3, e.RowIndex].Value.ToString();
                        EditSoftwareDetail edit = new EditSoftwareDetail(id,this.id);
                        edit.ShowDialog();
                        showdataGSoft();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGSoft.Columns[2].Index)
                {
                    //Hapus
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menghapus data ini? ",
            "Konfirmasi Penghapusan", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (dr == DialogResult.Yes)
                        {

                            String id = dataGSoft[3, e.RowIndex].Value.ToString();
                            Boolean cek = s.CekData(id);
                            if (cek == true)
                            {
                                s.delete(id);
                                showdataGSoft();
                            }
                            else
                            {
                                MessageBox.Show("Data ini mempunyai relasi dengan tabel lain!",
                       "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void AssetSoftwareDetail_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
    }
}
