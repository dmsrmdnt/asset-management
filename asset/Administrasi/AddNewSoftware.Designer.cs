﻿namespace asset.Administrasi
{
    partial class AddNewSoftware
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.cStatus = new System.Windows.Forms.ComboBox();
            this.cKategori = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.bOk = new System.Windows.Forms.Button();
            this.tSoft = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(14, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Status Software";
            // 
            // cStatus
            // 
            this.cStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cStatus.FormattingEnabled = true;
            this.cStatus.Location = new System.Drawing.Point(124, 70);
            this.cStatus.Name = "cStatus";
            this.cStatus.Size = new System.Drawing.Size(339, 21);
            this.cStatus.TabIndex = 6;
            // 
            // cKategori
            // 
            this.cKategori.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cKategori.FormattingEnabled = true;
            this.cKategori.Location = new System.Drawing.Point(124, 43);
            this.cKategori.Name = "cKategori";
            this.cKategori.Size = new System.Drawing.Size(339, 21);
            this.cKategori.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(14, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Kategori Software";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(14, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Software";
            // 
            // bOk
            // 
            this.bOk.Location = new System.Drawing.Point(12, 144);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(676, 52);
            this.bOk.TabIndex = 12;
            this.bOk.Text = "Save";
            this.bOk.UseVisualStyleBackColor = true;
            this.bOk.Click += new System.EventHandler(this.bOk_Click);
            // 
            // tSoft
            // 
            this.tSoft.Location = new System.Drawing.Point(124, 17);
            this.tSoft.Name = "tSoft";
            this.tSoft.Size = new System.Drawing.Size(564, 20);
            this.tSoft.TabIndex = 13;
            this.tSoft.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tSoft_KeyPress);
            // 
            // AddNewSoftware
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 228);
            this.Controls.Add(this.tSoft);
            this.Controls.Add(this.bOk);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cStatus);
            this.Controls.Add(this.cKategori);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AddNewSoftware";
            this.Text = "Tambah Software";
            this.Load += new System.EventHandler(this.AddNewSoftware_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AddNewSoftware_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cStatus;
        private System.Windows.Forms.ComboBox cKategori;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bOk;
        private System.Windows.Forms.TextBox tSoft;
    }
}