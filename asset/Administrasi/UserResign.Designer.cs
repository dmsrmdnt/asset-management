﻿namespace asset.Administrasi
{
    partial class UserResign
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.bCari = new System.Windows.Forms.Button();
            this.tFile = new System.Windows.Forms.TextBox();
            this.bOk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Upload BA";
            // 
            // bCari
            // 
            this.bCari.Location = new System.Drawing.Point(359, 34);
            this.bCari.Name = "bCari";
            this.bCari.Size = new System.Drawing.Size(75, 23);
            this.bCari.TabIndex = 18;
            this.bCari.Text = "Cari";
            this.bCari.UseVisualStyleBackColor = true;
            this.bCari.Click += new System.EventHandler(this.bCari_Click);
            // 
            // tFile
            // 
            this.tFile.Location = new System.Drawing.Point(76, 36);
            this.tFile.Name = "tFile";
            this.tFile.ReadOnly = true;
            this.tFile.Size = new System.Drawing.Size(277, 20);
            this.tFile.TabIndex = 19;
            // 
            // bOk
            // 
            this.bOk.Location = new System.Drawing.Point(76, 86);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(277, 46);
            this.bOk.TabIndex = 20;
            this.bOk.Text = "Proses";
            this.bOk.UseVisualStyleBackColor = true;
            this.bOk.Click += new System.EventHandler(this.bOk_Click);
            // 
            // UserResign
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 148);
            this.Controls.Add(this.bOk);
            this.Controls.Add(this.bCari);
            this.Controls.Add(this.tFile);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "UserResign";
            this.Text = "Upload Berita Acara";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.UserResign_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button bCari;
        private System.Windows.Forms.TextBox tFile;
        private System.Windows.Forms.Button bOk;
    }
}