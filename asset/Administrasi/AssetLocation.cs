﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class AssetLocation : Form
    {
        lib lib = new lib();
        private String idL = "";
        private String namaL = "";
        Class.process.mAssetLocation asset = new Class.process.mAssetLocation();
        public AssetLocation()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }
        public String idLocation
        {
            get
            {
                return this.idL;
            }
        }
        public String namaLocation
        {
            get
            {
                return this.namaL;
            }
        }
        private void AssetLocation_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewButtonColumn a = new DataGridViewButtonColumn();
                dataGLocation.Columns.Add(a);
                a.HeaderText = "Pilih";
                a.Text = "Pilih";
                a.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn b = new DataGridViewButtonColumn();
                dataGLocation.Columns.Add(b);
                b.HeaderText = "Ubah";
                b.Text = "Ubah";
                b.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn c = new DataGridViewButtonColumn();
                dataGLocation.Columns.Add(c);
                c.HeaderText = "Hapus";
                c.Text = "Hapus";
                c.UseColumnTextForButtonValue = true;
                showdataGLocation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void showdataGLocation()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_location";
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "locationTable");
                conn.Close();
                this.dataGLocation.DataSource = dataSet.Tables["locationTable"].DefaultView;
                this.dataGLocation.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bSearch_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearch.Text;
                searchLocation(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void searchLocation(String str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "search_data_location";
                myCommand.Parameters.AddWithValue("@str", str);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "locationTable");
                conn.Close();
                this.dataGLocation.DataSource = dataSet.Tables["locationTable"].DefaultView;
                this.dataGLocation.Columns["ID"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                showdataGLocation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bAdd_Click(object sender, EventArgs e)
        {
            try
            {
                AddLocation add = new AddLocation();
                add.ShowDialog();
                showdataGLocation();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGLocation_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGLocation.Columns[0].Index)
                {
                    //Pilih
                    try
                    {
                        String id = dataGLocation[3, e.RowIndex].Value.ToString();
                        String nama = dataGLocation[4, e.RowIndex].Value.ToString();
                        this.idL = id;
                        this.namaL = nama;
                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGLocation.Columns[1].Index)
                {
                    //Ubah
                    try
                    {
                        String id = dataGLocation[3, e.RowIndex].Value.ToString();
                        EditLocation edit = new EditLocation(id);
                        edit.ShowDialog();
                        showdataGLocation();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGLocation.Columns[2].Index)
                {
                    //Hapus
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menghapus data ini? ",
            "Konfirmasi Penghapusan", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                        if (dr == DialogResult.Yes)
                        {

                            String id = dataGLocation[3, e.RowIndex].Value.ToString();
                            Boolean cek = asset.CekData(id);
                            if (cek == true)
                            {
                                asset.delete(id);
                                showdataGLocation();
                            }
                            else
                            {
                                MessageBox.Show("Data ini mempunyai relasi dengan tabel lain!",
                       "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void AssetLocation_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            } 
        }
    }
}
