﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;


namespace asset
{
    public partial class ClaimManagement : Form
    {
        private String id;
        lib lib = new lib();
        Class.Email.email mail=new Class.Email.email();
        Class.process.mClaimManagement claim = new Class.process.mClaimManagement();
        public ClaimManagement(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
        }

        private static ClaimManagement form;
        public static ClaimManagement GetChildInstance(String id)
        {
            if (form == null)
                form = new ClaimManagement(id);
            return form;
        }

        private void ClaimManagement_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewButtonColumn a = new DataGridViewButtonColumn();
                dataGAssetL.Columns.Add(a);
                a.HeaderText = "Terima";
                a.Text = "Terima";
                a.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn b = new DataGridViewButtonColumn();
                dataGAssetL.Columns.Add(b);
                b.HeaderText = "Tolak";
                b.Text = "Tolak";
                b.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn c = new DataGridViewButtonColumn();
                dataGAssetL.Columns.Add(c);
                c.HeaderText = "Detail";
                c.Text = "Detail";
                c.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn d = new DataGridViewButtonColumn();
                dataGAssetB.Columns.Add(d);
                d.HeaderText = "Terima";
                d.Text = "Terima";
                d.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn f = new DataGridViewButtonColumn();
                dataGAssetB.Columns.Add(f);
                f.HeaderText = "Tolak";
                f.Text = "Tolak";
                f.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn g = new DataGridViewButtonColumn();
                dataGAssetB.Columns.Add(g);
                g.HeaderText = "Detail";
                g.Text = "Detail";
                g.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn h = new DataGridViewButtonColumn();
                dataGAssetF.Columns.Add(h);
                h.HeaderText = "Kirim Perbaikan";
                h.Text = "Proses";
                h.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn i = new DataGridViewButtonColumn();
                dataGAssetF.Columns.Add(i);
                i.HeaderText = "Batal";
                i.Text = "Batal";
                i.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn k = new DataGridViewButtonColumn();
                dataGAssetF.Columns.Add(k);
                k.HeaderText = "Rusak";
                k.Text = "Rusak";
                k.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn j = new DataGridViewButtonColumn();
                dataGAssetF.Columns.Add(j);
                j.HeaderText = "Detail";
                j.Text = "Detail";
                j.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn l = new DataGridViewButtonColumn();
                dataGAssetFi.Columns.Add(l);
                l.HeaderText = "Selesai Perbaikan";
                l.Text = "Proses";
                l.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn m = new DataGridViewButtonColumn();
                dataGAssetFi.Columns.Add(m);
                m.HeaderText = "Batal";
                m.Text = "Batal";
                m.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn n = new DataGridViewButtonColumn();
                dataGAssetFi.Columns.Add(n);
                n.HeaderText = "Rusak";
                n.Text = "Rusak";
                n.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn o = new DataGridViewButtonColumn();
                dataGAssetFi.Columns.Add(o);
                o.HeaderText = "Detail";
                o.Text = "Detail";
                o.UseColumnTextForButtonValue = true;

                DataGridViewLinkColumn links = new DataGridViewLinkColumn();
                links.UseColumnTextForLinkValue = true;
                links.HeaderText = "Download Berita Acara";
                links.DataPropertyName = "lnkColumn";
                links.ActiveLinkColor = Color.White;
                links.LinkBehavior = LinkBehavior.SystemDefault;
                links.LinkColor = Color.Blue;
                links.Text = "Click here to download";
                links.TrackVisitedState = true;
                links.VisitedLinkColor = Color.YellowGreen;
                dataGAssetL.Columns.Add(links);

                DataGridViewLinkColumn links1 = new DataGridViewLinkColumn();
                links1.UseColumnTextForLinkValue = true;
                links1.HeaderText = "Download Berita Acara";
                links1.DataPropertyName = "lnkColumn";
                links1.ActiveLinkColor = Color.White;
                links1.LinkBehavior = LinkBehavior.SystemDefault;
                links1.LinkColor = Color.Blue;
                links1.Text = "Click here to download";
                links1.TrackVisitedState = true;
                links1.VisitedLinkColor = Color.YellowGreen;
                dataGAssetB.Columns.Add(links1);

                DataGridViewLinkColumn links2 = new DataGridViewLinkColumn();
                links2.UseColumnTextForLinkValue = true;
                links2.HeaderText = "Download Berita Acara";
                links2.DataPropertyName = "lnkColumn";
                links2.ActiveLinkColor = Color.White;
                links2.LinkBehavior = LinkBehavior.SystemDefault;
                links2.LinkColor = Color.Blue;
                links2.Text = "Click here to download";
                links2.TrackVisitedState = true;
                links2.VisitedLinkColor = Color.YellowGreen;
                dataGAssetF.Columns.Add(links2);

                DataGridViewLinkColumn links3 = new DataGridViewLinkColumn();
                links3.UseColumnTextForLinkValue = true;
                links3.HeaderText = "Download Berita Acara";
                links3.DataPropertyName = "lnkColumn";
                links3.ActiveLinkColor = Color.White;
                links3.LinkBehavior = LinkBehavior.SystemDefault;
                links3.LinkColor = Color.Blue;
                links3.Text = "Click here to download";
                links3.TrackVisitedState = true;
                links3.VisitedLinkColor = Color.YellowGreen;
                dataGAssetFi.Columns.Add(links3);

                DataGridViewLinkColumn links4 = new DataGridViewLinkColumn();
                links4.UseColumnTextForLinkValue = true;
                links4.HeaderText = "Download Quotation";
                links4.DataPropertyName = "lnkColumn";
                links4.ActiveLinkColor = Color.White;
                links4.LinkBehavior = LinkBehavior.SystemDefault;
                links4.LinkColor = Color.Blue;
                links4.Text = "Click here to download";
                links4.TrackVisitedState = true;
                links4.VisitedLinkColor = Color.YellowGreen;
                dataGAssetFi.Columns.Add(links4);

                showDataGAssetL();
                showDataGAssetB();
                showDataGAssetF();
                showDataGAssetFi();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ClaimManagement_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        ////////////// klaim kehilangan ///////////////////////////////////////////////////

        public void showDataGAssetL()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter("select h.id_employee as \"ID\", g.nama as Pemilik, h.id_asset as \"ID Aset\", b.category as Kategori, c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                    "from m_asset a, m_category b, m_brand c, m_location d, m_status e, m_employee g, t_lose h " +
                    "where a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status and a.id_asset=h.id_asset and g.id_employee=h.id_employee and h.acc='2'", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "LoseAssetTable");
                conn.Close();
                this.dataGAssetL.DataSource = dataSet.Tables["LoseAssetTable"].DefaultView;
                this.dataGAssetL.Columns["ID"].Visible = false;
                this.dataGAssetL.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bSearchL_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearchL.Text;
                searchListL(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void searchListL(String str, String status = "4")
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter("select h.id_employee as \"ID\", g.nama as Pemilik, h.id_asset as \"ID Aset\", b.category as Kategori, c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                   "from m_asset a, m_category b, m_brand c, m_location d, m_status e, m_employee g, t_lose h " +
                   "where g.nama like '%" + str + "%' and a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status and a.id_asset=h.id_asset and g.id_employee=h.id_employee and h.acc='2'", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "ListLoseTable");
                conn.Close();
                this.dataGAssetL.DataSource = dataSet.Tables["ListLoseTable"].DefaultView;
                this.dataGAssetL.Columns["ID"].Visible = false;
                this.dataGAssetL.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGAssetL_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGAssetL.Columns[0].Index)
                {
                    //Terima
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menerima klaim kehilangan aset ini ? ",
                        "Konfirmasi Persetujuan", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            String id_asset = dataGAssetL[6, e.RowIndex].Value.ToString();
                            String id_employee = dataGAssetL[4, e.RowIndex].Value.ToString();
                            
                            View.SendEmail s = new View.SendEmail();
                            s.id_employee = id_employee;
                            s.id_asset = id_asset;
                            s.stat = 4;
                            s.indexStatus = 0;
                            s.form = 1;
                            s.ShowDialog();

                            if (asset.Class.Email.email.cek == true)
                            {
                                claim.updateL(id_employee, id_asset, this.id);
                                MessageBox.Show("Konfirmasi laporan kehilangan berhasil dikirim", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show("Konfirmasi laporan kehilangan gagal dikirim", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            
                            showDataGAssetL();
                        }
                        Main m = new Main(this.id);
                        m.showC();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetL.Columns[1].Index)
                {
                    //Tolak
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menolak klaim kehilangan aset ini ? ",
                        "Konfirmasi Persetujuan", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            String id_asset = dataGAssetL[6, e.RowIndex].Value.ToString();
                            String id_employee = dataGAssetL[4, e.RowIndex].Value.ToString();

                            View.SendEmail s = new View.SendEmail();
                            s.id_employee = id_employee;
                            s.id_asset = id_asset;
                            s.stat = 4;
                            s.indexStatus = 1;
                            s.form = 1;
                            s.ShowDialog();

                            if (asset.Class.Email.email.cek == true)
                            {
                                claim.deleteL(id_employee, id_asset);
                                MessageBox.Show("Konfirmasi laporan kehilangan berhasil dikirim", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show("Konfirmasi laporan kehilangan gagal dikirim", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                            showDataGAssetL();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetL.Columns[2].Index)
                {
                    //Detail
                    try
                    {
                        String id_asset = dataGAssetL[6, e.RowIndex].Value.ToString();
                        String id_employee = dataGAssetL[4, e.RowIndex].Value.ToString();
                        AssetDetailLB asset = new AssetDetailLB(id_employee, id_asset, "1", "2","0");
                        asset.ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetL.Columns[3].Index)
                {
                    //Download
                    try
                    {
                        String id = dataGAssetL[6, e.RowIndex].Value.ToString();
                        FileStream FS = null;
                        byte[] dbbyte;

                        //Get selected file ID field
                        SqlConnection conn = new SqlConnection(lib.param());
                        conn.Open();
                        SqlCommand sqlcmd = new SqlCommand();
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataTable dt = new DataTable();
                        sqlcmd = new SqlCommand("select * from t_lose where id_asset='" + id + "' and stat='0' and acc='2'", conn);
                        da = new SqlDataAdapter(sqlcmd);
                        dt = new DataTable();
                        da.Fill(dt);
                        conn.Close();


                        if (dt.Rows.Count > 0)
                        {
                            //Get a stored PDF bytes
                            String dname = dt.Rows[0]["dname"].ToString();
                            if (dname != "")
                            {

                                dbbyte = (byte[])dt.Rows[0]["dcontent"];
                                String[] filetype = dname.Split('.');

                                //store file Temporarily 
                                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                                string filepath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\BA\\" + dname + "." + filetype[1];

                                //Assign File path create file
                                FS = new FileStream(filepath, System.IO.FileMode.Create);

                                //Write bytes to create file
                                FS.Write(dbbyte, 0, dbbyte.Length);

                                //Close FileStream instance
                                FS.Close();

                                // Open fila after write 

                                //Create instance for process class
                                Process Proc = new Process();

                                //assign file path for process
                                Proc.StartInfo.FileName = filepath;
                                Proc.Start();
                            }
                            else
                            {
                                MessageBox.Show("Dokumen kerusakan tidak tersedia", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

       

        ////////////// klaim kerusakan ///////////////////////////////////////////////////

        public void showDataGAssetB()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter("select h.id_employee as \"ID\", g.nama as Pemilik, h.id_asset as \"ID Aset\", b.category as Kategori, c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                    "from m_asset a, m_category b, m_brand c, m_location d, m_status e, m_employee g, t_condition h " +
                    "where a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status and a.id_asset=h.id_asset and g.id_employee=h.id_employee and h.acc='2' and h.stat='3'", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "LoseAssetTable");
                conn.Close();
                this.dataGAssetB.DataSource = dataSet.Tables["LoseAssetTable"].DefaultView;
                this.dataGAssetB.Columns["ID"].Visible = false;
                this.dataGAssetB.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGAssetB_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGAssetB.Columns[0].Index)
                {
                    //Terima
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menerima klaim kerusakan aset ini ? ",
                        "Konfirmasi Persetujuan", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            DialogResult dr2 = MessageBox.Show("Apakah aset ini dapat diperbaiki ? ",
                            "Konfirmasi Perbaikan", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                            if (dr2 == DialogResult.Yes)
                            {
                                String id_asset = dataGAssetB[6, e.RowIndex].Value.ToString();
                                String id_employee = dataGAssetB[4, e.RowIndex].Value.ToString();

                                View.SendEmail s = new View.SendEmail();
                                s.id_employee = id_employee;
                                s.id_asset = id_asset;
                                s.stat = 3;
                                s.indexStatus = 0;
                                s.form = 1;
                                s.ShowDialog();

                                if (asset.Class.Email.email.cek == true)
                                {
                                    claim.updateB(id_employee, id_asset, this.id, 1, 1);
                                    MessageBox.Show("Konfirmasi laporan kerusakan berhasil dikirim", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    MessageBox.Show("Konfirmasi laporan kerusakan gagal dikirim", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                showDataGAssetB();
                            }
                            else if (dr2 == DialogResult.No)
                            {
                                String id_asset = dataGAssetB[6, e.RowIndex].Value.ToString();
                                String id_employee = dataGAssetB[4, e.RowIndex].Value.ToString();

                                View.SendEmail s = new View.SendEmail();
                                s.id_employee = id_employee;
                                s.id_asset = id_asset;
                                s.stat = 3;
                                s.indexStatus = 0;
                                s.form = 1;
                                s.ShowDialog();

                                if (asset.Class.Email.email.cek == true)
                                {
                                    claim.updateB(id_employee, id_asset, this.id, 0, 0);
                                    MessageBox.Show("Konfirmasi laporan kerusakan berhasil dikirim", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    MessageBox.Show("Konfirmasi laporan kerusakan gagal dikirim", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }

                                showDataGAssetB();
                            }
                        }
                        Main m = new Main(this.id);
                        m.showC();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetB.Columns[1].Index)
                {
                    //Tolak
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menolak klaim kerusakan aset ini ? ",
                        "Konfirmasi Persetujuan", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            String id_asset = dataGAssetB[6, e.RowIndex].Value.ToString();
                            String id_employee = dataGAssetB[4, e.RowIndex].Value.ToString();

                            View.SendEmail s = new View.SendEmail();
                            s.id_employee = id_employee;
                            s.id_asset = id_asset;
                            s.stat = 3;
                            s.indexStatus = 1;
                            s.form = 1;
                            s.ShowDialog();

                            if (asset.Class.Email.email.cek == true)
                            {
                                claim.deleteB(id_employee, id_asset);
                                MessageBox.Show("Konfirmasi laporan kerusakan berhasil dikirim", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show("Konfirmasi laporan kerusakan gagal dikirim", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            
                            showDataGAssetB();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetB.Columns[2].Index)
                {
                    //Detail
                    try
                    {
                        String id_asset = dataGAssetB[6, e.RowIndex].Value.ToString();
                        String id_employee = dataGAssetB[4, e.RowIndex].Value.ToString();
                        AssetDetailLB asset = new AssetDetailLB(id_employee, id_asset, "0", "2", "3");
                        asset.ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetB.Columns[3].Index)
                {
                    //Download
                    try
                    {
                        String id = dataGAssetB[6, e.RowIndex].Value.ToString();
                        FileStream FS = null;
                        byte[] dbbyte;

                        //Get selected file ID field
                        SqlConnection conn = new SqlConnection(lib.param());
                        conn.Open();
                        SqlCommand sqlcmd = new SqlCommand();
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataTable dt = new DataTable();
                        sqlcmd = new SqlCommand("select * from t_condition where id_asset='" + id + "' and stat='3' and acc='2'", conn);
                        da = new SqlDataAdapter(sqlcmd);
                        dt = new DataTable();
                        da.Fill(dt);
                        conn.Close();


                        if (dt.Rows.Count > 0)
                        {
                            //Get a stored PDF bytes
                            String dname = dt.Rows[0]["dname"].ToString();
                            if (dname != "")
                            {

                                dbbyte = (byte[])dt.Rows[0]["dcontent"];
                                String[] filetype = dname.Split('.');

                                //store file Temporarily 
                                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                                string filepath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\BA\\" + dname + "." + filetype[1];

                                //Assign File path create file
                                FS = new FileStream(filepath, System.IO.FileMode.Create);

                                //Write bytes to create file
                                FS.Write(dbbyte, 0, dbbyte.Length);

                                //Close FileStream instance
                                FS.Close();

                                // Open fila after write 

                                //Create instance for process class
                                Process Proc = new Process();

                                //assign file path for process
                                Proc.StartInfo.FileName = filepath;
                                Proc.Start();
                            }
                            else
                            {
                                MessageBox.Show("Dokumen kerusakan tidak tersedia", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void bSearchB_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearchL.Text;
                searchListB(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void searchListB(String str, String status = "4")
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter("select h.id_employee as \"ID\", g.nama as Pemilik, h.id_asset as \"ID Aset\", b.category as Kategori, c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                   "from m_asset a, m_category b, m_brand c, m_location d, m_status e, m_employee g, t_condition h " +
                   "where g.nama like '%" + str + "%' and a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status and a.id_asset=h.id_asset and g.id_employee=h.id_employee and h.acc='2' and h.stat='3'", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "ListLoseTable");
                conn.Close();
                this.dataGAssetL.DataSource = dataSet.Tables["ListLoseTable"].DefaultView;
                this.dataGAssetL.Columns["ID"].Visible = false;
                this.dataGAssetL.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        ////////////// perbaikan ///////////////////////////////////////////////////
        public void showDataGAssetF()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter("select h.id_employee as \"ID\", g.nama as Pemilik, h.id_asset as \"ID Aset\", b.category as Kategori, c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                    "from m_asset a, m_category b, m_brand c, m_location d, m_status e, m_employee g, t_condition h " +
                    "where a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status and a.id_asset=h.id_asset and g.id_employee=h.id_employee and h.acc='1' and h.stat='1'", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "LoseAssetTable");
                conn.Close();
                this.dataGAssetF.DataSource = dataSet.Tables["LoseAssetTable"].DefaultView;
                this.dataGAssetF.Columns["ID"].Visible = false;
                this.dataGAssetF.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void bSearchF_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearchF.Text;
                searchListF(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void searchListF(String str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter("select h.id_employee as \"ID\", g.nama as Pemilik, h.id_asset as \"ID Aset\", b.category as Kategori, c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                    "from m_asset a, m_category b, m_brand c, m_location d, m_status e, m_employee g, t_condition h " +
                    "where g.nama like '%" + str + "%' and a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status and a.id_asset=h.id_asset and g.id_employee=h.id_employee and h.acc='1' and h.stat='1'", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "LoseAssetTable");
                conn.Close();
                this.dataGAssetF.DataSource = dataSet.Tables["LoseAssetTable"].DefaultView;
                this.dataGAssetF.Columns["ID"].Visible = false;
                this.dataGAssetF.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void dataGAssetF_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGAssetF.Columns[0].Index)
                {
                    //Proses
                    try
                    {
                        String id_asset = dataGAssetF[7, e.RowIndex].Value.ToString();
                        String id_employee = dataGAssetF[5, e.RowIndex].Value.ToString();
                        RepAsset asset = new RepAsset(id_employee, id_asset, this.id);
                        asset.ShowDialog();
                        showDataGAssetF();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetF.Columns[1].Index)
                {
                    //Batal
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk membatalkan klaim kerusakan aset ini ? ",
                        "Konfirmasi Persetujuan", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            String id_asset = dataGAssetF[7, e.RowIndex].Value.ToString();
                            String id_employee = dataGAssetF[5, e.RowIndex].Value.ToString();
                            claim.deleteF(id_employee, id_asset);
                            showDataGAssetF();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetF.Columns[2].Index)
                {
                    //Rusak
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menghentikan proses perbaikan aset ini dan merubah kondisi aset menjadi rusak ? ",
                        "Konfirmasi Persetujuan", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            String id_asset = dataGAssetF[7, e.RowIndex].Value.ToString();
                            String id_employee = dataGAssetF[5, e.RowIndex].Value.ToString();
                            claim.updateF(id_employee, id_asset,this.id);
                            showDataGAssetF();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetF.Columns[3].Index)
                {
                    //Detail
                    try
                    {
                        String id_asset = dataGAssetF[7, e.RowIndex].Value.ToString();
                        String id_employee = dataGAssetF[5, e.RowIndex].Value.ToString();
                        AssetDetailLB asset = new AssetDetailLB(id_employee, id_asset, "0", "1", "1");
                        asset.ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetF.Columns[3].Index)
                {
                    //Download
                    try
                    {
                        String id = dataGAssetF[7, e.RowIndex].Value.ToString();
                        FileStream FS = null;
                        byte[] dbbyte;

                        //Get selected file ID field
                        SqlConnection conn = new SqlConnection(lib.param());
                        conn.Open();
                        SqlCommand sqlcmd = new SqlCommand();
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataTable dt = new DataTable();
                        sqlcmd = new SqlCommand("select * from t_condition where id_asset='" + id + "' and stat='1' and acc='1'", conn);
                        da = new SqlDataAdapter(sqlcmd);
                        dt = new DataTable();
                        da.Fill(dt);
                        conn.Close();


                        if (dt.Rows.Count > 0)
                        {
                            //Get a stored PDF bytes
                            String dname = dt.Rows[0]["dname"].ToString();
                            if (dname != "")
                            {

                                dbbyte = (byte[])dt.Rows[0]["dcontent"];
                                String[] filetype = dname.Split('.');

                                //store file Temporarily 
                                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                                string filepath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\BA\\" + dname + "." + filetype[1];

                                //Assign File path create file
                                FS = new FileStream(filepath, System.IO.FileMode.Create);

                                //Write bytes to create file
                                FS.Write(dbbyte, 0, dbbyte.Length);

                                //Close FileStream instance
                                FS.Close();

                                // Open fila after write 

                                //Create instance for process class
                                Process Proc = new Process();

                                //assign file path for process
                                Proc.StartInfo.FileName = filepath;
                                Proc.Start();
                            }
                            else
                            {
                                MessageBox.Show("Dokumen kerusakan tidak tersedia", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void tSearchF_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                String str = tSearchF.Text;
                searchListF(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //////////////selesai perbaikan//////////////////////////////////////////
        public void showDataGAssetFi()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter("select h.id_employee as \"ID\", g.nama as Pemilik, h.id_asset as \"ID Aset\", b.category as Kategori, c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                    "from m_asset a, m_category b, m_brand c, m_location d, m_status e, m_employee g, t_condition h " +
                    "where a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status and a.id_asset=h.id_asset and g.id_employee=h.id_employee and h.acc='1' and h.stat='2'", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "LoseAssetTable");
                conn.Close();
                this.dataGAssetFi.DataSource = dataSet.Tables["LoseAssetTable"].DefaultView;
                this.dataGAssetFi.Columns["ID"].Visible = false;
                this.dataGAssetFi.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void searchListFi(String str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlDataAdapter data = new SqlDataAdapter("select h.id_employee as \"ID\", g.nama as Pemilik, h.id_asset as \"ID Aset\", b.category as Kategori, c.brand as Brand, a.model as Model, a.serial as \"No. Serial\",a.price as Harga, a.pur_date as \"Tanggal Pembelian\", d.location as Lokasi, e.status as Status, a.description as Deskripsi " +
                    "from m_asset a, m_category b, m_brand c, m_location d, m_status e, m_employee g, t_condition h " +
                    "where g.nama like '%" + str + "%' and a.id_category=b.id_category and a.id_brand=c.id_brand and a.id_location=d.id_location and a.id_status=e.id_status and a.id_asset=h.id_asset and g.id_employee=h.id_employee and h.acc='1' and h.stat='2'", conn);
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.Fill(dataSet, "LoseAssetTable");
                conn.Close();
                this.dataGAssetFi.DataSource = dataSet.Tables["LoseAssetTable"].DefaultView;
                this.dataGAssetFi.Columns["ID"].Visible = false;
                this.dataGAssetFi.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void bSearchFi_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearchFi.Text;
                searchListFi(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void dataGAssetFi_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGAssetFi.Columns[0].Index)
                {
                    //Proses
                    try
                    {
                        String id_asset = dataGAssetFi[8, e.RowIndex].Value.ToString();
                        String id_employee = dataGAssetFi[6, e.RowIndex].Value.ToString();
                        FixAsset asset = new FixAsset(id_employee, id_asset, this.id);
                        asset.ShowDialog();
                        showDataGAssetFi();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetFi.Columns[1].Index)
                {
                    //Batal
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk membatalkan klaim kerusakan aset ini ? ",
                        "Konfirmasi Persetujuan", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            String id_asset = dataGAssetFi[8, e.RowIndex].Value.ToString();
                            String id_employee = dataGAssetFi[6, e.RowIndex].Value.ToString();
                            claim.deleteFi(id_employee, id_asset, this.id);
                            showDataGAssetFi();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetFi.Columns[2].Index)
                {
                    //Rusak
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menghentikan proses perbaikan aset ini dan merubah kondisi aset menjadi rusak ? ",
                        "Konfirmasi Persetujuan", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            String id_asset = dataGAssetFi[8, e.RowIndex].Value.ToString();
                            String id_employee = dataGAssetFi[6, e.RowIndex].Value.ToString();
                            claim.updateFi(id_employee, id_asset, this.id);
                            showDataGAssetFi();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetFi.Columns[3].Index)
                {
                    //Detail
                    try
                    {
                        String id_asset = dataGAssetFi[8, e.RowIndex].Value.ToString();
                        String id_employee = dataGAssetFi[6, e.RowIndex].Value.ToString();
                        AssetDetailLB asset = new AssetDetailLB(id_employee, id_asset, "0", "1","2");
                        asset.ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetFi.Columns[3].Index)
                {
                    //Download
                    try
                    {
                        String id = dataGAssetFi[8, e.RowIndex].Value.ToString();
                        FileStream FS = null;
                        byte[] dbbyte;

                        //Get selected file ID field
                        SqlConnection conn = new SqlConnection(lib.param());
                        conn.Open();
                        SqlCommand sqlcmd = new SqlCommand();
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataTable dt = new DataTable();
                        sqlcmd = new SqlCommand("select * from t_condition where id_asset='" + id + "' and stat='2' and acc='1'", conn);
                        da = new SqlDataAdapter(sqlcmd);
                        dt = new DataTable();
                        da.Fill(dt);
                        conn.Close();


                        if (dt.Rows.Count > 0)
                        {
                            //Get a stored PDF bytes
                            String dname = dt.Rows[0]["dname"].ToString();
                            if (dname != "")
                            {

                                dbbyte = (byte[])dt.Rows[0]["dcontent"];
                                String[] filetype = dname.Split('.');

                                //store file Temporarily 
                                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                                string filepath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\BA\\" + dname + "." + filetype[1];

                                //Assign File path create file
                                FS = new FileStream(filepath, System.IO.FileMode.Create);

                                //Write bytes to create file
                                FS.Write(dbbyte, 0, dbbyte.Length);

                                //Close FileStream instance
                                FS.Close();

                                // Open fila after write 

                                //Create instance for process class
                                Process Proc = new Process();

                                //assign file path for process
                                Proc.StartInfo.FileName = filepath;
                                Proc.Start();
                            }
                            else
                            {
                                MessageBox.Show("Dokumen kerusakan tidak tersedia", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetFi.Columns[4].Index)
                {
                    //Download Quotation
                    try
                    {
                        String id = dataGAssetFi[8, e.RowIndex].Value.ToString();
                        FileStream FS = null;
                        byte[] dbbyte;

                        //Get selected file ID field
                        SqlConnection conn = new SqlConnection(lib.param());
                        conn.Open();
                        SqlCommand sqlcmd = new SqlCommand();
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataTable dt = new DataTable();
                        sqlcmd = new SqlCommand("select * from t_condition where id_asset='" + id + "' and stat='2' and acc='1'", conn);
                        da = new SqlDataAdapter(sqlcmd);
                        dt = new DataTable();
                        da.Fill(dt);
                        conn.Close();


                        if (dt.Rows.Count > 0)
                        {
                            //Get a stored PDF bytes
                            String dname = dt.Rows[0]["qname"].ToString();
                            if (dname != "")
                            {

                                dbbyte = (byte[])dt.Rows[0]["qcontent"];
                                String[] filetype = dname.Split('.');

                                //store file Temporarily 
                                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                                string filepath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\BA\\" + dname + "." + filetype[1];

                                //Assign File path create file
                                FS = new FileStream(filepath, System.IO.FileMode.Create);

                                //Write bytes to create file
                                FS.Write(dbbyte, 0, dbbyte.Length);

                                //Close FileStream instance
                                FS.Close();

                                // Open fila after write 

                                //Create instance for process class
                                Process Proc = new Process();

                                //assign file path for process
                                Proc.StartInfo.FileName = filepath;
                                Proc.Start();
                            }
                            else
                            {
                                MessageBox.Show("Dokumen kerusakan tidak tersedia", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }
        private void ClaimManagement_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                showDataGAssetL();
                showDataGAssetB();
                showDataGAssetF();
                showDataGAssetFi();
                tSearchB.Clear();
                tSearchF.Clear();
                tSearchL.Clear();
                tSearchFi.Clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
