﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset.Administrasi
{
    public partial class SearchAsset : Form
    {
        lib lib = new lib();
        Class.Search.search s = new Class.Search.search();
        public SearchAsset()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }
        private static SearchAsset form;
        public static SearchAsset GetChildInstance()
        {
            if (form == null)
                form = new SearchAsset();
            return form;
        }

        private void SearchAsset_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

        private void SearchAsset_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void bSearch_Click(object sender, EventArgs e)
        {
            process();
        }
        void process() {
            int h1 = Convert.ToInt32(tHarga1.Value);
            int h2 = Convert.ToInt32(tHarga2.Value);
            DateTime date_start = Convert.ToDateTime(dAwal.Text.ToString());
            DateTime date_finish = Convert.ToDateTime(dAkhir.Text.ToString());
            if (s.cekTanggal(date_start, date_finish))
            {
                if (h1 <= h2)
                {
                    search(getInput());
                }
                else
                {
                    MessageBox.Show("Format harga salah!", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Format tanggal salah!", "Peringatan", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        String [] getInput() {
            String[] str = new String[14];
            str[0] = tEmail.Text;
            str[1] = tNama.Text;
            str[2] = tNoAsset.Text;
            str[3] = tItem.Text;
            str[4] = tModel.Text;
            str[5] = tNoSeri.Text;
            str[6] = tHarga1.Value.ToString();
            str[7] = tHarga2.Value.ToString();
            str[8] = dAwal.Text;
            str[9] = dAkhir.Text;
            str[10] = s.getIndex(cKategori.Text, cKategori.SelectedIndex.ToString());
            str[11] = s.getIndex(cBrand.Text, cBrand.SelectedIndex.ToString());
            str[12] = s.getIndex(cLokasi.Text, cLokasi.SelectedIndex.ToString());
            str[13] = s.getIndex(cStatus.Text, cStatus.SelectedIndex.ToString());
            return str;
        }
        void search(String []str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "search_asset";
                myCommand.Parameters.AddWithValue("@email", str[0]);
                myCommand.Parameters.AddWithValue("@nama", str[1]);
                myCommand.Parameters.AddWithValue("@noAsset", str[2]);
                myCommand.Parameters.AddWithValue("@item", str[3]);
                myCommand.Parameters.AddWithValue("@model", str[4]);
                myCommand.Parameters.AddWithValue("@noSerial", str[5]);
                myCommand.Parameters.AddWithValue("@h1", str[6]);
                myCommand.Parameters.AddWithValue("@h2", str[7]);
                myCommand.Parameters.AddWithValue("@t1", str[8]);
                myCommand.Parameters.AddWithValue("@t2", str[9]);
                myCommand.Parameters.AddWithValue("@category", str[10]);
                myCommand.Parameters.AddWithValue("@brand", str[11]);
                myCommand.Parameters.AddWithValue("@location", str[12]);
                myCommand.Parameters.AddWithValue("@status", str[13]);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                DataTable t = new DataTable();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(t);
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGSearch.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGSearch.Columns["ID"].Visible = false;
                this.dataGSearch.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void SearchAsset_Load(object sender, EventArgs e)
        {
            DataGridViewButtonColumn a = new DataGridViewButtonColumn();
            dataGSearch.Columns.Add(a);
            a.HeaderText = "Detail";
            a.Text = "Detail";
            a.UseColumnTextForButtonValue = true;
            showDataC();
            showDataB();
            showDataL();
            showDataS();
            process();
        }
        void showDataC()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sc = new SqlCommand("select id_category,category from m_category", conn);
                SqlDataReader read;
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(string));
                dt.Columns.Add("category", typeof(string));
                dt.Rows.Add(new object[] { "0", "All" });
                read = sc.ExecuteReader();
                while (read.Read())
                {
                    dt.Rows.Add(new object[] { read["id_category"].ToString(), read["category"].ToString() });
                }

                cKategori.ValueMember = "id";
                cKategori.DisplayMember = "category";
                cKategori.DataSource = dt;

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        void showDataB()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sc = new SqlCommand("select id_brand,brand from m_brand", conn);
                SqlDataReader read;
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(string));
                dt.Columns.Add("brand", typeof(string));
                dt.Rows.Add(new object[] { "0", "All" });
                read = sc.ExecuteReader();
                while (read.Read())
                {
                    dt.Rows.Add(new object[] { read["id_brand"].ToString(), read["brand"].ToString() });
                }

                cBrand.ValueMember = "id";
                cBrand.DisplayMember = "brand";
                cBrand.DataSource = dt;

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        void showDataL()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sc = new SqlCommand("select id_location,location from m_location", conn);
                SqlDataReader read;
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(string));
                dt.Columns.Add("location", typeof(string));
                dt.Rows.Add(new object[] { "0", "All" });
                read = sc.ExecuteReader();
                while (read.Read())
                {
                    dt.Rows.Add(new object[] { read["id_location"].ToString(), read["location"].ToString() });
                }

                cLokasi.ValueMember = "id";
                cLokasi.DisplayMember = "location";
                cLokasi.DataSource = dt;

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        void showDataS()
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand sc = new SqlCommand("select id_status,status from m_status", conn);
                SqlDataReader read;
                DataTable dt = new DataTable();
                dt.Columns.Add("id", typeof(string));
                dt.Columns.Add("status", typeof(string));
                dt.Rows.Add(new object[] { "0", "All" });
                read = sc.ExecuteReader();
                while (read.Read())
                {
                    dt.Rows.Add(new object[] { read["id_status"].ToString(), read["status"].ToString() });
                }

                cStatus.ValueMember = "id";
                cStatus.DisplayMember = "status";
                cStatus.DataSource = dt;

                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void tEmail_TextChanged(object sender, EventArgs e)
        {
            process();
        }

        private void tHarga1_ValueChanged(object sender, EventArgs e)
        {
            process();
        }

        private void dAwal_ValueChanged(object sender, EventArgs e)
        {
            process();
        }

        private void dataGSearch_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGSearch.Columns[0].Index)
                {
                    //Lihat
                    try
                    {
                        String id = dataGSearch[3, e.RowIndex].Value.ToString();
                        AssetDetail asset = new AssetDetail(id);
                        asset.ShowDialog();
                        asset.BringToFront();
                        this.dataGSearch.Refresh();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void cStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            process();
        }

        private void cBrand_SelectedIndexChanged(object sender, EventArgs e)
        {
            process();
        }

        private void cLokasi_SelectedIndexChanged(object sender, EventArgs e)
        {
            process();
        }

        private void cKategori_SelectedIndexChanged(object sender, EventArgs e)
        {
            process();
        }
    }
}
