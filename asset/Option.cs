﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class Option : Form
    {
        lib lib = new lib();
        public Option()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
        }

        private void Option_Load(object sender, EventArgs e)
        {
            getDB();
        }
        private void getDB()
        {

            foreach (string key in ConfigurationManager.AppSettings)
            {
                if (key == "dbConn")
                {
                    
                    string value = ConfigurationManager.AppSettings[key];
                    string[] splitvalue = value.Split(';');
                    string[] databaseserversplit = splitvalue[0].Split('=');
                    tServer.Text = databaseserversplit[1];
                    string[] databasenamesplit = splitvalue[1].Split('=');
                    tDb.Text = databasenamesplit[1];
                    string[] usernamenamesplit = splitvalue[2].Split('=');
                    tUsr.Text = usernamenamesplit[1];
                    string[] passwordnamesplit = splitvalue[3].Split('=');
                    tPasswd.Text = passwordnamesplit[1];
                }
            }

        }
        private void button1_Click(object sender, EventArgs e)
        {
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings.Remove("dbConn");
            config.AppSettings.Settings.Add("dbConn", "server=" + tServer.Text + ";db=" + tDb.Text + ";username=" + tUsr.Text + ";password=" + tPasswd.Text + ";");
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
            MessageBox.Show(" Daftar Koneksi berhasil, silahkan login kembali.", "Informasi!", MessageBoxButtons.OK,
                                                 MessageBoxIcon.Exclamation,
                                                 MessageBoxDefaultButton.Button1);
            this.Close();
        }

        private void Option_Paint(object sender, PaintEventArgs e)
        {
            System.Drawing.Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }
    }
}
