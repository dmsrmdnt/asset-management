﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Drawing.Drawing2D;
using System.Configuration;

using System.IO;
using System.Diagnostics;
namespace asset
{
    public partial class login : Form
    {
        lib lib = new lib();
        Class.login.mlogin mlogin = new Class.login.mlogin();
        public login()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }
        
        private void bLogin_Click(object sender, EventArgs e)
        {
            if (cekParam())
            {
                try
                {
                    String email = lib.getEmail(tNIK.Text);
                    String password = tPassword.Text;
                    Boolean cek = mlogin.cekInput(email, password);

                    if (cek == true)
                    {
                        if (mlogin.cekData(email, password) == "1")
                        {
                            if (checkBox1.Checked)
                            {
                                mlogin.saveUsrPasswd(email, password);
                            }
                            else
                            {
                                mlogin.unsaveUsrPasswd();
                            }
                            Main fa = new Main(mlogin.getID(email, password));
                            fa.Show();
                            this.Hide();
                        }
                        else
                        {

                            lNotify.Text = "Username/Password salah!";
                        }
                    }
                    else
                    {
                        lNotify.Text = "Salah input!";
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Gagal terhubung ke database server", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else 
            {
                MessageBox.Show("Set Koneksi database anda ke server terlebih dahulu sebelum mulai menggunakan aplikasi.");
            }
        }
        private Boolean cekParam() 
        {
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (config.AppSettings.Settings.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private void login_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void login_Paint(object sender, PaintEventArgs e)
        {
            System.Drawing.Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

        private void login_Load(object sender, EventArgs e)
        {
            UsernamePasswordCheck();
        }
        
        private void UsernamePasswordCheck()
        {

            foreach (string key in ConfigurationManager.AppSettings)
            {
                if (key == "UsrPasswd")
                {
                    checkBox1.Checked = true;
                    string value = ConfigurationManager.AppSettings[key];
                    string[] splitvalue = value.Split(';');
                    string[] databaseserversplit = splitvalue[0].Split('=');
                    tNIK.Text = databaseserversplit[1];
                    string[] databasenamesplit = splitvalue[1].Split('=');
                    tPassword.Text = databasenamesplit[1];
                }
            }
            
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            asset.Option op = new asset.Option();
            op.ShowDialog();
        }
    }
    
}
