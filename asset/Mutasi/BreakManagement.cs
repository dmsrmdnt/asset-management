﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class BreakManagement : Form
    {
        private String id;
        private String id_asset;
        lib lib = new lib();
        Class.mutasi.mSendBreak send = new Class.mutasi.mSendBreak();
        Class.Email.email mail=new Class.Email.email();
        public BreakManagement(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
        }
        private static BreakManagement form;
        public static BreakManagement GetChildInstance(String id)
        {
            if (form == null)
                form = new BreakManagement(id);
            return form;
        }

        private void BreakManagement_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void BreakManagement_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewButtonColumn a = new DataGridViewButtonColumn();
                dataGAssetL.Columns.Add(a);
                a.HeaderText = "Detail";
                a.Text = "Detail";
                a.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn b = new DataGridViewButtonColumn();
                dataGAssetLP.Columns.Add(b);
                b.HeaderText = "Batal";
                b.Text = "Batal";
                b.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn c = new DataGridViewButtonColumn();
                dataGAssetLP.Columns.Add(c);
                c.HeaderText = "Detail";
                c.Text = "Detail";
                c.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn d = new DataGridViewButtonColumn();
                dataGAssetB.Columns.Add(d);
                d.HeaderText = "Detail";
                d.Text = "Detail";
                d.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn f = new DataGridViewButtonColumn();
                dataGAssetBP.Columns.Add(f);
                f.HeaderText = "Batal";
                f.Text = "Batal";
                f.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn g = new DataGridViewButtonColumn();
                dataGAssetBP.Columns.Add(g);
                g.HeaderText = "Detail";
                g.Text = "Detail";
                g.UseColumnTextForButtonValue = true;

                showDataGL(this.id);
                showDataGB(this.id);
                showDataGLP(this.id);
                showDataGBP(this.id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                showDataGL(this.id);
                showDataGB(this.id);
                showDataGLP(this.id);
                showDataGBP(this.id);
                tSearchB.Text = "";
                tSearchBP.Text = "";
                tSearchL.Text = "";
                tSearchLP.Text = "";
                clearField();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        ///////////////////////// Kehilangan /////////////////////////////////////
        public void showDataGL(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_assetlost";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "LoseAssetTable");
                conn.Close();
                this.dataGAssetL.DataSource = dataSet.Tables["LoseAssetTable"].DefaultView;
                this.dataGAssetL.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void showDataGLP(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_assetlostP";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "LoseAssetTable");
                conn.Close();
                this.dataGAssetLP.DataSource = dataSet.Tables["LoseAssetTable"].DefaultView;
                this.dataGAssetL.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bSearchL_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearchL.Text;
                searchListL(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void searchListL(String str, String status = "4")
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "search_data_assetListL";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                myCommand.Parameters.AddWithValue("@str", str);
                myCommand.Parameters.AddWithValue("@status", status);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "ListLoseTable");
                conn.Close();
                this.dataGAssetL.DataSource = dataSet.Tables["ListLoseTable"].DefaultView;
                this.dataGAssetL.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bSearchLP_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearchLP.Text;
                searchListLP(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void searchListLP(String str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "search_data_assetListLP";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                myCommand.Parameters.AddWithValue("@str", str);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "ListLoseTable");
                conn.Close();
                this.dataGAssetLP.DataSource = dataSet.Tables["ListLoseTable"].DefaultView;
                this.dataGAssetLP.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGAssetL_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGAssetL.Columns[0].Index)
                {
                    //Detail
                    try
                    {
                        String id = dataGAssetL[2, e.RowIndex].Value.ToString();
                        AssetDetailLB asset = new AssetDetailLB(this.id, id, "1", "0", "1");
                        asset.ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void dataGAssetLP_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGAssetL.Columns[0].Index)
                {
                    //Batal
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk membatalkan proses laporan kehilangan aset ini ? ",
                        "Konfirmasi Pembatalan", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            String id = dataGAssetLP[3, e.RowIndex].Value.ToString();
                            send.deleteL(this.id, id);
                            showDataGLP(this.id);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetL.Columns[1].Index)
                {
                    //Detail
                    try
                    {
                        String id = dataGAssetLP[3, e.RowIndex].Value.ToString();
                        AssetDetailLB asset = new AssetDetailLB(this.id, id, "1", "2", "1");
                        asset.ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        ///////////////////////// Kerusakan /////////////////////////////////////

        public void showDataGB(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_assetbreak";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "LoseAssetTable");
                conn.Close();
                this.dataGAssetB.DataSource = dataSet.Tables["LoseAssetTable"].DefaultView;
                this.dataGAssetB.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void showDataGBP(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_assetbreakP";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "LoseAssetTable");
                conn.Close();
                this.dataGAssetBP.DataSource = dataSet.Tables["LoseAssetTable"].DefaultView;
                this.dataGAssetBP.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bSearchB_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearchB.Text;
                searchListB(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void searchListB(String str, String status = "4")
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "search_data_assetListB";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                myCommand.Parameters.AddWithValue("@str", str);
                myCommand.Parameters.AddWithValue("@status", status);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "ListLoseTable");
                conn.Close();
                this.dataGAssetB.DataSource = dataSet.Tables["ListLoseTable"].DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bSearchBP_Click(object sender, EventArgs e)
        {
            try
            {
                String str = tSearchBP.Text;
                searchListBP(str);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void searchListBP(String str)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "search_data_assetListBP";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                myCommand.Parameters.AddWithValue("@str", str);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "ListLoseTable");
                conn.Close();
                this.dataGAssetBP.DataSource = dataSet.Tables["ListLoseTable"].DefaultView;
                this.dataGAssetBP.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGAssetB_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGAssetB.Columns[0].Index)
                {
                    //Detail
                    try
                    {
                        String id = dataGAssetB[2, e.RowIndex].Value.ToString();
                        AssetDetailLB asset = new AssetDetailLB(this.id, id, "0", "0", "0");
                        asset.ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void dataGAssetBP_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGAssetBP.Columns[0].Index)
                {
                    //Batal
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk membatalkan proses laporan kerusakan aset ini ? ",
                        "Konfirmasi Pembatalan", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            String id = dataGAssetBP[3, e.RowIndex].Value.ToString();
                            send.deleteB(this.id, id);
                            showDataGBP(this.id);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGAssetBP.Columns[1].Index)
                {
                    //Detail
                    try
                    {
                        String id = dataGAssetBP[3, e.RowIndex].Value.ToString();
                        AssetDetailLB asset = new AssetDetailLB(this.id, id, "0", "2", "3");
                        asset.ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        ///////////////////////// Klaim Kehilangan Kerusakan /////////////////////////////////////

        public void showDataAsset(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_asset_detail";
                myCommand.Parameters.AddWithValue("@id_asset", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    tCategory.Text = read["category"].ToString();
                    tBrand.Text = read["brand"].ToString();
                    tLocation.Text = read["location"].ToString();
                    tModel.Text = read["model"].ToString();
                    tSerial.Text = read["serial"].ToString();
                    tPrice.Text = read["price"].ToString();
                    tDesc.Text = read["description"].ToString();
                    purDate.Text = read["pur_date"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void baAsset_Click(object sender, EventArgs e)
        {
            try
            {
                AssetM cat = new AssetM(this.id);
                cat.ShowDialog();
                this.id_asset = cat.idA;
                showDataAsset(this.id_asset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bSend_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekInput = send.cekInput(this.id, this.id_asset, tFile.Text.ToString());
                int cekCondition = send.cekCondition(this.id_asset);
                int cekAssetM = send.cekAssetM(this.id_asset);
                int cekAssetL = send.cekAssetL(this.id_asset);
                int cekAssetB = send.cekAssetB(this.id_asset);
                int cekTCondition = send.cekTCondition(this.id_asset);
                if (cekInput == true && (rHilang.Checked == true || rRusak.Checked == true))
                {
                    //Upload file
                    string filetype;
                    string filename;

                    filetype = tFile.Text.Substring(Convert.ToInt32(tFile.Text.LastIndexOf(".")) + 1, tFile.Text.Length - (Convert.ToInt32(tFile.Text.LastIndexOf(".")) + 1));
                    filename = "BA" + DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ') + this.id + this.id_asset + "." + filetype;
                   
                    byte[] FileBytes = null;

                    try
                    {
                        // Open file to read using file path
                        FileStream FS = new FileStream(tFile.Text, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                        // Add filestream to binary reader
                        BinaryReader BR = new BinaryReader(FS);

                        // get total byte length of the file
                        long allbytes = new FileInfo(tFile.Text).Length;

                        // read entire file into buffer
                        FileBytes = BR.ReadBytes((Int32)allbytes);

                        // close all instances
                        FS.Close();
                        FS.Dispose();
                        BR.Close();

                        //Store File Bytes in database image filed

                        string appPath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\BA\\";
                        System.IO.FileInfo fi = new System.IO.FileInfo(tFile.Text);
                        fi.Refresh();
                        fi.CopyTo(appPath + filename);
                        List<String> file = new List<String>();
                        file.Add((appPath + filename));
                        if (cekAssetL == 0)
                        {
                            if (cekAssetB == 0)
                            {
                                if (cekAssetM == 0)
                                {
                                    if (rHilang.Checked == true)
                                    {
                                        //insert lose
                                        String note = tAlasan.Text;

                                        View.SendEmail s = new View.SendEmail();
                                        s.id_employee = this.id;
                                        s.id_asset = this.id_asset;
                                        s.file = file.ToArray();
                                        s.desc = note;
                                        s.stat = 0;
                                        s.form = 0;
                                        s.ShowDialog();

                                        if (asset.Class.Email.email.cek == true)
                                        {
                                            send.insertL(this.id, this.id_asset, note, filename, FileBytes);
                                            MessageBox.Show("Laporan kehilangan berhasil dikirim", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            clearField();
                                        }
                                        else
                                        {
                                            MessageBox.Show("Laporan kehilangan gagal dikirim", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                    }
                                    else if (rRusak.Checked == true)
                                    {
                                        //insert break
                                        String note = tAlasan.Text;

                                        View.SendEmail s = new View.SendEmail();
                                        s.id_employee = this.id;
                                        s.id_asset = this.id_asset;
                                        s.desc = note;
                                        s.file = file.ToArray();
                                        s.stat = 1;
                                        s.form = 0;
                                        s.ShowDialog();

                                        if (asset.Class.Email.email.cek == true)
                                        {
                                            send.insertB(this.id, this.id_asset, note, filename, FileBytes);
                                            MessageBox.Show("Laporan kerusakan berhasil dikirim", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                            clearField();
                                        }
                                        else
                                        {
                                            MessageBox.Show("Laporan kerusakan gagal dikirim", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        }
                                    }
                                }
                                else
                                {
                                    lNotify.Text = "Aset ini dalam proses mutasi!";
                                }
                            }
                            else
                            {
                                lNotify.Text = "Aset ini dalam proses perbaikan/tidak dalam kondisi baik!";
                            }
                        }
                        else
                        {
                            lNotify.Text = "Aset ini dalam proses kehilangan!";
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error during File Read " + ex.ToString());
                    }
                }   
                else
                {
                    lNotify.Text = "Salah input!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            clearField();
        }
        public void clearField() {
            try
            {
                this.id_asset = "";
                tCategory.Text = "";
                tBrand.Text = "";
                tLocation.Text = "";
                tModel.Text = "";
                tSerial.Text = "";
                tPrice.Text = "";
                tDesc.Text = "";
                purDate.Text = "";
                rRusak.Checked = true;
                rHilang.Checked = false;
                tAlasan.Text = "";
                lNotify.Text = "";
                tFile.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BreakManagement_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            } 
        }

        private void bCari_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fDialog = new OpenFileDialog();
                fDialog.Title = "Pilih file untuk diunggah";
                fDialog.Filter = "All Files|*.pdf;*.doc;*.docx;*.jpg;*.jpeg;*.png|PDF Files|*.pdf|Word Document|*.doc;*.docx|Image|*.jpg;*.jpeg;*.png";
                if (fDialog.ShowDialog() == DialogResult.OK)
                {
                    tFile.Text = fDialog.FileName.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
