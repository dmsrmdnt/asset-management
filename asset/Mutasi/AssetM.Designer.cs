﻿namespace asset
{
    partial class AssetM
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tSearch = new System.Windows.Forms.TextBox();
            this.bSearch = new System.Windows.Forms.Button();
            this.dataGAsset = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGAsset)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tSearch
            // 
            this.tSearch.Location = new System.Drawing.Point(11, 21);
            this.tSearch.Name = "tSearch";
            this.tSearch.Size = new System.Drawing.Size(293, 20);
            this.tSearch.TabIndex = 0;
            this.tSearch.TextChanged += new System.EventHandler(this.bSearch_Click);
            // 
            // bSearch
            // 
            this.bSearch.ForeColor = System.Drawing.Color.Black;
            this.bSearch.Location = new System.Drawing.Point(310, 19);
            this.bSearch.Name = "bSearch";
            this.bSearch.Size = new System.Drawing.Size(75, 23);
            this.bSearch.TabIndex = 1;
            this.bSearch.Text = "Cari";
            this.bSearch.UseVisualStyleBackColor = true;
            this.bSearch.Click += new System.EventHandler(this.bSearch_Click);
            // 
            // dataGAsset
            // 
            this.dataGAsset.AllowUserToAddRows = false;
            this.dataGAsset.AllowUserToDeleteRows = false;
            this.dataGAsset.AllowUserToResizeColumns = false;
            this.dataGAsset.AllowUserToResizeRows = false;
            this.dataGAsset.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGAsset.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGAsset.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGAsset.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGAsset.Location = new System.Drawing.Point(12, 73);
            this.dataGAsset.MultiSelect = false;
            this.dataGAsset.Name = "dataGAsset";
            this.dataGAsset.ReadOnly = true;
            this.dataGAsset.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGAsset.Size = new System.Drawing.Size(403, 152);
            this.dataGAsset.TabIndex = 3;
            this.dataGAsset.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGAsset_CellContentClick);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.bSearch);
            this.groupBox1.Controls.Add(this.tSearch);
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(403, 55);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Model";
            // 
            // AssetM
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 237);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dataGAsset);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AssetM";
            this.Text = "Daftar Aset";
            this.Load += new System.EventHandler(this.AssetOwner_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AssetM_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.dataGAsset)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tSearch;
        private System.Windows.Forms.Button bSearch;
        private System.Windows.Forms.DataGridView dataGAsset;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}