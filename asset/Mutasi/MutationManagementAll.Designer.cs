﻿namespace asset
{
    partial class MutationManagementAll
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tDMAsset = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGIn = new System.Windows.Forms.DataGridView();
            this.tPMAsset = new System.Windows.Forms.TabPage();
            this.tPMutasi = new System.Windows.Forms.TabPage();
            this.tabControl1.SuspendLayout();
            this.tDMAsset.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGIn)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tDMAsset);
            this.tabControl1.Controls.Add(this.tPMAsset);
            this.tabControl1.Controls.Add(this.tPMutasi);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(984, 551);
            this.tabControl1.TabIndex = 0;
            // 
            // tDMAsset
            // 
            this.tDMAsset.Controls.Add(this.groupBox1);
            this.tDMAsset.Controls.Add(this.dataGIn);
            this.tDMAsset.Location = new System.Drawing.Point(4, 22);
            this.tDMAsset.Name = "tDMAsset";
            this.tDMAsset.Padding = new System.Windows.Forms.Padding(3);
            this.tDMAsset.Size = new System.Drawing.Size(976, 525);
            this.tDMAsset.TabIndex = 0;
            this.tDMAsset.Text = "Daftar Mutasi Aset";
            this.tDMAsset.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(964, 64);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pemilik";
            // 
            // dataGIn
            // 
            this.dataGIn.AllowUserToAddRows = false;
            this.dataGIn.AllowUserToDeleteRows = false;
            this.dataGIn.AllowUserToResizeColumns = false;
            this.dataGIn.AllowUserToResizeRows = false;
            this.dataGIn.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGIn.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGIn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGIn.Location = new System.Drawing.Point(6, 76);
            this.dataGIn.MultiSelect = false;
            this.dataGIn.Name = "dataGIn";
            this.dataGIn.ReadOnly = true;
            this.dataGIn.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGIn.Size = new System.Drawing.Size(964, 443);
            this.dataGIn.TabIndex = 8;
            this.dataGIn.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGIn_CellContentClick);
            // 
            // tPMAsset
            // 
            this.tPMAsset.Location = new System.Drawing.Point(4, 22);
            this.tPMAsset.Name = "tPMAsset";
            this.tPMAsset.Padding = new System.Windows.Forms.Padding(3);
            this.tPMAsset.Size = new System.Drawing.Size(976, 525);
            this.tPMAsset.TabIndex = 1;
            this.tPMAsset.Text = "Daftar Mutasi Aset(Pending)";
            this.tPMAsset.UseVisualStyleBackColor = true;
            // 
            // tPMutasi
            // 
            this.tPMutasi.Location = new System.Drawing.Point(4, 22);
            this.tPMutasi.Name = "tPMutasi";
            this.tPMutasi.Size = new System.Drawing.Size(976, 525);
            this.tPMutasi.TabIndex = 2;
            this.tPMutasi.Text = "Permintaan Mutasi";
            this.tPMutasi.UseVisualStyleBackColor = true;
            // 
            // MutationManagementAll
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 575);
            this.Controls.Add(this.tabControl1);
            this.Name = "MutationManagementAll";
            this.Text = "Mutasi Aset";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MutationManagementAll_FormClosed);
            this.Load += new System.EventHandler(this.MutationManagementAll_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MutationManagementAll_Paint);
            this.tabControl1.ResumeLayout(false);
            this.tDMAsset.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGIn)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tDMAsset;
        private System.Windows.Forms.TabPage tPMAsset;
        private System.Windows.Forms.TabPage tPMutasi;
        private System.Windows.Forms.DataGridView dataGIn;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}