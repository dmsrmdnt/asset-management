﻿namespace asset
{
    partial class BreakManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Kehilangan = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dataGAssetLP = new System.Windows.Forms.DataGridView();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tSearchLP = new System.Windows.Forms.TextBox();
            this.bSearchLP = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGAssetL = new System.Windows.Forms.DataGridView();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tSearchL = new System.Windows.Forms.TextBox();
            this.bSearchL = new System.Windows.Forms.Button();
            this.Kerusakan = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dataGAssetBP = new System.Windows.Forms.DataGridView();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.tSearchBP = new System.Windows.Forms.TextBox();
            this.bSearchBP = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.dataGAssetB = new System.Windows.Forms.DataGridView();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.tSearchB = new System.Windows.Forms.TextBox();
            this.bSearchB = new System.Windows.Forms.Button();
            this.Permintaan = new System.Windows.Forms.TabPage();
            this.bSend = new System.Windows.Forms.Button();
            this.gAsset = new System.Windows.Forms.GroupBox();
            this.baAsset = new System.Windows.Forms.Button();
            this.purDate = new System.Windows.Forms.DateTimePicker();
            this.lDate = new System.Windows.Forms.Label();
            this.tDesc = new System.Windows.Forms.TextBox();
            this.tLocation = new System.Windows.Forms.TextBox();
            this.tPrice = new System.Windows.Forms.TextBox();
            this.tBrand = new System.Windows.Forms.TextBox();
            this.tCategory = new System.Windows.Forms.TextBox();
            this.tSerial = new System.Windows.Forms.TextBox();
            this.tModel = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lLocation = new System.Windows.Forms.Label();
            this.lPrice = new System.Windows.Forms.Label();
            this.lBrand = new System.Windows.Forms.Label();
            this.lCategory = new System.Windows.Forms.Label();
            this.lSerial = new System.Windows.Forms.Label();
            this.lModel = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tAlasan = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.rHilang = new System.Windows.Forms.RadioButton();
            this.rRusak = new System.Windows.Forms.RadioButton();
            this.lNotify = new System.Windows.Forms.Label();
            this.bCancel = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.bCari = new System.Windows.Forms.Button();
            this.tFile = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.Kehilangan.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGAssetLP)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGAssetL)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.Kerusakan.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGAssetBP)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGAssetB)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.Permintaan.SuspendLayout();
            this.gAsset.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Kehilangan);
            this.tabControl1.Controls.Add(this.Kerusakan);
            this.tabControl1.Controls.Add(this.Permintaan);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(552, 709);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // Kehilangan
            // 
            this.Kehilangan.Controls.Add(this.groupBox4);
            this.Kehilangan.Controls.Add(this.groupBox2);
            this.Kehilangan.Location = new System.Drawing.Point(4, 22);
            this.Kehilangan.Name = "Kehilangan";
            this.Kehilangan.Padding = new System.Windows.Forms.Padding(3);
            this.Kehilangan.Size = new System.Drawing.Size(544, 683);
            this.Kehilangan.TabIndex = 1;
            this.Kehilangan.Text = "Kehilangan";
            this.Kehilangan.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.dataGAssetLP);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Location = new System.Drawing.Point(6, 326);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(532, 351);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Klaim Kehilangan Aset Tertunda";
            // 
            // dataGAssetLP
            // 
            this.dataGAssetLP.AllowUserToAddRows = false;
            this.dataGAssetLP.AllowUserToDeleteRows = false;
            this.dataGAssetLP.AllowUserToResizeColumns = false;
            this.dataGAssetLP.AllowUserToResizeRows = false;
            this.dataGAssetLP.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGAssetLP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGAssetLP.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGAssetLP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGAssetLP.Location = new System.Drawing.Point(6, 72);
            this.dataGAssetLP.MultiSelect = false;
            this.dataGAssetLP.Name = "dataGAssetLP";
            this.dataGAssetLP.ReadOnly = true;
            this.dataGAssetLP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGAssetLP.Size = new System.Drawing.Size(520, 273);
            this.dataGAssetLP.TabIndex = 4;
            this.dataGAssetLP.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGAssetLP_CellContentClick);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tSearchLP);
            this.groupBox5.Controls.Add(this.bSearchLP);
            this.groupBox5.Location = new System.Drawing.Point(6, 19);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(520, 47);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Model";
            // 
            // tSearchLP
            // 
            this.tSearchLP.Location = new System.Drawing.Point(6, 19);
            this.tSearchLP.Name = "tSearchLP";
            this.tSearchLP.Size = new System.Drawing.Size(427, 20);
            this.tSearchLP.TabIndex = 1;
            this.tSearchLP.TextChanged += new System.EventHandler(this.bSearchLP_Click);
            // 
            // bSearchLP
            // 
            this.bSearchLP.Location = new System.Drawing.Point(439, 17);
            this.bSearchLP.Name = "bSearchLP";
            this.bSearchLP.Size = new System.Drawing.Size(75, 23);
            this.bSearchLP.TabIndex = 0;
            this.bSearchLP.Text = "Cari";
            this.bSearchLP.UseVisualStyleBackColor = true;
            this.bSearchLP.Click += new System.EventHandler(this.bSearchLP_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGAssetL);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(532, 314);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Daftar Aset Hilang";
            // 
            // dataGAssetL
            // 
            this.dataGAssetL.AllowUserToAddRows = false;
            this.dataGAssetL.AllowUserToDeleteRows = false;
            this.dataGAssetL.AllowUserToResizeColumns = false;
            this.dataGAssetL.AllowUserToResizeRows = false;
            this.dataGAssetL.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGAssetL.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGAssetL.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGAssetL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGAssetL.Location = new System.Drawing.Point(6, 72);
            this.dataGAssetL.MultiSelect = false;
            this.dataGAssetL.Name = "dataGAssetL";
            this.dataGAssetL.ReadOnly = true;
            this.dataGAssetL.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGAssetL.Size = new System.Drawing.Size(520, 231);
            this.dataGAssetL.TabIndex = 4;
            this.dataGAssetL.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGAssetL_CellContentClick);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tSearchL);
            this.groupBox3.Controls.Add(this.bSearchL);
            this.groupBox3.Location = new System.Drawing.Point(6, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(520, 47);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Model";
            // 
            // tSearchL
            // 
            this.tSearchL.Location = new System.Drawing.Point(6, 19);
            this.tSearchL.Name = "tSearchL";
            this.tSearchL.Size = new System.Drawing.Size(427, 20);
            this.tSearchL.TabIndex = 1;
            this.tSearchL.TextChanged += new System.EventHandler(this.bSearchL_Click);
            // 
            // bSearchL
            // 
            this.bSearchL.Location = new System.Drawing.Point(439, 17);
            this.bSearchL.Name = "bSearchL";
            this.bSearchL.Size = new System.Drawing.Size(75, 23);
            this.bSearchL.TabIndex = 0;
            this.bSearchL.Text = "Cari";
            this.bSearchL.UseVisualStyleBackColor = true;
            this.bSearchL.Click += new System.EventHandler(this.bSearchL_Click);
            // 
            // Kerusakan
            // 
            this.Kerusakan.Controls.Add(this.groupBox6);
            this.Kerusakan.Controls.Add(this.groupBox8);
            this.Kerusakan.Location = new System.Drawing.Point(4, 22);
            this.Kerusakan.Name = "Kerusakan";
            this.Kerusakan.Size = new System.Drawing.Size(544, 683);
            this.Kerusakan.TabIndex = 2;
            this.Kerusakan.Text = "Kerusakan";
            this.Kerusakan.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.dataGAssetBP);
            this.groupBox6.Controls.Add(this.groupBox7);
            this.groupBox6.Location = new System.Drawing.Point(6, 326);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(532, 351);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Klaim Kerusakan Aset Tertunda";
            // 
            // dataGAssetBP
            // 
            this.dataGAssetBP.AllowUserToAddRows = false;
            this.dataGAssetBP.AllowUserToDeleteRows = false;
            this.dataGAssetBP.AllowUserToResizeColumns = false;
            this.dataGAssetBP.AllowUserToResizeRows = false;
            this.dataGAssetBP.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGAssetBP.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGAssetBP.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGAssetBP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGAssetBP.Location = new System.Drawing.Point(6, 72);
            this.dataGAssetBP.MultiSelect = false;
            this.dataGAssetBP.Name = "dataGAssetBP";
            this.dataGAssetBP.ReadOnly = true;
            this.dataGAssetBP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGAssetBP.Size = new System.Drawing.Size(520, 273);
            this.dataGAssetBP.TabIndex = 4;
            this.dataGAssetBP.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGAssetBP_CellContentClick);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.tSearchBP);
            this.groupBox7.Controls.Add(this.bSearchBP);
            this.groupBox7.Location = new System.Drawing.Point(6, 19);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(520, 47);
            this.groupBox7.TabIndex = 0;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Model";
            // 
            // tSearchBP
            // 
            this.tSearchBP.Location = new System.Drawing.Point(6, 19);
            this.tSearchBP.Name = "tSearchBP";
            this.tSearchBP.Size = new System.Drawing.Size(427, 20);
            this.tSearchBP.TabIndex = 1;
            this.tSearchBP.TextChanged += new System.EventHandler(this.bSearchBP_Click);
            // 
            // bSearchBP
            // 
            this.bSearchBP.Location = new System.Drawing.Point(439, 17);
            this.bSearchBP.Name = "bSearchBP";
            this.bSearchBP.Size = new System.Drawing.Size(75, 23);
            this.bSearchBP.TabIndex = 0;
            this.bSearchBP.Text = "Cari";
            this.bSearchBP.UseVisualStyleBackColor = true;
            this.bSearchBP.Click += new System.EventHandler(this.bSearchBP_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.dataGAssetB);
            this.groupBox8.Controls.Add(this.groupBox9);
            this.groupBox8.Location = new System.Drawing.Point(6, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(532, 314);
            this.groupBox8.TabIndex = 2;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Daftar Aset Rusak/Diperbaiki";
            // 
            // dataGAssetB
            // 
            this.dataGAssetB.AllowUserToAddRows = false;
            this.dataGAssetB.AllowUserToDeleteRows = false;
            this.dataGAssetB.AllowUserToResizeColumns = false;
            this.dataGAssetB.AllowUserToResizeRows = false;
            this.dataGAssetB.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.dataGAssetB.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGAssetB.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGAssetB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGAssetB.Location = new System.Drawing.Point(6, 72);
            this.dataGAssetB.MultiSelect = false;
            this.dataGAssetB.Name = "dataGAssetB";
            this.dataGAssetB.ReadOnly = true;
            this.dataGAssetB.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGAssetB.Size = new System.Drawing.Size(520, 231);
            this.dataGAssetB.TabIndex = 4;
            this.dataGAssetB.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGAssetB_CellContentClick);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.tSearchB);
            this.groupBox9.Controls.Add(this.bSearchB);
            this.groupBox9.Location = new System.Drawing.Point(6, 19);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(520, 47);
            this.groupBox9.TabIndex = 0;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Model";
            // 
            // tSearchB
            // 
            this.tSearchB.Location = new System.Drawing.Point(6, 19);
            this.tSearchB.Name = "tSearchB";
            this.tSearchB.Size = new System.Drawing.Size(427, 20);
            this.tSearchB.TabIndex = 1;
            this.tSearchB.TextChanged += new System.EventHandler(this.bSearchB_Click);
            // 
            // bSearchB
            // 
            this.bSearchB.Location = new System.Drawing.Point(439, 17);
            this.bSearchB.Name = "bSearchB";
            this.bSearchB.Size = new System.Drawing.Size(75, 23);
            this.bSearchB.TabIndex = 0;
            this.bSearchB.Text = "Cari";
            this.bSearchB.UseVisualStyleBackColor = true;
            this.bSearchB.Click += new System.EventHandler(this.bSearchB_Click);
            // 
            // Permintaan
            // 
            this.Permintaan.Controls.Add(this.groupBox10);
            this.Permintaan.Controls.Add(this.bSend);
            this.Permintaan.Controls.Add(this.gAsset);
            this.Permintaan.Controls.Add(this.groupBox1);
            this.Permintaan.Controls.Add(this.lNotify);
            this.Permintaan.Controls.Add(this.bCancel);
            this.Permintaan.Location = new System.Drawing.Point(4, 22);
            this.Permintaan.Name = "Permintaan";
            this.Permintaan.Padding = new System.Windows.Forms.Padding(3);
            this.Permintaan.Size = new System.Drawing.Size(544, 683);
            this.Permintaan.TabIndex = 0;
            this.Permintaan.Text = "Kirim Klaim Perbaikan/Kehilangan Aset";
            this.Permintaan.UseVisualStyleBackColor = true;
            // 
            // bSend
            // 
            this.bSend.Location = new System.Drawing.Point(284, 505);
            this.bSend.Name = "bSend";
            this.bSend.Size = new System.Drawing.Size(120, 40);
            this.bSend.TabIndex = 36;
            this.bSend.Text = "Kirim";
            this.bSend.UseVisualStyleBackColor = true;
            this.bSend.Click += new System.EventHandler(this.bSend_Click);
            // 
            // gAsset
            // 
            this.gAsset.Controls.Add(this.baAsset);
            this.gAsset.Controls.Add(this.purDate);
            this.gAsset.Controls.Add(this.lDate);
            this.gAsset.Controls.Add(this.tDesc);
            this.gAsset.Controls.Add(this.tLocation);
            this.gAsset.Controls.Add(this.tPrice);
            this.gAsset.Controls.Add(this.tBrand);
            this.gAsset.Controls.Add(this.tCategory);
            this.gAsset.Controls.Add(this.tSerial);
            this.gAsset.Controls.Add(this.tModel);
            this.gAsset.Controls.Add(this.label1);
            this.gAsset.Controls.Add(this.lLocation);
            this.gAsset.Controls.Add(this.lPrice);
            this.gAsset.Controls.Add(this.lBrand);
            this.gAsset.Controls.Add(this.lCategory);
            this.gAsset.Controls.Add(this.lSerial);
            this.gAsset.Controls.Add(this.lModel);
            this.gAsset.Location = new System.Drawing.Point(6, 6);
            this.gAsset.Name = "gAsset";
            this.gAsset.Size = new System.Drawing.Size(529, 288);
            this.gAsset.TabIndex = 34;
            this.gAsset.TabStop = false;
            this.gAsset.Text = "Informasi Aset";
            // 
            // baAsset
            // 
            this.baAsset.Location = new System.Drawing.Point(332, 22);
            this.baAsset.Name = "baAsset";
            this.baAsset.Size = new System.Drawing.Size(66, 23);
            this.baAsset.TabIndex = 27;
            this.baAsset.Text = "Cari";
            this.baAsset.UseVisualStyleBackColor = true;
            this.baAsset.Click += new System.EventHandler(this.baAsset_Click);
            // 
            // purDate
            // 
            this.purDate.Enabled = false;
            this.purDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.purDate.Location = new System.Drawing.Point(106, 128);
            this.purDate.Name = "purDate";
            this.purDate.Size = new System.Drawing.Size(148, 20);
            this.purDate.TabIndex = 46;
            // 
            // lDate
            // 
            this.lDate.AutoSize = true;
            this.lDate.Location = new System.Drawing.Point(8, 134);
            this.lDate.Name = "lDate";
            this.lDate.Size = new System.Drawing.Size(98, 13);
            this.lDate.TabIndex = 45;
            this.lDate.Text = "Tanggal Pembelian";
            // 
            // tDesc
            // 
            this.tDesc.Location = new System.Drawing.Point(106, 206);
            this.tDesc.Multiline = true;
            this.tDesc.Name = "tDesc";
            this.tDesc.ReadOnly = true;
            this.tDesc.Size = new System.Drawing.Size(400, 70);
            this.tDesc.TabIndex = 41;
            // 
            // tLocation
            // 
            this.tLocation.Location = new System.Drawing.Point(106, 180);
            this.tLocation.Name = "tLocation";
            this.tLocation.ReadOnly = true;
            this.tLocation.Size = new System.Drawing.Size(148, 20);
            this.tLocation.TabIndex = 40;
            // 
            // tPrice
            // 
            this.tPrice.Location = new System.Drawing.Point(106, 154);
            this.tPrice.Name = "tPrice";
            this.tPrice.ReadOnly = true;
            this.tPrice.Size = new System.Drawing.Size(148, 20);
            this.tPrice.TabIndex = 39;
            // 
            // tBrand
            // 
            this.tBrand.Location = new System.Drawing.Point(106, 102);
            this.tBrand.Name = "tBrand";
            this.tBrand.ReadOnly = true;
            this.tBrand.Size = new System.Drawing.Size(148, 20);
            this.tBrand.TabIndex = 38;
            // 
            // tCategory
            // 
            this.tCategory.Location = new System.Drawing.Point(106, 76);
            this.tCategory.Name = "tCategory";
            this.tCategory.ReadOnly = true;
            this.tCategory.Size = new System.Drawing.Size(148, 20);
            this.tCategory.TabIndex = 37;
            // 
            // tSerial
            // 
            this.tSerial.Location = new System.Drawing.Point(106, 50);
            this.tSerial.Name = "tSerial";
            this.tSerial.ReadOnly = true;
            this.tSerial.Size = new System.Drawing.Size(220, 20);
            this.tSerial.TabIndex = 36;
            // 
            // tModel
            // 
            this.tModel.Location = new System.Drawing.Point(106, 24);
            this.tModel.Name = "tModel";
            this.tModel.ReadOnly = true;
            this.tModel.Size = new System.Drawing.Size(220, 20);
            this.tModel.TabIndex = 35;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 206);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 34;
            this.label1.Text = "Deskripsi";
            // 
            // lLocation
            // 
            this.lLocation.AutoSize = true;
            this.lLocation.Location = new System.Drawing.Point(8, 183);
            this.lLocation.Name = "lLocation";
            this.lLocation.Size = new System.Drawing.Size(38, 13);
            this.lLocation.TabIndex = 33;
            this.lLocation.Text = "Lokasi";
            // 
            // lPrice
            // 
            this.lPrice.AutoSize = true;
            this.lPrice.Location = new System.Drawing.Point(8, 157);
            this.lPrice.Name = "lPrice";
            this.lPrice.Size = new System.Drawing.Size(36, 13);
            this.lPrice.TabIndex = 32;
            this.lPrice.Text = "Harga";
            // 
            // lBrand
            // 
            this.lBrand.AutoSize = true;
            this.lBrand.Location = new System.Drawing.Point(8, 105);
            this.lBrand.Name = "lBrand";
            this.lBrand.Size = new System.Drawing.Size(35, 13);
            this.lBrand.TabIndex = 31;
            this.lBrand.Text = "Brand";
            // 
            // lCategory
            // 
            this.lCategory.AutoSize = true;
            this.lCategory.Location = new System.Drawing.Point(8, 79);
            this.lCategory.Name = "lCategory";
            this.lCategory.Size = new System.Drawing.Size(46, 13);
            this.lCategory.TabIndex = 30;
            this.lCategory.Text = "Kategori";
            // 
            // lSerial
            // 
            this.lSerial.AutoSize = true;
            this.lSerial.Location = new System.Drawing.Point(8, 53);
            this.lSerial.Name = "lSerial";
            this.lSerial.Size = new System.Drawing.Size(53, 13);
            this.lSerial.TabIndex = 29;
            this.lSerial.Text = "No. Serial";
            // 
            // lModel
            // 
            this.lModel.AutoSize = true;
            this.lModel.Location = new System.Drawing.Point(8, 27);
            this.lModel.Name = "lModel";
            this.lModel.Size = new System.Drawing.Size(36, 13);
            this.lModel.TabIndex = 28;
            this.lModel.Text = "Model";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tAlasan);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.rHilang);
            this.groupBox1.Controls.Add(this.rRusak);
            this.groupBox1.Location = new System.Drawing.Point(6, 300);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(529, 140);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Kondisi/Alasan";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 44;
            this.label3.Text = "Kondisi";
            // 
            // tAlasan
            // 
            this.tAlasan.Location = new System.Drawing.Point(106, 61);
            this.tAlasan.Multiline = true;
            this.tAlasan.Name = "tAlasan";
            this.tAlasan.Size = new System.Drawing.Size(400, 71);
            this.tAlasan.TabIndex = 43;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 42;
            this.label2.Text = "Alasan";
            // 
            // rHilang
            // 
            this.rHilang.AutoSize = true;
            this.rHilang.Location = new System.Drawing.Point(186, 28);
            this.rHilang.Name = "rHilang";
            this.rHilang.Size = new System.Drawing.Size(55, 17);
            this.rHilang.TabIndex = 1;
            this.rHilang.Text = "Hilang";
            this.rHilang.UseVisualStyleBackColor = true;
            // 
            // rRusak
            // 
            this.rRusak.AutoSize = true;
            this.rRusak.Checked = true;
            this.rRusak.Location = new System.Drawing.Point(106, 28);
            this.rRusak.Name = "rRusak";
            this.rRusak.Size = new System.Drawing.Size(56, 17);
            this.rRusak.TabIndex = 0;
            this.rRusak.TabStop = true;
            this.rRusak.Text = "Rusak";
            this.rRusak.UseVisualStyleBackColor = true;
            // 
            // lNotify
            // 
            this.lNotify.AutoSize = true;
            this.lNotify.ForeColor = System.Drawing.Color.Red;
            this.lNotify.Location = new System.Drawing.Point(14, 613);
            this.lNotify.Name = "lNotify";
            this.lNotify.Size = new System.Drawing.Size(0, 13);
            this.lNotify.TabIndex = 38;
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(415, 505);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(120, 40);
            this.bCancel.TabIndex = 37;
            this.bCancel.Text = "Batal";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.bCari);
            this.groupBox10.Controls.Add(this.tFile);
            this.groupBox10.Controls.Add(this.label4);
            this.groupBox10.Location = new System.Drawing.Point(6, 446);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(529, 53);
            this.groupBox10.TabIndex = 39;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Upload Berita Acara";
            // 
            // bCari
            // 
            this.bCari.Location = new System.Drawing.Point(389, 17);
            this.bCari.Name = "bCari";
            this.bCari.Size = new System.Drawing.Size(75, 23);
            this.bCari.TabIndex = 21;
            this.bCari.Text = "Cari";
            this.bCari.UseVisualStyleBackColor = true;
            this.bCari.Click += new System.EventHandler(this.bCari_Click);
            // 
            // tFile
            // 
            this.tFile.Location = new System.Drawing.Point(106, 19);
            this.tFile.Name = "tFile";
            this.tFile.ReadOnly = true;
            this.tFile.Size = new System.Drawing.Size(277, 20);
            this.tFile.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(12, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 20;
            this.label4.Text = "Upload BA";
            // 
            // BreakManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 734);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "BreakManagement";
            this.Text = "Kerusakan/Kehilangan";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.BreakManagement_FormClosed);
            this.Load += new System.EventHandler(this.BreakManagement_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.BreakManagement_Paint);
            this.tabControl1.ResumeLayout(false);
            this.Kehilangan.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGAssetLP)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGAssetL)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.Kerusakan.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGAssetBP)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGAssetB)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.Permintaan.ResumeLayout(false);
            this.Permintaan.PerformLayout();
            this.gAsset.ResumeLayout(false);
            this.gAsset.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage Permintaan;
        private System.Windows.Forms.Button bSend;
        private System.Windows.Forms.GroupBox gAsset;
        private System.Windows.Forms.Button baAsset;
        private System.Windows.Forms.DateTimePicker purDate;
        private System.Windows.Forms.Label lDate;
        private System.Windows.Forms.TextBox tDesc;
        private System.Windows.Forms.TextBox tLocation;
        private System.Windows.Forms.TextBox tPrice;
        private System.Windows.Forms.TextBox tBrand;
        private System.Windows.Forms.TextBox tCategory;
        private System.Windows.Forms.TextBox tSerial;
        private System.Windows.Forms.TextBox tModel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lLocation;
        private System.Windows.Forms.Label lPrice;
        private System.Windows.Forms.Label lBrand;
        private System.Windows.Forms.Label lCategory;
        private System.Windows.Forms.Label lSerial;
        private System.Windows.Forms.Label lModel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tAlasan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton rHilang;
        private System.Windows.Forms.RadioButton rRusak;
        private System.Windows.Forms.Label lNotify;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.TabPage Kehilangan;
        private System.Windows.Forms.TabPage Kerusakan;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tSearchL;
        private System.Windows.Forms.Button bSearchL;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dataGAssetLP;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox tSearchLP;
        private System.Windows.Forms.Button bSearchLP;
        private System.Windows.Forms.DataGridView dataGAssetL;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView dataGAssetBP;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox tSearchBP;
        private System.Windows.Forms.Button bSearchBP;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView dataGAssetB;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox tSearchB;
        private System.Windows.Forms.Button bSearchB;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button bCari;
        private System.Windows.Forms.TextBox tFile;
        private System.Windows.Forms.Label label4;

    }
}