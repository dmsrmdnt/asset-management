﻿namespace asset
{
    partial class MutationsManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.infoMutasi = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGOut = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGIn = new System.Windows.Forms.DataGridView();
            this.pendingMutasi = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGPendding = new System.Windows.Forms.DataGridView();
            this.requestMutasi = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.dataGRequest = new System.Windows.Forms.DataGridView();
            this.sendMutasi = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.bCari = new System.Windows.Forms.Button();
            this.tFile = new System.Windows.Forms.TextBox();
            this.lNotify = new System.Windows.Forms.Label();
            this.bCancel = new System.Windows.Forms.Button();
            this.bSend = new System.Windows.Forms.Button();
            this.groupBoxOwner = new System.Windows.Forms.GroupBox();
            this.lDepartment = new System.Windows.Forms.Label();
            this.tDept = new System.Windows.Forms.TextBox();
            this.tName = new System.Windows.Forms.TextBox();
            this.lName = new System.Windows.Forms.Label();
            this.baOwner = new System.Windows.Forms.Button();
            this.tNIK = new System.Windows.Forms.TextBox();
            this.lNIK = new System.Windows.Forms.Label();
            this.gAsset = new System.Windows.Forms.GroupBox();
            this.baAsset = new System.Windows.Forms.Button();
            this.purDate = new System.Windows.Forms.DateTimePicker();
            this.lDate = new System.Windows.Forms.Label();
            this.tDesc = new System.Windows.Forms.TextBox();
            this.tLocation = new System.Windows.Forms.TextBox();
            this.tPrice = new System.Windows.Forms.TextBox();
            this.tBrand = new System.Windows.Forms.TextBox();
            this.tCategory = new System.Windows.Forms.TextBox();
            this.tSerial = new System.Windows.Forms.TextBox();
            this.tModel = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lLocation = new System.Windows.Forms.Label();
            this.lPrice = new System.Windows.Forms.Label();
            this.lBrand = new System.Windows.Forms.Label();
            this.lCategory = new System.Windows.Forms.Label();
            this.lSerial = new System.Windows.Forms.Label();
            this.lModel = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.infoMutasi.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGOut)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGIn)).BeginInit();
            this.pendingMutasi.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGPendding)).BeginInit();
            this.requestMutasi.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGRequest)).BeginInit();
            this.sendMutasi.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBoxOwner.SuspendLayout();
            this.gAsset.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.infoMutasi);
            this.tabControl1.Controls.Add(this.pendingMutasi);
            this.tabControl1.Controls.Add(this.requestMutasi);
            this.tabControl1.Controls.Add(this.sendMutasi);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(970, 561);
            this.tabControl1.TabIndex = 22;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // infoMutasi
            // 
            this.infoMutasi.Controls.Add(this.groupBox2);
            this.infoMutasi.Controls.Add(this.groupBox1);
            this.infoMutasi.Location = new System.Drawing.Point(4, 22);
            this.infoMutasi.Name = "infoMutasi";
            this.infoMutasi.Padding = new System.Windows.Forms.Padding(3);
            this.infoMutasi.Size = new System.Drawing.Size(962, 535);
            this.infoMutasi.TabIndex = 0;
            this.infoMutasi.Text = "Informasi Mutasi";
            this.infoMutasi.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGOut);
            this.groupBox2.Location = new System.Drawing.Point(6, 241);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(950, 229);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Mutasi Keluar";
            // 
            // dataGOut
            // 
            this.dataGOut.AllowUserToAddRows = false;
            this.dataGOut.AllowUserToDeleteRows = false;
            this.dataGOut.AllowUserToResizeColumns = false;
            this.dataGOut.AllowUserToResizeRows = false;
            this.dataGOut.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGOut.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGOut.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGOut.Location = new System.Drawing.Point(6, 19);
            this.dataGOut.MultiSelect = false;
            this.dataGOut.Name = "dataGOut";
            this.dataGOut.ReadOnly = true;
            this.dataGOut.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGOut.Size = new System.Drawing.Size(938, 204);
            this.dataGOut.TabIndex = 21;
            this.dataGOut.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGOut_CellContentClick);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGIn);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(950, 229);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Mutasi Masuk";
            // 
            // dataGIn
            // 
            this.dataGIn.AllowUserToAddRows = false;
            this.dataGIn.AllowUserToDeleteRows = false;
            this.dataGIn.AllowUserToResizeColumns = false;
            this.dataGIn.AllowUserToResizeRows = false;
            this.dataGIn.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGIn.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGIn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGIn.Location = new System.Drawing.Point(6, 19);
            this.dataGIn.MultiSelect = false;
            this.dataGIn.Name = "dataGIn";
            this.dataGIn.ReadOnly = true;
            this.dataGIn.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGIn.Size = new System.Drawing.Size(938, 204);
            this.dataGIn.TabIndex = 20;
            this.dataGIn.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGIn_CellContentClick);
            // 
            // pendingMutasi
            // 
            this.pendingMutasi.Controls.Add(this.groupBox3);
            this.pendingMutasi.Location = new System.Drawing.Point(4, 22);
            this.pendingMutasi.Name = "pendingMutasi";
            this.pendingMutasi.Padding = new System.Windows.Forms.Padding(3);
            this.pendingMutasi.Size = new System.Drawing.Size(962, 535);
            this.pendingMutasi.TabIndex = 1;
            this.pendingMutasi.Text = "Permintaan Mutasi Keluar";
            this.pendingMutasi.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGPendding);
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(950, 469);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Permintaan Mutasi Keluar";
            // 
            // dataGPendding
            // 
            this.dataGPendding.AllowUserToAddRows = false;
            this.dataGPendding.AllowUserToDeleteRows = false;
            this.dataGPendding.AllowUserToResizeColumns = false;
            this.dataGPendding.AllowUserToResizeRows = false;
            this.dataGPendding.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGPendding.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGPendding.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGPendding.Location = new System.Drawing.Point(6, 19);
            this.dataGPendding.MultiSelect = false;
            this.dataGPendding.Name = "dataGPendding";
            this.dataGPendding.ReadOnly = true;
            this.dataGPendding.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGPendding.Size = new System.Drawing.Size(938, 444);
            this.dataGPendding.TabIndex = 7;
            this.dataGPendding.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGPendding_CellContentClick);
            // 
            // requestMutasi
            // 
            this.requestMutasi.Controls.Add(this.groupBox4);
            this.requestMutasi.Location = new System.Drawing.Point(4, 22);
            this.requestMutasi.Name = "requestMutasi";
            this.requestMutasi.Size = new System.Drawing.Size(962, 535);
            this.requestMutasi.TabIndex = 2;
            this.requestMutasi.Text = "Permintaan Mutasi Masuk";
            this.requestMutasi.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.dataGRequest);
            this.groupBox4.Location = new System.Drawing.Point(6, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(950, 469);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Permintaan Mutasi Masuk";
            // 
            // dataGRequest
            // 
            this.dataGRequest.AllowUserToAddRows = false;
            this.dataGRequest.AllowUserToDeleteRows = false;
            this.dataGRequest.AllowUserToResizeColumns = false;
            this.dataGRequest.AllowUserToResizeRows = false;
            this.dataGRequest.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGRequest.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGRequest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGRequest.Location = new System.Drawing.Point(6, 19);
            this.dataGRequest.MultiSelect = false;
            this.dataGRequest.Name = "dataGRequest";
            this.dataGRequest.ReadOnly = true;
            this.dataGRequest.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGRequest.Size = new System.Drawing.Size(938, 444);
            this.dataGRequest.TabIndex = 18;
            this.dataGRequest.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGRequest_CellContentClick);
            // 
            // sendMutasi
            // 
            this.sendMutasi.Controls.Add(this.groupBox5);
            this.sendMutasi.Controls.Add(this.lNotify);
            this.sendMutasi.Controls.Add(this.bCancel);
            this.sendMutasi.Controls.Add(this.bSend);
            this.sendMutasi.Controls.Add(this.groupBoxOwner);
            this.sendMutasi.Controls.Add(this.gAsset);
            this.sendMutasi.Location = new System.Drawing.Point(4, 22);
            this.sendMutasi.Name = "sendMutasi";
            this.sendMutasi.Size = new System.Drawing.Size(962, 535);
            this.sendMutasi.TabIndex = 3;
            this.sendMutasi.Text = "Kirim Permintaan Mutasi";
            this.sendMutasi.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.bCari);
            this.groupBox5.Controls.Add(this.tFile);
            this.groupBox5.Location = new System.Drawing.Point(3, 459);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(757, 60);
            this.groupBox5.TabIndex = 33;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Upload Dokumen Mutasi";
            // 
            // bCari
            // 
            this.bCari.Location = new System.Drawing.Point(676, 19);
            this.bCari.Name = "bCari";
            this.bCari.Size = new System.Drawing.Size(75, 23);
            this.bCari.TabIndex = 3;
            this.bCari.Text = "Cari";
            this.bCari.UseVisualStyleBackColor = true;
            this.bCari.Click += new System.EventHandler(this.bCari_Click);
            // 
            // tFile
            // 
            this.tFile.Location = new System.Drawing.Point(6, 21);
            this.tFile.Name = "tFile";
            this.tFile.ReadOnly = true;
            this.tFile.Size = new System.Drawing.Size(664, 20);
            this.tFile.TabIndex = 17;
            // 
            // lNotify
            // 
            this.lNotify.AutoSize = true;
            this.lNotify.ForeColor = System.Drawing.Color.Red;
            this.lNotify.Location = new System.Drawing.Point(11, 522);
            this.lNotify.Name = "lNotify";
            this.lNotify.Size = new System.Drawing.Size(0, 13);
            this.lNotify.TabIndex = 32;
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(799, 57);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(120, 40);
            this.bCancel.TabIndex = 5;
            this.bCancel.Text = "Batal";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // bSend
            // 
            this.bSend.Location = new System.Drawing.Point(799, 11);
            this.bSend.Name = "bSend";
            this.bSend.Size = new System.Drawing.Size(120, 40);
            this.bSend.TabIndex = 4;
            this.bSend.Text = "Kirim";
            this.bSend.UseVisualStyleBackColor = true;
            this.bSend.Click += new System.EventHandler(this.bSend_Click);
            // 
            // groupBoxOwner
            // 
            this.groupBoxOwner.Controls.Add(this.lDepartment);
            this.groupBoxOwner.Controls.Add(this.tDept);
            this.groupBoxOwner.Controls.Add(this.tName);
            this.groupBoxOwner.Controls.Add(this.lName);
            this.groupBoxOwner.Controls.Add(this.baOwner);
            this.groupBoxOwner.Controls.Add(this.tNIK);
            this.groupBoxOwner.Controls.Add(this.lNIK);
            this.groupBoxOwner.Location = new System.Drawing.Point(3, 3);
            this.groupBoxOwner.Name = "groupBoxOwner";
            this.groupBoxOwner.Size = new System.Drawing.Size(755, 127);
            this.groupBoxOwner.TabIndex = 29;
            this.groupBoxOwner.TabStop = false;
            this.groupBoxOwner.Text = "Mutasi Ke";
            // 
            // lDepartment
            // 
            this.lDepartment.AutoSize = true;
            this.lDepartment.Location = new System.Drawing.Point(11, 83);
            this.lDepartment.Name = "lDepartment";
            this.lDepartment.Size = new System.Drawing.Size(65, 13);
            this.lDepartment.TabIndex = 28;
            this.lDepartment.Text = "Departemen";
            // 
            // tDept
            // 
            this.tDept.Location = new System.Drawing.Point(109, 80);
            this.tDept.Name = "tDept";
            this.tDept.ReadOnly = true;
            this.tDept.Size = new System.Drawing.Size(145, 20);
            this.tDept.TabIndex = 8;
            // 
            // tName
            // 
            this.tName.Location = new System.Drawing.Point(109, 54);
            this.tName.Name = "tName";
            this.tName.ReadOnly = true;
            this.tName.Size = new System.Drawing.Size(217, 20);
            this.tName.TabIndex = 7;
            // 
            // lName
            // 
            this.lName.AutoSize = true;
            this.lName.Location = new System.Drawing.Point(11, 57);
            this.lName.Name = "lName";
            this.lName.Size = new System.Drawing.Size(35, 13);
            this.lName.TabIndex = 26;
            this.lName.Text = "Nama";
            // 
            // baOwner
            // 
            this.baOwner.Location = new System.Drawing.Point(332, 26);
            this.baOwner.Name = "baOwner";
            this.baOwner.Size = new System.Drawing.Size(66, 23);
            this.baOwner.TabIndex = 1;
            this.baOwner.Text = "Cari";
            this.baOwner.UseVisualStyleBackColor = true;
            this.baOwner.Click += new System.EventHandler(this.baOwner_Click);
            // 
            // tNIK
            // 
            this.tNIK.Location = new System.Drawing.Point(109, 28);
            this.tNIK.Name = "tNIK";
            this.tNIK.ReadOnly = true;
            this.tNIK.Size = new System.Drawing.Size(217, 20);
            this.tNIK.TabIndex = 6;
            // 
            // lNIK
            // 
            this.lNIK.AutoSize = true;
            this.lNIK.Location = new System.Drawing.Point(11, 31);
            this.lNIK.Name = "lNIK";
            this.lNIK.Size = new System.Drawing.Size(32, 13);
            this.lNIK.TabIndex = 25;
            this.lNIK.Text = "Email";
            // 
            // gAsset
            // 
            this.gAsset.Controls.Add(this.baAsset);
            this.gAsset.Controls.Add(this.purDate);
            this.gAsset.Controls.Add(this.lDate);
            this.gAsset.Controls.Add(this.tDesc);
            this.gAsset.Controls.Add(this.tLocation);
            this.gAsset.Controls.Add(this.tPrice);
            this.gAsset.Controls.Add(this.tBrand);
            this.gAsset.Controls.Add(this.tCategory);
            this.gAsset.Controls.Add(this.tSerial);
            this.gAsset.Controls.Add(this.tModel);
            this.gAsset.Controls.Add(this.label1);
            this.gAsset.Controls.Add(this.lLocation);
            this.gAsset.Controls.Add(this.lPrice);
            this.gAsset.Controls.Add(this.lBrand);
            this.gAsset.Controls.Add(this.lCategory);
            this.gAsset.Controls.Add(this.lSerial);
            this.gAsset.Controls.Add(this.lModel);
            this.gAsset.Location = new System.Drawing.Point(3, 136);
            this.gAsset.Name = "gAsset";
            this.gAsset.Size = new System.Drawing.Size(757, 318);
            this.gAsset.TabIndex = 28;
            this.gAsset.TabStop = false;
            this.gAsset.Text = "Aset";
            // 
            // baAsset
            // 
            this.baAsset.Location = new System.Drawing.Point(332, 22);
            this.baAsset.Name = "baAsset";
            this.baAsset.Size = new System.Drawing.Size(66, 23);
            this.baAsset.TabIndex = 2;
            this.baAsset.Text = "Cari";
            this.baAsset.UseVisualStyleBackColor = true;
            this.baAsset.Click += new System.EventHandler(this.baAsset_Click);
            // 
            // purDate
            // 
            this.purDate.Enabled = false;
            this.purDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.purDate.Location = new System.Drawing.Point(106, 128);
            this.purDate.Name = "purDate";
            this.purDate.Size = new System.Drawing.Size(148, 20);
            this.purDate.TabIndex = 13;
            // 
            // lDate
            // 
            this.lDate.AutoSize = true;
            this.lDate.Location = new System.Drawing.Point(8, 134);
            this.lDate.Name = "lDate";
            this.lDate.Size = new System.Drawing.Size(98, 13);
            this.lDate.TabIndex = 45;
            this.lDate.Text = "Tanggal Pembelian";
            // 
            // tDesc
            // 
            this.tDesc.Location = new System.Drawing.Point(106, 206);
            this.tDesc.Multiline = true;
            this.tDesc.Name = "tDesc";
            this.tDesc.ReadOnly = true;
            this.tDesc.Size = new System.Drawing.Size(645, 101);
            this.tDesc.TabIndex = 16;
            // 
            // tLocation
            // 
            this.tLocation.Location = new System.Drawing.Point(106, 180);
            this.tLocation.Name = "tLocation";
            this.tLocation.ReadOnly = true;
            this.tLocation.Size = new System.Drawing.Size(148, 20);
            this.tLocation.TabIndex = 15;
            // 
            // tPrice
            // 
            this.tPrice.Location = new System.Drawing.Point(106, 154);
            this.tPrice.Name = "tPrice";
            this.tPrice.ReadOnly = true;
            this.tPrice.Size = new System.Drawing.Size(148, 20);
            this.tPrice.TabIndex = 14;
            // 
            // tBrand
            // 
            this.tBrand.Location = new System.Drawing.Point(106, 102);
            this.tBrand.Name = "tBrand";
            this.tBrand.ReadOnly = true;
            this.tBrand.Size = new System.Drawing.Size(148, 20);
            this.tBrand.TabIndex = 12;
            // 
            // tCategory
            // 
            this.tCategory.Location = new System.Drawing.Point(106, 76);
            this.tCategory.Name = "tCategory";
            this.tCategory.ReadOnly = true;
            this.tCategory.Size = new System.Drawing.Size(148, 20);
            this.tCategory.TabIndex = 11;
            // 
            // tSerial
            // 
            this.tSerial.Location = new System.Drawing.Point(106, 50);
            this.tSerial.Name = "tSerial";
            this.tSerial.ReadOnly = true;
            this.tSerial.Size = new System.Drawing.Size(220, 20);
            this.tSerial.TabIndex = 10;
            // 
            // tModel
            // 
            this.tModel.Location = new System.Drawing.Point(106, 24);
            this.tModel.Name = "tModel";
            this.tModel.ReadOnly = true;
            this.tModel.Size = new System.Drawing.Size(220, 20);
            this.tModel.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 206);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 34;
            this.label1.Text = "Deskripsi";
            // 
            // lLocation
            // 
            this.lLocation.AutoSize = true;
            this.lLocation.Location = new System.Drawing.Point(8, 183);
            this.lLocation.Name = "lLocation";
            this.lLocation.Size = new System.Drawing.Size(38, 13);
            this.lLocation.TabIndex = 33;
            this.lLocation.Text = "Lokasi";
            // 
            // lPrice
            // 
            this.lPrice.AutoSize = true;
            this.lPrice.Location = new System.Drawing.Point(8, 157);
            this.lPrice.Name = "lPrice";
            this.lPrice.Size = new System.Drawing.Size(36, 13);
            this.lPrice.TabIndex = 32;
            this.lPrice.Text = "Harga";
            // 
            // lBrand
            // 
            this.lBrand.AutoSize = true;
            this.lBrand.Location = new System.Drawing.Point(8, 105);
            this.lBrand.Name = "lBrand";
            this.lBrand.Size = new System.Drawing.Size(35, 13);
            this.lBrand.TabIndex = 31;
            this.lBrand.Text = "Brand";
            // 
            // lCategory
            // 
            this.lCategory.AutoSize = true;
            this.lCategory.Location = new System.Drawing.Point(8, 79);
            this.lCategory.Name = "lCategory";
            this.lCategory.Size = new System.Drawing.Size(46, 13);
            this.lCategory.TabIndex = 30;
            this.lCategory.Text = "Kategori";
            // 
            // lSerial
            // 
            this.lSerial.AutoSize = true;
            this.lSerial.Location = new System.Drawing.Point(8, 53);
            this.lSerial.Name = "lSerial";
            this.lSerial.Size = new System.Drawing.Size(53, 13);
            this.lSerial.TabIndex = 29;
            this.lSerial.Text = "No. Serial";
            // 
            // lModel
            // 
            this.lModel.AutoSize = true;
            this.lModel.Location = new System.Drawing.Point(8, 27);
            this.lModel.Name = "lModel";
            this.lModel.Size = new System.Drawing.Size(36, 13);
            this.lModel.TabIndex = 28;
            this.lModel.Text = "Model";
            // 
            // MutationsManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 585);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MutationsManagement";
            this.Text = "Kelola Mutasi";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MutationsManagement_FormClosed);
            this.Load += new System.EventHandler(this.MutationsManagement_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MutationsManagement_Paint);
            this.tabControl1.ResumeLayout(false);
            this.infoMutasi.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGOut)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGIn)).EndInit();
            this.pendingMutasi.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGPendding)).EndInit();
            this.requestMutasi.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGRequest)).EndInit();
            this.sendMutasi.ResumeLayout(false);
            this.sendMutasi.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBoxOwner.ResumeLayout(false);
            this.groupBoxOwner.PerformLayout();
            this.gAsset.ResumeLayout(false);
            this.gAsset.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage infoMutasi;
        private System.Windows.Forms.TabPage pendingMutasi;
        private System.Windows.Forms.TabPage requestMutasi;
        private System.Windows.Forms.TabPage sendMutasi;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGOut;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGIn;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dataGPendding;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DataGridView dataGRequest;
        private System.Windows.Forms.GroupBox groupBoxOwner;
        private System.Windows.Forms.Label lDepartment;
        private System.Windows.Forms.TextBox tDept;
        private System.Windows.Forms.TextBox tName;
        private System.Windows.Forms.Label lName;
        private System.Windows.Forms.Button baOwner;
        private System.Windows.Forms.TextBox tNIK;
        private System.Windows.Forms.Label lNIK;
        private System.Windows.Forms.GroupBox gAsset;
        private System.Windows.Forms.Button baAsset;
        private System.Windows.Forms.DateTimePicker purDate;
        private System.Windows.Forms.Label lDate;
        private System.Windows.Forms.TextBox tDesc;
        private System.Windows.Forms.TextBox tLocation;
        private System.Windows.Forms.TextBox tPrice;
        private System.Windows.Forms.TextBox tBrand;
        private System.Windows.Forms.TextBox tCategory;
        private System.Windows.Forms.TextBox tSerial;
        private System.Windows.Forms.TextBox tModel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lLocation;
        private System.Windows.Forms.Label lPrice;
        private System.Windows.Forms.Label lBrand;
        private System.Windows.Forms.Label lCategory;
        private System.Windows.Forms.Label lSerial;
        private System.Windows.Forms.Label lModel;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Button bSend;
        private System.Windows.Forms.Label lNotify;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button bCari;
        private System.Windows.Forms.TextBox tFile;
    }
}