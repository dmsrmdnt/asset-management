﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class MutationManagementAll : Form
    {
        private String id;
        lib lib = new lib();
        public MutationManagementAll(String id)
        {
            InitializeComponent();
            this.id = id;
        }
        private static MutationManagementAll form;
        public static MutationManagementAll GetChildInstance(String id)
        {
            if (form == null)
                form = new MutationManagementAll(id);
            return form;
        }
        private void MutationManagementAll_Load(object sender, EventArgs e)
        {

        }

        private void MutationManagementAll_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void MutationManagementAll_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }

        private void dataGIn_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

    }
}
