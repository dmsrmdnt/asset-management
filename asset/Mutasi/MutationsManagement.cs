﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace asset
{
    public partial class MutationsManagement : Form
    {
        lib lib = new lib();
        private String id;
        private String id2;
        private String id_asset;
        Class.mutasi.mSend send = new Class.mutasi.mSend();
        Class.mutasi.mPendding pendding = new Class.mutasi.mPendding();
        Class.mutasi.mRequest request = new Class.mutasi.mRequest();
        Class.Email.email mail = new Class.Email.email();
        public MutationsManagement(String id)
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            this.id = id;
        }
        private static MutationsManagement form;
        public static MutationsManagement GetChildInstance(String id)
        {
            if (form == null)
                form = new MutationsManagement(id);
            return form;
        }

        private void MutationsManagement_Load(object sender, EventArgs e)
        {
            try
            {
                DataGridViewButtonColumn d = new DataGridViewButtonColumn();
                dataGIn.Columns.Add(d);
                d.HeaderText = "Detail";
                d.Text = "Detail";
                d.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn g = new DataGridViewButtonColumn();
                dataGOut.Columns.Add(g);
                g.HeaderText = "Detail";
                g.Text = "Detail";
                g.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn acc = new DataGridViewButtonColumn();
                dataGRequest.Columns.Add(acc);
                acc.HeaderText = "Terima";
                acc.Text = "Terima";
                acc.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn reject = new DataGridViewButtonColumn();
                dataGRequest.Columns.Add(reject);
                reject.HeaderText = "Tolak";
                reject.Text = "Tolak";
                reject.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn h = new DataGridViewButtonColumn();
                dataGRequest.Columns.Add(h);
                h.HeaderText = "Detail";
                h.Text = "Detail";
                h.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn batal = new DataGridViewButtonColumn();
                dataGPendding.Columns.Add(batal);
                batal.HeaderText = "Batal";
                batal.Text = "Batal";
                batal.UseColumnTextForButtonValue = true;

                DataGridViewButtonColumn f = new DataGridViewButtonColumn();
                dataGPendding.Columns.Add(f);
                f.HeaderText = "Detail";
                f.Text = "Detail";
                f.UseColumnTextForButtonValue = true;

                DataGridViewLinkColumn links = new DataGridViewLinkColumn();
                links.UseColumnTextForLinkValue = true;
                links.HeaderText = "Download Dokumen Mutasi";
                links.DataPropertyName = "lnkColumn";
                links.ActiveLinkColor = Color.White;
                links.LinkBehavior = LinkBehavior.SystemDefault;
                links.LinkColor = Color.Blue;
                links.Text = "Click here to download";
                links.TrackVisitedState = true;
                links.VisitedLinkColor = Color.YellowGreen;
                dataGPendding.Columns.Add(links);
                DataGridViewLinkColumn links1 = new DataGridViewLinkColumn();
                links1.UseColumnTextForLinkValue = true;
                links1.HeaderText = "Download Dokumen Mutasi";
                links1.DataPropertyName = "lnkColumn";
                links1.ActiveLinkColor = Color.White;
                links1.LinkBehavior = LinkBehavior.SystemDefault;
                links1.LinkColor = Color.Blue;
                links1.Text = "Click here to download";
                links1.TrackVisitedState = true;
                links1.VisitedLinkColor = Color.YellowGreen;
                dataGRequest.Columns.Add(links1);
                DataGridViewLinkColumn links2 = new DataGridViewLinkColumn();
                links2.UseColumnTextForLinkValue = true;
                links2.HeaderText = "Download Dokumen Mutasi";
                links2.DataPropertyName = "lnkColumn";
                links2.ActiveLinkColor = Color.White;
                links2.LinkBehavior = LinkBehavior.SystemDefault;
                links2.LinkColor = Color.Blue;
                links2.Text = "Click here to download";
                links2.TrackVisitedState = true;
                links2.VisitedLinkColor = Color.YellowGreen;
                dataGIn.Columns.Add(links2);
                DataGridViewLinkColumn links3 = new DataGridViewLinkColumn();
                links3.UseColumnTextForLinkValue = true;
                links3.HeaderText = "Download Dokumen Mutasi";
                links3.DataPropertyName = "lnkColumn";
                links3.ActiveLinkColor = Color.White;
                links3.LinkBehavior = LinkBehavior.SystemDefault;
                links3.LinkColor = Color.Blue;
                links3.Text = "Click here to download";
                links3.TrackVisitedState = true;
                links3.VisitedLinkColor = Color.YellowGreen;
                dataGOut.Columns.Add(links3);


                showDataGIn(this.id);
                showDataGOut(this.id);
                showDataPendding(this.id);
                showDataRequest(this.id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void MutationsManagement_FormClosed(object sender, FormClosedEventArgs e)
        {
            form = null;
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                showDataGIn(this.id);
                showDataGOut(this.id);
                showDataPendding(this.id);
                showDataRequest(this.id);
                clearField();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        ///////////////////////// Informasi Mutasi /////////////////////////////////////
        public void showDataGIn(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_mutation_in";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGIn.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGIn.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void showDataGOut(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_mutation_out";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGOut.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGOut.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGIn_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGIn.Columns[0].Index)
                {
                    //Detail
                    try
                    {
                        String id = dataGIn[5, e.RowIndex].Value.ToString();
                        AssetDetail asset = new AssetDetail(id);
                        asset.ShowDialog();
                        asset.BringToFront();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGIn.Columns[1].Index)
                {
                    //Download
                    try
                    {
                        String id = dataGIn[5, e.RowIndex].Value.ToString();
                        FileStream FS = null;
                        byte[] dbbyte;

                        //Get selected file ID field
                        SqlConnection conn = new SqlConnection(lib.param());
                        conn.Open();
                        SqlCommand sqlcmd = new SqlCommand();
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataTable dt = new DataTable();
                        sqlcmd = new SqlCommand("select * from t_mutation where id_asset='" + id + "' and stat='0' and id_employee2='" + this.id + "'", conn);
                        da = new SqlDataAdapter(sqlcmd);
                        dt = new DataTable();
                        da.Fill(dt);
                        conn.Close();


                        if (dt.Rows.Count > 0)
                        {
                            //Get a stored PDF bytes
                            String dname = dt.Rows[0]["dname"].ToString();
                            if (dname != "")
                            {
                                dbbyte = (byte[])dt.Rows[0]["dcontent"];
                                String[] filetype = dname.Split('.');

                                //store file Temporarily 
                                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                                string filepath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\BA\\" +dname+"." + filetype[1];

                                //Assign File path create file
                                FS = new FileStream(filepath, System.IO.FileMode.Create);

                                //Write bytes to create file
                                FS.Write(dbbyte, 0, dbbyte.Length);

                                //Close FileStream instance
                                FS.Close();

                                // Open fila after write 

                                //Create instance for process class
                                Process Proc = new Process();

                                //assign file path for process
                                Proc.StartInfo.FileName = filepath;
                                Proc.Start();
                            }
                            else
                            {
                                MessageBox.Show("Dokumen mutasi tidak tersedia", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void dataGOut_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGOut.Columns[0].Index)
                {
                    //Detail
                    try
                    {
                        String id = dataGOut[5, e.RowIndex].Value.ToString();
                        AssetDetail asset = new AssetDetail(id);
                        asset.ShowDialog();
                        asset.BringToFront();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGOut.Columns[1].Index)
                {
                    //Detail
                    try
                    {
                        String id = dataGOut[5, e.RowIndex].Value.ToString();
                        FileStream FS = null;
                        byte[] dbbyte;

                        //Get selected file ID field
                        SqlConnection conn = new SqlConnection(lib.param());
                        conn.Open();
                        SqlCommand sqlcmd = new SqlCommand();
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataTable dt = new DataTable();
                        sqlcmd = new SqlCommand("select * from t_mutation where id_asset='" + id + "' and stat='0' and id_employee1='"+ this.id +"'", conn);
                        da = new SqlDataAdapter(sqlcmd);
                        dt = new DataTable();
                        da.Fill(dt);
                        conn.Close();


                        if (dt.Rows.Count > 0)
                        {
                            //Get a stored PDF bytes
                            String dname = dt.Rows[0]["dname"].ToString();
                            if (dname != "")
                            {

                                dbbyte = (byte[])dt.Rows[0]["dcontent"];
                                String[] filetype = dname.Split('.');

                                //store file Temporarily 
                                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                                string filepath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\BA\\" + dname+ "." + filetype[1];

                                //Assign File path create file
                                FS = new FileStream(filepath, System.IO.FileMode.Create);

                                //Write bytes to create file
                                FS.Write(dbbyte, 0, dbbyte.Length);

                                //Close FileStream instance
                                FS.Close();

                                // Open fila after write 

                                //Create instance for process class
                                Process Proc = new Process();

                                //assign file path for process
                                Proc.StartInfo.FileName = filepath;
                                Proc.Start();
                            }
                            else
                            {
                                MessageBox.Show("Dokumen mutasi tidak tersedia", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        ///////////////////////// Permintaan Mutasi Keluar /////////////////////////////////////

        public void showDataPendding(String id_employee)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "req_data_mutation_out";
                myCommand.Parameters.AddWithValue("@id_employee", id_employee);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand=myCommand;
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGPendding.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGPendding.Columns["ID Aset"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dataGPendding_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGPendding.Columns[0].Index)
                {
                    //Batal
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk membatalkan proses mutasi aset ini ? ",
                        "Konfirmasi Pembatalan", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            String id = dataGPendding[5, e.RowIndex].Value.ToString();
                            pendding.delete(this.id, id);
                            showDataPendding(this.id);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGPendding.Columns[1].Index)
                {
                    //Detail
                    try
                    {
                        String id = dataGPendding[5, e.RowIndex].Value.ToString();
                        AssetDetail asset = new AssetDetail(id);
                        asset.ShowDialog();
                        asset.BringToFront();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGPendding.Columns[2].Index)
                {
                    //Download
                    try
                    {
                        String id = dataGPendding[5, e.RowIndex].Value.ToString();
                        
                        FileStream FS = null;
                        byte[] dbbyte;

                        //Get selected file ID field
                        SqlConnection conn = new SqlConnection(lib.param());
                        conn.Open();
                        SqlCommand sqlcmd = new SqlCommand();
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataTable dt = new DataTable();
                        sqlcmd = new SqlCommand("select * from t_mutation where id_asset='" + id + "' and stat='1'", conn);
                        da = new SqlDataAdapter(sqlcmd);
                        dt = new DataTable();
                        da.Fill(dt);
                        conn.Close();


                        if (dt.Rows.Count > 0)
                        {
                            //Get a stored PDF bytes
                            String dname = dt.Rows[0]["dname"].ToString();
                            if (dname != "")
                            {

                                dbbyte = (byte[])dt.Rows[0]["dcontent"];
                                String[] filetype = dname.Split('.');

                                //store file Temporarily 
                                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                                string filepath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\BA\\" + dname+ "." + filetype[1];

                                //Assign File path create file
                                FS = new FileStream(filepath, System.IO.FileMode.Create);

                                //Write bytes to create file
                                FS.Write(dbbyte, 0, dbbyte.Length);

                                //Close FileStream instance
                                FS.Close();

                                // Open fila after write 

                                //Create instance for process class
                                Process Proc = new Process();

                                //assign file path for process
                                Proc.StartInfo.FileName = filepath;
                                Proc.Start();
                            }
                            else
                            {
                                MessageBox.Show("Dokumen mutasi tidak tersedia", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }   

        ///////////////////////// Permintaan Mutasi Masuk /////////////////////////////////////

        public void showDataRequest(String id_employee)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "req_data_mutation_in";
                myCommand.Parameters.AddWithValue("@id_employee", id_employee);
                SqlDataAdapter data = new SqlDataAdapter();
                DataSet dataSet = new DataSet();
                dataSet.Clear();
                data.SelectCommand = myCommand;
                data.Fill(dataSet, "AssetTable");
                conn.Close();
                this.dataGRequest.DataSource = dataSet.Tables["AssetTable"].DefaultView;
                this.dataGRequest.Columns["ID Aset"].Visible = false;
                this.dataGRequest.Columns["ID Employee"].Visible = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void dataGRequest_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                if (e.ColumnIndex == dataGRequest.Columns[0].Index)
                {
                    //Terima
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menerima permintaan mutasi aset ini ? ",
                        "Konfirmasi Persetujuan", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            String id = dataGRequest[7, e.RowIndex].Value.ToString();
                            String id_employee = dataGRequest[4, e.RowIndex].Value.ToString();

                            View.SendEmail s = new View.SendEmail();
                            s.id_employee = id_employee;
                            s.id_asset = id;
                            s.stat = 6;
                            s.indexStatus = 0;
                            s.form = 3;
                            s.ShowDialog();

                            if (asset.Class.Email.email.cek == true)
                            {
                                request.update(this.id, id);
                                showDataRequest(this.id);
                                MessageBox.Show("Konfirmasi persetujuan mutasi berhasil dikirim", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show("Konfirmasi persetujuan mutasi gagal dikirim", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                            
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGPendding.Columns[1].Index)
                {
                    //Tolak
                    try
                    {
                        DialogResult dr = MessageBox.Show("Apakah anda yakin untuk menolak permintaan mutasi aset ini ? ",
                        "Konfirmasi Persetujuan", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (dr == DialogResult.Yes)
                        {
                            String id = dataGRequest[7, e.RowIndex].Value.ToString();
                            String id_employee = dataGRequest[4, e.RowIndex].Value.ToString();

                            View.SendEmail s = new View.SendEmail();
                            s.id_employee = id_employee;
                            s.id_asset = id;
                            s.stat = 6;
                            s.indexStatus = 1;
                            s.form = 3;
                            s.ShowDialog();

                            if (asset.Class.Email.email.cek == true)
                            {
                                request.delete(this.id, id);
                                showDataRequest(this.id);
                                MessageBox.Show("Konfirmasi persetujuan mutasi berhasil dikirim", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }
                            else
                            {
                                MessageBox.Show("Konfirmasi persetujuan mutasi gagal dikirim", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGRequest.Columns[2].Index)
                {
                    //Detail
                    try
                    {
                        String id = dataGRequest[7, e.RowIndex].Value.ToString();
                        AssetDetail asset = new AssetDetail(id);
                        asset.ShowDialog();
                        asset.BringToFront();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else if (e.ColumnIndex == dataGRequest.Columns[3].Index)
                {
                    //Download
                    try
                    {
                        String id = dataGRequest[7, e.RowIndex].Value.ToString();
                        FileStream FS = null;
                        byte[] dbbyte;

                        //Get selected file ID field
                        SqlConnection conn = new SqlConnection(lib.param());
                        conn.Open();
                        SqlCommand sqlcmd = new SqlCommand();
                        SqlDataAdapter da = new SqlDataAdapter();
                        DataTable dt = new DataTable();
                        sqlcmd = new SqlCommand("select * from t_mutation where id_asset='" + id + "' and stat='1'", conn);
                        da = new SqlDataAdapter(sqlcmd);
                        dt = new DataTable();
                        da.Fill(dt);
                        conn.Close();


                        if (dt.Rows.Count > 0)
                        {
                            //Get a stored PDF bytes
                            String dname = dt.Rows[0]["dname"].ToString();
                            if (dname != "")
                            {

                                dbbyte = (byte[])dt.Rows[0]["dcontent"];
                                String[] filetype = dname.Split('.');

                                //store file Temporarily 
                                String date = DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ').Trim();
                                string filepath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\BA\\" + dname + "." + filetype[1];

                                //Assign File path create file
                                FS = new FileStream(filepath, System.IO.FileMode.Create);

                                //Write bytes to create file
                                FS.Write(dbbyte, 0, dbbyte.Length);

                                //Close FileStream instance
                                FS.Close();

                                // Open fila after write 

                                //Create instance for process class
                                Process Proc = new Process();

                                //assign file path for process
                                Proc.StartInfo.FileName = filepath;
                                Proc.Start();
                            }
                            else
                            {
                                MessageBox.Show("Dokumen mutasi tidak tersedia", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        conn.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        ///////////////////////// Kirim Permintaan Mutasi /////////////////////////////////////

        
        private void baOwner_Click(object sender, EventArgs e)
        {
            try
            {
                AssetOwnerM cat = new AssetOwnerM(this.id);
                cat.ShowDialog();
                this.id2 = cat.idOwner;
                showDataOwner(this.id2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void showDataOwner(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_owner";
                myCommand.Parameters.AddWithValue("@id_employee", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    tNIK.Text = read["email"].ToString();
                    tName.Text = read["nama"].ToString();
                    tDept.Text = read["dept"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void baAsset_Click(object sender, EventArgs e)
        {
            try
            {
                AssetM cat = new AssetM(this.id);
                cat.ShowDialog();
                this.id_asset = cat.idA;
                showDataAsset(this.id_asset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void showDataAsset(String id)
        {
            try
            {
                SqlConnection conn = new SqlConnection(lib.param());
                SqlDataReader read = null;
                conn.Open();
                SqlCommand myCommand = conn.CreateCommand();
                myCommand.CommandType = CommandType.StoredProcedure;
                myCommand.CommandText = "show_data_asset_detail";
                myCommand.Parameters.AddWithValue("@id_asset", id);
                read = myCommand.ExecuteReader();
                while (read.Read())
                {
                    tCategory.Text = read["category"].ToString();
                    tBrand.Text = read["brand"].ToString();
                    tLocation.Text = read["location"].ToString();
                    tModel.Text = read["model"].ToString();
                    tSerial.Text = read["serial"].ToString();
                    tPrice.Text = read["price"].ToString();
                    tDesc.Text = read["description"].ToString();
                    purDate.Text = read["pur_date"].ToString();
                }
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bSend_Click(object sender, EventArgs e)
        {
            try
            {
                Boolean cekInput = send.cekInput(this.id2, this.id_asset, tFile.Text);
                int cekCondition = send.cekCondition(this.id_asset);
                int cekAssetM = send.cekAssetM(this.id_asset);
                int cekAssetL = send.cekAssetL(this.id_asset);
                int cekAssetB = send.cekAssetB(this.id_asset);
                int cekTCondition = send.cekTCondition(this.id_asset);
                if (cekInput == true)
                {
                    if (cekAssetL == 0)
                    {
                        if (cekAssetB == 0)
                        {
                            if (cekAssetM == 0)
                            {

                                //Upload file
                                string filetype;
                                string filename;
                                
                                filetype = tFile.Text.Substring(Convert.ToInt32(tFile.Text.LastIndexOf(".")) + 1, tFile.Text.Length - (Convert.ToInt32(tFile.Text.LastIndexOf(".")) + 1));
                                filename = "BA" + DateTime.Now.ToString().Replace(':', ' ').Replace('/', ' ') + this.id + this.id2 + this.id_asset+"."+filetype;
                                
                                byte[] FileBytes = null;

                                try
                                {
                                    // Open file to read using file path
                                    FileStream FS = new FileStream(tFile.Text, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                                    // Add filestream to binary reader
                                    BinaryReader BR = new BinaryReader(FS);

                                    // get total byte length of the file
                                    long allbytes = new FileInfo(tFile.Text).Length;

                                    // read entire file into buffer
                                    FileBytes = BR.ReadBytes((Int32)allbytes);

                                    // close all instances
                                    FS.Close();
                                    FS.Dispose();
                                    BR.Close();

                                    //Store File Bytes in database image filed

                                    string appPath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Data\\BA\\";
                                    System.IO.FileInfo fi = new System.IO.FileInfo(tFile.Text);
                                    fi.Refresh();
                                    fi.CopyTo(appPath + filename);

                                    List<String> file = new List<String>();
                                    file.Add((appPath + filename));

                                    View.SendEmail s = new View.SendEmail();
                                    s.id_employee1 = this.id;
                                    s.id_employee2 = this.id2;
                                    s.id_asset = this.id_asset;
                                    s.stat = 5;
                                    s.file = file.ToArray();
                                    s.form = 2;
                                    s.ShowDialog();

                                    if (asset.Class.Email.email.cek == true)
                                    {
                                        send.insert(this.id, this.id2, this.id_asset, filename, FileBytes);
                                        MessageBox.Show("Permintaan mutasi aset berhasil dikirim", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        clearField();
                                    }
                                    else
                                    {
                                        MessageBox.Show("Permintaan mutasi aset gagal dikirim", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }

                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show("Error during File Read " + ex.ToString());
                                }

                            }
                            else
                            {
                                lNotify.Text = "Aset ini dalam proses mutasi!";
                            }
                        }
                        else
                        {
                            lNotify.Text = "Aset ini dalam proses perbaikan/tidak dalam kondisi baik!";
                        }
                    }
                    else
                    {
                        lNotify.Text = "Aset ini dalam proses kehilangan!";
                    }
                }
                else
                {
                    lNotify.Text = "Salah input!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            clearField();
        }
        public void clearField()
        {
            this.id2 = "";
            this.id_asset = "";
            tNIK.Text = "";
            tName.Text = "";
            tDept.Text = "";
            tCategory.Text = "";
            tBrand.Text = "";
            tLocation.Text = "";
            tModel.Text = "";
            tSerial.Text = "";
            tPrice.Text = "";
            tDesc.Text = "";
            purDate.Text = "";
            tFile.Text = "";
            lNotify.Text = "";
        }

        private void bCari_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fDialog = new OpenFileDialog();
                fDialog.Title = "Pilih file untuk diunggah";
                fDialog.Filter = "All Files|*.pdf;*.doc;*.docx;*.jpg;*.jpeg;*.png|PDF Files|*.pdf|Word Document|*.doc;*.docx|Image|*.jpg;*.jpeg;*.png";
                if (fDialog.ShowDialog() == DialogResult.OK)
                {
                    tFile.Text = fDialog.FileName.ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void MutationsManagement_Paint(object sender, PaintEventArgs e)
        {
            Rectangle r = this.ClientRectangle;
            if (r.Width > 0 && r.Height > 0)
            {
                LinearGradientBrush br = new LinearGradientBrush(r, lib.color1(), lib.color2(), 45.0f, true);
                e.Graphics.FillRectangle(br, r);
            }
        }


   
    }
}
